import { LayoutModule } from '@angular/cdk/layout';
import { ObserversModule } from '@angular/cdk/observers';
import { CommonModule, registerLocaleData } from '@angular/common';
import localeEn from '@angular/common/locales/en';
import localeFr from '@angular/common/locales/fr';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { ApiFrontModule, API_PATH } from '@api/front';
import { ApiMocksModule } from '@api/mocks';
import { CriteriaModule } from '@front/criteria';
import {
  AUTH_CLIENT_ID,
  AUTH_GRANT_TYPE,
  AUTH_PATH,
  AUTH_REALM,
  AUTH_SCOPE,
  SIGN_UP_PATH,
  UserModule,
} from '@front/user';
import { NgAnimeDriverModule } from '@lheido/ng-anime-driver';
import { NgSelectModule } from '@ng-select/ng-select';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { ToastNoAnimationModule, ToastrModule } from 'ngx-toastr';
import { from, Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { APP_DEFAULT_SEARCH_LIMIT, APP_PROJECT_LINKS } from './app.token';
import { BackHomeEffects, BackHomeReducer, BACK_HOME_FEATURE_KEY } from './features/back-home';
import { AppCoordinationsModule } from './features/coordinations/coordinations.module';
import { AppFeaturesToggleModule } from './features/features-toggle';
import { AppItemsModule } from './features/items';
import { AppLanguageModule, defaultUserLanguage } from './features/language';
import { AppFormModule } from './features/shared/form/form.module';
import { SearchFormModule } from './features/shared/search-form/search-form.module';
import { UiModule } from './features/shared/ui/ui.module';
import { UtilModule } from './features/shared/util/util.module';
import { ScIsParagraphPipe } from './features/static-content/sc-is-paragraph.pipe';
import { StaticContentPipe } from './features/static-content/static-content.pipe';
import { AppThemeModule } from './features/theme';
import { TitleEffects, TitleReducer, TITLE_FEATURE_KEY } from './features/title';
import { ToastComponent } from './features/toast';
import { UserContentModule } from './features/user-content/user-content.module';
import { AppUserModule } from './features/user/user.module';
import { AboutProjectComponent } from './pages/about-project/about-project.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { CoordinationComponent } from './pages/coordination/coordination.component';
import { HomeComponent } from './pages/home/home.component';
import { MyClosetComponent } from './pages/my-closet/my-closet.component';
import { MyCoordChecklistComponent } from './pages/my-coord-checklist/my-coord-checklist.component';
import { MyWishlistComponent } from './pages/my-wishlist/my-wishlist.component';
import { SearchComponent } from './pages/search/search.component';
import { SignInComponent } from './pages/sign-in/sign-in.component';

registerLocaleData(localeEn, 'en');
registerLocaleData(localeFr, 'fr');

export class WebpackTranslateLoader implements TranslateLoader {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getTranslation(lang: string): Observable<any> {
    return from(import(`../assets/i18n/${lang}.json`));
  }
}

@NgModule({
  declarations: [
    AppComponent,
    ToastComponent,
    HomeComponent,
    SignInComponent,
    ScIsParagraphPipe,
    StaticContentPipe,
    AboutProjectComponent,
    AboutUsComponent,
    SearchComponent,
    MyClosetComponent,
    MyWishlistComponent,
    MyCoordChecklistComponent,
    CoordinationComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NgAnimeDriverModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:20000',
    }),
    TranslateModule.forRoot({
      defaultLanguage: defaultUserLanguage,
      loader: {
        provide: TranslateLoader,
        useClass: WebpackTranslateLoader,
      },
    }),
    NgSelectModule,
    ObserversModule,
    NgxPaginationModule,
    LayoutModule,
    NgxChartsModule,
    NgScrollbarModule,
    ToastNoAnimationModule.forRoot(),
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-center',
      enableHtml: true,
      closeButton: true,
      toastComponent: ToastComponent,
      preventDuplicates: true,
    }),
    StoreModule.forRoot({}),
    StoreModule.forFeature(TITLE_FEATURE_KEY, TitleReducer),
    StoreModule.forFeature(BACK_HOME_FEATURE_KEY, BackHomeReducer),
    EffectsModule.forRoot([TitleEffects, BackHomeEffects]),
    UserModule,
    ApiFrontModule,
    AppRoutingModule,
    AppLanguageModule,
    AppThemeModule,
    AppFeaturesToggleModule,
    UiModule,
    UtilModule,
    SearchFormModule,
    AppUserModule,
    AppItemsModule,
    AppFormModule,
    AppCoordinationsModule,
    UserContentModule,
    CriteriaModule,
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    !environment.production ? ApiMocksModule : [],
  ],
  providers: [
    { provide: API_PATH, useValue: environment.domains.mel },
    { provide: AUTH_PATH, useValue: environment.domains.login },
    { provide: AUTH_REALM, useValue: environment.api.auth.realm },
    { provide: AUTH_CLIENT_ID, useValue: environment.api.auth.client_id },
    { provide: AUTH_SCOPE, useValue: environment.api.auth.scope },
    { provide: AUTH_GRANT_TYPE, useValue: environment.api.auth.grant_type },
    { provide: SIGN_UP_PATH, useValue: environment.domains.registration },
    { provide: APP_PROJECT_LINKS, useValue: environment.links },
    { provide: APP_DEFAULT_SEARCH_LIMIT, useValue: 500 },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
