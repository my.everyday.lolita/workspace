import { animate, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';
import { CriteriaFacade } from '@front/criteria';
import { ThemeFacade } from '../../features/theme';

@Component({
  templateUrl: './my-wishlist.component.html',
  styleUrls: ['./my-wishlist.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('pageAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate('330ms linear', style({ opacity: 1 }))]),
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(5%)' })),
      ]),
    ]),
  ],
})
export class MyWishlistComponent {
  @HostBinding('@pageAnimation') private animation = true;
  @HostBinding('class.my-wishlist') private hostClass = true;

  footerEE = 0;

  constructor(public themeFacade: ThemeFacade, public criteriaFacade: CriteriaFacade) {}

  toggleFooterEE(): void {
    this.footerEE = (this.footerEE + 1) % 2;
  }
}
