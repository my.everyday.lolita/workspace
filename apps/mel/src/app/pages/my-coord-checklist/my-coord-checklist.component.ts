import { animate, query, stagger, style, transition, trigger } from '@angular/animations';
import { Component, HostBinding } from '@angular/core';
import { ApiModels } from '@api/shared';
import { UserContentFacade } from '../../features/user-content/state/user-content.facade';

@Component({
  templateUrl: './my-coord-checklist.component.html',
  styleUrls: ['./my-coord-checklist.component.scss'],
  animations: [
    trigger('pageAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate('330ms linear', style({ opacity: 1 }))]),
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(5%)' })),
      ]),
    ]),
    trigger('remove', [
      transition(':enter', [
        style({ opacity: 0, transform: 'scale(0.5) translate(50%, -50%)' }),
        animate('200ms linear', style({ opacity: 1, transform: 'scale(1) translate(50%, -50%)' })),
      ]),
      transition(':leave', [
        style({ opacity: 1, transform: 'scale(1) translate(50%, -50%)' }),
        animate('200ms linear', style({ opacity: 0, transform: 'scale(0.5) translate(50%, -50%)' })),
      ]),
    ]),
    trigger('list', [
      transition('* => *', [
        query(':enter', [style({ opacity: 0 }), stagger(100, animate('330ms linear', style({ opacity: 1 })))], {
          optional: true,
        }),
        query(
          ':leave',
          [
            style({ opacity: 1, transform: 'translateX(0%)' }),
            animate('330ms linear', style({ opacity: 0, transform: 'translateX(-20%)' })),
          ],
          { optional: true }
        ),
      ]),
    ]),
  ],
})
export class MyCoordChecklistComponent {
  @HostBinding('@pageAnimation') private pageAnimation = true;
  trashMode = false;

  constructor(public readonly userContentFacade: UserContentFacade) {
    this.userContentFacade.load();
  }

  toggleTrashMode(): void {
    this.trashMode = !this.trashMode;
  }

  remove(coord: ApiModels.Coordination): void {
    this.userContentFacade.removeCoordination(coord);
  }
}
