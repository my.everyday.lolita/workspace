import { animate, style, transition, trigger } from '@angular/animations';
import { Component, HostBinding } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { delay } from 'rxjs/operators';
import { AppUserFacade } from '../../features/user/user.facade';

@Component({
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  animations: [
    trigger('pageAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate('330ms 330ms linear', style({ opacity: 1 }))]),
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(5%)' })),
      ]),
    ]),
  ],
})
export class SignInComponent {
  @HostBinding('@pageAnimation') private pageAnimation = true;

  form: FormGroup;

  loading$ = this.userFacade.loading$.pipe(delay(500));

  constructor(private fb: FormBuilder, private userFacade: AppUserFacade) {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  get username(): FormControl {
    return this.form.controls.username as FormControl;
  }

  get password(): FormControl {
    return this.form.controls.password as FormControl;
  }

  onSubmit(): void {
    const { username, password } = this.form.value;
    this.userFacade.login(username, password);
  }
}
