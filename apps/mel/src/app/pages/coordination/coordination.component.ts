import { animate, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, HostBinding, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiModels } from '@api/shared';
import { saveAs } from 'file-saver';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { ItemsHelper } from '../../features/items/helpers';
import { buildVariantId } from '../../features/items/items.utils';
import { DialogService } from '../../features/shared/ui/dialog';
import { TitleFacade } from '../../features/title';
import { UserContentFacade } from '../../features/user-content/state/user-content.facade';
// import { CacheService } from 'src/app/features/cache/cache.service';
// import { DialogService } from 'src/app/features/dialog/dialog.service';
// import { Coordination, CoordinationField, CoordinationFieldType, coordinationTypeMap, coordinationTypeTranslateLabels } from 'src/app/features/resources/user-content/user-content.model';
// import { UserContentService } from 'src/app/features/resources/user-content/user-content.service';
// import { TitleService } from 'src/app/features/title/title.service';

@Component({
  templateUrl: './coordination.component.html',
  styleUrls: ['./coordination.component.scss'],
  animations: [
    trigger('pageAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate('330ms linear', style({ opacity: 1 }))]),
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(5%)' })),
      ]),
    ]),
  ],
})
export class CoordinationComponent implements OnInit, OnDestroy {
  CoordinationFieldTypes = ApiModels.CoordinationFieldType;
  coordinationTypeTranslateLabels = ApiModels.coordinationTypeTranslateLabels;

  @ViewChild('modal', { static: true }) modalTemplate!: TemplateRef<unknown>;
  @ViewChild('canvas', { static: true }) canvas!: ElementRef<HTMLCanvasElement>;
  @ViewChild('floordination', { static: true }) floordination!: ElementRef<SVGElement>;

  @HostBinding('@pageAnimation') private pageAnimation = true;

  form!: FormGroup;
  id: string;
  isNew: boolean;
  coordination: ApiModels.Coordination;
  canvasWidth!: number;
  canvasHeight!: number;

  private unsubscriber = new Subject<void>();
  private groupedItems: { [key in ApiModels.CoordinationFieldType]?: { id: string; photo: string }[] } = {};

  constructor(
    public userContentFacade: UserContentFacade,
    private title: TitleFacade,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    // private cacheService: CacheService,
    // private itemsService: ItemsService,
    private dialogService: DialogService,
    private itemsHelper: ItemsHelper
  ) {
    this.id = this.activatedRoute.snapshot.params.id;
    this.coordination = this.activatedRoute.snapshot.data.coordination as ApiModels.Coordination;
    this.isNew = this.id === 'new';
    this.unsubscriber.next();
    this.initForm();
    this.form.valueChanges.pipe(takeUntil(this.unsubscriber)).subscribe((values) => {
      if (values.title !== '') {
        this.title.updateTitle(values.title, true);
      } else {
        if (this.isNew) {
          this.title.updateTitle('COORDINATION.TITLES.NEW');
        }
      }
    });

    this.canvasWidth = Math.round(
      (window.innerWidth < window.innerHeight ? window.innerWidth : window.innerHeight) * 0.7
    );
    this.canvasHeight = Math.round(this.canvasWidth / 0.7);
    if (this.canvasHeight >= window.innerHeight) {
      this.canvasWidth *= 0.7;
      this.canvasWidth = Math.round(this.canvasWidth);
      this.canvasHeight *= 0.7;
      this.canvasHeight = Math.round(this.canvasHeight);
    }
  }

  ngOnInit(): void {
    setTimeout(() => {
      if (this.isNew) {
        this.title.updateTitle('COORDINATION.TITLES.NEW');
      } else {
        this.updateTitle();
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  get dateControl(): FormControl {
    return this.form.controls.date as FormControl;
  }

  get themeControl(): FormControl {
    return this.form.controls.theme as FormControl;
  }

  get fieldsControls(): FormGroup[] {
    return (this.form.controls.fields as FormArray).controls as FormGroup[];
  }

  edit(control: AbstractControl, key: string): void {
    this.dialogService.open(this.modalTemplate, {
      modal: true,
      data: {
        key,
        control,
        closet$: this.getItems$(this.userContentFacade.closet$, control.value.type),
        wishlist$: this.getItems$(this.userContentFacade.wishlist$, control.value.type),
      },
    });
  }

  onSubmit(): void {
    if (this.isNew) {
      this.userContentFacade.addCoordination(this.form.value);
      this.isNew = false;
    } else {
      this.userContentFacade.updateCoordination(this.form.value);
    }
    this.coordination = this.form.value;
    this.updateTitle();
    this.form.markAsPristine();
  }

  checkItemMultiple(event: Event, formArray: FormArray): void {
    const target = event.target as HTMLInputElement;
    if (target.checked) {
      const exist = !!formArray.controls.find((control) => control.value === target.value);
      if (!exist) {
        formArray.push(this.fb.control(target.value));
        this.form.markAsDirty();
      }
    } else {
      const indexToRemove = formArray.controls.findIndex((control) => control.value === target.value);
      if (indexToRemove >= 0) {
        formArray.removeAt(indexToRemove);
        this.form.markAsDirty();
      }
    }
  }

  /**
   * Not working for now, waiting for image storage.
   */
  downloadFloordination(): void {
    const clone = this.floordination.nativeElement.cloneNode(true) as SVGElement;
    const blob = new Blob([clone.outerHTML], { type: 'image/svg+xml;charset=utf-8' });
    const URL = window.URL || window.webkitURL || window;
    const tmpImage = new Image();
    tmpImage.onload = () => {
      const ctx = this.canvas.nativeElement.getContext('2d');
      ctx?.drawImage(tmpImage, 0, 0, this.canvasWidth, this.canvasHeight);
      // generate png file from canvas.
      const png = this.canvas.nativeElement.toDataURL();
      saveAs(png, this.form.value.title);
    };
    tmpImage.src = URL.createObjectURL(blob);
  }

  private initForm(): void {
    this.form = this.fb.group({
      id: [this.coordination.id, Validators.required],
      title: [this.coordination.title, Validators.required],
      event: [this.coordination.event],
      place: [this.coordination.place],
      theme: [this.coordination.theme],
      date: [this.coordination.date],
      memo: [this.coordination.memo],
      fields: this.fb.array(
        this.coordination.fields.map((field) => {
          return this.fb.group({
            type: [field.type, Validators.required],
            value: this.createFieldValueControl(field.type, field),
            customText: [field.customText || ''],
          });
        })
      ),
    });
  }

  private createFieldValueControl(type: string, initialValue: ApiModels.CoordinationField): AbstractControl {
    let control: AbstractControl;
    switch (type) {
      case ApiModels.CoordinationFieldType.ACCESSORIES:
      case ApiModels.CoordinationFieldType.LEGWEAR:
      case ApiModels.CoordinationFieldType.UNDERWEAR:
      case ApiModels.CoordinationFieldType.OTHERS:
        control = this.fb.array(initialValue.value.map((val: unknown) => this.fb.control(val)));
        break;
      default:
        control = this.fb.control(initialValue.value);
        break;
    }
    return control;
  }

  private updateTitle(): void {
    this.title.updateTitle(this.coordination.title, true);
  }

  private getItems$(
    ucData$: Observable<ApiModels.UserContentCollectionItem[]>,
    type: ApiModels.CoordinationFieldType
  ): Observable<{ id: string; photo: string }[]> {
    const categories = ApiModels.coordinationTypeMap[type];
    return this.itemsHelper.getItems(ucData$).pipe(
      map((items) =>
        items
          .filter(
            (item) =>
              categories?.includes(item.category.name) ||
              (item.category.parent && categories?.includes(item.category.parent.name)) ||
              (item.category.parent &&
                item.category.parent.parent &&
                categories?.includes(item.category.parent.parent.name))
          )
          .map((item) => ({
            id: buildVariantId(item),
            photo: item.variants[0].photos[0],
          }))
      )
    );
  }
}
