import { animate, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';
import { CriteriaFacade } from '@front/criteria';
import { ThemeFacade } from '../../features/theme';

@Component({
  templateUrl: './my-closet.component.html',
  styleUrls: ['./my-closet.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('closetAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate('330ms linear', style({ opacity: 1 }))]),
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(5%)' })),
      ]),
    ]),
  ],
})
export class MyClosetComponent {
  @HostBinding('@closetAnimation') private animation = true;
  @HostBinding('class.my-closet') private hostClass = true;

  footerEE = 0;

  constructor(public themeFacade: ThemeFacade, public criteriaFacade: CriteriaFacade) {}

  toggleFooterEE(): void {
    this.footerEE = (this.footerEE + 1) % 3;
  }
}
