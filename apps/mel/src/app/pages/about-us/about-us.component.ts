import { animate, style, transition, trigger } from '@angular/animations';
import { Component, HostBinding, Inject, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { APP_PROJECT_LINKS } from '../../app.token';
import { Content } from '../../features/static-content/static-content.model';
import { StaticContentService } from '../../features/static-content/static-content.service';

@Component({
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss'],
  animations: [
    trigger('pageAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate('330ms 330ms linear', style({ opacity: 1 }))]),
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(5%)' })),
      ]),
    ]),
  ],
})
export class AboutUsComponent implements OnInit, OnDestroy {
  @HostBinding('@pageAnimation') private pageAnimation = true;

  data: Content[] = [];

  translateParams = {
    patreon: this.links.patreon,
    youtube: this.links.rockuu.youtube,
    youtubeLolita: this.links.rockuu.youtubeLolita,
    instagram: this.links.rockuu.instagram,
    facebook: this.links.rockuu.facebook,
  };

  private unsubscriber = new Subject<void>();

  constructor(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    @Inject(APP_PROJECT_LINKS) private links: any,
    private staticContentService: StaticContentService
  ) {}

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  ngOnInit(): void {
    this.staticContentService
      .getAndlisten('ABOUT_US')
      .pipe(takeUntil(this.unsubscriber))
      .subscribe({
        next: (data) => {
          this.data = data;
        },
      });
  }
}
