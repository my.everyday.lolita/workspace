import { Directive, OnDestroy } from '@angular/core';
import { ApiModels } from '@api/shared';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Directive()
export abstract class InAbstract implements OnDestroy {
  public abstract variantIndex: number;
  public abstract buildIsInObservable(item: ApiModels.Item): Observable<boolean>;

  public isIn$ = new BehaviorSubject<boolean>(false);

  private unsubscriber = new Subject<void>();

  protected ucItem?: ApiModels.Item;
  public handleItemInput(item: ApiModels.Item): void {
    if (!item) return;
    this.ucItem = {
      ...item,
      variants: [item.variants[0]],
    };
    this.unsubscriber.next();
    this.buildIsInObservable(this.ucItem)
      .pipe(takeUntil(this.unsubscriber))
      .subscribe({ next: (isIn) => this.isIn$.next(isIn) });
  }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }
}
