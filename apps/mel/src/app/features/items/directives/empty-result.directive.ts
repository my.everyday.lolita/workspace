import { Directive } from '@angular/core';

@Directive({
  selector: '[melEmptyResult]',
})
export class EmptyResultDirective {}
