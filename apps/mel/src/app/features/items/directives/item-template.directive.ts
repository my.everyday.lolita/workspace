import { Directive } from '@angular/core';

@Directive({
  selector: '[melItemTemplate]',
})
export class ItemTemplateDirective {}
