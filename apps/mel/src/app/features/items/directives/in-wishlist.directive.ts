import { Directive, HostListener, Input } from '@angular/core';
import { ApiModels } from '@api/shared';
import { Observable } from 'rxjs';
import { ItemsFacade } from '../items.facade';
import { InAbstract } from './in.abstract';

@Directive({
  selector: '[melInWishlist]',
  exportAs: 'inWishlist',
})
export class InWishlistDirective extends InAbstract {
  @Input() set item(item: ApiModels.Item) {
    this.handleItemInput(item);
  }

  @Input() variantIndex = 0;

  constructor(private readonly itemsFacade: ItemsFacade) {
    super();
  }

  @HostListener('click')
  toggle(): void {
    if (!this.ucItem) return;
    this.itemsFacade.toggleWishlist(this.ucItem);
  }

  public buildIsInObservable(item: ApiModels.Item): Observable<boolean> {
    return this.itemsFacade.isInWishlist(item);
  }
}
