import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CRITERIA_FACTORY } from '@front/criteria';
import { NgxPaginationModule } from 'ngx-pagination';
import { UiModule } from '../shared/ui/ui.module';
import { UserContentModule } from '../user-content/user-content.module';
import { ResItemComponent } from './components/res-item/res-item.component';
import { UcClosetComponent } from './components/uc-closet/uc-closet.component';
import { UcItemsComponent } from './components/uc-items/uc-items.component';
import { UcWishlistComponent } from './components/uc-wishlist/uc-wishlist.component';
import { EmptyResultDirective, InClosetDirective, InWishlistDirective, ItemTemplateDirective } from './directives';
import { buildDreamDressCriteria } from './factories/dream-dress.criteria';
import { buildWantToSellCriteria } from './factories/want-to-sell.criteria';
import { CacheItemNamePipe, CacheItemPhotoPipe, CacheItemPipe, ItemPhotoPipe, PhotosPipe } from './pipes';

@NgModule({
  declarations: [
    InClosetDirective,
    InWishlistDirective,
    EmptyResultDirective,
    ItemTemplateDirective,
    ResItemComponent,
    PhotosPipe,
    ItemPhotoPipe,
    UcClosetComponent,
    UcWishlistComponent,
    UcItemsComponent,
    CacheItemPipe,
    CacheItemNamePipe,
    CacheItemPhotoPipe,
  ],
  exports: [
    InClosetDirective,
    InWishlistDirective,
    EmptyResultDirective,
    ItemTemplateDirective,
    ResItemComponent,
    PhotosPipe,
    ItemPhotoPipe,
    UcClosetComponent,
    UcWishlistComponent,
    UcItemsComponent,
    CacheItemPipe,
    CacheItemNamePipe,
    CacheItemPhotoPipe,
  ],
  imports: [CommonModule, RouterModule, UserContentModule, UiModule, NgxPaginationModule],
  providers: [
    { provide: CRITERIA_FACTORY, useValue: { type: 'wantToSell', factory: buildWantToSellCriteria }, multi: true },
    { provide: CRITERIA_FACTORY, useValue: { type: 'dreamDress', factory: buildDreamDressCriteria }, multi: true },
    CacheItemPipe,
    CacheItemPhotoPipe,
  ],
})
export class AppItemsModule {}
