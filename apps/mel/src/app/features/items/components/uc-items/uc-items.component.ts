import { Component, ContentChild, Input, TemplateRef } from '@angular/core';
import { ApiModels } from '@api/shared';
import { PaginationInstance } from 'ngx-pagination';
import { Observable } from 'rxjs';
import { EmptyResultDirective } from '../../directives/empty-result.directive';
import { ItemTemplateDirective } from '../../directives/item-template.directive';
import { UcItemsFacade } from './state/uc-items.facade';

@Component({
  selector: 'mel-uc-items, [uc-items]',
  templateUrl: './uc-items.component.html',
  exportAs: 'ucItems',
})
export class UcItemsComponent {
  @Input() set field(value: 'closet' | 'wishlist') {
    this.results$ = value === 'closet' ? this.facade.closet$ : this.facade.wishlist$;
  }

  @ContentChild(ItemTemplateDirective, { static: true, read: TemplateRef }) itemTemplate!: TemplateRef<unknown>;
  @ContentChild(EmptyResultDirective, { static: true, read: TemplateRef }) emptyTemplate!: TemplateRef<unknown>;

  items: ApiModels.Item[] = [];
  // results: ApiModels.Item[] = [];
  results$?: Observable<ApiModels.Item[]>;
  // content: (ApiModels.UserContentWishlistItem | ApiModels.UserContentClosetItem)[] = [];
  nbItems = 0;
  totalEstimatedPrice = 0;
  paginationConfig: PaginationInstance = {
    id: 'mel-pager',
    itemsPerPage: 20,
    currentPage: 1,
  };
  lastSearch?: string;
  lastSort?: string;
  lastLength?: number;
  selectedCriteria: ApiModels.Criterium[] = [];

  constructor(public facade: UcItemsFacade) {}

  trackByFn(index: number, item: ApiModels.Item): string {
    return item._variantId as string;
  }

  onPageChange(page: number): void {
    this.paginationConfig.currentPage = page;
  }
}
