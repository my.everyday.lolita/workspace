import { Injectable } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ApiModels } from '@api/shared';
import { map, Observable, switchMap } from 'rxjs';
import { UserContentFacade } from '../../../../user-content/state/user-content.facade';
import { filterByCriteria, ItemsHelper, itemsSort } from '../../../helpers';

@Injectable({
  providedIn: 'root',
})
export class UcItemsFacade {
  closet$ = this.getFiltererItems(this.userContentFacade.closet$).pipe(
    map((items) => items.filter((item) => !item._wrongVariantId))
  );
  wishlist$ = this.getFiltererItems(this.userContentFacade.wishlist$).pipe(
    map((items) => items.filter((item) => !item._wrongVariantId))
  );

  constructor(
    private readonly userContentFacade: UserContentFacade,
    private readonly itemsHelper: ItemsHelper,
    private readonly activatedRoute: ActivatedRoute
  ) {
    this.userContentFacade.load();
  }

  private getFiltererItems(ucData$: Observable<ApiModels.UserContentCollectionItem[]>) {
    return this.itemsHelper.getItems(ucData$).pipe(
      switchMap((items) =>
        this.activatedRoute.queryParams.pipe(
          map((params) => {
            const _params = this.resolveParams(params);
            return items
              .filter((item) => filterByCriteria(item, _params.criteria))
              .sort((a, b) => itemsSort(_params.sort, a, b));
          })
        )
      )
    );
  }

  private resolveParams(params: Params): { criteria: ApiModels.Criterium[]; sort: string } {
    return {
      criteria: params.criteria !== undefined ? JSON.parse(params.criteria) : [],
      sort: this.ensureSortParams(params.sort),
    };
  }

  private ensureSortParams(sortParam: string): ApiModels.SortOptions {
    return ApiModels.sortOptions.includes(sortParam) ? sortParam : 'alpha_asc';
  }
}
