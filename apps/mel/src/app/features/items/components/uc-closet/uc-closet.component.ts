import { Component, Input } from '@angular/core';
import { ApiModels } from '@api/shared';
import { ItemsFacade } from '../../items.facade';

@Component({
  selector: 'mel-uc-closet',
  templateUrl: './uc-closet.component.html',
})
export class UcClosetComponent {
  @Input() item!: ApiModels.Item;

  constructor(public itemFacade: ItemsFacade) {}

  toggleWantToSellProperty(): void {
    this.itemFacade.toggleWantToSell(this.item);
  }

  remove(): void {
    this.itemFacade.removeFromCloset(this.item);
  }
}
