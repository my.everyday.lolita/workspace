import { Component, HostBinding, Input } from '@angular/core';
import { ApiModels } from '@api/shared';

@Component({
  selector: 'mel-res-item, [res-item]',
  templateUrl: './res-item.component.html',
})
export class ResItemComponent {
  @HostBinding('class.item') classItem = true;

  @Input() item!: ApiModels.Item;
}
