import { Component, Input } from '@angular/core';
import { ApiModels } from '@api/shared';
import { ItemsFacade } from '../../items.facade';

@Component({
  selector: 'mel-uc-wishlist',
  templateUrl: './uc-wishlist.component.html',
})
export class UcWishlistComponent {
  @Input() item!: ApiModels.Item;

  constructor(public itemFacade: ItemsFacade) {}

  toggleDreamDressProperty(): void {
    this.itemFacade.toggleDreamDress(this.item);
  }

  remove(): void {
    this.itemFacade.removeFromWishlist(this.item);
  }
}
