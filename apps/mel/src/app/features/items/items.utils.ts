import { ApiModels } from '@api/shared';

export const separator = ':';
export const colorSeparator = ',';

export function buildVariantId(item: ApiModels.Item, index = 0): string {
  return `${item._id}${separator}${item.variants[index].colors.map((c) => c._id).join(colorSeparator)}`;
}

export function splitVariantId(id: string): string[] {
  return id.split(separator);
}
