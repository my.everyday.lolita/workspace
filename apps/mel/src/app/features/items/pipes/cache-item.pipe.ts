import { Pipe, PipeTransform } from '@angular/core';
import { ApiModels } from '@api/shared';
import { Observable } from 'rxjs';
import { ItemsHelper } from '../helpers';

@Pipe({
  name: 'cacheItem',
})
export class CacheItemPipe implements PipeTransform {
  constructor(private readonly itemsHelper: ItemsHelper) {}

  transform(id: string): Observable<ApiModels.Item> {
    return this.itemsHelper.getItemFromCacheOrApi(id);
  }
}
