import { Pipe, PipeTransform } from '@angular/core';
import { ApiModels } from '@api/shared';

@Pipe({
  name: 'photos',
})
export class PhotosPipe implements PipeTransform {
  transform(item: ApiModels.Item): string[] {
    return item.variants.reduce((acc, variant) => {
      variant.photos.forEach((photo) => {
        acc.push(photo);
      });
      return acc;
    }, [] as string[]);
  }
}
