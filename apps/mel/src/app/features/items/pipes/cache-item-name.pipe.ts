import { Pipe, PipeTransform } from '@angular/core';
import { ApiModels } from '@api/shared';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ItemsHelper } from '../helpers';
import { splitVariantId } from '../items.utils';

@Pipe({
  name: 'cacheItemName',
})
export class CacheItemNamePipe implements PipeTransform {
  constructor(private readonly itemsHelper: ItemsHelper) {}

  transform(idOrObs: string | Observable<ApiModels.Item | undefined>): Observable<string> {
    if (typeof idOrObs === 'string') {
      const [itemId] = splitVariantId(idOrObs);
      return this.itemsHelper.getItemFromCacheOrApi(itemId).pipe(map((item) => this.mapToName(item)));
    }
    return idOrObs.pipe(map((item) => this.mapToName(item)));
  }

  private mapToName(item?: ApiModels.Item): string {
    return `${item?.brand.shortname || item?.brand.name} ${item?.collectionn || ''}` ?? '';
  }
}
