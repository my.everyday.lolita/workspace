import { Pipe, PipeTransform } from '@angular/core';
import { ApiModels } from '@api/shared';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ItemsHelper } from '../helpers';
import { splitVariantId } from '../items.utils';

@Pipe({
  name: 'cacheItemPhoto',
})
export class CacheItemPhotoPipe implements PipeTransform {
  constructor(private readonly itemsHelper: ItemsHelper) {}

  transform(idOrObs: string | Observable<ApiModels.Item | undefined>): Observable<string> {
    if (typeof idOrObs === 'string') {
      const [itemId] = splitVariantId(idOrObs);
      return this.itemsHelper.getItemFromCacheOrApi(itemId).pipe(map((item) => this.mapToPhoto(item, idOrObs)));
    }
    return idOrObs.pipe(map((item) => this.mapToPhoto(item)));
  }

  private mapToPhoto(item?: ApiModels.Item, id?: string): string {
    if (id) {
      const splittedId = splitVariantId(id);
      if (splittedId.length > 1 && item) {
        const _item = this.itemsHelper.getVariantItem(id, item);
        return _item?.variants[0].photos[0] ?? '';
      }
    }
    return item?.variants[0].photos[0] ?? '';
  }
}
