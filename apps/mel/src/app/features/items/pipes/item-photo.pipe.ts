import { Pipe, PipeTransform } from '@angular/core';
import { ApiModels } from '@api/shared';

@Pipe({
  name: 'itemPhoto',
})
export class ItemPhotoPipe implements PipeTransform {
  transform(item: ApiModels.Item): string {
    return item.variants[0].photos[0];
  }
}
