import { CriteriaFactory } from '@front/criteria';

export const buildWantToSellCriteria: CriteriaFactory<string> = (value) => ({
  type: 'wantToSell',
  displayType: 'personal filter',
  displayValue: 'WANT TO SELL',
  value,
});
