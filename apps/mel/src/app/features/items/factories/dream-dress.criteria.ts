import { CriteriaFactory } from '@front/criteria';

export const buildDreamDressCriteria: CriteriaFactory<string> = (value) => ({
  type: 'dreamDress',
  displayType: 'personal filter',
  displayValue: 'DREAM DRESS',
  value,
});
