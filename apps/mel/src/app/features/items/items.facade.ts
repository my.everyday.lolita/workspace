import { Injectable } from '@angular/core';
import { ApiModels } from '@api/shared';
import { map, Observable } from 'rxjs';
import { UserContentFacade } from '../user-content/state/user-content.facade';
import { buildVariantId } from './items.utils';

@Injectable({ providedIn: 'root' })
export class ItemsFacade {
  isInCloset(item: ApiModels.Item): Observable<boolean> {
    return this.isIn(this.userContentFacade.closet$, item);
  }

  isInWishlist(item: ApiModels.Item): Observable<boolean> {
    return this.isIn(this.userContentFacade.wishlist$, item);
  }

  constructor(private readonly userContentFacade: UserContentFacade) {}

  toggleCloset(item: ApiModels.Item): void {
    this.userContentFacade.toggleCloset(buildVariantId(item));
  }

  toggleWishlist(item: ApiModels.Item): void {
    this.userContentFacade.toggleWishlist(buildVariantId(item));
  }

  addToCloset(item: ApiModels.Item): void {
    this.userContentFacade.addToCloset(buildVariantId(item));
  }

  removeFromCloset(item: ApiModels.Item): void {
    this.userContentFacade.removeFromCloset(buildVariantId(item));
  }

  addToWishlist(item: ApiModels.Item): void {
    this.userContentFacade.addToWishlist(buildVariantId(item));
  }

  removeFromWishlist(item: ApiModels.Item): void {
    this.userContentFacade.removeFromWishlist(buildVariantId(item));
  }

  toggleWantToSell(item: ApiModels.Item): void {
    this.userContentFacade.toggleWantToSell(buildVariantId(item));
  }

  toggleDreamDress(item: ApiModels.Item): void {
    this.userContentFacade.toggleDreamDress(buildVariantId(item));
  }

  private isIn(
    collection$: Observable<(ApiModels.UserContentWishlistItem | ApiModels.UserContentClosetItem)[]>,
    item: ApiModels.Item
  ): Observable<boolean> {
    return collection$.pipe(
      map((collection) => {
        const variantId = buildVariantId(item);
        return collection.findIndex((vi) => vi.id === variantId) !== -1;
      })
    );
  }
}
