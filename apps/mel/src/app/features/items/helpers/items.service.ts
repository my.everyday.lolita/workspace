import { Injectable } from '@angular/core';
import { ItemsResourceService } from '@api/front';
import { ApiModels } from '@api/shared';
import { CacheService } from '@front/cache';
import { from, map, Observable, switchMap, tap, zip } from 'rxjs';
import { buildVariantId } from '../items.utils';

@Injectable({ providedIn: 'root' })
export class ItemsHelper {
  constructor(private readonly cache: CacheService, private readonly api: ItemsResourceService) {}

  public getItems(ucData$: Observable<ApiModels.UserContentCollectionItem[]>) {
    return ucData$.pipe(
      switchMap((items) =>
        zip(
          items
            .filter((item) => !item._wrongVariantId)
            .map((item) => {
              const [itemId] = item.id.split(':');
              return this.getItemFromCacheOrApi(itemId).pipe(
                map((cachedItem) => {
                  const almostReadyitem = this.getVariantItem(item.id, cachedItem);
                  if (almostReadyitem) {
                    if ('wantToSell' in item) {
                      almostReadyitem.wantToSell = (item as ApiModels.UserContentClosetItem).wantToSell;
                    }
                    if ('dreamDress' in item) {
                      almostReadyitem.dreamDress = (item as ApiModels.UserContentWishlistItem).dreamDress;
                    }
                  }
                  return almostReadyitem;
                })
              );
            })
        )
      )
    );
  }

  public getItem(id: string) {
    return this.api.findById(id).pipe(tap((item) => this.cache.put(id, item as never)));
  }

  public getItemFromCacheOrApi(id: string): Observable<ApiModels.Item> {
    return this.cache
      .match(id)
      .pipe(switchMap((cache) => (cache ? from(cache?.json() as Promise<ApiModels.Item>) : this.getItem(id))));
  }

  public getVariantItem(variantId: string, item: ApiModels.Item): ApiModels.Item {
    const clone: ApiModels.Item = JSON.parse(JSON.stringify(item));
    const variant = clone.variants.find((_, i) => buildVariantId(clone, i) === variantId);
    if (variant) {
      clone.variants = [variant];
    } else {
      clone._wrongVariantId = true;
    }
    return clone;
  }
}
