import { ApiModels } from '@api/shared';

export function itemsSort(sort: ApiModels.SortOptions, a: ApiModels.Item, b: ApiModels.Item) {
  let result: number;
  let aPrice: number;
  let bPrice: number;
  let aBrand: string;
  let bBrand: string;
  let aCollection: string;
  let bCollection: string;
  switch (sort) {
    case 'price_asc':
      aPrice = a.estimatedPrice || 0;
      bPrice = b.estimatedPrice || 0;
      result = aPrice > bPrice ? 1 : aPrice < bPrice ? -1 : 0;
      break;

    case 'price_desc':
      aPrice = a.estimatedPrice || 0;
      bPrice = b.estimatedPrice || 0;
      result = aPrice > bPrice ? -1 : aPrice < bPrice ? 1 : 0;
      break;

    case 'year_asc':
      aPrice = a.year || 0;
      bPrice = b.year || 0;
      result = aPrice > bPrice ? 1 : aPrice < bPrice ? -1 : 0;
      break;

    case 'year_desc':
      aPrice = a.year || 0;
      bPrice = b.year || 0;
      result = aPrice > bPrice ? -1 : aPrice < bPrice ? 1 : 0;
      break;

    case 'alpha_asc':
      aBrand = a.brand.name.toLowerCase();
      aCollection = (a.collectionn || '').toLowerCase();
      bBrand = b.brand.name.toLowerCase();
      bCollection = (b.collectionn || '').toLowerCase();
      if (aBrand === bBrand) {
        result = aCollection > bCollection ? 1 : aCollection < bCollection ? -1 : 0;
      } else {
        result = aBrand > bBrand ? 1 : aBrand < bBrand ? -1 : 0;
      }
      break;

    case 'alpha_desc':
      aBrand = a.brand.name.toLowerCase();
      aCollection = (a.collectionn || '').toLowerCase();
      bBrand = b.brand.name.toLowerCase();
      bCollection = (b.collectionn || '').toLowerCase();
      if (aBrand === bBrand) {
        result = aCollection > bCollection ? -1 : aCollection < bCollection ? 1 : 0;
      } else {
        result = aBrand > bBrand ? -1 : aBrand < bBrand ? 1 : 0;
      }
      break;

    default:
      result = 0;
      break;
  }
  return result;
}
