import { ApiModels } from '@api/shared';

export const criteriaFn: { [key: string]: (criteriumValue: string, item: ApiModels.Item) => boolean } = {
  brand: (value, item) => item.brand.name === value,
  category: (value, item) =>
    item.category.name === value ||
    item.category.parent?.name === value ||
    item.category.parent?.parent?.name === value,
  wantToSell: (value, item) => item.wantToSell === true,
  dreamDress: (value, item) => item.dreamDress === true,
};

export function filterByCriteria(item: ApiModels.Item, criteria: ApiModels.Criterium[]): boolean {
  return criteria.reduce((shouldBeDisplayed: boolean, criterium) => {
    if (!shouldBeDisplayed) return false;
    if (criterium.type in criteriaFn) {
      return criteriaFn[criterium.type](criterium.value, item);
    }
    return true;
  }, true);
}
