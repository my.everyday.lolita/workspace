import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { updateTitle } from './title.actions';
import { getTitle } from './title.selectors';

@Injectable({ providedIn: 'root' })
export class TitleFacade {
  readonly title$ = this.store.select(getTitle);

  constructor(private readonly store: Store) {}

  updateTitle(title?: string, disableTranslate?: boolean) {
    this.store.dispatch(updateTitle({ title, disableTranslate }));
  }
}
