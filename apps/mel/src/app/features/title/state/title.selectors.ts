import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TitleState } from '../title.model';
import { TITLE_FEATURE_KEY } from './title.reducer';

export const getTitleState = createFeatureSelector<TitleState>(TITLE_FEATURE_KEY);

export const getTitle = createSelector(getTitleState, (state) => state.title);
