import { Action, createReducer, on } from '@ngrx/store';
import { TitleState } from '../title.model';
import { titleChange } from './title.actions';

export const TITLE_FEATURE_KEY = 'app_title';

export const initialState: TitleState = {
  title: undefined,
};

const titleReducer = createReducer(
  initialState,
  on(titleChange, (_, { title }) => ({ title }))
);

export function reducer(state: TitleState, action: Action) {
  return titleReducer(state, action);
}
