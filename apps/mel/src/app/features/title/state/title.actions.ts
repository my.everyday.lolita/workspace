import { createAction, props } from '@ngrx/store';

export const titleChange = createAction('[Title] title change', props<{ title?: string }>());

export const updateTitle = createAction(
  '[Title] update title',
  props<{ title?: string; disableTranslate?: boolean }>()
);
