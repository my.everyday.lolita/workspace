import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { TitleService } from '../title.service';
import { titleChange, updateTitle } from './title.actions';

@Injectable({ providedIn: 'root' })
export class TitleEffects {
  readonly router$ = createEffect(() =>
    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      switchMap(() => this.activatedRoute.firstChild?.data || of({})),
      filter(({ pageTitle }) => pageTitle && pageTitle !== ''),
      map(({ pageTitle }) => updateTitle({ title: pageTitle }))
    )
  );

  readonly updateTitle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateTitle),
      switchMap(({ title, disableTranslate }) => this.processTitle(title, disableTranslate)),
      map((title) => titleChange({ title }))
    )
  );

  readonly titleChange$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(titleChange),
        tap(({ title }) => this.titleService.set(title))
      ),
    { dispatch: false }
  );

  constructor(
    private readonly actions$: Actions,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly translate: TranslateService,
    private readonly titleService: TitleService
  ) {}

  private processTitle(title?: string, disableTranslate?: boolean): Observable<string | undefined> {
    if (disableTranslate || !title) {
      return of(title);
    }
    return this.translate.get(title);
  }
}
