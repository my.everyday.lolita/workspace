import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root',
})
export class TitleService {
  constructor(private readonly title: Title) {}

  set(title?: string): void {
    if (title) {
      this.title.setTitle(`${title} | My Everyday Lolita`);
    } else {
      this.title.setTitle(`My Everyday Lolita`);
    }
  }
}
