import { formatDate } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, UrlTree } from '@angular/router';
import { ApiModels } from '@api/shared';
import { map, Observable, take } from 'rxjs';
import { UserContentFacade } from '../user-content/state/user-content.facade';

@Injectable({
  providedIn: 'root',
})
export class CoordinationResolver implements Resolve<ApiModels.Coordination | UrlTree> {
  constructor(private userContentFacade: UserContentFacade, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): ApiModels.Coordination | Observable<ApiModels.Coordination> | UrlTree {
    if (route.params.id === 'new') {
      return {
        id: this.generateUuid(),
        title: '',
        event: '',
        place: '',
        theme: '',
        date: formatDate(new Date(), 'yyyy-MM-dd', 'en'),
        fields: [
          { type: ApiModels.CoordinationFieldType.HEADDRESS, value: null },
          { type: ApiModels.CoordinationFieldType.HAIRSTYLE, value: null },
          { type: ApiModels.CoordinationFieldType.MAIN_PIECE, value: null },
          { type: ApiModels.CoordinationFieldType.TOPWEAR, value: null },
          { type: ApiModels.CoordinationFieldType.OUTERWEAR, value: null },
          { type: ApiModels.CoordinationFieldType.BAG, value: null },
          { type: ApiModels.CoordinationFieldType.ACCESSORIES, value: [] },
          { type: ApiModels.CoordinationFieldType.LEGWEAR, value: [] },
          { type: ApiModels.CoordinationFieldType.SHOES, value: null },
          { type: ApiModels.CoordinationFieldType.UNDERWEAR, value: [] },
          { type: ApiModels.CoordinationFieldType.OTHERS, value: [] },
        ],
        memo: '',
      };
    }
    this.userContentFacade.load();
    return this.userContentFacade.coordinations$.pipe(
      map((coordinations) => {
        const coord = coordinations.find((coord) => coord.id === route.params.id) as ApiModels.Coordination;
        return coord ?? this.router.createUrlTree(['/', 'my-coord-checklist']);
      }),
      take(1)
    );
  }

  generateUuid(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      // tslint:disable
      const r = (Math.random() * 16) | 0;
      const v = c === 'x' ? r : (r & 0x3) | 0x8;
      return v.toString(16);
      // tslint:enable
    });
  }
}
