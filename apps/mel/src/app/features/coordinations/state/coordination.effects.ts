import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import * as UserContentActions from '../../user-content';

@Injectable()
export class CoordinationEffects {
  addNewCoordination$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserContentActions.addCoordination),
        tap(({ coord }) => this.router.navigateByUrl(`/my-coord-checklist/${coord.id}`, { replaceUrl: true }))
      ),
    { dispatch: false }
  );

  constructor(private readonly actions$: Actions, private readonly router: Router) {}
}
