import { Pipe, PipeTransform } from '@angular/core';
import { ApiModels } from '@api/shared';

@Pipe({
  name: 'coordMainPiece',
})
export class CoordMainPiecePipe implements PipeTransform {
  transform(coordination: ApiModels.Coordination): string {
    const mainPiece = coordination.fields.find((field) => field.type === ApiModels.CoordinationFieldType.MAIN_PIECE);
    if (mainPiece && mainPiece.value) {
      return mainPiece.value;
    }
    return '';
  }
}
