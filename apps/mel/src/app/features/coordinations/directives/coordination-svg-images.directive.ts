import { Directive, Input, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { ApiModels } from '@api/shared';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CacheItemPhotoPipe } from '../../items/pipes/cache-item-photo.pipe';

@Directive({
  selector: '[melCoordinationSvgImages]',
  exportAs: 'appCoordinationSvgImages',
})
export class CoordinationSvgImagesDirective implements OnInit, OnDestroy {
  @Input() form!: FormGroup;
  @Input() fieldType!: ApiModels.CoordinationFieldType;
  @Input() items = 0;

  images: (Observable<string> | undefined)[] = [];

  private unsubscriber = new Subject<void>();

  constructor(private cacheItemPhotoPipe: CacheItemPhotoPipe) {}

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  ngOnInit(): void {
    if (!this.form) {
      return;
    }
    const control = (this.form.get('fields') as FormArray).controls.find(
      (c) => c.value.type === this.fieldType
    ) as FormGroup;
    if (control) {
      if (control.value.value) {
        this.setup(control.value);
      }
      control.valueChanges.pipe(takeUntil(this.unsubscriber)).subscribe({
        next: () => {
          this.setup(control.value);
        },
      });
    }
  }

  private setup(data: ApiModels.CoordinationField): void {
    const isArray = Array.isArray(data.value);
    this.images = Array.from(Array(this.items), (v, i) => {
      const value = isArray ? data.value[i] : data.value;
      if (value) {
        return this.getPhoto(value);
      }
      return undefined;
    });
  }

  private getPhoto(id: string): Observable<string> {
    return this.cacheItemPhotoPipe.transform(id);
  }
}
