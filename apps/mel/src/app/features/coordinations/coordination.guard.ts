import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, UrlTree } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserContentFacade } from '../user-content/state/user-content.facade';

@Injectable({
  providedIn: 'root',
})
export class CoordinationGuard implements CanActivate {
  constructor(private userContentFacade: UserContentFacade, private router: Router, private toastr: ToastrService) {}

  canActivate(
    route: ActivatedRouteSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.userContentFacade.load();
    return this.userContentFacade.coordinations$.pipe(
      map((coordinations) => {
        if (route.params.id === 'new') {
          return true;
        }
        if (!coordinations.find((coord) => coord.id === route.params.id)) {
          this.toastr.error('COORDINATION.ERRORS.NOT_FOUND', undefined, { disableTimeOut: true, closeButton: true });
          return this.router.createUrlTree(['/', 'my-coord-checklist']);
        }
        return true;
      })
    );
  }
}
