import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { AppItemsModule } from '../items';
import { CoordinationSvgImageDirective, CoordinationSvgImagesDirective } from './directives';
import { CoordMainPiecePipe } from './pipes';
import { CoordinationEffects } from './state/coordination.effects';

@NgModule({
  imports: [CommonModule, AppItemsModule, EffectsModule.forFeature([CoordinationEffects])],
  declarations: [CoordinationSvgImageDirective, CoordinationSvgImagesDirective, CoordMainPiecePipe],
  exports: [CoordinationSvgImageDirective, CoordinationSvgImagesDirective, CoordMainPiecePipe],
})
export class AppCoordinationsModule {}
