import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StaticContentService } from './static-content.service';

@Pipe({
  name: 'melStaticContent',
})
export class StaticContentPipe implements PipeTransform {
  constructor(private staticContent: StaticContentService) {}

  transform(translationPath: string): Observable<string> {
    return this.staticContent.getAndListenTranslation(translationPath).pipe(
      map((content) => {
        return content
          .map((elt) => {
            if (typeof elt === 'object') {
              if ('ITEMS' in elt && Array.isArray(elt.ITEMS)) {
                return `<ul>${elt.ITEMS.map((item) => `<li>${item}</li>`).join('')}</ul>`;
              }
            }
            return `<p>${elt}</p>`;
          })
          .join('');
      })
    );
  }
}
