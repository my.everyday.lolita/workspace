import { Injectable } from '@angular/core';
import { UserFacade } from '@front/user';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AppUserFacade {
  private readonly isLoading = new BehaviorSubject(false);
  loading$ = this.isLoading.asObservable();
  isLoggedInt$ = this.facade.isLoggedIn$;

  constructor(private readonly facade: UserFacade) {}

  setLoading(value: boolean): void {
    this.isLoading.next(value);
  }

  login(username: string, password: string): void {
    this.facade.login(username, password);
  }
}
