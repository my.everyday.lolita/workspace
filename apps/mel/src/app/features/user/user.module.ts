import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UserModule } from '@front/user';
import { EffectsModule } from '@ngrx/effects';
import { UiModule } from '../shared/ui/ui.module';
import { AppUserEffects } from './user.effects';

@NgModule({
  imports: [CommonModule, UserModule, UiModule, EffectsModule.forFeature([AppUserEffects])],
})
export class AppUserModule {}
