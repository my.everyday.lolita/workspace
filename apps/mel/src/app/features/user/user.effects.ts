import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserActions } from '@front/user';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs';
import { AppUserFacade } from './user.facade';

@Injectable({ providedIn: 'root' })
export class AppUserEffects {
  login$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserActions.login),
        tap(() => this.appUserFacade.setLoading(true))
      ),
    { dispatch: false }
  );

  loginSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserActions.loginSuccess),
        tap(() => {
          const redirect = this.activatedRoute.snapshot.queryParams.redirect || undefined;
          this.router.navigateByUrl(redirect, { replaceUrl: true });
        })
      ),
    { dispatch: false }
  );

  loginEnd$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserActions.loginSuccess, UserActions.loginFailure),
        tap(() => this.appUserFacade.setLoading(false))
      ),
    { dispatch: false }
  );

  constructor(
    private readonly actions$: Actions,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly appUserFacade: AppUserFacade
  ) {}
}
