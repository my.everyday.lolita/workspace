import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiModels } from '@api/shared';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'mel-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
  exportAs: 'searchForm',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchFormComponent implements OnInit {
  @Output() search = new EventEmitter<{ criteria: ApiModels.Criterium[]; sort: string }>();

  @Input() enableKeywords = true;
  @Input() criteria: ApiModels.Criterium[] = [];
  @Input() placeholder = '';
  @Input() sortable = false;

  form: FormGroup;
  selectedCriteria: ApiModels.Criterium[] = [];
  currentSort = localStorage.getItem(`${this.activatedRoute.snapshot.url}:sort`) || 'alpha_asc';
  signedIn = false;

  private unsubscriber = new Subject();
  private lastSearch: any;
  private lastSort?: string;

  constructor(private fb: FormBuilder, private activatedRoute: ActivatedRoute, private router: Router) {
    this.form = this.fb.group({
      criteria: [[]],
      sort: [this.currentSort],
    });
  }

  ngOnInit(): void {
    this.form.valueChanges.pipe(takeUntil(this.unsubscriber)).subscribe((values) => {
      this.lastSearch = JSON.stringify(this.selectedCriteria);
      this.lastSort = this.currentSort;
      this.selectedCriteria = values.criteria;
      this.currentSort = values.sort;
      localStorage.setItem(`${this.activatedRoute.snapshot.url}:sort`, this.currentSort);
      this.triggerSearch();
    });

    this.activatedRoute.queryParams.pipe(takeUntil(this.unsubscriber)).subscribe({
      next: (queryParams: { criteria?: string; sort?: string }) => {
        const criteria = (queryParams.criteria && JSON.parse(queryParams.criteria)) || undefined;
        const sort = queryParams.sort || undefined;
        if (
          (criteria === undefined &&
            sort === undefined &&
            this.lastSearch === undefined &&
            this.lastSort === undefined) ||
          (this.lastSearch && this.lastSearch === queryParams.criteria && this.lastSort && this.lastSort === sort)
        ) {
          return;
        }
        this.lastSearch = queryParams.criteria;
        this.lastSort = sort;
        this.selectedCriteria = criteria || [];
        this.currentSort = sort || 'alpha_asc';
        this.form.setValue(
          {
            criteria: this.selectedCriteria,
            sort: this.currentSort,
          },
          { emitEvent: false }
        );
        this.triggerHostSearch();
      },
    });
  }

  onSubmit(): void {
    this.triggerSearch();
  }

  searchCriterias(term: string, item: ApiModels.Criterium): boolean {
    term = term.toLowerCase();
    return (
      (item.displayValue && item.displayValue.toLowerCase().indexOf(term) > -1) ||
      item.type.toLowerCase().indexOf(term) > -1
    );
  }

  addKeywords(term: string): ApiModels.Criterium {
    return {
      value: term,
      displayValue: term,
      type: 'keyword',
    };
  }

  openSortSelect(sortSelect: NgSelectComponent): void {
    sortSelect.focus();
  }

  private triggerSearch(): void {
    this.router.navigate([], {
      replaceUrl: true,
      queryParams: {
        criteria: JSON.stringify(this.selectedCriteria),
        sort: this.currentSort,
      },
    });
  }

  private triggerHostSearch(): void {
    this.search.emit({
      criteria: this.selectedCriteria,
      sort: this.currentSort,
    });
  }
}
