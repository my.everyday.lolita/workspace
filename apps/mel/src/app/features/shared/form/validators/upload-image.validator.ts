import { AbstractControl } from '@angular/forms';

export function uploadImage(apiUrl: string) {
  return (control: AbstractControl): { [key: string]: unknown } | null => {
    if (!control.value) {
      return null;
    }
    if (!control.value.includes(apiUrl)) {
      return { wrongImageLink: true };
    }
    return null;
  };
}
