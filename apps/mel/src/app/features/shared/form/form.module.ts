import { NgModule } from '@angular/core';
import { CoordinationFieldIsEmptyPipe } from './coordination-field-is-empty.pipe';
import { FormArrayControlsPipe } from './form-array-controls.pipe';
import { FormArrayLengthPipe } from './form-array-length.pipe';
import { IsArrayPipe } from './is-array.pipe';
import { IsCheckedPipe } from './is-checked.pipe';
import { IsFormArrayPipe } from './is-form-array.pipe';

@NgModule({
  imports: [],
  declarations: [
    IsArrayPipe,
    IsCheckedPipe,
    IsFormArrayPipe,
    FormArrayLengthPipe,
    FormArrayControlsPipe,
    CoordinationFieldIsEmptyPipe,
  ],
  exports: [
    IsArrayPipe,
    IsCheckedPipe,
    IsFormArrayPipe,
    FormArrayLengthPipe,
    FormArrayControlsPipe,
    CoordinationFieldIsEmptyPipe,
  ],
})
export class AppFormModule {}
