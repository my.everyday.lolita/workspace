import { ChangeDetectionStrategy, Component, HostBinding, HostListener, Input } from '@angular/core';
import { TadaService } from '../../effects/tada';

@Component({
  selector: 'mel-menu, [menu-button]',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuComponent {
  @HostBinding('class.active')
  @Input()
  active: boolean | undefined = false;

  constructor(private tadaService: TadaService) {}

  @HostListener('click', ['$event'])
  onClick(event: MouseEvent): void {
    this.tadaService.tada({
      x: event.clientX,
      y: event.clientY,
      size: 75,
    });
  }
}
