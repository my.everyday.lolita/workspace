import { BackComponent } from './back/back.component';
import { ButtonComponent } from './button/button.component';
import { MenuComponent } from './menu/menu.component';

export const declarations = [ButtonComponent, BackComponent, MenuComponent];
