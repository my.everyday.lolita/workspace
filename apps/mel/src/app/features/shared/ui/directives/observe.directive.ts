import { ChangeDetectorRef, Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { AsyncSubject, concatMap, Observable, Subject, takeUntil } from 'rxjs';

export class Context<T = unknown> {
  $implicit: T;
  melObserve: T;

  constructor(value: T) {
    this.$implicit = value;
    this.melObserve = value;
  }
}

@Directive({
  selector: '[melObserve]',
})
export class ObserveDirective<T = unknown> implements OnInit, OnDestroy {
  private unsubscriber = new Subject<void>();
  private internalObs = new AsyncSubject<void>();
  private source: Observable<T> | undefined;

  constructor(
    private view: ViewContainerRef,
    private templateRef: TemplateRef<{ $implicit: T }>,
    private changes: ChangeDetectorRef
  ) {}

  static ngTemplateContextGuard<T>(dir: ObserveDirective<T>, ctx: unknown): ctx is Context<T> {
    return true;
  }

  ngOnInit() {
    this.internalObs.next(undefined);
    this.internalObs.complete();
  }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  @Input()
  set melObserve(source: Observable<T>) {
    if (this.source && this.source !== source) {
      this.unsubscriber.next();
    }
    if (source && this.source !== source) {
      this.internalObs
        .pipe(
          concatMap(() => source),
          takeUntil(this.unsubscriber)
        )
        .subscribe((value) => {
          this.view.clear();
          this.view.createEmbeddedView(this.templateRef, new Context(value));
          this.changes.markForCheck();
        });
    }
    this.source = source;
  }
}
