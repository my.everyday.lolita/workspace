import { animate, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'mel-simple-loader',
  templateUrl: './simple-loader.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('loader', [
      transition(':enter', [
        style({ opacity: 0, width: '0px' }),
        animate('330ms linear', style({ opacity: 1, width: '{{width}}px' })),
      ]),
    ]),
  ],
})
export class SimpleLoaderComponent {
  @HostBinding('class.simple-loader') hostClass = true;

  @Input() height = 36;
}
