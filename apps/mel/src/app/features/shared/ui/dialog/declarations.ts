import { DialogAttachComponent } from '.';
import { DefaultDialogContentComponent } from './default-dialog-content/default-dialog-content.component';
import { DetailsComponent } from './details/details.component';
import { DialogInnerContentDirective } from './dialog-inner-content.directive';
import { DialogComponent } from './dialog/dialog.component';
import { ImageZoomDirective } from './image-zoom.directive';

export const declarations = [
  DialogInnerContentDirective,
  ImageZoomDirective,
  DialogComponent,
  DetailsComponent,
  DefaultDialogContentComponent,
  DialogAttachComponent,
];
