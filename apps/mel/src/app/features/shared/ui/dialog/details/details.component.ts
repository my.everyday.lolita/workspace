import { Component, ChangeDetectionStrategy, ViewChild, TemplateRef } from '@angular/core';
import { DialogService } from '../dialog.service';

@Component({
  selector: 'mel-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailsComponent {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @ViewChild('modalTemplate') template!: TemplateRef<any>;

  constructor(private dialogService: DialogService) {}

  open(): void {
    this.dialogService.open(this.template, { dialogClass: 'details-dialog', modal: true });
  }
}
