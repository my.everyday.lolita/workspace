import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: 'mel-dialog-inner-content, [melDialogInnerContent]',
})
export class DialogInnerContentDirective implements AfterViewInit {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input('melDialogInnerContent') public dialogRef: any;
  @Input() public padding = 0;

  constructor(private eltRef: ElementRef) {}

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dialogRef.height = this.eltRef.nativeElement.getBoundingClientRect().height + this.padding * 2;
    });
  }
}
