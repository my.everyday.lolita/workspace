export * from './dialog.model';
export * from './dialog.service';
export * from './dialog-inner-content.directive';
export * from './image-zoom.directive';
export * from './dialog-attach/dialog-container.component';
export * from './dialog/dialog.component';
export * from './details/details.component';
export * from './default-dialog-content/default-dialog-content.component';
export * as DialogDeclarations from './declarations';
