import { animate, animateChild, style, transition, trigger } from '@angular/animations';
import { Component, HostBinding, TemplateRef } from '@angular/core';
import { DialogConfiguration } from '../dialog.model';

@Component({
  selector: 'mel-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
  animations: [
    trigger('dialogAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate('330ms linear', style({ opacity: 1 })), animateChild()]),
      transition(':leave', [style({ opacity: 1 }), animateChild(), animate('330ms linear', style({ opacity: 0 }))]),
    ]),
  ],
})
export class DialogComponent {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  template!: TemplateRef<any>;
  config?: DialogConfiguration;
  opened = false;
  height!: number;

  onClose?: () => void;

  @HostBinding('@dialogAnimation') private dialogAnimation = true;

  @HostBinding('class')
  get classes(): string {
    return this.config?.dialogClass || '';
  }

  @HostBinding('class.modal')
  get modal(): boolean {
    return this.config?.modal || false;
  }

  show(): void {
    this.opened = true;
  }

  close(): void {
    this.opened = false;
    if (this.onClose) {
      this.onClose();
    }
  }
}
