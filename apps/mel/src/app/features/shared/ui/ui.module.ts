import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgxPaginationModule } from 'ngx-pagination';
import { ButtonDeclarations } from './buttons';
import { LocalizedDatePipe } from './date/localized-date.pipe';
import { DialogDeclarations } from './dialog';
import { ObserveDirective } from './directives/observe.directive';
import { TadaDeclarations } from './effects/tada';
import { FieldsetComponent } from './fieldset/fieldset.component';
import { FormFieldComponent } from './form-field/form-field.component';
import { SimpleLoaderComponent } from './loaders/simple-loader/simple-loader.component';
import {
  PaginateInfoPipe,
  PaginationComponent,
  PaginationInfoComponent,
  PaginationQueryParamsPipe,
} from './pagination';

@NgModule({
  declarations: [
    ...ButtonDeclarations.declarations,
    ...TadaDeclarations.declarations,
    ...DialogDeclarations.declarations,
    SimpleLoaderComponent,
    ObserveDirective,
    FieldsetComponent,
    FormFieldComponent,
    PaginateInfoPipe,
    PaginationQueryParamsPipe,
    PaginationComponent,
    PaginationInfoComponent,
    LocalizedDatePipe,
  ],
  exports: [
    ...ButtonDeclarations.declarations,
    ...TadaDeclarations.declarations,
    ...DialogDeclarations.declarations,
    SimpleLoaderComponent,
    ObserveDirective,
    FieldsetComponent,
    FormFieldComponent,
    PaginateInfoPipe,
    PaginationQueryParamsPipe,
    PaginationComponent,
    PaginationInfoComponent,
    LocalizedDatePipe,
  ],
  imports: [CommonModule, TranslateModule, RouterModule, NgxPaginationModule],
  providers: [DatePipe],
})
export class UiModule {}
