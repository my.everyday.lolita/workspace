import { TadaOverlayDirective } from './tada-overlay.directive';
import { TadaComponent } from './tada.component';
import { TadaDirective } from './tada.directive';

export const declarations = [TadaOverlayDirective, TadaComponent, TadaDirective];
