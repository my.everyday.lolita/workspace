import { Directive, HostListener, Input } from '@angular/core';
import { AppButtonColor } from '../../buttons';
import { TadaService } from './tada.service';

@Directive({
  selector: '[melTada]',
})
export class TadaDirective {
  @Input() centerOn?: HTMLElement;
  @Input() color: AppButtonColor = 'default';
  @Input() tadaSize = 75;

  constructor(private tadaService: TadaService) {}

  @HostListener('click', ['$event'])
  onClick(event: MouseEvent): void {
    let x = event.clientX;
    let y = event.clientY;
    if (this.centerOn) {
      const box = this.centerOn.getBoundingClientRect();
      x = box.x + box.width / 2;
      y = box.y + box.height / 2;
    }
    this.tadaService.tada({
      x,
      y,
      size: this.tadaSize,
      color: `${this.color}-shadow`,
    });
  }
}
