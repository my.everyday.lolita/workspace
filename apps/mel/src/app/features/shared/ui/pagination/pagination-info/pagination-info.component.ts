import { Component, Input } from '@angular/core';

@Component({
  selector: 'mel-pagination-info',
  templateUrl: './pagination-info.component.html',
  styleUrls: ['./pagination-info.component.scss'],
})
export class PaginationInfoComponent {
  @Input() id!: string;
}
