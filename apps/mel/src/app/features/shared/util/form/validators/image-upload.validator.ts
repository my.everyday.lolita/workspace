import { AbstractControl, ValidatorFn } from '@angular/forms';

export function imageUpload(domain: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: unknown } | null => {
    if (!control.value) {
      return null;
    }
    if (!control.value.includes(domain)) {
      return { wrongImageLink: true };
    }
    return null;
  };
}
