import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as ThemeActions from './theme.actions';
import { tap } from 'rxjs/operators';
import { setThemeToLocalStorage } from './functions/local';
import { updateBodyClass } from './functions/body';
import { updateStatusBar } from './functions/status-bar';

@Injectable({ providedIn: 'root' })
export class ThemeEffects {
  themeChange$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ThemeActions.themeChange),
        tap((action) => {
          setThemeToLocalStorage(action.theme);
          updateBodyClass(action.theme);
          updateStatusBar();
        })
      ),
    { dispatch: false }
  );

  constructor(private readonly actions$: Actions) {}
}
