/* eslint-disable @typescript-eslint/no-explicit-any */
import { createAction, props } from '@ngrx/store';

export const theme = createAction('[Theme] Get');
export const themeChange = createAction('[Theme] Change', props<{ theme: string }>());
