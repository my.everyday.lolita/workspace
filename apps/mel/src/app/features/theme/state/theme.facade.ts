import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { getThemeFromLocalStorage } from './functions/local';
import * as ThemeActions from './theme.actions';
import * as ThemeSelectors from './theme.selectors';

@Injectable({ providedIn: 'root' })
export class ThemeFacade {
  theme$ = this.store.select(ThemeSelectors.getTheme);

  constructor(private readonly store: Store) {}

  setTheme(theme: string) {
    this.store.dispatch(ThemeActions.themeChange({ theme: theme }));
  }

  getCurrentTheme(): string {
    return getThemeFromLocalStorage();
  }
}
