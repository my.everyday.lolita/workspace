export function updateStatusBar(): void {
  try {
    const metaThemeColor = document.querySelector('meta[name="theme-color"]');
    if (metaThemeColor) {
      const cssDeclaration = window.getComputedStyle(document.body);
      const themeColor = cssDeclaration.getPropertyValue('--primary-shadow').replace(/\s+/g, '');
      if (metaThemeColor.getAttribute('content') !== themeColor) {
        metaThemeColor.setAttribute('content', themeColor);
      }
    }
  } catch {
    // Do nothing.
  }
}
