export function updateBodyClass(theme: string): void {
  const body = document.querySelector('body');
  if (body && !body.classList.contains(`theme-${theme}`)) {
    body.classList.remove('theme-sweet', 'theme-gothic', 'theme-classic');
    body.classList.add(`theme-${theme}`);
  }
}
