export function getThemeFromLocalStorage(): string {
  return localStorage.getItem('theme') || 'sweet';
}

export function setThemeToLocalStorage(theme: string): void {
  localStorage.setItem('theme', theme);
}
