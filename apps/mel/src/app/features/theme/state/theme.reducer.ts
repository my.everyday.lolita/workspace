import { Action, createReducer, on } from '@ngrx/store';
import { updateBodyClass } from './functions/body';
import { getThemeFromLocalStorage } from './functions/local';
import { updateStatusBar } from './functions/status-bar';
import * as ThemeActions from './theme.actions';
import { ThemeState } from './theme.model';

export const THEME_FEATURE_KEY = 'app_theme';

export const initialTheme = getThemeFromLocalStorage();

updateBodyClass(initialTheme);
updateStatusBar();

export const initialState: ThemeState = {
  theme: initialTheme,
};

const themeReducer = createReducer(
  initialState,
  on(ThemeActions.themeChange, (state, action) => ({ theme: action.theme }))
);

export function reducer(state: ThemeState, action: Action) {
  return themeReducer(state, action);
}
