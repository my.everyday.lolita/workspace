import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ThemeState } from './theme.model';
import { THEME_FEATURE_KEY } from './theme.reducer';

export const getThemeState = createFeatureSelector<ThemeState>(THEME_FEATURE_KEY);

export const getTheme = createSelector(getThemeState, (state: ThemeState) => state.theme);
