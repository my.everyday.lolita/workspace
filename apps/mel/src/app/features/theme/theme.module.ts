import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ThemeButtonComponent } from './components/theme/theme.component';
import { ThemeEffects } from './state/theme.effects';
import { reducer, THEME_FEATURE_KEY } from './state/theme.reducer';

@NgModule({
  declarations: [ThemeButtonComponent],
  exports: [ThemeButtonComponent],
  imports: [CommonModule, StoreModule.forFeature(THEME_FEATURE_KEY, reducer), EffectsModule.forFeature([ThemeEffects])],
})
export class AppThemeModule {}
