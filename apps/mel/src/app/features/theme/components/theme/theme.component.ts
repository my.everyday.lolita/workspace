/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, HostBinding, HostListener, Input, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ThemeFacade } from '../../state/theme.facade';

@Component({
  selector: 'mel-theme, [theme-button]',
  templateUrl: './theme.component.html',
  styleUrls: ['./theme.component.scss'],
})
export class ThemeButtonComponent implements OnInit, OnDestroy {
  @Input() theme?: string;

  icon?: TemplateRef<any>;

  @HostBinding('class.current')
  current?: boolean;

  @ViewChild('sweet', { static: true }) sweetIcon: any;
  @ViewChild('gothic', { static: true }) gothicIcon: any;
  @ViewChild('classic', { static: true }) classicIcon: any;

  private unsubscriber = new Subject<void>();

  constructor(private themeFacade: ThemeFacade) {}

  ngOnInit(): void {
    this.themeFacade.theme$.pipe(takeUntil(this.unsubscriber)).subscribe({
      next: (theme) => {
        this.current = this.theme === theme;
      },
    });
    if (this.theme) {
      switch (this.theme) {
        case 'sweet':
          this.icon = this.sweetIcon;
          break;
        case 'gothic':
          this.icon = this.gothicIcon;
          break;
        case 'classic':
          this.icon = this.classicIcon;
          break;

        default:
          break;
      }
    }
  }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  @HostListener('click')
  onClick(): void {
    if (this.theme) {
      this.themeFacade.setTheme(this.theme);
    }
  }
}
