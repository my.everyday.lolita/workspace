import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { UserModule } from '@front/user';
import { TranslateModule } from '@ngx-translate/core';
import { UiModule } from '../shared/ui/ui.module';
import { RegistrationRoutingModule } from './registration-routing.module';
import { RegistrationComponent } from './registration.component';

@NgModule({
  declarations: [RegistrationComponent],
  imports: [
    CommonModule,
    RegistrationRoutingModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    UiModule,
    UserModule,
  ],
})
export class RegistrationModule {}
