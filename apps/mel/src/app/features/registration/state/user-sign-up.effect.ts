import { Injectable } from '@angular/core';
import { UserActions } from '@front/user';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class UserSignUpEffect {
  signUpSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.signUpSuccess),
      tap(() => {
        // @TODO: show toast
      })
      // map(() => {
      //   // @TODO: login user.
      //   throw new Error('Not implemented yet');
      // })
    )
  );

  constructor(private readonly actions$: Actions) {}
}
