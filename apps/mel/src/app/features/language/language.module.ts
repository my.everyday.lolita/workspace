import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { LanguageEffects } from './state/language.effects';
import { LANGUAGE_FEATURE_KEY, reducer } from './state/language.reducer';

@NgModule({
  imports: [StoreModule.forFeature(LANGUAGE_FEATURE_KEY, reducer), EffectsModule.forFeature([LanguageEffects])],
})
export class AppLanguageModule {}
