/* eslint-disable @typescript-eslint/no-explicit-any */
import { createAction, props } from '@ngrx/store';

export const language = createAction('[Language] Get');
export const languageChange = createAction('[Language] Change', props<{ language: string }>());
