import { createFeatureSelector, createSelector } from '@ngrx/store';
import { LanguageState } from './language.model';
import { LANGUAGE_FEATURE_KEY } from './language.reducer';

export const getLanguageState = createFeatureSelector<LanguageState>(LANGUAGE_FEATURE_KEY);

export const getLanguage = createSelector(getLanguageState, (state: LanguageState) => state.lang);
