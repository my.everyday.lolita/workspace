import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as LanguageActions from './language.actions';
import { tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { setLangToLocalStorage } from './functions/local';
import { updateHtmlLang } from './functions/html';

@Injectable({ providedIn: 'root' })
export class LanguageEffects {
  langChange$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(LanguageActions.languageChange),
        tap((action) => {
          this.translateService.use(action.language);
          setLangToLocalStorage(action.language);
          updateHtmlLang(action.language);
        })
      ),
    { dispatch: false }
  );

  constructor(private readonly actions$: Actions, private readonly translateService: TranslateService) {}
}
