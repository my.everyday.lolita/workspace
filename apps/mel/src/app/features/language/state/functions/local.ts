const navigatorLang = navigator.language.split('-')[0];
export const defaultUserLanguage =
  getLangFromLocalStorage() || (['fr', 'en'].includes(navigatorLang) ? navigatorLang : 'en');

setLangToLocalStorage(defaultUserLanguage);

export function getLangFromLocalStorage(): string {
  return (localStorage.getItem('lang') as string) || 'en';
}

export function setLangToLocalStorage(lang: string) {
  localStorage.setItem('lang', lang);
}
