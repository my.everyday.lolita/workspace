import { defaultUserLanguage } from './local';

export function updateHtmlLang(lang: string): void {
  const html = document.querySelector('html');
  if (html && html.getAttribute('lang') !== lang) {
    html.setAttribute('lang', lang);
  }
}

updateHtmlLang(defaultUserLanguage);
