import { Action, createReducer, on } from '@ngrx/store';
import { defaultUserLanguage } from './functions/local';
import * as LanguageActions from './language.actions';
import { LanguageState } from './language.model';

export const LANGUAGE_FEATURE_KEY = 'app_language';

export const initialState: LanguageState = {
  lang: defaultUserLanguage,
};

const languageReducer = createReducer(
  initialState,
  on(LanguageActions.languageChange, (state, action) => ({ lang: action.language }))
);

export function reducer(state: LanguageState, action: Action) {
  return languageReducer(state, action);
}
