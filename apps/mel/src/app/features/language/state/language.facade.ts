import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { getLangFromLocalStorage } from './functions/local';
import * as LanguageActions from './language.actions';
import * as LanguageSelectors from './language.selectors';

@Injectable({ providedIn: 'root' })
export class LanguageFacade {
  lang$ = this.store.select(LanguageSelectors.getLanguage);

  constructor(private readonly store: Store) {}

  setLang(lang: string) {
    this.store.dispatch(LanguageActions.languageChange({ language: lang }));
  }

  getCurrentLang(): string {
    return getLangFromLocalStorage();
  }
}
