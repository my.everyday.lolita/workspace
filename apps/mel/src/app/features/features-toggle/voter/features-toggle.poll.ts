import { createPoll } from '@front/voter';

export const systemInfosFeature = createPoll('poll:system-infos');
