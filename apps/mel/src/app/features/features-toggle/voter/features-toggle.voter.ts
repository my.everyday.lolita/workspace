import { Injectable } from '@angular/core';
import { createVoter } from '@front/voter';
import { of } from 'rxjs';
import { systemInfosFeature } from './features-toggle.poll';

@Injectable({ providedIn: 'root' })
export class FeaturesToggleVoter {
  systemInfos = createVoter(systemInfosFeature.type, () => of(false));
}
