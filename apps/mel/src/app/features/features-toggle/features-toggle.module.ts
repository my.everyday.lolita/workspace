import { NgModule } from '@angular/core';
import { NgVoterModule, NG_VOTER } from '@front/voter';
import { FeaturesToggleVoter } from './voter/features-toggle.voter';

@NgModule({
  imports: [NgVoterModule],
  providers: [{ provide: NG_VOTER, useClass: FeaturesToggleVoter, multi: true }],
})
export class AppFeaturesToggleModule {}
