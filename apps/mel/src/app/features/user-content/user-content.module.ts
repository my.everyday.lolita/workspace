import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AppUserModule } from '../user/user.module';
import { UserContentEffects } from './state/user-content.effects';
import { reducer, USER_CONTENT_FEATURE_KEY } from './state/user-content.reducer';

@NgModule({
  imports: [
    AppUserModule,
    StoreModule.forFeature(USER_CONTENT_FEATURE_KEY, reducer),
    EffectsModule.forFeature([UserContentEffects]),
  ],
})
export class UserContentModule {}
