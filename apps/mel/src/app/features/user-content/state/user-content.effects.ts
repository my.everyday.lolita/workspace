import { Injectable } from '@angular/core';
import { ItemsResourceService, UserContentResourceService } from '@api/front';
import { CacheService } from '@front/cache';
import { UserActions } from '@front/user';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, of, switchMap } from 'rxjs';
import { catchError, debounceTime, filter, take, tap, withLatestFrom } from 'rxjs/operators';
import { AppUserFacade } from '../../user/user.facade';
import * as UserContentActions from './user-content.actions';
import { UserContentFacade } from './user-content.facade';
import * as UserContentFunctions from './user-content.functions';

@Injectable({ providedIn: 'root' })
export class UserContentEffects {
  load$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserContentActions.load, UserActions.loginSuccess),
      withLatestFrom(this.facade.init$),
      filter(([, init]) => !init),
      map(([action]) =>
        action.type === UserActions.loginSuccess.type
          ? UserContentActions.loadFromBackend()
          : UserContentActions.loadFromLocal()
      )
    )
  );

  loadFromLocal$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserContentActions.loadFromLocal),
      map(() =>
        UserContentActions.loadSuccess({
          payload: UserContentFunctions.getFromLocalStorage(),
        })
      )
    )
  );

  loadFromBackend$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserContentActions.loadFromBackend),
      switchMap(() =>
        this.service.get().pipe(
          map((data) => UserContentActions.loadSuccess({ payload: data })),
          catchError((error) => of(UserContentActions.loadFailure({ error })))
        )
      )
    )
  );

  toggleCloset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserContentActions.toggleCloset),
      withLatestFrom(this.facade.closet$),
      map(([{ variantId }, collection]) => {
        if (collection.findIndex((i) => i.id === variantId) === -1) {
          return UserContentActions.addToCloset({ variantId });
        }
        return UserContentActions.removeFromCloset({ variantId });
      })
    )
  );

  toggleWishlist$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserContentActions.toggleWishlist),
      withLatestFrom(this.facade.wishlist$),
      map(([{ variantId }, collection]) => {
        if (collection.findIndex((i) => i.id === variantId) === -1) {
          return UserContentActions.addToWishlist({ variantId });
        }
        return UserContentActions.removeFromWishlist({ variantId });
      })
    )
  );

  addToCloset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserContentActions.addToCloset),
      withLatestFrom(this.facade.wishlist$),
      filter(([{ variantId }, collection]) => collection.findIndex((i) => i.id === variantId) !== -1),
      map(([{ variantId }]) => UserContentActions.removeFromWishlist({ variantId }))
    )
  );

  addToWishlist$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserContentActions.addToWishlist),
      withLatestFrom(this.facade.closet$),
      filter(([{ variantId }, collection]) => collection.findIndex((i) => i.id === variantId) !== -1),
      map(([{ variantId }]) => UserContentActions.removeFromCloset({ variantId }))
    )
  );

  cache$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserContentActions.addToCloset, UserContentActions.addToWishlist),
        map(({ variantId }) => variantId.split(':')[0]),
        switchMap((id) =>
          this.cache.match(id).pipe(
            filter((response) => !response),
            switchMap(() => this.itemsService.findById(id).pipe(switchMap((item) => this.cache.put(id, item as never))))
          )
        )
      ),
    { dispatch: false }
  );

  save$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          UserContentActions.addToCloset,
          UserContentActions.addToWishlist,
          UserContentActions.removeFromCloset,
          UserContentActions.removeFromWishlist,
          UserContentActions.toggleWantToSell,
          UserContentActions.toggleDreamDress,
          UserContentActions.addCoordination,
          UserContentActions.removeCoordination,
          UserContentActions.updateCoordination
        ),
        debounceTime(500),
        switchMap(() => this.facade.content$.pipe(take(1))),
        tap((userContentToSave) => {
          UserContentFunctions.saveToLocalStorage(userContentToSave);
        }),
        withLatestFrom(this.userFacade.isLoggedInt$),
        filter(([, isLoggedIn]) => isLoggedIn),
        tap(([userContent]) => {
          this.service.update(userContent).subscribe();
        })
      ),
    { dispatch: false }
  );

  constructor(
    private readonly actions$: Actions,
    private readonly service: UserContentResourceService,
    private readonly facade: UserContentFacade,
    private readonly userFacade: AppUserFacade,
    private readonly cache: CacheService,
    private readonly itemsService: ItemsResourceService
  ) {}
}
