import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UserContentState } from './user-content.model';
import { USER_CONTENT_FEATURE_KEY } from './user-content.reducer';

export const getUserContentState = createFeatureSelector<UserContentState>(USER_CONTENT_FEATURE_KEY);

export const getData = createSelector(getUserContentState, (state: UserContentState) => state.data);
export const isInitialized = createSelector(getUserContentState, (state: UserContentState) => state.init ?? false);

export const getCloset = createSelector(getUserContentState, (state: UserContentState) => state.data?.closet ?? []);
export const getWishlist = createSelector(getUserContentState, (state: UserContentState) => state.data?.wishlist ?? []);
export const getCoordinations = createSelector(
  getUserContentState,
  (state: UserContentState) => state.data?.coordinations ?? []
);
export const getLevelUpQuiz = createSelector(getUserContentState, (state: UserContentState) => state.data?.levelUpQuiz);
