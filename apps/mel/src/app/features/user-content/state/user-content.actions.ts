import { ApiModels } from '@api/shared';
import { createAction, props } from '@ngrx/store';

export const load = createAction('[UserContent] load user content');
export const loadFromLocal = createAction('[UserContent] load user content from local');
export const loadFromBackend = createAction('[UserContent] load user content from backend');
export const loadSuccess = createAction(
  '[UserContent] load user content success',
  props<{ payload: ApiModels.UserContent }>()
);
export const loadFailure = createAction('[UserContent] load user content failure', props<{ error: unknown }>());

export const addToCloset = createAction('[UserContent] add to closet', props<{ variantId: string }>());
export const removeFromCloset = createAction('[UserContent] remove from closet', props<{ variantId: string }>());
export const toggleCloset = createAction('[UserContent] Toggle closet', props<{ variantId: string }>());

export const addToWishlist = createAction('[UserContent] add to wishlist', props<{ variantId: string }>());
export const removeFromWishlist = createAction('[UserContent] remove from wishlist', props<{ variantId: string }>());
export const toggleWishlist = createAction('[UserContent] Toggle wishlist', props<{ variantId: string }>());

export const toggleWantToSell = createAction(
  '[UserContent] toggle the want to sell property',
  props<{ variantId: string }>()
);
export const toggleDreamDress = createAction(
  '[UserContent] toggle the dream dress property',
  props<{ variantId: string }>()
);

export const removeCoordination = createAction('[UserContent] remove a coordination', props<{ id: string }>());

export const addCoordination = createAction(
  '[UserContent] add a coordination',
  props<{ coord: ApiModels.Coordination }>()
);

export const updateCoordination = createAction(
  '[UserContent] update a coordination',
  props<{ coord: ApiModels.Coordination }>()
);
