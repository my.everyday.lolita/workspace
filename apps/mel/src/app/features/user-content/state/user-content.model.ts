import { ApiModels } from '@api/shared';

export interface UserContentState {
  data: ApiModels.UserContent;
  init: boolean;
  error?: unknown;
}

export type PartialUserContentState = Partial<UserContentState>;
