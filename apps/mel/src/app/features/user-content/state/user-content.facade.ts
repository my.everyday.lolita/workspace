import { Injectable } from '@angular/core';
import { ApiModels } from '@api/shared';
import { Store } from '@ngrx/store';
import * as UserContentActions from './user-content.actions';
import * as UserContentSelectors from './user-content.selectors';

@Injectable({ providedIn: 'root' })
export class UserContentFacade {
  content$ = this.store.select(UserContentSelectors.getData);
  init$ = this.store.select(UserContentSelectors.isInitialized);
  closet$ = this.store.select(UserContentSelectors.getCloset);
  wishlist$ = this.store.select(UserContentSelectors.getWishlist);
  coordinations$ = this.store.select(UserContentSelectors.getCoordinations);
  levelUpQuiz$ = this.store.select(UserContentSelectors.getLevelUpQuiz);

  constructor(private readonly store: Store) {}

  load(): void {
    this.store.dispatch(UserContentActions.load());
  }

  toggleCloset(variantId: string): void {
    this.store.dispatch(UserContentActions.toggleCloset({ variantId }));
  }

  toggleWishlist(variantId: string): void {
    this.store.dispatch(UserContentActions.toggleWishlist({ variantId }));
  }

  addToCloset(variantId: string): void {
    this.store.dispatch(UserContentActions.addToCloset({ variantId }));
  }

  addToWishlist(variantId: string): void {
    this.store.dispatch(UserContentActions.addToWishlist({ variantId }));
  }

  removeFromCloset(variantId: string): void {
    this.store.dispatch(UserContentActions.removeFromCloset({ variantId }));
  }

  removeFromWishlist(variantId: string): void {
    this.store.dispatch(UserContentActions.removeFromWishlist({ variantId }));
  }

  toggleWantToSell(variantId: string): void {
    this.store.dispatch(UserContentActions.toggleWantToSell({ variantId }));
  }

  toggleDreamDress(variantId: string): void {
    this.store.dispatch(UserContentActions.toggleDreamDress({ variantId }));
  }

  removeCoordination(coord: ApiModels.Coordination): void {
    this.store.dispatch(UserContentActions.removeCoordination({ id: coord.id }));
  }

  addCoordination(coord: ApiModels.Coordination): void {
    this.store.dispatch(UserContentActions.addCoordination({ coord }));
  }

  updateCoordination(coord: ApiModels.Coordination): void {
    this.store.dispatch(UserContentActions.updateCoordination({ coord }));
  }
}
