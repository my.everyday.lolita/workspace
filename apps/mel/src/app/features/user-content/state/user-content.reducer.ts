import { Action, createReducer, on } from '@ngrx/store';
import * as UserContentActions from './user-content.actions';
import * as UserContentFunctions from './user-content.functions';
import { PartialUserContentState, UserContentState } from './user-content.model';

export const USER_CONTENT_FEATURE_KEY = 'app_user_content';

export const initialState: PartialUserContentState = {
  data: undefined,
  init: false,
};

const userContentReducer = createReducer(
  initialState,
  on(UserContentActions.loadSuccess, (_, { payload }) => ({ data: payload, init: true })),
  on(UserContentActions.loadFailure, (state, { error }) => ({ ...state, error })),
  on(UserContentActions.addToCloset, (state, { variantId }) => UserContentFunctions.stateAddToCloset(state, variantId)),
  on(UserContentActions.addToWishlist, (state, { variantId }) =>
    UserContentFunctions.stateAddToWishlist(state, variantId)
  ),
  on(UserContentActions.removeFromCloset, (state, { variantId }) =>
    UserContentFunctions.stateRemoveFromCloset(state, variantId)
  ),
  on(UserContentActions.removeFromWishlist, (state, { variantId }) =>
    UserContentFunctions.stateRemoveFromWishlist(state, variantId)
  ),
  on(UserContentActions.toggleWantToSell, (state, { variantId }) =>
    UserContentFunctions.stateToggleWantToSell(state, variantId)
  ),
  on(UserContentActions.toggleDreamDress, (state, { variantId }) =>
    UserContentFunctions.stateToggleDreamDress(state, variantId)
  ),
  on(UserContentActions.removeCoordination, (state, { id }) => UserContentFunctions.removeCoordination(state, id)),
  on(UserContentActions.addCoordination, (state, { coord }) => UserContentFunctions.addCoordination(state, coord)),
  on(UserContentActions.updateCoordination, (state, { coord }) => UserContentFunctions.updateCoordination(state, coord))
);

export function reducer(state: UserContentState, action: Action) {
  return userContentReducer(state, action);
}
