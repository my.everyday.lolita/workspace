import { ApiModels } from '@api/shared';
import { USER_CONTENT_STORAGE_KEY } from '../user-content.constants';
import { PartialUserContentState, UserContentState } from './user-content.model';

export function saveToLocalStorage(data: ApiModels.UserContent) {
  localStorage.setItem(USER_CONTENT_STORAGE_KEY, JSON.stringify(data));
}

export function removeFromLocalStorage() {
  localStorage.removeItem(USER_CONTENT_STORAGE_KEY);
}

export function getFromLocalStorage(): ApiModels.UserContent {
  const data = localStorage.getItem(USER_CONTENT_STORAGE_KEY);
  return typeof data === 'string' ? JSON.parse(data) : getDefaultData();
}

export function getDefaultData(): ApiModels.UserContent {
  return { closet: [], wishlist: [], coordinations: [], levelUpQuiz: [], modified: -1 };
}

export function ensureData(state: PartialUserContentState): UserContentState {
  const _state = state as UserContentState;
  return {
    init: state.init ?? false,
    error: state.error ?? undefined,
    data: {
      ...getDefaultData(),
      ...(_state.data ?? {}),
    },
  };
}

export function addToCollection<T extends ApiModels.UserContentCollectionItem>(
  id: string,
  collection: T[],
  add: (id: string) => T
): void {
  if (collection.findIndex((i) => i.id === id) === -1) {
    collection.push(add(id));
  }
}

export function addToCloset(id: string, collection: ApiModels.UserContentClosetItem[]): void {
  addToCollection(id, collection, (itemId) => ({ id: itemId, wantToSell: false }));
}

export function addToWishlist(id: string, collection: ApiModels.UserContentWishlistItem[]): void {
  addToCollection(id, collection, (itemId) => ({ id: itemId, dreamDress: false }));
}

export function stateToggleWantToSell(state: PartialUserContentState, variantId: string): PartialUserContentState {
  const _state = JSON.parse(JSON.stringify(ensureData(state))) as UserContentState;
  const closetItem = _state.data.closet.find((vi) => vi.id === variantId);
  if (closetItem) {
    closetItem.wantToSell = !closetItem.wantToSell;
  }
  return _state;
}

export function stateToggleDreamDress(state: PartialUserContentState, variantId: string): PartialUserContentState {
  const _state = JSON.parse(JSON.stringify(ensureData(state))) as UserContentState;
  const wishlistItem = _state.data.wishlist.find((vi) => vi.id === variantId);
  if (wishlistItem) {
    wishlistItem.dreamDress = !wishlistItem.dreamDress;
  }
  return _state;
}

export function stateRemoveFromWishlist(state: PartialUserContentState, variantId: string): PartialUserContentState {
  const _state = ensureData(state);
  return {
    ..._state,
    data: {
      ..._state.data,
      wishlist: _state.data.wishlist.filter((item) => item.id !== variantId),
    },
  };
}

export function stateRemoveFromCloset(state: PartialUserContentState, variantId: string): PartialUserContentState {
  const _state = ensureData(state);
  return {
    ..._state,
    data: {
      ..._state.data,
      closet: _state.data.closet.filter((item) => item.id !== variantId),
    },
  };
}

export function stateAddToWishlist(state: PartialUserContentState, variantId: string): PartialUserContentState {
  const _state = ensureData(state);
  const wishlist = [...(_state as UserContentState).data.wishlist];
  addToWishlist(variantId, wishlist);
  return { ..._state, data: { ..._state.data, wishlist } };
}

export function stateAddToCloset(state: PartialUserContentState, variantId: string): PartialUserContentState {
  const _state = ensureData(state);
  const closet = [...(_state as UserContentState).data.closet];
  addToCloset(variantId, closet);
  return { ..._state, data: { ..._state.data, closet } };
}

export function removeCoordination(state: PartialUserContentState, coordinationId: string): PartialUserContentState {
  const _state = ensureData(state);
  return {
    ..._state,
    data: { ..._state.data, coordinations: _state.data.coordinations.filter((c) => c.id !== coordinationId) ?? [] },
  };
}

export function addCoordination(
  state: PartialUserContentState,
  coord: ApiModels.Coordination
): PartialUserContentState {
  const _state = ensureData(state);
  const coordinations = [...(_state as UserContentState).data.coordinations, coord];
  return { ..._state, data: { ..._state.data, coordinations } };
}

export function updateCoordination(
  state: PartialUserContentState,
  coord: ApiModels.Coordination
): PartialUserContentState {
  const _state = ensureData(state);
  return {
    ..._state,
    data: { ..._state.data, coordinations: _state.data.coordinations.map((c) => (c.id === coord.id ? coord : c)) },
  };
}
