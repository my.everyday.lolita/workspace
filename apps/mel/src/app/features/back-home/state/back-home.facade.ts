import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { getBackHome } from './back-home.selectors';

@Injectable({ providedIn: 'root' })
export class BackHomeFacade {
  readonly displayBackHome$ = this.store.select(getBackHome);

  constructor(private readonly store: Store) {}
}
