import { Action, createReducer, on } from '@ngrx/store';
import { BackHomeState } from '../back-home.model';
import { displayBackHome } from './back-home.actions';

export const BACK_HOME_FEATURE_KEY = 'app_back_home';

export const initialState: BackHomeState = {
  backHome: false,
};

const backHomeReducer = createReducer(
  initialState,
  on(displayBackHome, (_, { backHome }) => ({ backHome }))
);

export function reducer(state: BackHomeState, action: Action) {
  return backHomeReducer(state, action);
}
