import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { displayBackHome } from './back-home.actions';

@Injectable({ providedIn: 'root' })
export class BackHomeEffects {
  readonly router$ = createEffect(() =>
    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      switchMap(() => this.activatedRoute.firstChild?.data || of({})),
      map(({ backHome }) => displayBackHome({ backHome }))
    )
  );

  constructor(private readonly router: Router, private readonly activatedRoute: ActivatedRoute) {}
}
