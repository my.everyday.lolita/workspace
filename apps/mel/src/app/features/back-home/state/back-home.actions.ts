import { createAction, props } from '@ngrx/store';

export const displayBackHome = createAction('[BackHome] display the back home button', props<{ backHome: boolean }>());
