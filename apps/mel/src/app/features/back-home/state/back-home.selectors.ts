import { createFeatureSelector, createSelector } from '@ngrx/store';
import { BackHomeState } from '../back-home.model';
import { BACK_HOME_FEATURE_KEY } from './back-home.reducer';

export const getState = createFeatureSelector<BackHomeState>(BACK_HOME_FEATURE_KEY);

export const getBackHome = createSelector(getState, (state) => state.backHome);
