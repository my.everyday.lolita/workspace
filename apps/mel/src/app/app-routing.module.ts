import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { APP_ROUTES } from './app.token';
import { CoordinationGuard } from './features/coordinations/coordination.guard';
import { CoordinationResolver } from './features/coordinations/coordination.resolver';
import { AboutProjectComponent } from './pages/about-project/about-project.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { CoordinationComponent } from './pages/coordination/coordination.component';
import { HomeComponent } from './pages/home/home.component';
import { MyClosetComponent } from './pages/my-closet/my-closet.component';
import { MyCoordChecklistComponent } from './pages/my-coord-checklist/my-coord-checklist.component';
import { MyWishlistComponent } from './pages/my-wishlist/my-wishlist.component';
import { SearchComponent } from './pages/search/search.component';
import { SignInComponent } from './pages/sign-in/sign-in.component';

export const routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      animation: 'home',
      isHome: true,
    },
  },

  {
    path: 'my-closet',
    component: MyClosetComponent,
    data: {
      home: true,
      linkLabel: 'HOME.LINKS.MY_CLOSET',
      animation: 'my_closet',
      pageTitle: 'PAGES.TITLES.MY_CLOSET',
    },
  },
  {
    path: 'my-wishlist',
    component: MyWishlistComponent,
    data: {
      home: true,
      linkLabel: 'HOME.LINKS.MY_WISHLIST',
      animation: 'my_wishlist',
      pageTitle: 'PAGES.TITLES.MY_WISHLIST',
    },
  },
  {
    path: 'my-coord-checklist',
    component: MyCoordChecklistComponent,
    data: {
      home: true,
      linkLabel: 'HOME.LINKS.MY_COORD_CHECKLIST',
      animation: 'my_coord_checklist',
      pageTitle: 'PAGES.TITLES.MY_COORD_CHECKLIST',
    },
  },
  {
    path: 'my-coord-checklist/:id',
    component: CoordinationComponent,
    resolve: {
      coordination: CoordinationResolver,
    },
    canActivate: [CoordinationGuard],
    data: {
      animation: 'coordination',
      backHome: true,
    },
  },
  {
    path: 'search',
    component: SearchComponent,
    data: {
      home: true,
      linkLabel: 'HOME.LINKS.SEARCH',
      animation: 'search',
      pageTitle: 'PAGES.TITLES.SEARCH',
    },
  },
  {
    path: 'sign-in',
    component: SignInComponent,
    data: {
      home: false,
      linkLabel: 'MENU.LINKS.SIGN_IN',
      animation: 'sign_in',
      pageTitle: 'PAGES.TITLES.SIGN_IN',
    },
  },
  {
    path: 'registration',
    loadChildren: () => import('./features/registration/registration.module').then((m) => m.RegistrationModule),
  },
  {
    path: 'about-us',
    component: AboutUsComponent,
    data: {
      home: false,
      others: true,
      backHome: true,
      linkLabel: 'MENU.LINKS.ABOUT_US',
      animation: 'about_us',
      pageTitle: 'PAGES.TITLES.ABOUT_US',
    },
  },
  {
    path: 'about-the-project',
    component: AboutProjectComponent,
    data: {
      home: false,
      others: true,
      linkLabel: 'MENU.LINKS.ABOUT_PROJECT',
      animation: 'about_project',
      pageTitle: 'PAGES.TITLES.ABOUT_PROJECT',
    },
  },
  // {
  //   path: 'registration',
  //   component: RegistrationComponent,
  //   data: {
  //     home: false,
  //     linkLabel: 'MENU.LINKS.REGISTRATION',
  //     animation: 'registration',
  //     pageTitle: 'PAGES.TITLES.REGISTRATION',
  //   },
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' })],
  exports: [RouterModule],
  providers: [{ provide: APP_ROUTES, useValue: routes }],
})
export class AppRoutingModule {}
