import { animate, animateChild, group, query, stagger, style, transition, trigger } from '@angular/animations';
import { Component, Inject, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { RouterOutlet, Routes } from '@angular/router';
import { SystemService } from '@api/front';
import { ApiModels } from '@api/shared';
import { CriteriaFacade } from '@front/criteria';
import { UserFacade } from '@front/user';
import { NgVoterService } from '@front/voter';
import { interval } from 'rxjs';
import { filter, startWith, switchMap } from 'rxjs/operators';
import { PageData } from './app.model';
import { APP_ROUTES } from './app.token';
import { BackHomeFacade } from './features/back-home';
import { systemInfosFeature } from './features/features-toggle';
import { LanguageFacade } from './features/language';
import { DialogAttachComponent, DialogComponent, DialogService } from './features/shared/ui/dialog';
import { TitleFacade } from './features/title';
import { UpdateService } from './features/update';

@Component({
  selector: 'mel-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('stripAnimations', [
      transition(':enter', [
        group([
          query(
            '.left',
            [
              style({ opacity: 0, transform: 'translateX(-100%)' }),
              stagger('100ms', [
                animate('2s easeOutElastic(1, .8)', style({ opacity: 1, transform: 'translateX(0%)' })),
              ]),
            ],
            { optional: true }
          ),
          query(
            '.right',
            [
              style({ opacity: 0, transform: 'translateX(100%)' }),
              stagger('100ms', [
                animate('2s easeOutElastic(1, .8)', style({ opacity: 1, transform: 'translateX(0%)' })),
              ]),
            ],
            { optional: true }
          ),
        ]),
      ]),
    ]),
    trigger('cloudsAnimations', [
      transition(':enter', [
        group([
          query(
            '#top-left-cloud-svg',
            [
              style({ opacity: 1, transform: 'translateX(-42%)' }),
              stagger('100ms', [animate('800ms linear', style({ opacity: 1, transform: 'translateX(0%)' }))]),
            ],
            { optional: true }
          ),
          query(
            '#top-right-cloud-svg',
            [
              style({ opacity: 1, transform: 'translateX(42%)' }),
              stagger('100ms', [animate('800ms linear', style({ opacity: 1, transform: 'translateX(0%)' }))]),
            ],
            { optional: true }
          ),
        ]),
      ]),
    ]),
    trigger('routeAnimations', [
      transition('* <=> *', [
        style({ position: 'relative' }),
        query(
          ':enter, :leave',
          [
            style({
              top: 0,
              left: 0,
              width: '100vw',
            }),
          ],
          { optional: true }
        ),
        query(':leave', animateChild(), { optional: true }),
        query(':enter', [animateChild(), style({ position: 'relative' })], { optional: true }),
      ]),
    ]),
    trigger('toolbarItemAnimation', [
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(20%)' })),
      ]),
      transition(':enter', [
        style({ opacity: 0, transform: 'translateY(20%)' }),
        animate('330ms linear', style({ opacity: 1, transform: 'translateY(0%)' })),
      ]),
    ]),
    trigger('backHome', [
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(20%)' })),
      ]),
      transition(':enter', [
        style({ opacity: 0, transform: 'translateY(20%)' }),
        animate('330ms linear', style({ opacity: 1, transform: 'translateY(0%)' })),
      ]),
    ]),
    trigger('maintenance', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('330ms linear', style({ opacity: 1 })),
        group([
          query('#maintenance-cone', [style({ opacity: 0 }), animate('330ms linear', style({ opacity: 1 }))], {
            optional: true,
          }),
          query('#maintenance-cone-2', [style({ opacity: 0 }), animate('330ms linear', style({ opacity: 1 }))], {
            optional: true,
          }),
          query(
            '#maintenance-barier',
            [
              style({ opacity: 0, transform: 'translateX(100%)' }),
              animate('330ms linear', style({ opacity: 1, transform: 'translateX(0%)' })),
            ],
            { optional: true }
          ),
          query(
            '#maintenance-barier-2',
            [
              style({ opacity: 0, transform: 'translateX(-100%)' }),
              animate('330ms linear', style({ opacity: 1, transform: 'translateX(0%)' })),
            ],
            { optional: true }
          ),
          query(
            'h1, #maintenance-bunny, p',
            [
              style({ opacity: 0, transform: 'translateY(20%)' }),
              stagger('150ms', [animate('330ms 330ms linear', style({ opacity: 1, transform: 'translateY(0%)' }))]),
            ],
            { optional: true }
          ),
        ]),
      ]),
    ]),
  ],
})
export class AppComponent implements OnInit {
  @ViewChild('menu', { static: true }) private menuTemplate?: TemplateRef<unknown>;
  @ViewChild('menuContainer', { static: true }) private menuContainer!: DialogAttachComponent;

  currentPage?: PageData;
  isMenuOpened?: boolean;
  menuComponent?: DialogComponent;
  inMaintenance?: boolean;
  systemInfos?: ApiModels.SystemInfos;
  languageControl: FormControl;
  currentPath = '';
  signedIn = false;
  replaceUrl = false;
  others: Routes;

  get laceWidth(): number {
    return window.innerWidth;
  }

  constructor(
    public readonly viewContainerRef: ViewContainerRef,
    public readonly criteriaFacade: CriteriaFacade,
    public readonly userFacade: UserFacade,
    public readonly titleFacade: TitleFacade,
    public readonly backHomeFacade: BackHomeFacade,
    private readonly languageFacade: LanguageFacade,
    private readonly systemInfosService: SystemService,
    private readonly dialogService: DialogService,
    private readonly updateService: UpdateService,
    private readonly voterService: NgVoterService,
    @Inject(APP_ROUTES) routes: Routes
  ) {
    this.others = routes.filter((route) => route.data && route.data.others);
    this.updateService.init();
    this.languageControl = new FormControl(this.languageFacade.getCurrentLang());
    this.languageControl.valueChanges.pipe().subscribe({
      next: (value) => {
        this.languageFacade.setLang(value);
      },
    });
  }

  ngOnInit(): void {
    // Check system infos every 60s.
    interval(60000)
      .pipe(
        startWith(null),
        switchMap(() => this.voterService.ask(systemInfosFeature)),
        filter((isOk) => isOk),
        switchMap(() => this.systemInfosService.getSystemInfo())
      )
      .subscribe({
        next: (systemInfos) => {
          this.systemInfos = systemInfos;
          this.inMaintenance = systemInfos.status === ApiModels.SystemStatus.MAINTENANCE;
        },
      });
  }

  prepareRoute(outlet: RouterOutlet): string {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

  toggleMenu(): void {
    if (!this.menuTemplate) return;
    if (!this.isMenuOpened) {
      this.menuComponent = this.dialogService.open(
        this.menuTemplate,
        {
          dialogClass: 'main-menu',
          modal: true,
        },
        this.menuContainer.viewContainerRef
      );
      const originalClose = this.menuComponent.onClose;
      this.menuComponent.onClose = () => {
        if (originalClose) {
          originalClose.bind(this.menuComponent)();
        }
        this.isMenuOpened = false;
      };
      this.isMenuOpened = true;
    } else {
      if (this.menuComponent) {
        this.menuComponent.close();
      }
    }
  }
}
