import { InjectionToken } from '@angular/core';
import { Routes } from '@angular/router';

export const APP_ROUTES = new InjectionToken<Routes>('AppRoutes');
export const APP_USER_DEFAULT_LANGUAGE = new InjectionToken<Routes>('AppUserDefaultLanguage');
export const APP_GAMES_TIPS_ROUTES = new InjectionToken<Routes>('AppGamesAndTipsRoutes');

export const APP_PROJECT_LINKS = new InjectionToken<unknown>('AppProjectLinks');

export const APP_DEFAULT_SEARCH_LIMIT = new InjectionToken<number>('AppDefaultSearchLimit');
