(function (window) {
  let domains = {
    registration: '${MEL_REGISTRATION_DOMAIN}',
    login: '${MEL_KEYCLOAK_DOMAIN}',
    mel: '${MEL_API_DOMAIN}',
  };
  let links = {
    patreon: '${PATREON_URL}',
    rockuu: {
      youtube: '${YOUTUBE_URL}',
      youtubeLolita: '${YOUTUBE_LOLITA_URL}',
      instagram: '${INSTAGRAM_URL}',
      facebook: '${FACEBOOK_URL}'
    }
  };
  window["env"] = window["env"] || {
    domains,
    links
  };

})(this);
