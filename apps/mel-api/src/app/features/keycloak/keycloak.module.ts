import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { APP_INTERCEPTOR } from "@nestjs/core";
import { UserHelperInterceptor } from "./interceptors/user-helper.interceptor";
import { KeycloakService } from "./keycloak.service";

@Module({
    imports: [HttpModule, ConfigModule],
    providers: [
        KeycloakService,
        // { provide: APP_INTERCEPTOR, useClass: UserHelperInterceptor }
    ],
    exports: [KeycloakService]
})
export class KeycloakModule { }