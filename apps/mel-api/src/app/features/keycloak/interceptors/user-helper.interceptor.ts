/* eslint-disable @typescript-eslint/no-explicit-any */
import { CallHandler, ExecutionContext, HttpException, HttpStatus, Injectable, NestInterceptor } from '@nestjs/common';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { KeycloakService } from '../keycloak.service';
import { KeycloakUser } from '../user.model';

@Injectable()
export class UserHelperInterceptor implements NestInterceptor {
  constructor(private keycloakService: KeycloakService) {}

  intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> {
    const request = context.switchToHttp().getRequest();
    if (request.keycloakUserData !== undefined) {
      return next.handle();
    } else {
      const headers = request.headers;
      if (!headers.authorization) {
        return next.handle();
      }
      const token = headers.authorization.replace('Bearer ', '');
      if (token === '') {
        return next.handle();
      }
      const user = jwtDecode<KeycloakUser>(token);
      if (!user.preferred_username) {
        return next.handle();
      }
      return this.keycloakService.introspect(user.preferred_username, token).pipe(
        switchMap((response) => {
          if (response.status !== HttpStatus.OK) {
            throw new HttpException(
              'An unknown error was occured during authentication check',
              HttpStatus.INTERNAL_SERVER_ERROR
            );
          }
          const keycloakUserData = response.data;
          if (keycloakUserData.active === false) {
            throw new HttpException(
              {
                statusCode: HttpStatus.UNAUTHORIZED,
                message: 'Inactive user',
                inactive: true,
              },
              HttpStatus.UNAUTHORIZED
            );
          }
          request.keycloakUserData = keycloakUserData;
          return next.handle();
        })
      );
    }
  }
}
