import { createParamDecorator, ExecutionContext } from "@nestjs/common";

export const GetUserHelper = createParamDecorator((_, context: ExecutionContext) => {
    const request = context.switchToHttp().getRequest();
    const data = request.keycloakUserData;
    const roles = data && data.realm_access.roles || [];
    return {
        sub: data && data.sub || undefined,
        isAnonymous: data === undefined,
        roles,
        isOwn: (data && request.body && (request.body.owner === data.sub || request.body.user === data.sub)) || false,
        isAdmin: roles.includes('admin')
    };
});

export interface UserHelper {
    roles: string[];
    isOwn: boolean;
    isAdmin: boolean;
    isAnonymous: boolean;
    sub: string;
}