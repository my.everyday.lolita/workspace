import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ApiModels } from '@api/shared';

export type SystemInfosDocument = SystemInfos & Document;

@Schema()
export class SystemInfos {
  @Prop({ required: true })
  status: ApiModels.SystemStatus;

  @Prop()
  estimatedDuration?: number;

  @Prop()
  startDate?: Date;
}

export const SystemInfosSchema = SchemaFactory.createForClass(SystemInfos);
