import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { SystemController } from './system.controller';
import { SystemInfos, SystemInfosSchema } from './system.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: SystemInfos.name, schema: SystemInfosSchema }]), KeycloakModule],
  controllers: [SystemController],
})
export class SystemModule {}
