/* eslint-disable @typescript-eslint/no-explicit-any */
import { Type } from 'class-transformer';
import { IsBoolean, IsDateString, IsNotEmpty, IsOptional, IsUUID, ValidateNested } from 'class-validator';

export class UserContentVariantDto {
  @IsNotEmpty()
  id: string;

  @IsBoolean()
  @IsOptional()
  wantToSell: boolean;

  @IsBoolean()
  @IsOptional()
  dreamDress: boolean;
}

export class UserContentLevelUpQuizDto {
  @IsDateString()
  date: Date;

  @IsBoolean({ each: true })
  items: boolean[];
}

export class UserContentCoordinationFieldDto {
  @IsNotEmpty()
  type: string;

  value: any;
}

export class UserContentCoordinationDto {
  @IsNotEmpty()
  title: string;

  @IsOptional()
  theme: string;

  @IsOptional()
  event: string;

  @IsOptional()
  place: string;

  @IsOptional()
  date: Date;

  @ValidateNested({ each: true })
  @Type(() => UserContentCoordinationFieldDto)
  fields: UserContentCoordinationFieldDto[];
}

export class CreateUserContentDto {
  @ValidateNested({ each: true })
  @Type(() => UserContentVariantDto)
  closet: UserContentVariantDto[];

  @ValidateNested({ each: true })
  @Type(() => UserContentVariantDto)
  wishlist: UserContentVariantDto[];

  @ValidateNested({ each: true })
  @Type(() => UserContentCoordinationDto)
  coordinations: UserContentCoordinationDto[];

  @ValidateNested({ each: true })
  @Type(() => UserContentLevelUpQuizDto)
  levelUpQuiz: UserContentLevelUpQuizDto[];

  @IsUUID()
  @IsNotEmpty()
  user: string;

  @IsNotEmpty()
  modified: number;
}
