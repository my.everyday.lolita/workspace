import { IsString, IsNotEmpty, IsIn } from 'class-validator';
import { ShareableDataKeys } from './user-contents.service';

export class ShareParams {
  @IsString()
  @IsNotEmpty()
  id: string;

  @IsIn(ShareableDataKeys)
  key: string;
}
