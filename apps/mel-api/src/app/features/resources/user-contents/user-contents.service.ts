/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserContent, UserContentDocument } from './user-contents.schema';
import { CreateUserContentDto } from './dto/create-user-content.dto';
import { UpdateUserContentDto } from './dto/update-user-content.dto';

export const ShareableDataKeys = ['closet', 'wishlist'] as const;
export type ShareableData = typeof ShareableDataKeys[number];

@Injectable()
export class UserContentsService {
  constructor(@InjectModel(UserContent.name) private userContentModel: Model<UserContentDocument>) {}

  async getOrCreate(user: string): Promise<UserContent> {
    const content = await this.userContentModel.findOne({ user });
    if (!content) {
      return this.create({
        user,
        closet: [],
        wishlist: [],
        coordinations: [],
        levelUpQuiz: [],
        modified: Date.now(),
      });
    }
    if (content.levelUpQuiz === undefined) {
      content.levelUpQuiz = [];
    }
    return content;
  }

  async getById(id: string): Promise<UserContent> {
    return this.userContentModel.findById(id).exec();
  }

  async create(createUserContentDto: CreateUserContentDto): Promise<UserContent> {
    const createdUserContent = new this.userContentModel(createUserContentDto);
    return createdUserContent.save();
  }

  async update(updateUserContentDto: UpdateUserContentDto): Promise<UserContent> {
    const item = await this.userContentModel.findById(updateUserContentDto._id);
    item.closet = updateUserContentDto.closet;
    item.wishlist = updateUserContentDto.wishlist;
    item.coordinations = updateUserContentDto.coordinations;
    item.levelUpQuiz = updateUserContentDto.levelUpQuiz;
    item.modified = updateUserContentDto.modified;
    return item.save();
  }

  async findAll(): Promise<UserContent[]> {
    return this.userContentModel.find().exec();
  }

  /**
   * Get the total size of the collection (storage size and index size) in byte.
   */
  async totalSize(): Promise<number> {
    const stats = await this.userContentModel.collection.stats();
    return stats.storageSize + stats.totalIndexSize;
  }

  async stats(): Promise<any> {
    return this.userContentModel
      .aggregate([
        {
          $facet: {
            count_users: [{ $count: 'total' }],
            count_coordi: [{ $unwind: '$coordinations' }, { $count: 'total' }],
            count_closet: [{ $unwind: '$closet' }, { $count: 'total' }],
            count_wishlist: [{ $unwind: '$wishlist' }, { $count: 'total' }],
          },
        },
      ])
      .exec();
  }
}
