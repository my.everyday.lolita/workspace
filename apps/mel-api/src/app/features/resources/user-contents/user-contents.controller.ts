/* eslint-disable @typescript-eslint/no-explicit-any */
import { Body, Controller, Get, Param, Patch, SetMetadata, UseGuards } from '@nestjs/common';
import { AuthGuard } from '../../keycloak/guards/auth.guard';
import { RolesGuard } from '../../keycloak/guards/roles.guard';
import { UserContent } from './user-contents.schema';
import { UserContentsService } from './user-contents.service';
import { User } from '../../keycloak/user.decorator';
import { KeycloakUser } from '../../keycloak/user.model';
import { UpdateUserContentDto } from './dto/update-user-content.dto';
import { ShareParams } from './share-params';

@Controller('api/resources/user-contents')
export class UserContentsController {
  constructor(private userContentsService: UserContentsService) {}

  @Get('me')
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['uma_authorization'])
  getOrCreate(@User() user: KeycloakUser): Promise<UserContent> {
    return this.userContentsService.getOrCreate(user.sub);
  }

  @Patch()
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['own', 'admin'])
  update(@Body() data: UpdateUserContentDto): Promise<UserContent> {
    return this.userContentsService.update(data);
  }

  @Get('share/:id/:key')
  async shareData(@Param() params: ShareParams): Promise<any> {
    const data = await this.userContentsService.getById(params.id);
    return data[params.key];
  }
}
