import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Brand, BrandSchema } from './brands/brand.schema';
import { BrandsController } from './brands/brands.controller';
import { BrandsService } from './brands/brands.service';
import { CategoriesController } from './categories/categories.controller';
import { CategoriesService } from './categories/categories.service';
import { Category, CategorySchema } from './categories/category.schema';
import { FeaturesController } from './features/features.controller';
import { FeaturesService } from './features/features.service';
import { ResourcesController } from './resources.controller';
import { Feature, FeatureSchema } from './features/feature.schema';
import { Color, ColorSchema } from './colors/color.schema';
import { ColorsController } from './colors/colors.controller';
import { ColorsService } from './colors/colors.service';
import { ItemsService } from './items/items.service';
import { Item, ItemSchema } from './items/item.schema';
import { ItemsController } from './items/items.controller';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { UserContent, UserContentSchema } from './user-contents/user-contents.schema';
import { UserContentsController } from './user-contents/user-contents.controller';
import { UserContentsService } from './user-contents/user-contents.service';
import { ConfigModule } from '@nestjs/config';
import { HttpModule } from '@nestjs/axios';
import { ItemsSuperAdminController } from './items/items-super-admin.controller';
import { TinifyModule } from '../tinify/tinify.module';
import { LogsModule } from '../logs/logs.module';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Category.name, schema: CategorySchema },
      { name: Brand.name, schema: BrandSchema },
      { name: Feature.name, schema: FeatureSchema },
      { name: Color.name, schema: ColorSchema },
      { name: Item.name, schema: ItemSchema },
      { name: UserContent.name, schema: UserContentSchema },
    ]),
    KeycloakModule,
    HttpModule,
    ConfigModule,
    TinifyModule,
    LogsModule,
    MulterModule.register({
      dest: process.env.ASSETS_PATH,
    }),
  ],
  controllers: [
    CategoriesController,
    BrandsController,
    FeaturesController,
    ColorsController,
    ItemsController,
    ItemsSuperAdminController,
    UserContentsController,
    ResourcesController,
  ],
  providers: [CategoriesService, BrandsService, FeaturesService, ColorsService, ItemsService, UserContentsService],
})
export class ResourcesModule {}
