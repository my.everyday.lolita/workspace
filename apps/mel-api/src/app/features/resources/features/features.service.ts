/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateFeatureDto } from './dto/create-feature.dto';
import { UpdateFeatureDto } from './dto/update-feature.dto';
import { Feature, FeatureDocument } from './feature.schema';

@Injectable()
export class FeaturesService {
  constructor(@InjectModel(Feature.name) private featureModel: Model<FeatureDocument>) {}

  async create(createFeatureDto: CreateFeatureDto): Promise<Feature> {
    const createdFeature = new this.featureModel(createFeatureDto);
    return createdFeature.save();
  }

  async update(updateFeatureDto: UpdateFeatureDto): Promise<Feature> {
    const feature = await this.featureModel.findById(updateFeatureDto._id);
    feature.name = updateFeatureDto.name;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    feature.categories = updateFeatureDto.categories as any;
    return feature.save();
  }

  async insertMany(items: CreateFeatureDto[]): Promise<Feature[]> {
    return this.featureModel.insertMany(items);
  }

  async findAll(): Promise<Feature[]> {
    return this.featureModel.find().exec();
  }

  async delete(id: string): Promise<any> {
    const feature = this.featureModel.findById(id);
    if (feature) {
      return feature.deleteOne().exec();
    }
    throw new HttpException('There is no item to delete for the given id', 410);
  }

  /**
   * Get the total size of the collection (storage size and index size) in byte.
   */
  async totalSize(): Promise<number> {
    const stats = await this.featureModel.collection.stats();
    return stats.storageSize + stats.totalIndexSize;
  }
}
