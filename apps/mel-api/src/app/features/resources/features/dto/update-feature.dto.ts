import { IsNotEmpty } from 'class-validator';
import { CreateFeatureDto } from './create-feature.dto';

export class UpdateFeatureDto extends CreateFeatureDto {
  @IsNotEmpty()
  _id: string;
}
