import { IsNotEmpty } from 'class-validator';
import { CreateItemDto } from './create-item.dto';

export class UpdateItemDto extends CreateItemDto {
  @IsNotEmpty()
  _id: string;
}
