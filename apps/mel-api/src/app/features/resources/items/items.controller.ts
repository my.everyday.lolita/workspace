/* eslint-disable @typescript-eslint/no-explicit-any */
import { ResourcesEndpoints } from '@api/shared';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Put,
  Query,
  SetMetadata,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { GetUserHelper, UserHelper } from '../../keycloak/decorators/user-helper.decorator';
import { AuthGuard } from '../../keycloak/guards/auth.guard';
import { RolesGuard } from '../../keycloak/guards/roles.guard';
import { UserHelperInterceptor } from '../../keycloak/interceptors/user-helper.interceptor';
import { CreateItemDto } from './dto/create-item.dto';
import { Criterium } from './dto/criterium.dto';
import { UpdateItemDto } from './dto/update-item.dto';
import { Item } from './item.schema';
import { ItemsService } from './items.service';

@Controller(ResourcesEndpoints.ITEMS_ROOT)
export class ItemsController {
  constructor(private itemsService: ItemsService) {}

  @Post('search/')
  @HttpCode(200)
  @UseInterceptors(UserHelperInterceptor)
  findByCriteria(
    @Body() criteria: Criterium[],
    @Query('skip', ParseIntPipe) skip: number,
    @Query('limit', ParseIntPipe) limit: number,
    @GetUserHelper() userHelper: UserHelper
  ): Promise<Item[]> {
    return this.itemsService
      .findByCriteria((Array.isArray(criteria) && criteria) || [], skip || 0, userHelper, limit || 20)
      .exec();
  }

  @Post('find/')
  @HttpCode(200)
  @UseInterceptors(UserHelperInterceptor)
  async findByCriteria2(
    @Body() criteria: Criterium[],
    @Query('skip', ParseIntPipe) skip: number,
    @Query('limit', ParseIntPipe) limit: number,
    @GetUserHelper() userHelper: UserHelper
  ): Promise<{ total: number; items: Item[] }> {
    const items: Item[] = await this.itemsService
      .findByCriteria((Array.isArray(criteria) && criteria) || [], skip || 0, userHelper, limit || 20)
      .exec();
    const total: number = await this.itemsService
      .findByCriteria((Array.isArray(criteria) && criteria) || [], null, userHelper, null, true)
      .countDocuments()
      .exec();
    return { total, items };
  }

  @Get('recently-added')
  @UseInterceptors(UserHelperInterceptor)
  recentlyAdded(@GetUserHelper() userHelper: UserHelper): Promise<Item[]> {
    return this.itemsService.recentlyAdded(userHelper);
  }

  @Get('get/:id')
  @UseInterceptors(UserHelperInterceptor)
  findOne(@Param('id') id: string, @GetUserHelper() userHelper: UserHelper): Promise<Item> {
    return this.itemsService.findById(id, userHelper);
  }

  @Delete(':id')
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['admin', 'own'])
  delete(@Param('id') id: string): Promise<any> {
    return this.itemsService.delete(id);
  }

  @Put()
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['create-item', 'admin'])
  create(@Body() data: CreateItemDto, @GetUserHelper() userHelper: UserHelper): Promise<Item> {
    return this.itemsService.create(data, userHelper);
  }

  @Patch()
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['admin', 'own'])
  update(@Body() data: UpdateItemDto, @GetUserHelper() userHelper: UserHelper): Promise<Item> {
    return this.itemsService.update(data, userHelper);
  }

  @Post('import')
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['admin'])
  insertMany(@Body() data: CreateItemDto[]): Promise<Item[]> {
    return this.itemsService.insertMany(data);
  }

  @Get('count-drafts')
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['dashboard-access', 'admin'])
  countDrafts(): Promise<any> {
    return this.itemsService.countDrafts();
  }

  @Post('upload-image/:key')
  @UseInterceptors(
    FileInterceptor('file', {
      fileFilter: (req, file, next) => {
        next(
          null,
          ['image/jpeg', 'image/png'].includes(file.mimetype) &&
            parseInt(req.headers['content-length'], 10) < 5 * 1024 * 1024
        );
      },
    })
  )
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['create-item', 'admin'])
  uploadImage(@Param('key') itemId: string, @UploadedFile() file: Express.Multer.File) {
    return {
      id: this.itemsService.uploadImage(itemId, file),
    };
  }

  @Post('upload-image-url/:key')
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['create-item', 'admin'])
  async uploadImageFromUrl(@Param('key') key: string, @Body() data: { url: string }) {
    return {
      id: await this.itemsService.uploadImageFromUrl(key, data.url),
    };
  }
}
