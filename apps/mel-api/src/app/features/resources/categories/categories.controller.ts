/* eslint-disable @typescript-eslint/no-explicit-any */
import { ResourcesEndpoints } from '@api/shared';
import { Body, Controller, Delete, Get, Param, Patch, Post, Put, SetMetadata, UseGuards } from '@nestjs/common';
import { AuthGuard } from '../../keycloak/guards/auth.guard';
import { RolesGuard } from '../../keycloak/guards/roles.guard';
import { CategoriesService } from './categories.service';
import { Category } from './category.schema';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';

@Controller(ResourcesEndpoints.CATEGORIES_ROOT)
export class CategoriesController {
  constructor(private categoriesService: CategoriesService) {}

  @Get()
  findAll(): Promise<Category[]> {
    return this.categoriesService.findAll();
  }

  @Put()
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['create-category', 'admin'])
  create(@Body() data: CreateCategoryDto): Promise<Category> {
    return this.categoriesService.create(data);
  }

  @Patch()
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['edit-category', 'admin'])
  update(@Body() data: UpdateCategoryDto): Promise<Category> {
    return this.categoriesService.update(data);
  }

  @Post('import')
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['admin'])
  insertMany(@Body() data: CreateCategoryDto[]): Promise<Category[]> {
    return this.categoriesService.insertMany(data);
  }

  @Delete(':id')
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['admin'])
  delete(@Param('id') id: string): Promise<any> {
    return this.categoriesService.delete(id);
  }
}
