import { IsNotEmpty, Matches } from 'class-validator';

export class CreateColorDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @Matches(/^#[a-z0-9]{3,6}/)
  hex: string;
}
