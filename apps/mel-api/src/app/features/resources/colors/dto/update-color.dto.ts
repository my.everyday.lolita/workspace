import { IsNotEmpty } from 'class-validator';
import { CreateColorDto } from './create-color.dto';

export class UpdateColorDto extends CreateColorDto {
  @IsNotEmpty()
  _id: string;
}
