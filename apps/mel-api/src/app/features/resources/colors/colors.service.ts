/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ItemsService } from '../items/items.service';
import { Color, ColorDocument } from './color.schema';
import { CreateColorDto } from './dto/create-color.dto';
import { UpdateColorDto } from './dto/update-color.dto';

@Injectable()
export class ColorsService {
  constructor(@InjectModel(Color.name) private colorModel: Model<ColorDocument>, private itemsService: ItemsService) {}

  async create(createColorDto: CreateColorDto): Promise<Color> {
    const createdColor = new this.colorModel(createColorDto);
    return createdColor.save();
  }

  async update(updateColorDto: UpdateColorDto): Promise<Color> {
    const color = await this.colorModel.findById(updateColorDto._id);
    color.name = updateColorDto.name;
    color.hex = updateColorDto.hex;
    this.itemsService.bulkWrite([
      {
        updateMany: {
          filter: { 'variants.colors._id': { $eq: updateColorDto._id } },
          update: {
            $set: {
              'variants.$[].colors.$[color].name': color.name,
              'variants.$[].colors.$[color].hex': color.hex,
            },
          },
          arrayFilters: [{ 'color._id': { $eq: updateColorDto._id } }],
        },
      },
    ]);
    return color.save();
  }

  async insertMany(items: CreateColorDto[]): Promise<Color[]> {
    return this.colorModel.insertMany(items);
  }

  async findAll(): Promise<Color[]> {
    return this.colorModel.find().exec();
  }

  async delete(id: string): Promise<any> {
    const color = this.colorModel.findById(id);
    if (color) {
      return color.deleteOne().exec();
    }
    throw new HttpException('There is no item to delete for the given id', 410);
  }

  /**
   * Get the total size of the collection (storage size and index size) in byte.
   */
  async totalSize(): Promise<number> {
    const stats = await this.colorModel.collection.stats();
    return stats.storageSize + stats.totalIndexSize;
  }
}
