import { IsNotEmpty } from 'class-validator';
import { CreateBrandDto } from './create-brand.dto';

export class UpdateBrandDto extends CreateBrandDto {
  @IsNotEmpty()
  _id: string;
}
