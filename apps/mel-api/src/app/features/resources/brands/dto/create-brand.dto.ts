import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateBrandDto {
  @IsNotEmpty()
  name: string;

  @IsOptional()
  @IsString()
  shortname?: string;

  @IsOptional()
  @IsString()
  shop?: string;
}
