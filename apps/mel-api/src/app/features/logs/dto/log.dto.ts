import { IsEnum, IsNotEmpty, IsOptional } from 'class-validator';
import { LogType } from '../logs.schema';

export class FrontLog {
  @IsNotEmpty()
  message: string;

  @IsNotEmpty()
  @IsEnum(LogType)
  type: LogType;

  @IsOptional()
  trace?: string;

  @IsOptional()
  context?: string;
}
