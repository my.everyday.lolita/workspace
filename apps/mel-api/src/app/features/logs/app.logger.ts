/* eslint-disable @typescript-eslint/no-explicit-any */
import { Logger, Injectable } from '@nestjs/common';
import { LogOrigin } from './logs.schema';
import { LogsService } from './logs.service';

@Injectable()
export class AppLogger extends Logger {
  constructor(private logsService: LogsService) {
    super();
  }

  error(message: any, trace?: string, context?: string, origin = LogOrigin.BACK): void {
    super.error(message, trace, context);
    this.logsService.error(message, trace, origin, context);
  }

  warn(message: any, context?: string, origin = LogOrigin.BACK): void {
    super.error(message, context);
    this.logsService.warn(message, origin, context);
  }
}
