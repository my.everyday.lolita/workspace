import { Body, Controller, Get, Post, Query, SetMetadata, UseGuards } from '@nestjs/common';
import { AuthGuard } from '../keycloak/guards/auth.guard';
import { RolesGuard } from '../keycloak/guards/roles.guard';
import { AppLogger } from './app.logger';
import { FrontLog } from './dto/log.dto';
import { LogOrigin, LogType } from './logs.schema';
import { LogsService } from './logs.service';
import { LogsEndpoints } from '@api/shared';

@Controller(LogsEndpoints.ROOT)
export class LogsController {
  constructor(private logger: AppLogger, private logsService: LogsService) {}

  @Post()
  log(@Body() data: FrontLog): void {
    switch (data.type) {
      case LogType.ERROR:
        this.logger.error(data.message, data.trace, data.context, LogOrigin.FRONT);
        break;

      case LogType.WARNING:
        this.logger.warn(data.message, data.context, LogOrigin.FRONT);
        break;

      default:
        break;
    }
  }

  @Get()
  @UseGuards(AuthGuard, RolesGuard)
  @SetMetadata('roles', ['super-admin'])
  async getLogs(@Query('skip') skip: string, @Query('limit') limit: string) {
    const items = await this.logsService.getLogs(parseInt(skip, 10), parseInt(limit, 10)).exec();
    const total = await this.logsService.getLogs(undefined, undefined).countDocuments().exec();
    return { total, items };
  }
}
