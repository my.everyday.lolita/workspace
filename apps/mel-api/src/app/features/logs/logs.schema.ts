/* eslint-disable @typescript-eslint/no-explicit-any */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { Document } from 'mongoose';

export enum LogOrigin {
  FRONT = 'front',
  BACK = 'back',
}

export enum LogType {
  ERROR = 'error',
  WARNING = 'warning',
}

@Schema({
  capped: {
    size: 1024 * 100,
  },
})
export class Log {
  @Prop()
  @IsNotEmpty()
  origin: LogOrigin;

  @Prop()
  @IsNotEmpty()
  type: LogType;

  @Prop({ type: {} })
  @IsNotEmpty()
  message: any;

  @Prop({ default: () => new Date() })
  date: Date;

  @Prop()
  @IsOptional()
  trace: string;

  @Prop()
  @IsOptional()
  context: string;
}

export const LogSchema = SchemaFactory.createForClass(Log);

export type LogDocument = Log & Document;
