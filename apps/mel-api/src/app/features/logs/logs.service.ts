/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Log, LogDocument, LogOrigin, LogType } from './logs.schema';

@Injectable()
export class LogsService {
  constructor(@InjectModel(Log.name) private model: Model<LogDocument>) {}

  error(message: any, trace?: string, origin = LogOrigin.BACK, context?: string): void {
    const document = new this.model({ message, trace, origin, type: LogType.ERROR, context });
    document.save();
  }

  warn(message: any, origin = LogOrigin.BACK, context?: string): void {
    const document = new this.model({ message, undefined, origin, type: LogType.WARNING, context });
    document.save();
  }

  getLogs(skip: number, limit: number) {
    let query = this.model.find();
    if (skip) {
      query = query.skip(skip);
    }
    if (limit) {
      query = query.limit(limit);
    }
    return query.sort({ date: -1 });
  }
}
