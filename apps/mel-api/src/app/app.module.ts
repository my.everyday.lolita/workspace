import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ServeStaticModule } from '@nestjs/serve-static';
import { RateLimiterModule } from 'nestjs-rate-limiter';
import { ResourcesModule } from './features/resources/resources.module';
import { LogsModule } from './features/logs/logs.module';
import { SystemModule } from './features/system/system.module';

@Module({
  imports: [
    RateLimiterModule,
    HttpModule,
    ConfigModule.forRoot({
      ignoreEnvFile: false,
      cache: true,
    }),
    MongooseModule.forRoot(`mongodb://${process.env.MONGO_URL}/${process.env.MONGO_DB}`, {
      auth: {
        username: process.env.MONGO_ROOT,
        password: process.env.MONGO_ROOT_PASSWORD,
      },
      authSource: process.env.MONGO_AUTH_SOURCE,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: process.env.ASSETS_PATH,
      serveRoot: '/files',
      exclude: ['/api*'],
    }),
    ResourcesModule,
    LogsModule,
    SystemModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
