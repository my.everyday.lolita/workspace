/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable } from '@angular/core';
import { from, Observable, of, ReplaySubject } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CacheService {
  private cache$ = new ReplaySubject<Cache>(1);
  private inMemoryFallback: { [key: string]: Response } = {};

  constructor() {
    this.open().subscribe((cache) => {
      this.cache$.next(cache);
    });
  }

  open(): Observable<Cache> {
    return from(caches.open('mel-cache')).pipe(catchError((_) => of(this.initFallback())));
  }

  match(id: RequestInfo): Observable<Response | undefined> {
    return this.cache$.pipe(switchMap((cache) => from(cache.match(`/${id}`))));
  }

  put(id: string, body: never): Observable<boolean> {
    return this.cache$.pipe(
      switchMap((cache) =>
        from(cache.put(`/${id}`, new Response(JSON.stringify(body)))).pipe(
          catchError((error) => {
            console.error(error);
            return of(false);
          }),
          map(() => true)
        )
      )
    );
  }

  delete(id: string): Observable<boolean> {
    return this.cache$.pipe(
      switchMap((cache) =>
        from(cache.delete(`/${id}`)).pipe(
          catchError((error) => {
            console.error(error);
            return of(false);
          }),
          map(() => true)
        )
      )
    );
  }

  private initFallback(): Cache {
    console.error('Cache unavailable (☍﹏⁰), use the session storage as fallback.');
    return {
      put: (id: RequestInfo, response: Response): Promise<void> => {
        return new Promise((resolve) => {
          response.json().then((value) => {
            sessionStorage.setItem(id as string, JSON.stringify(value));
            resolve();
          });
        });
      },
      delete: (id: RequestInfo): Promise<boolean> => {
        return new Promise((resolve) => {
          sessionStorage.removeItem(id as string);
          resolve(true);
        });
      },
      add(id: RequestInfo): Promise<void> {
        return Promise.reject('sessionStorage fallback add() method is not available.');
      },
      addAll(ids: RequestInfo[]): Promise<void> {
        return Promise.reject('sessionStorage fallback addAll() method is not available.');
      },
      match: (id: RequestInfo): Promise<Response | undefined> => {
        return new Promise((resolve) => {
          const value = sessionStorage.getItem(id as string);
          if (value) {
            resolve(new Response(value));
          }
          resolve(undefined);
        });
      },
      matchAll: (id?: RequestInfo | undefined): Promise<never> => {
        return Promise.reject('sessionStorage fallback matchAll() method is not available.');
      },
      keys: (request?: RequestInfo | undefined): Promise<readonly Request[]> => {
        return new Promise((resolve) => {
          resolve(Object.keys(this.inMemoryFallback).map((id) => new Request(id)));
        });
      },
    };
  }
}
