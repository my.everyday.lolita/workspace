import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as UserActions from './user.actions';
import * as UserSelectors from './user.selectors';

@Injectable({ providedIn: 'root' })
export class UserFacade {
  user$ = this.store.select(UserSelectors.getUser);
  isLoggedIn$ = this.store.select(UserSelectors.isLoggedIn);
  auth$ = this.store.select(UserSelectors.getAuth);

  constructor(private readonly store: Store) {}

  login(username: string, password: string) {
    this.store.dispatch(UserActions.login({ username, password }));
  }

  logout() {
    this.store.dispatch(UserActions.logout());
  }

  refreshToken() {
    this.store.dispatch(UserActions.refreshToken());
  }
}
