import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, createFeatureSelector, createSelector, MemoizedSelector, State, StoreModule } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { NxModule } from '@nrwl/angular';
import { Observable } from 'rxjs';
import { User } from '../model/user.model';
import { login } from './user.actions';
import { UserEffects } from './user.effects';
import { initialState, reducer, USER_FEATURE_KEY } from './user.reducer';

describe('User Reducer', () => {

  describe('valid User actions', () => {
    // it('getUser should return the user object with valid initialState', () => {
    //   const action = getUser();

    //   const result = reducer({
    //     active: true,
    //     email: 'lorem@ipsum.fr',
    //     preferred_username: 'lorem',
    //     sub: 'HASH2564ALL',
    //     realm_access: { roles: ['ipsum'] },
    //   }, action);

    //   expect(result.active).toBe(true);
    //   expect(result.email).toBe('lorem@ipsum.fr');
    //   expect(result.preferred_username).toBe('lorem');
    // });

    it('login should do nothing', () => {
      const action = login({ username: 'lorem', password: 'Azerty123#' });

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as Action;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('login flow', () => {
    let actions: Observable<Action>;
    let userState: any;
    let loginUser: MemoizedSelector<State<User>, User>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [
          NxModule.forRoot(),
        ],
        providers: [
          UserEffects,
          provideMockActions(() => actions),
          provideMockStore({initialState}),
        ],
      });

      TestBed.inject(UserEffects);
    });

    it('Should work', () => {
      userState = createFeatureSelector<State<User>>(USER_FEATURE_KEY)(initialState);
      loginUser = createSelector(userState, (state: User) => state);

      loginUser(userState);
    });
  });
});
