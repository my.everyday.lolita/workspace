import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UserState } from '../model/user.model';
import { USER_FEATURE_KEY } from './user.reducer';

// Lookup the 'User' feature state managed by NgRx
export const getUserState = createFeatureSelector<UserState>(USER_FEATURE_KEY);

export const getUser = createSelector(getUserState, (state: UserState) => state.user);

export const getAuth = createSelector(getUserState, (state: UserState) => state.auth);

export const isLoggedIn = createSelector(getUserState, (state: UserState) => state.auth?.access_token != undefined);
