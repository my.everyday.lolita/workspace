import { createAction, props } from '@ngrx/store';
import { Auth, User, UserSignUpDto } from '../model/user.model';

export const login = createAction('[User] Login', props<{ username: string; password: string }>());
export const loginSuccess = createAction('[User] Login Success', props<{ auth: Auth }>());
export const loginFailure = createAction('[User] Login Failure', props<{ error: string | Error }>());
export const loadUser = createAction('[User] Load User', props<{ user: User }>());

export const logout = createAction('[User] Logout');

export const signUp = createAction('[User] Sign Up', props<{ signUp: UserSignUpDto }>());
export const signUpSuccess = createAction('[User] Sign Up Success');
export const signUpFailure = createAction('[User] Sign Up Failure', props<{ error: string | Error }>());

export const refreshToken = createAction('[User] Refresh Token');
export const refreshTokenSuccess = createAction('[User] Refresh Token Success', props<{ auth: Auth }>());
export const refreshTokenFailure = createAction('[User] Refresh Token Failure', props<{ error: string | Error }>());
