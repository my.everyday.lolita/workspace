import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { NxModule } from '@nrwl/angular';
import { Observable } from 'rxjs';
import { login, loginSuccess } from './user.actions';

import { UserEffects } from './user.effects';
import { hot } from 'jasmine-marbles';

describe('UserEffects', () => {
  let actions: Observable<Action>;
  let effects: UserEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot()],
      providers: [UserEffects, provideMockActions(() => actions), provideMockStore()],
    });

    effects = TestBed.inject(UserEffects);
  });

  it('Login effect should be called on login action', () => {
    actions = hot('-a-|', { a: login({ username: 'lorem', password: 'ipsum' }) });

    const expected = hot('-a-|', {
      a: loginSuccess(),
    });

    expect(effects.login$).toBeObservable(expected);
  });
});
