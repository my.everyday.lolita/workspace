import { Action, createReducer, on } from '@ngrx/store';
import * as AuthFunctions from '../functions/auth.functions';
import * as UserFunctions from '../functions/user.functions';
import { User, UserState } from '../model/user.model';
import { loadUser, loginSuccess, logout, refreshTokenSuccess } from './user.actions';

export const USER_FEATURE_KEY = 'user_module';

const initialAuth = AuthFunctions.getFromLocalStorage();
let initialUser: User | undefined = undefined;
if (initialAuth && initialAuth.access_token) {
  initialUser = UserFunctions.getFromAccessToken(initialAuth.access_token);
}

export const initialState: UserState = {
  user: initialUser,
  auth: initialAuth,
};

const userReducer = createReducer(
  initialState,
  on(loginSuccess, (state, action) => {
    return { ...state, auth: action.auth };
  }),
  on(loadUser, (state, action) => {
    return { ...state, user: action.user };
  }),
  on(refreshTokenSuccess, (state, action) => {
    return { ...state, auth: action.auth };
  }),
  on(logout, (state) => {
    return { ...state, auth: undefined, user: undefined };
  })
);

export function reducer(state: UserState, action: Action) {
  return userReducer(state, action);
}
