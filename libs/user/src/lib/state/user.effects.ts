import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import * as AuthFunctions from '../functions/auth.functions';
import * as UserFunctions from '../functions/user.functions';
import { UserState } from '../model/user.model';
import { AuthService } from '../services/auth.service';
import * as UserActions from './user.actions';
import * as UserSelectors from './user.selectors';

@Injectable({ providedIn: 'root' })
export class UserEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.login),
      switchMap((action) =>
        this.authService.login(action.username, action.password).pipe(
          map((response) =>
            UserActions.loginSuccess({
              auth: {
                access_token: response.access_token,
                refresh_token: response.refresh_token,
                expires_in: response.expires_in,
                refresh_expires_in: response.refresh_expires_in,
              },
            })
          ),
          catchError((error) => of(UserActions.loginFailure(error)))
        )
      )
    )
  );

  loginSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.loginSuccess),
      map((action) => {
        return UserActions.loadUser({ user: UserFunctions.getFromAccessToken(action.auth.access_token) });
      })
    )
  );

  refreshToken$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.refreshToken),
      withLatestFrom(this.store.select(UserSelectors.getAuth)),
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      switchMap(([_, auth]) => {
        if (!auth || !auth.refresh_token) {
          return of(UserActions.refreshTokenFailure({ error: new Error('No refresh token') }));
        }
        return this.authService.refreshToken(auth.refresh_token).pipe(
          map((response) =>
            UserActions.refreshTokenSuccess({
              auth: {
                access_token: response.access_token,
                refresh_token: response.refresh_token,
                expires_in: response.expires_in,
                refresh_expires_in: response.refresh_expires_in,
              },
            })
          ),
          catchError((error) => of(UserActions.refreshTokenFailure(error)))
        );
      })
    )
  );

  saveToLocalStorage$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserActions.loginSuccess, UserActions.refreshTokenSuccess),
        switchMap(() => this.store.select(UserSelectors.getAuth)),
        tap((auth) => {
          if (auth) {
            AuthFunctions.saveToLocalStorage(auth);
          }
        })
      ),
    { dispatch: false }
  );

  signUp$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.signUp),
      switchMap((action) =>
        this.authService.signUp(action.signUp).pipe(
          map(() => UserActions.signUpSuccess()),
          catchError((error) => of(UserActions.signUpFailure({ error })))
        )
      )
    )
  );

  logout$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserActions.logout),
        tap(() => {
          AuthFunctions.removeFromLocalStorage();
        })
      ),
    { dispatch: false }
  );

  constructor(
    private store: Store<UserState>,
    private readonly actions$: Actions,
    private readonly authService: AuthService
  ) {}
}
