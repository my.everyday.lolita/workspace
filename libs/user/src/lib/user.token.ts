import { InjectionToken } from '@angular/core';

export const AUTH_PATH = new InjectionToken<string>('USER_AUTH_PATH');

export const AUTH_REALM = new InjectionToken<string>('USER_AUTH_REALM');

export const AUTH_GRANT_TYPE = new InjectionToken<string>('USER_AUTH_GRANT_TYPE');

export const AUTH_CLIENT_ID = new InjectionToken<string>('USER_AUTH_CLIENT_ID');

export const AUTH_SCOPE = new InjectionToken<string>('USER_AUTH_SCOPE');

export const SIGN_UP_PATH = new InjectionToken<string>('USER_SIGN_UP_PATH');
