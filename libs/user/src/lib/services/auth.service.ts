import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserSignInResponse, UserSignUpDto, UserSignUpResponse } from '../model/user.model';
import { AUTH_CLIENT_ID, AUTH_GRANT_TYPE, AUTH_PATH, AUTH_REALM, AUTH_SCOPE, SIGN_UP_PATH } from '../user.token';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private url = `${this.path}/auth/realms/${this.realm}/protocol/openid-connect/token`;

  constructor(
    private http: HttpClient,
    @Inject(SIGN_UP_PATH) private signUppath: string,
    @Inject(AUTH_PATH) private path: string,
    @Inject(AUTH_REALM) private realm: string,
    @Inject(AUTH_GRANT_TYPE) private grantType: string,
    @Inject(AUTH_CLIENT_ID) private clientId: string,
    @Inject(AUTH_SCOPE) private scope: string
  ) {}

  login(username: string, password: string) {
    const body = new URLSearchParams();
    body.set('username', username);
    body.set('password', password);
    body.set('grant_type', this.grantType);
    body.set('client_id', this.clientId);
    body.set('scope', this.scope);
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };
    return this.http.post<UserSignInResponse>(this.url, body, options);
  }

  refreshToken(token: string): Observable<UserSignInResponse> {
    const body = new URLSearchParams();
    body.set('grant_type', 'refresh_token');
    body.set('client_id', this.clientId);
    body.set('refresh_token', token);
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };
    return this.http.post<UserSignInResponse>(this.url, body, options);
  }

  signUp(data: UserSignUpDto): Observable<UserSignUpResponse> {
    return this.http.post<UserSignUpResponse>(`${this.signUppath}/register`, data);
  }
}
