import { Auth } from '../model/user.model';
import { AUTH_STORAGE_KEY } from '../user.constant';

export function saveToLocalStorage(auth: Auth) {
  localStorage.setItem(AUTH_STORAGE_KEY, JSON.stringify(auth));
}

export function removeFromLocalStorage() {
  localStorage.removeItem(AUTH_STORAGE_KEY);
}

export function getFromLocalStorage(): Auth {
  const auth = localStorage.getItem(AUTH_STORAGE_KEY);
  return auth ? JSON.parse(auth) : undefined;
}
