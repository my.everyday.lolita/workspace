import { User } from '../model/user.model';
import jwt_decode from 'jwt-decode';

export function getFromAccessToken(accessToken: string): User {
  return jwt_decode(accessToken);
}
