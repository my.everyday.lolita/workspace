export interface UserState {
  user?: User;
  auth?: Auth;
}

export interface User {
  preferred_username: string;
  email: string;
  active: boolean;
  realm_access: {
    roles: string[];
  };
  sub: string;
}

export interface UserSignUpDto {
  username: string;
  email: string;
  password: string;
  confirmPassword: string;
}

export interface UserSignInResponse {
  access_token: string;
  expires_in: number;
  refresh_expires_in: number;
  refresh_token: string;
  token_type: string;
  id_token: string;
  'not-before-policy': number;
  session_state: string;
  scope: string;
}

export interface Auth {
  access_token: string;
  expires_in: number;
  refresh_expires_in: number;
  refresh_token: string;
}

export interface UserSignInInfos extends UserSignInResponse {
  datetime: number;
}

export interface UserSignUpResponse {
  response: string;
}
