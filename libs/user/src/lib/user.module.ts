import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducer, USER_FEATURE_KEY } from './state/user.reducer';
import { UserEffects } from './state/user.effects';
import { AUTH_CLIENT_ID, AUTH_GRANT_TYPE, AUTH_REALM, AUTH_SCOPE } from './user.token';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forFeature(USER_FEATURE_KEY, reducer),
    EffectsModule.forFeature([UserEffects]),
  ],
  providers: [
    { provide: AUTH_REALM, useValue: 'my-everyday-lolita-realm' },
    { provide: AUTH_CLIENT_ID, useValue: 'web-lolita' },
    { provide: AUTH_GRANT_TYPE, useValue: 'password' },
    { provide: AUTH_SCOPE, useValue: 'openid' },
  ],
})
export class UserModule {}
