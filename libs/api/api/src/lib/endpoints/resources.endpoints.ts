import { GlobalPrefix } from './prefix';

export const ROOT = GlobalPrefix.V1 + 'resources';

export const STATS = 'stats';
export const DUMP = 'dump';
export const TOTAL_SIZE = 'total-size';

export const BRANDS_ROOT = ROOT + '/brands';
export const CATEGORIES_ROOT = ROOT + '/categories';
export const COLORS_ROOT = ROOT + '/colors';
export const FEATURES_ROOT = ROOT + '/features';
export const ITEMS_ROOT = ROOT + '/items';
export const USER_CONTENT_ROOT = ROOT + '/user-content';
