export const sortOptions = ['price_asc', 'price_desc', 'year_asc', 'year_desc', 'alpha_asc', 'alpha_desc'];
export type SortOptions = typeof sortOptions[number];
