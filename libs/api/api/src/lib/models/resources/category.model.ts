import { Resource } from './resource.model';

export interface Category extends Resource {
  name: string;
  shortname?: string;
  children?: Category[];
}

export interface ExtendedCategory extends Category {
  disabled?: boolean;
  _lvlClass?: string;
  children?: ExtendedCategory[];
  parent?: ExtendedCategory;
}

export interface ItemCategory {
  name: string;
  shortname?: string;
  parent?: ItemCategory;
  _id?: string;
}
