/* eslint-disable @typescript-eslint/no-explicit-any */
import { Brand } from './brand.model';
import { ExtendedCategory } from './category.model';
import { Color } from './color.model';
import { Feature } from './feature.model';
import { Resource, StatsCollection } from './resource.model';

export enum ItemStatus {
  DRAFT = 'draft',
  VALIDATED = 'validated',
}

export interface ItemVariant {
  colors: Color[];
  photos: string[];
}

export interface Item extends Resource {
  brand: Brand;
  collectionn?: string;
  category: ExtendedCategory;
  features: Feature[];
  variants: ItemVariant[];
  year?: number;
  japanese?: string;
  measurments?: string;
  estimatedPrice?: number;
  keywords?: string[];
  substyles?: string[];
  owner: string;
  created: Date;
  modified: Date;
  status?: ItemStatus;
  draft?: any;
  incomplete?: boolean;

  /**
   * Specific closet/wishlist
   */
  wantToSell?: boolean;
  dreamDress?: boolean;
  _variantId?: string;
  _wrongVariantId?: boolean;
}

export type CriteriumType =
  | 'brand'
  | 'color'
  | 'feature'
  | 'category'
  | 'keyword'
  | 'own'
  | 'id'
  | 'drafts'
  | 'mel-error'
  | 'incomplete'
  | 'user'
  | string;

export interface Criterium {
  type: CriteriumType;
  value: string;
  displayValue?: string;
  displayType?: string;
  parents?: string[];
  _lvlClass?: string;
}

export interface ItemStats {
  count: number;
  _id: {
    name: string;
    _id: string;
    [key: string]: any;
  };
}

export interface ItemStatsCollection extends StatsCollection {
  brands: ItemStats[];
  categories: ItemStats[];
  colors: ItemStats[];
  count_items: { total: number }[];
  count_variants: { total: number }[];
}

export const substyles = [
  'Sweet lolita',
  'Gothic lolita',
  'Classic lolita',
  'Punk lolita',
  'Sailor lolita',
  'Bride lolita',
  'Steam lolita',
  'Military lolita',
  'Pirate lolita',
  'Ero lolita',
  'Country lolita',
  'Vintage / retro lolita',
  'Hime lolita',
  'Wa lolita (Japanese)',
  'Qi lolita (Chinese)',
  'Han lolita (Korean)',
  'Monochrome (Shiro / Kuro / Ao / Pinku)',
  'Guro lolita (gore)',
  'Circus lolita',
  'Cyber lolita',
  'Halloween',
  'Christmas',
  'Easter',
  'Valentine’s Day',
  'Ōji / Ouji',
  'Old school',
  'Alice lolita',
];
