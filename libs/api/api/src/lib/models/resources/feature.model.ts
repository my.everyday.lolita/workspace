import { Resource } from './resource.model';

export interface Feature extends Resource {
  name: string;
  categories: { name: string }[];
}
