/* eslint-disable @typescript-eslint/no-explicit-any */
export interface Resource {
  _id?: string;
}

export interface StatsCollection {
  [key: string]: any;
}

export enum RequestAuthOptions {
  AUTO = 'auto',
  IF_SIGNED = 'if_signed',
}

export type RequestAuth = RequestAuthOptions.AUTO | RequestAuthOptions.IF_SIGNED | boolean;
