import { Resource } from './resource.model';

export interface Color extends Resource {
  name: string;
  hex: string;
}
