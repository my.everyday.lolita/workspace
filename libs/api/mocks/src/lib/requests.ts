/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpRequest } from '@angular/common/http';
import { availableBrands } from './data/brands';
import { availableCatories } from './data/categories';
import { availableColors } from './data/colors';
import { availableFeatures } from './data/features';
import { availableItems, recentlyAddedItems } from './data/items';

export function extractIdFromRequest(req: HttpRequest<unknown>, mock: any): string {
  const parts = req.url.split(mock.url);
  return parts[parts.length - 1];
}

export const requests = [
  {
    method: 'GET',
    response: availableColors,
    status: 200,
    url: '/api/resources/colors',
  },
  {
    method: 'GET',
    status: 200,
    response: availableBrands,
    url: '/api/resources/brands',
  },
  {
    method: 'GET',
    response: availableFeatures,
    status: 200,
    url: '/api/resources/features',
  },
  {
    method: 'GET',
    status: 200,
    response: availableCatories,
    url: '/api/resources/categories',
  },
  {
    method: 'POST',
    response: '',
    status: 200,
    url: '/api/logs',
  },
  {
    method: 'PATCH',
    response: '',
    status: 200,
    url: '/api/resources/user-content',
  },
  {
    method: 'POST',
    status: 200,
    response: availableItems,
    url: '/api/resources/items/search',
  },
  {
    method: 'GET',
    status: 200,
    response: (req: HttpRequest<unknown>, mock: any) => {
      const id = extractIdFromRequest(req, mock);
      return availableItems.find((item) => {
        return item._id === id;
      });
    },
    url: '/api/resources/items/get/',
  },
  {
    method: 'GET',
    status: 200,
    response: recentlyAddedItems,
    url: '/api/resources/items/recently-added',
  },
];
