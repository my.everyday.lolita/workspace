import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { requests as data } from './requests';

@Injectable({ providedIn: 'root' })
export class ApiMockInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const mock = data.find((mock) => req.url.includes(mock.url) && mock.method === req.method);
    if (mock) {
      console.log(`Mock API: ${mock.url}`);
      let body;
      if (typeof mock.response === 'function') {
        body = mock.response(req, mock);
      } else {
        body = mock.response ?? [];
      }
      return of(new HttpResponse({ status: mock.status ?? 200, body }));
    }
    return next.handle(req);
  }
}
