export function getOrCreate<T>(id: string, createFn: () => T): T {
  let data: T;
  const fromStorage = localStorage.getItem(id);
  if (!fromStorage) {
    data = createFn();
    localStorage.setItem(id, JSON.stringify(data));
  } else {
    data = JSON.parse(fromStorage);
  }
  return data;
}
