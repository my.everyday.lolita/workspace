export function getFloat(min: number, max: number, decimal = 2): number {
  return parseFloat((min + Math.random() * (max + 1 - min)).toFixed(decimal));
}
