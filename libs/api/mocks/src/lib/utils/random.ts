import { getInt } from './int';

export function getRandom(list: unknown[]): unknown {
  const position = getInt(0, list?.length - 1);
  return list[position];
}
