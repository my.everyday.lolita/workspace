import { getInt } from './int';
import { getRandom } from './random';

export const getRandomAsList = (list: unknown[], min = 0, max = 10): unknown[] => {
  return Array.from(Array(getInt(min, max)), () => getRandom(list));
};
