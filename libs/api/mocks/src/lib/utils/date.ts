import { getInt } from './int';

export const getDateObject = (): Date => {
  const m = getInt(0, 11);
  const d = getInt(0, m === 2 ? 28 : 30);
  const y = getInt(2021, 2023);
  return new Date(y, m, d, 8, 0);
};

export const getDate = (from?: Date | string, hours = 12, minutes = 0): string => {
  if (from) {
    const fromDate = new Date(from);
    fromDate.setHours(hours);
    fromDate.setMinutes(minutes);
    return fromDate.toISOString();
  }
  return getDateObject().toISOString();
};
