import { CriteriaHelpers } from '@front/criteria';
import { getId } from '../utils';
import { getOrCreate } from '../utils/storage';

export const availableCatories = getOrCreate('dev:categories', () => [
  {
    children: [
      { name: 'Jumperskirts', shortname: 'JSK' },
      { name: 'One pieces', shortname: 'OP' },
    ],
    _id: getId(),
    name: 'Dresses',
  },
  {
    children: [{ name: 'Skirts' }, { name: 'Salopettes' }, { name: 'Pants' }],
    _id: getId(),
    name: 'Bottomwear',
  },
  {
    children: [
      { name: 'Blouses', shortname: '', children: [] },
      { name: 'Cutsews', shortname: '', children: [] },
      { name: 'Corset', shortname: '', children: [] },
    ],
    _id: getId(),
    name: 'Topwear',
    shortname: '',
  },
  {
    children: [
      { name: 'Cardigans', shortname: '', children: [] },
      { name: 'Boleros', shortname: '', children: [] },
      { name: 'Jackets', shortname: '', children: [] },
      { name: 'Coats', shortname: '', children: [] },
      { name: 'Capes', shortname: '', children: [] },
      { name: 'Vests and haoris', shortname: '', children: [] },
    ],
    _id: getId(),
    name: 'Outerwear',
    shortname: '',
  },
  {
    children: [
      {
        name: 'Socks',
        shortname: '',
        children: [
          { name: 'Over and under the knee socks', shortname: 'OTK' },
          { name: 'Ankle socks', shortname: '' },
        ],
      },
      { name: 'Tights', shortname: '', children: [] },
      { name: 'Legwarmers / Gaiters', shortname: '', children: [] },
    ],
    _id: getId(),
    name: 'Legwear',
    shortname: '',
  },
  {
    children: [
      { name: 'Headbows', shortname: '', children: [] },
      { name: 'Headbands', shortname: '', children: [] },
      { name: 'Bonnets / halfbonnets', shortname: '', children: [] },
      { name: 'Hats / Mini hats / Berets', shortname: '', children: [] },
    ],
    _id: getId(),
    name: 'Headdress',
    shortname: '',
  },
  {
    children: [{ name: 'Flat shoes' }, { name: 'Heels and platform shoes' }, { name: 'High shoes' }],
    _id: getId(),
    name: 'Shoes',
  },
  {
    children: [
      { name: 'Cross body bags', shortname: '', children: [] },
      { name: 'Handbags', shortname: '', children: [] },
      { name: 'Backpacks', shortname: '', children: [] },
      { name: 'Tote bags', shortname: '', children: [] },
    ],
    _id: getId(),
    name: 'Bags',
    shortname: '',
  },
  {
    children: [
      { name: 'Wristcuffs', shortname: '', children: [] },
      {
        name: 'Jewels',
        shortname: '',
        children: [
          { name: 'Necklaces', shortname: '' },
          { name: 'Chokers', shortname: '' },
          { name: 'Bracelets', shortname: '' },
          { name: 'Rings', shortname: '' },
          { name: 'Pins, brooches and others', shortname: '' },
        ],
      },
      { name: 'Clips, hairbands, veils and others', shortname: '', children: [] },
      { name: 'Earrings', shortname: '', children: [] },
      { name: 'Gloves mittens and others', shortname: '', children: [] },
      { name: 'Obis / Sashs / Belts', shortname: '', children: [] },
      { name: 'Scarfs and muffler', shortname: '', children: [] },
    ],
    _id: getId(),
    name: 'Accessories',
    shortname: '',
  },
  {
    children: [
      { name: 'Bloomers', shortname: '', children: [] },
      { name: 'Petticoats', shortname: '', children: [] },
      { name: 'Crinoline', shortname: '', children: [] },
      { name: 'Underskirt', shortname: '', children: [] },
    ],
    _id: getId(),
    name: 'Underwear',
    shortname: '',
  },
  {
    children: [
      { name: 'Aprons', shortname: '', children: [] },
      { name: 'Umbrellas / Parasols', shortname: '', children: [] },
      { name: 'Overskirts / Overdresses', shortname: '', children: [] },
      { name: 'Wigs', shortname: '', children: [] },
      { name: 'Makeup', shortname: '', children: [] },
      { name: 'Stuffed toys', shortname: '', children: [] },
      { name: 'Cups, books and home decor', shortname: '', children: [] },
      { name: 'Miscellaneous', shortname: '', children: [] },
      { name: 'Phone cases', shortname: '', children: [] },
    ],
    _id: getId(),
    name: 'Other',
    shortname: '',
  },
  { children: [], _id: getId(), name: 'Stickers', shortname: '' },
]);

export const extendedCategories = CriteriaHelpers.categoryTreeToSelect(availableCatories);
