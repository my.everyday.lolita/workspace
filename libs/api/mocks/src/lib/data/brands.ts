import { getId } from '../utils';
import { getOrCreate } from '../utils/storage';

export const availableBrands = getOrCreate('dev:brands', () => [
  { _id: getId(), name: '(OFFBRAND)' },
  {
    _id: getId(),
    name: '6%DOKIDOKI',
    shop: 'https://shop.6dokidoki.com/',
    shortname: '',
  },
  {
    _id: getId(),
    name: '69th Department (TaoBao)',
    shortname: '',
    shop: 'https://shop33463349.taobao.com/',
  },
  {
    _id: getId(),
    name: 'A little Fun ',
    shortname: '小小的开心',
    shop: 'https://shop143947009.taobao.com/',
  },
  {
    _id: getId(),
    name: 'A.Sylvia (TaoBao)',
    shortname: '',
    shop: 'https://market.m.taobao.com/apps/market/shop/simpleshop.html?wh_weex=true&shopId=69736218&sellerId=390300018&spm=a2141.7631565.backup.0&_inNestedEmbed=true&inWeexShop=true&ignoreShopHeadEvent=false&weexShopTransparentBG=true&_page_inside_embed_=true&_page_home_isweex_=true&useIframeInWeb=false',
  },
  {
    _id: getId(),
    name: 'ACDC  Rags',
    shortname: '',
    shop: 'https://acdcrag.com/en/',
  },
  {
    _id: getId(),
    name: 'ALICE and the PIRATES',
    shortname: 'AATP',
    shop: 'https://shop.baby-aatp.com/collections/alice-and-the-pirates',
  },
  {
    _id: getId(),
    name: 'Acorn Lolita (TaoBao)',
    shortname: '',
    shop: 'https://shop434506286.taobao.com/',
  },
  {
    _id: getId(),
    name: 'Adios Eden (TaoBao)',
    shortname: '',
    shop: 'https://shop105380603.world.taobao.com/',
  },
  {
    _id: getId(),
    name: 'Advertising Balloon (TaoBao)',
    shortname: '',
    shop: 'https://shop128321089.taobao.com/',
  },
  {
    _id: getId(),
    name: 'Aerial Cat (TaoBao)',
    shortname: '',
    shop: 'https://shop59541430.taobao.com/',
  },
  {
    _id: getId(),
    name: 'After 12 a.m. (TaoBao)',
    shortname: '',
    shop: 'https://market.m.taobao.com/apps/market/shop/simpleshop.html?wh_weex=true&shopId=116726784&sellerId=774756169&spm=a2141.7631565.backup.0&_inNestedEmbed=true&inWeexShop=true&ignoreShopHeadEvent=false&weexShopTransparentBG=true&_page_inside_embed_=true&_page_home_isweex_=true&useIframeInWeb=false',
  },
  {
    _id: getId(),
    name: 'Air Freeing (TaoBao)',
    shortname: '',
    shop: 'https://shop372084166.taobao.com/',
  },
]);
