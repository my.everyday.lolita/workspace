import { getDate, getFloat, getId, getInt, getRandom, getRandomAsList } from '../utils';
import { getOrCreate } from '../utils/storage';
import { availableBrands } from './brands';
import { extendedCategories } from './categories';
import { availableColors } from './colors';
import { availableFeatures } from './features';
import { availableImages } from './images';

const substyles = ['Gothic lolita', 'Classic lolita', 'Sweet lolita', 'Punk lolita'];
const keywords = ['crown', 'religious', 'bunny', 'bat', 'violin'];
const collections = Array.from(Array(10), (i) => `Collection ${i}`);

export const getItem = () => {
  const created = getDate();
  const modified = getDate(created);
  return {
    substyles: getRandomAsList(substyles, 0, 3),
    keywords: getRandomAsList(keywords, 0, 5),
    variants: Array.from(Array(getInt(1, 5)), () => ({
      colors: getRandomAsList(availableColors, 1, 2),
      photos: getRandomAsList(availableImages, 1, 3),
    })),
    features: getRandomAsList(availableFeatures, 1, 5),
    _id: getId(),
    brand: getRandom(availableBrands),
    collectionn: getRandom(collections),
    category: getRandom(extendedCategories),
    year: getInt(1970, 2020),
    japanese: '',
    measurments: '',
    estimatedPrice: getFloat(10, 300, 2),
    owner: getId(),
    created,
    modified,
  };
};

export const availableItems = getOrCreate('dev:items', () =>
  Array.from(Array(150), () => getItem()).sort((a, b) => {
    const ta = new Date(a.created).getTime();
    const tb = new Date(b.created).getTime();
    return ta > tb ? -1 : ta < tb ? 1 : 0;
  })
);

export const recentlyAddedItems = availableItems.slice(0, 20);
