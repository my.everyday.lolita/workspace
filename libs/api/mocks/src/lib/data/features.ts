import { getId } from '../utils';
import { getOrCreate } from '../utils/storage';

export const availableFeatures = getOrCreate('dev:features', () => [
  {
    categories: [{ name: 'Dresses' }, { name: 'Bottomwear' }],
    _id: getId(),
    name: 'Full back shirring',
  },
  {
    categories: [{ name: 'Dresses' }, { name: 'Bottomwear' }],
    _id: getId(),
    name: 'Partial Shirring',
  },
  {
    categories: [{ name: 'Bags' }, { name: 'Bottomwear' }, { name: 'Dresses' }, { name: 'Shoes' }],
    _id: getId(),
    name: 'Adjustable straps',
  },
  {
    categories: [
      { name: 'Other' },
      { name: 'Topwear' },
      { name: 'Outerwear' },
      { name: 'Legwear' },
      { name: 'Dresses' },
    ],
    _id: getId(),
    name: 'Front lacing',
  },
  {
    categories: [{ name: 'Topwear' }, { name: 'Bottomwear' }, { name: 'Dresses' }],
    _id: getId(),
    name: 'Detachable waist ties',
  },
  {
    categories: [{ name: 'Dresses' }, { name: 'Bottomwear' }, { name: 'Outerwear' }, { name: 'Shoes' }],
    _id: getId(),
    name: 'Scalloped',
  },
  {
    categories: [{ name: 'Outerwear' }, { name: 'Bottomwear' }, { name: 'Dresses' }],
    _id: getId(),
    name: 'Pockets',
  },
  {
    categories: [{ name: 'Topwear' }, { name: 'Dresses' }],
    _id: getId(),
    name: 'Frog closure',
  },
  {
    categories: [{ name: 'Topwear' }, { name: 'Outerwear' }, { name: 'Dresses' }],
    _id: getId(),
    name: 'Long sleeves',
  },
  { categories: [{ name: 'Outerwear' }], _id: getId(), name: 'Knitted' },
  {
    categories: [{ name: 'Topwear' }, { name: 'Outerwear' }, { name: 'Dresses' }],
    _id: getId(),
    name: 'Short sleeves',
  },
  {
    categories: [{ name: 'Topwear' }, { name: 'Outerwear' }, { name: 'Dresses' }],
    _id: getId(),
    name: 'Detachable sleeves',
  },
  {
    categories: [{ name: 'Topwear' }, { name: 'Outerwear' }, { name: 'Dresses' }],
    _id: getId(),
    name: 'Princess sleeves',
  },
  {
    categories: [
      { name: 'Headdress' },
      { name: 'Shoes' },
      { name: 'Bags' },
      { name: 'Accessories' },
      { name: 'Other' },
    ],
    _id: getId(),
    name: 'Pearls',
  },
]);
