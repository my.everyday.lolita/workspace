import { ErrorHandler, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { API_PATH } from './api-front.token';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { GLOBAL_RESOURCE_FEATURE_KEY, reducer } from './state/resource/resource.reducer';
import { GlobalResourceEffects } from './state/resource/resource.effects';
import { LogsHandler } from './logs/logs.handler';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forFeature(GLOBAL_RESOURCE_FEATURE_KEY, reducer),
    EffectsModule.forFeature([GlobalResourceEffects]),
  ],
  providers: [
    { provide: ErrorHandler, useClass: LogsHandler },
    { provide: API_PATH, useValue: 'http://api.mel.localhost' },
  ],
})
export class ApiFrontModule {}
