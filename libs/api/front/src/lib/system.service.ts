import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { SystemEndpoints } from '@api/shared';
import { API_PATH } from './api-front.token';
import { ApiModels } from '@api/shared';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SystemService {
  baseUrl = `${this.apiPath}/${SystemEndpoints.ROOT}`;

  constructor(@Inject(API_PATH) private apiPath: string, private http: HttpClient) {}

  getSystemInfo(): Observable<ApiModels.SystemInfos> {
    return this.http.get<ApiModels.SystemInfos>(this.baseUrl);
  }

  updateSystemInfo(systemInfo: ApiModels.SystemInfos): Observable<ApiModels.SystemInfos> {
    return this.http.put<ApiModels.SystemInfos>(this.baseUrl, systemInfo);
  }
}
