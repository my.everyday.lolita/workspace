import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { publish } from 'rxjs/operators';
import { API_PATH } from '../api-front.token';
import { LogsEndpoints } from '@api/shared';

@Injectable({
  providedIn: 'root',
})
export class LogsService {
  baseUrl = `${this.apiPath}/${LogsEndpoints.ROOT}`;

  constructor(@Inject(API_PATH) private apiPath: string, private http: HttpClient) {}

  error(message: string, trace?: string, context?: string): void {
    publish()(
      this.http.post(this.baseUrl, {
        message,
        trace,
        context,
        type: 'error',
      })
    ).connect();
  }

  warn(message: string, context?: string): void {
    publish()(
      this.http.post(this.baseUrl, {
        message,
        context,
        type: 'warning',
      })
    ).connect();
  }
}
