/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ResourcesEndpoints, ApiModels } from '@api/shared';
import { Observable } from 'rxjs';
import { API_PATH } from '../api-front.token';
import { ResourceBaseService } from './base-resource';

@Injectable({
  providedIn: 'root',
})
export class ItemsResourceService extends ResourceBaseService<ApiModels.Item> {
  baseUrl = `${this.apiPath}/${ResourcesEndpoints.ITEMS_ROOT}`;

  constructor(@Inject(API_PATH) private apiPath: string, http: HttpClient) {
    super(http);
  }

  create(data: ApiModels.Item): Observable<ApiModels.Item> {
    return super.create(data, ApiModels.RequestAuthOptions.AUTO);
  }

  update(data: ApiModels.Item): Observable<ApiModels.Item> {
    return super.update(data, ApiModels.RequestAuthOptions.AUTO);
  }

  findById(id: string): Observable<ApiModels.Item> {
    return super.findById(id, ApiModels.RequestAuthOptions.IF_SIGNED);
  }

  delete(id: string): Observable<any> {
    return super.delete(id, ApiModels.RequestAuthOptions.AUTO);
  }

  findByCriteria(criteria: ApiModels.Criterium[], skip = 0, limit = 60): Observable<ApiModels.Item[]> {
    const params = new HttpParams({
      fromObject: {
        limit: `${limit}`,
        skip: `${skip}`,
      },
    });
    return this.http.post<ApiModels.Item[]>(`${this.baseUrl}/search`, criteria, {
      params,
      headers: this.getDefaultHeaders(ApiModels.RequestAuthOptions.IF_SIGNED),
    });
  }

  findByCriteria2(
    criteria: ApiModels.Criterium[],
    skip = 0,
    limit = 60
  ): Observable<{ total: number; items: ApiModels.Item[] }> {
    const params = new HttpParams({
      fromObject: {
        limit: `${limit}`,
        skip: `${skip}`,
      },
    });
    return this.http.post<{ total: number; items: ApiModels.Item[] }>(`${this.baseUrl}/find`, criteria, {
      params,
      headers: this.getDefaultHeaders(ApiModels.RequestAuthOptions.IF_SIGNED),
    });
  }

  recentlyAdded(): Observable<ApiModels.Item[]> {
    return this.http.get<ApiModels.Item[]>(`${this.baseUrl}/recently-added`, {
      headers: this.getDefaultHeaders(ApiModels.RequestAuthOptions.IF_SIGNED),
    });
  }

  countDrafts(): Observable<number> {
    return this.http.get<number>(`${this.baseUrl}/count-drafts`, {
      headers: this.getDefaultHeaders(ApiModels.RequestAuthOptions.AUTO),
    });
  }

  uploadImage(key: string, file: File): Observable<{ id: string }> {
    const formData = new FormData();
    formData.append('file', file);
    return this.http.post<{ id: string }>(`${this.baseUrl}/upload-image/${key}`, formData, {
      headers: this.getDefaultHeaders(ApiModels.RequestAuthOptions.AUTO),
    });
  }

  uploadImageFromUrl(key: string, url: string): Observable<{ id: string }> {
    return this.http.post<{ id: string }>(
      `${this.baseUrl}/upload-image-url/${key}`,
      { url },
      {
        headers: this.getDefaultHeaders(ApiModels.RequestAuthOptions.AUTO),
      }
    );
  }
}
