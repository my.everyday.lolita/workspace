import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiModels, ResourcesEndpoints } from '@api/shared';
import { Observable } from 'rxjs';
import { API_PATH } from '../api-front.token';

@Injectable({
  providedIn: 'root',
})
export class UserContentResourceService {
  baseUrl = `${this.apiPath}/${ResourcesEndpoints.USER_CONTENT_ROOT}`;

  constructor(@Inject(API_PATH) private readonly apiPath: string, private readonly http: HttpClient) {}

  get(): Observable<ApiModels.UserContent> {
    return this.http.get<ApiModels.UserContent>(`${this.baseUrl}/me`, {
      headers: new HttpHeaders({
        Authorization: 'auto',
      }),
    });
  }

  update(data: ApiModels.UserContent): Observable<ApiModels.UserContent> {
    return this.http.patch<ApiModels.UserContent>(`${this.baseUrl}`, data, {
      headers: new HttpHeaders({
        Authorization: 'auto',
      }),
    });
  }
}
