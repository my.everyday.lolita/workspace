import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ResourcesEndpoints, ApiModels } from '@api/shared';
import { API_PATH } from '../api-front.token';
import { ResourceBaseService } from './base-resource';

@Injectable({
  providedIn: 'root',
})
export class CategoriesResourceService extends ResourceBaseService<ApiModels.Category> {
  baseUrl = `${this.apiPath}/${ResourcesEndpoints.CATEGORIES_ROOT}`;

  constructor(@Inject(API_PATH) private apiPath: string, http: HttpClient) {
    super(http);
  }
}
