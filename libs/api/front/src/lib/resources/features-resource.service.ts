import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ResourcesEndpoints, ApiModels } from '@api/shared';
import { API_PATH } from '../api-front.token';
import { ResourceBaseService } from './base-resource';

@Injectable({
  providedIn: 'root',
})
export class FeaturesResourceService extends ResourceBaseService<ApiModels.Feature> {
  baseUrl = `${this.apiPath}/${ResourcesEndpoints.FEATURES_ROOT}`;

  constructor(@Inject(API_PATH) private apiPath: string, http: HttpClient) {
    super(http);
  }
}
