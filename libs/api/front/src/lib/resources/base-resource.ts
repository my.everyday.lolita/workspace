/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiModels } from '@api/shared';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

export abstract class ResourceBaseService<T> {
  abstract baseUrl: string;

  constructor(protected http: HttpClient) {}

  findAll(auth: ApiModels.RequestAuth = false): Observable<T[]> {
    return this.http
      .get<T[]>(this.baseUrl, {
        headers: this.getDefaultHeaders(auth),
      })
      .pipe(
        catchError((error) => {
          console.error(error);
          return of([]);
        })
      );
  }

  create(data: T, auth: ApiModels.RequestAuth = true): Observable<T> {
    return this.http.put<T>(this.baseUrl, data, {
      headers: this.getDefaultHeaders(auth),
    });
  }

  update(data: T, auth: ApiModels.RequestAuth = true): Observable<T> {
    return this.http.patch<T>(this.baseUrl, data, {
      headers: this.getDefaultHeaders(auth),
    });
  }

  findById(id: string, auth: ApiModels.RequestAuth = false): Observable<T> {
    return this.http.get<T>(`${this.baseUrl}/get/${id}`, {
      headers: this.getDefaultHeaders(auth),
    });
  }

  delete(id: string, auth: ApiModels.RequestAuth = true): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}/${id}`, {
      headers: this.getDefaultHeaders(auth),
    });
  }

  protected getDefaultHeaders(auth: ApiModels.RequestAuth = false): HttpHeaders {
    let headers = new HttpHeaders();
    if (auth) {
      headers = headers.append('Authorization', typeof auth === 'boolean' ? 'auto' : auth);
    }
    return headers;
  }
}
