import { Injectable } from '@angular/core';
import {
  BrandsResourceService,
  CategoriesResourceService,
  ColorsResourceService,
  FeaturesResourceService,
} from '../../resources';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as ResourceActions from './resource.actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class GlobalResourceEffects {
  brands$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ResourceActions.brands),
      switchMap(() =>
        this.brandsResource.findAll().pipe(
          map((data) => ResourceActions.brandsSuccess({ data })),
          catchError((error) => of(ResourceActions.brandsFailure({ error })))
        )
      )
    )
  );

  categories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ResourceActions.categories),
      switchMap(() =>
        this.categoriesResource.findAll().pipe(
          map((data) => ResourceActions.categoriesSuccess({ data })),
          catchError((error) => of(ResourceActions.categoriesFailure({ error })))
        )
      )
    )
  );

  colors$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ResourceActions.colors),
      switchMap(() =>
        this.colorsResource.findAll().pipe(
          map((data) => ResourceActions.colorsSuccess({ data })),
          catchError((error) => of(ResourceActions.colorsFailure({ error })))
        )
      )
    )
  );

  features$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ResourceActions.features),
      switchMap(() =>
        this.featuresResource.findAll().pipe(
          map((data) => ResourceActions.featuresSuccess({ data })),
          catchError((error) => of(ResourceActions.featuresFailure({ error })))
        )
      )
    )
  );

  constructor(
    private readonly actions$: Actions,
    private readonly brandsResource: BrandsResourceService,
    private readonly categoriesResource: CategoriesResourceService,
    private readonly colorsResource: ColorsResourceService,
    private readonly featuresResource: FeaturesResourceService
  ) {}
}
