import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as ResourceActions from './resource.actions';
import * as ResourceSelectors from './resource.selectors';

@Injectable({ providedIn: 'root' })
export class GlobalResourceFacade {
  brands$ = this.store.select(ResourceSelectors.getBrands);
  isBrandsFetched$ = this.store.select(ResourceSelectors.getBrandsFetched);

  categories$ = this.store.select(ResourceSelectors.getCategories);
  isCategoriesFetched$ = this.store.select(ResourceSelectors.getCategoriesFetched);

  colors$ = this.store.select(ResourceSelectors.getColors);
  isColorsFetched$ = this.store.select(ResourceSelectors.getColorsFetched);

  features$ = this.store.select(ResourceSelectors.getFeatures);
  isFeaturesFetched$ = this.store.select(ResourceSelectors.getFeaturesFetched);

  constructor(private readonly store: Store) {}

  fetchBrands() {
    this.store.dispatch(ResourceActions.brands());
  }

  fetchCategories() {
    this.store.dispatch(ResourceActions.categories());
  }

  fetchColors() {
    this.store.dispatch(ResourceActions.colors());
  }

  fetchFeatures() {
    this.store.dispatch(ResourceActions.features());
  }
}
