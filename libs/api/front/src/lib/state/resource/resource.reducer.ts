import { Action, createReducer, on } from '@ngrx/store';
import { brandsSuccess, categoriesSuccess, colorsSuccess, featuresSuccess } from './resource.actions';
import { GlobalResourceState } from './resource.model';

export const GLOBAL_RESOURCE_FEATURE_KEY = 'global_resource';

export const initialState: GlobalResourceState = {
  brands: [],
  brandsFetched: false,
  categories: [],
  categoriesFetched: false,
  colors: [],
  colorsFetched: false,
  features: [],
  featuresFetched: false,
};

const globalResourceReducer = createReducer(
  initialState,
  on(brandsSuccess, (state, action) => ({ ...state, brands: action.data, brandsFetched: true })),
  on(categoriesSuccess, (state, action) => ({ ...state, categories: action.data, categoriesFetched: true })),
  on(colorsSuccess, (state, action) => ({ ...state, colors: action.data, colorsFetched: true })),
  on(featuresSuccess, (state, action) => ({ ...state, features: action.data, featuresFetched: true }))
);

export function reducer(state: GlobalResourceState, action: Action) {
  return globalResourceReducer(state, action);
}
