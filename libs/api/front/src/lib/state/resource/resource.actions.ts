/* eslint-disable @typescript-eslint/no-explicit-any */
import { ApiModels } from '@api/shared';
import { createAction, props } from '@ngrx/store';

export const brands = createAction('[Resource/Brand] Get');
export const brandsSuccess = createAction('[Resource/Brand] Load Success', props<{ data: ApiModels.Brand[] }>());
export const brandsFailure = createAction('[Resource/Brand] Load Failure', props<{ error: any }>());

export const categories = createAction('[Resource/Category] Get');
export const categoriesSuccess = createAction(
  '[Resource/Category] Load Success',
  props<{ data: ApiModels.Category[] }>()
);
export const categoriesFailure = createAction('[Resource/Category] Load Failure', props<{ error: any }>());

export const colors = createAction('[Resource/Color] Get');
export const colorsSuccess = createAction('[Resource/Color] Load Success', props<{ data: ApiModels.Color[] }>());
export const colorsFailure = createAction('[Resource/Color] Load Failure', props<{ error: any }>());

export const features = createAction('[Resource/Feature] Get');
export const featuresSuccess = createAction('[Resource/Feature] Load Success', props<{ data: ApiModels.Feature[] }>());
export const featuresFailure = createAction('[Resource/Feature] Load Failure', props<{ error: any }>());
