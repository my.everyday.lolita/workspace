import { ApiModels } from '@api/shared';

export interface GlobalResourceState {
  brands: ApiModels.Brand[];
  brandsFetched: boolean;
  categories: ApiModels.Category[];
  categoriesFetched: boolean;
  colors: ApiModels.Color[];
  colorsFetched: boolean;
  features: ApiModels.Feature[];
  featuresFetched: boolean;
}
