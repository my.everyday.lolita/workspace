import { createFeatureSelector, createSelector } from '@ngrx/store';
import { GlobalResourceState } from './resource.model';
import { GLOBAL_RESOURCE_FEATURE_KEY } from './resource.reducer';

export const getGlobalResourceState = createFeatureSelector<GlobalResourceState>(GLOBAL_RESOURCE_FEATURE_KEY);

export const getBrands = createSelector(getGlobalResourceState, (state: GlobalResourceState) => state.brands);
export const getBrandsFetched = createSelector(
  getGlobalResourceState,
  (state: GlobalResourceState) => state.brandsFetched
);
export const getCategories = createSelector(getGlobalResourceState, (state: GlobalResourceState) => state.categories);
export const getCategoriesFetched = createSelector(
  getGlobalResourceState,
  (state: GlobalResourceState) => state.categoriesFetched
);
export const getColors = createSelector(getGlobalResourceState, (state: GlobalResourceState) => state.colors);
export const getColorsFetched = createSelector(
  getGlobalResourceState,
  (state: GlobalResourceState) => state.colorsFetched
);
export const getFeatures = createSelector(getGlobalResourceState, (state: GlobalResourceState) => state.features);
export const getFeaturesFetched = createSelector(
  getGlobalResourceState,
  (state: GlobalResourceState) => state.featuresFetched
);
