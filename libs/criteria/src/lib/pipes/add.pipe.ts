import { Inject, Pipe, PipeTransform } from '@angular/core';
import { ApiModels } from '@api/shared';
import { map, Observable } from 'rxjs';
import { CRITERIA_FACTORY } from '../criteria.token';
import { CriteriaFactory, CriteriaToken } from '../models';

/**
 * Allow others to add types to a criteria list for a specific case in app.
 */
@Pipe({
  name: 'add',
})
export class AddCriteriaPipe implements PipeTransform {
  constructor(@Inject(CRITERIA_FACTORY) private facotires: CriteriaToken[]) {}

  transform(criteria: Observable<ApiModels.Criterium[]>, ...tmpTypesToAdd: string[]) {
    return criteria.pipe(
      map((c) => {
        tmpTypesToAdd.forEach((type) => {
          const factory = this.getFactory(type);
          if (factory) {
            c.push(factory(void 0));
          } else {
            console.warn('Unknown criterium type', type);
          }
        });
        return c;
      })
    );
  }

  private getFactory(type: string): CriteriaFactory<unknown> | undefined {
    return this.facotires.find((token) => token.type === type)?.factory;
  }
}
