import { Pipe, PipeTransform } from '@angular/core';
import { ApiModels } from '@api/shared';
import { map, Observable } from 'rxjs';

@Pipe({
  name: 'includeCriteria',
})
export class IncludeCriteriaPipe implements PipeTransform {
  transform(value: Observable<ApiModels.Criterium[]>, ...types: string[]) {
    return value.pipe(map((criteria) => criteria.filter((c) => types.includes(c.type))));
  }
}
