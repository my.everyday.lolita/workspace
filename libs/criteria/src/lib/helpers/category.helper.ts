import { ApiModels } from '@api/shared';
import { buildPartialCategoryCriteria } from '../factories/resource.criteria';

export function categoryTreeToCriteria(tree: ApiModels.Category[]): ApiModels.Criterium[] {
  const criteria: ApiModels.Criterium[] = [];
  tree.forEach((category) => {
    criteria.push({
      ...buildPartialCategoryCriteria(category),
      _lvlClass: 'lvl-0',
    });
    if (category.children) {
      category.children.forEach((category2) => {
        criteria.push({
          ...buildPartialCategoryCriteria(category2),
          parents: [category.name],
          _lvlClass: 'lvl-1',
        });
        if (category2.children) {
          category2.children.forEach((category3) => {
            criteria.push({
              ...buildPartialCategoryCriteria(category3),
              parents: [category.name, category2.name],
              _lvlClass: 'lvl-2',
            });
          });
        }
      });
    }
  });
  return criteria;
}

export function categoryTreeToSelect(categories: ApiModels.Category[]): ApiModels.ExtendedCategory[] {
  return (categories as ApiModels.ExtendedCategory[]).reduce((acc, root) => {
    root.disabled = root.children !== undefined && root.children.length > 0;
    root._lvlClass = 'lvl-0';
    acc.push(root);
    if (root.children) {
      root.children.forEach((child) => {
        child.disabled = child.children !== undefined && child.children.length > 0;
        child._lvlClass = 'lvl-1';
        child.parent = { ...root, children: undefined, disabled: undefined, _lvlClass: undefined };
        acc.push(child);
        if (child.children) {
          child.children.forEach((leaf) => {
            leaf.disabled = false;
            leaf._lvlClass = 'lvl-2';
            leaf.parent = { ...child, children: undefined, _lvlClass: undefined, disabled: undefined };
            acc.push(leaf);
          });
        }
      });
    }
    return acc;
  }, [] as ApiModels.ExtendedCategory[]);
}
