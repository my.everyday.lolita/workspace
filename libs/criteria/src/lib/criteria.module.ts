import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ApiFrontModule } from '@api/front';
import { UserModule } from '@front/user';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { CRITERIA_FACTORY } from './criteria.token';
import {
  buildBrandCriteria,
  buildColorCriteria,
  buildFeatureCriteria,
  buildOwnCriteria,
  buildPartialCategoryCriteria,
} from './factories';
import { AddCriteriaPipe, ExcludeCriteriaPipe, IncludeCriteriaPipe } from './pipes';
import { CRITERIA_FEATURE_KEY, reducer } from './state/criteria.reducer';
import { CriteriaOwnEffect } from './state/effects/own.effect';
import { CriteriaResourcesEffect } from './state/effects/resources.effect';

@NgModule({
  imports: [
    CommonModule,
    ApiFrontModule,
    UserModule,
    StoreModule.forFeature(CRITERIA_FEATURE_KEY, reducer),
    EffectsModule.forFeature([CriteriaResourcesEffect, CriteriaOwnEffect]),
  ],
  declarations: [IncludeCriteriaPipe, ExcludeCriteriaPipe, AddCriteriaPipe],
  exports: [IncludeCriteriaPipe, ExcludeCriteriaPipe, AddCriteriaPipe],
  providers: [
    { provide: CRITERIA_FACTORY, useValue: { type: 'brand', factory: buildBrandCriteria }, multi: true },
    { provide: CRITERIA_FACTORY, useValue: { type: 'color', factory: buildColorCriteria }, multi: true },
    { provide: CRITERIA_FACTORY, useValue: { type: 'feature', factory: buildFeatureCriteria }, multi: true },
    { provide: CRITERIA_FACTORY, useValue: { type: 'category', factory: buildPartialCategoryCriteria }, multi: true },
    { provide: CRITERIA_FACTORY, useValue: { type: 'own', factory: buildOwnCriteria }, multi: true },
  ],
})
export class CriteriaModule {}
