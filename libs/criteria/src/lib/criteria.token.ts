import { InjectionToken } from '@angular/core';
import { CriteriaToken } from './models';

export const CRITERIA_FACTORY = new InjectionToken<CriteriaToken>('CRITERIA_FACTORY');
