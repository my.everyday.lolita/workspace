import { ApiModels } from '@api/shared';
import { CriteriaFactory } from '../models';

export const buildColorCriteria: CriteriaFactory<ApiModels.Color> = (color) => ({
  type: 'color',
  displayValue: color.name,
  value: color.name,
});

export const buildBrandCriteria: CriteriaFactory<ApiModels.Brand> = (brand) => ({
  type: 'brand',
  displayValue: brand.shortname ? `${brand.name} (${brand.shortname})` : brand.name,
  value: brand.name,
});

export const buildFeatureCriteria: CriteriaFactory<ApiModels.Feature> = (feature) => ({
  type: 'brand',
  displayValue: feature.name,
  value: feature.name,
});

export const buildPartialCategoryCriteria: CriteriaFactory<ApiModels.Category> = (category) => ({
  type: 'category',
  displayValue: category.shortname ? `${category.name} (${category.shortname})` : category.name,
  value: category.name,
});
