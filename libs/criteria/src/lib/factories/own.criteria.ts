import { CriteriaFactory } from '../models';

export const buildOwnCriteria: CriteriaFactory<string> = (value) => ({
  type: 'own',
  displayValue: 'My items',
  value,
});
