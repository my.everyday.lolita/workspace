import { ApiModels } from '@api/shared';
import { createAction, props } from '@ngrx/store';

export const load = createAction('[Criteria] load criteria');

export const addTypes = createAction('[Criteria] add types', props<{ types: ApiModels.CriteriumType[] }>());
export const removeTypes = createAction('[Criteria] remove types', props<{ types: ApiModels.CriteriumType[] }>());

export const addCriteria = createAction('[Criteria] add criteria', props<{ criteria: ApiModels.Criterium[] }>());
export const removeCriteria = createAction('[Criteria] remove criteria', props<{ criteria: ApiModels.Criterium[] }>());
