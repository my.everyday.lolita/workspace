import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CriteriaState } from './criteria.model';
import { CRITERIA_FEATURE_KEY } from './criteria.reducer';

export const getCriteriaState = createFeatureSelector<CriteriaState>(CRITERIA_FEATURE_KEY);

export const getCriteria = createSelector(getCriteriaState, (state: CriteriaState) => state.criteria);
export const getTypes = createSelector(getCriteriaState, (state: CriteriaState) => state.types);
