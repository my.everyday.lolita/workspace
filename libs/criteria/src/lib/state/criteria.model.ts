import { ApiModels } from '@api/shared';

export interface CriteriaState {
  criteria: ApiModels.Criterium[];
  types: ApiModels.CriteriumType[];
}
