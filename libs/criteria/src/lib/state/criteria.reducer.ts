import { ApiModels } from '@api/shared';
import { Action, createReducer, on } from '@ngrx/store';
import * as CriteriaActions from './criteria.actions';
import { CriteriaState } from './criteria.model';

export const CRITERIA_FEATURE_KEY = 'app_criteria';

export const defaultTypes: ApiModels.CriteriumType[] = ['brand', 'category', 'color', 'feature'];

export const initialState: CriteriaState = {
  criteria: [],
  types: defaultTypes,
};

const userContentReducer = createReducer(
  initialState,
  on(CriteriaActions.addTypes, (state, action) => ({
    ...state,
    types: state.types.concat(action.types.filter((at) => !state.types.includes(at))),
  })),
  on(CriteriaActions.removeTypes, (state, action) => ({
    criteria: state.criteria.filter((sc) => !action.types.includes(sc.type)),
    types: state.types.filter((st) => !action.types.includes(st)),
  })),
  on(CriteriaActions.addCriteria, (state, action) => ({
    ...state,
    criteria: state.criteria.concat(action.criteria),
  })),
  on(CriteriaActions.removeCriteria, (state, action) => ({
    ...state,
    criteria: state.criteria.filter(
      (sc) => !!action.criteria.find((rc) => rc.type === sc.type && rc.value === rc.value)
    ),
  }))
);

export function reducer(state: CriteriaState, action: Action) {
  return userContentReducer(state, action);
}
