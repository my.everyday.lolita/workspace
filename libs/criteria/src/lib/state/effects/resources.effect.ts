import { Injectable } from '@angular/core';
import { GlobalResourceFacade } from '@api/front';
import { createEffect } from '@ngrx/effects';
import { combineLatestWith, filter, map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import { buildBrandCriteria, buildColorCriteria, buildFeatureCriteria } from '../../factories';
import { categoryTreeToCriteria } from '../../helpers';
import * as CriteriaActions from '../criteria.actions';
import { CriteriaFacade } from '../criteria.facade';

@Injectable({ providedIn: 'root' })
export class CriteriaResourcesEffect {
  getColorsCriteria$ = createEffect(() =>
    this.criteriaFacade.types$.pipe(
      filter((types) => types.includes('color')),
      withLatestFrom(this.resourcesFacade.isColorsFetched$),
      tap(([, isFetched]) => {
        if (!isFetched) {
          this.resourcesFacade.fetchColors();
        }
      }),
      combineLatestWith(this.resourcesFacade.isColorsFetched$.pipe(filter((isFetched) => isFetched))),
      switchMap(() => this.resourcesFacade.colors$.pipe(take(1))),
      withLatestFrom(this.criteriaFacade.criteria$),
      filter(([data, criteria]) => data.some((d) => !criteria.find((c) => c.type === 'color' && c.value === d.name))),
      map(([colors]) =>
        CriteriaActions.addCriteria({
          criteria: colors.map((color) => buildColorCriteria(color)),
        })
      )
    )
  );

  getBrandsCriteria$ = createEffect(() =>
    this.criteriaFacade.types$.pipe(
      filter((types) => types.includes('brand')),
      withLatestFrom(this.resourcesFacade.isBrandsFetched$),
      tap(([, isFetched]) => {
        if (!isFetched) {
          this.resourcesFacade.fetchBrands();
        }
      }),
      combineLatestWith(this.resourcesFacade.isBrandsFetched$.pipe(filter((isFetched) => isFetched))),
      switchMap(() => this.resourcesFacade.brands$.pipe(take(1))),
      withLatestFrom(this.criteriaFacade.criteria$),
      filter(([data, criteria]) => data.some((d) => !criteria.find((c) => c.type === 'brand' && c.value === d.name))),
      map(([brands]) =>
        CriteriaActions.addCriteria({
          criteria: brands.map((brand) => buildBrandCriteria(brand)),
        })
      )
    )
  );

  getFeaturesCriteria$ = createEffect(() =>
    this.criteriaFacade.types$.pipe(
      filter((types) => types.includes('feature')),
      withLatestFrom(this.resourcesFacade.isFeaturesFetched$),
      tap(([, isFetched]) => {
        if (!isFetched) {
          this.resourcesFacade.fetchFeatures();
        }
      }),
      combineLatestWith(this.resourcesFacade.isFeaturesFetched$.pipe(filter((isFetched) => isFetched))),
      switchMap(() => this.resourcesFacade.features$.pipe(take(1))),
      withLatestFrom(this.criteriaFacade.criteria$),
      filter(([data, criteria]) => data.some((d) => !criteria.find((c) => c.type === 'feature' && c.value === d.name))),
      map(([features]) =>
        CriteriaActions.addCriteria({
          criteria: features.map((feature) => buildFeatureCriteria(feature)),
        })
      )
    )
  );

  getCategoriesCriteria$ = createEffect(() =>
    this.criteriaFacade.types$.pipe(
      filter((types) => types.includes('category')),
      withLatestFrom(this.resourcesFacade.isCategoriesFetched$),
      tap(([, isFetched]) => {
        if (!isFetched) {
          this.resourcesFacade.fetchCategories();
        }
      }),
      combineLatestWith(this.resourcesFacade.isCategoriesFetched$.pipe(filter((isFetched) => isFetched))),
      switchMap(() => this.resourcesFacade.categories$.pipe(take(1))),
      withLatestFrom(this.criteriaFacade.criteria$),
      filter(([data, criteria]) =>
        data.some((d) => !criteria.find((c) => c.type === 'category' && c.value === d.name))
      ),
      map(([categories]) => {
        return CriteriaActions.addCriteria({
          criteria: categoryTreeToCriteria(categories),
        });
      })
    )
  );

  constructor(
    private readonly criteriaFacade: CriteriaFacade,
    private readonly resourcesFacade: GlobalResourceFacade
  ) {}
}
