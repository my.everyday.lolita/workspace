import { Injectable } from '@angular/core';
import { UserFacade } from '@front/user';
import { createEffect } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import * as CriteriaActions from '../criteria.actions';
import { CriteriaFacade } from '../criteria.facade';

@Injectable({ providedIn: 'root' })
export class CriteriaMelErrorEffect {
  onUser$ = createEffect(() =>
    this.userFacade.user$.pipe(
      map((user) =>
        user !== undefined
          ? CriteriaActions.addTypes({ types: ['mel-error'] })
          : CriteriaActions.removeTypes({ types: ['mel-error'] })
      )
    )
  );

  constructor(private readonly criteriaFacade: CriteriaFacade, private readonly userFacade: UserFacade) {}
}
