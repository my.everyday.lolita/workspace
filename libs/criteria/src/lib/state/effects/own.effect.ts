import { Injectable } from '@angular/core';
import { UserFacade } from '@front/user';
import { createEffect } from '@ngrx/effects';
import { filter, map, switchMap, take } from 'rxjs/operators';
import { buildOwnCriteria } from '../../factories';
import * as CriteriaActions from '../criteria.actions';
import { CriteriaFacade } from '../criteria.facade';

@Injectable({ providedIn: 'root' })
export class CriteriaOwnEffect {
  addOwnCriteria$ = createEffect(() =>
    this.criteriaFacade.types$.pipe(
      filter((types) => types.includes('own')),
      switchMap(() => this.criteriaFacade.criteria$),
      filter((criteria) => !criteria.find((c) => c.type === 'own')),
      switchMap(() => this.userFacade.user$.pipe(take(1))),
      filter((user) => user !== undefined),
      map((user) =>
        CriteriaActions.addCriteria({
          criteria: [buildOwnCriteria(user?.sub as string)],
        })
      )
    )
  );

  onUser$ = createEffect(() =>
    this.userFacade.user$.pipe(
      map((user) =>
        user !== undefined
          ? CriteriaActions.addTypes({ types: ['own'] })
          : CriteriaActions.removeTypes({ types: ['own'] })
      )
    )
  );

  constructor(private readonly criteriaFacade: CriteriaFacade, private readonly userFacade: UserFacade) {}
}
