import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as CriteriaActions from './criteria.actions';
import * as CriteriaSelectors from './criteria.selectors';

@Injectable({ providedIn: 'root' })
export class CriteriaFacade {
  criteria$ = this.store.select(CriteriaSelectors.getCriteria);
  types$ = this.store.select(CriteriaSelectors.getTypes);

  constructor(private readonly store: Store) {}

  load(): void {
    this.store.dispatch(CriteriaActions.load());
  }
}
