import { ApiModels } from '@api/shared';

export type CriteriaFactory<T> = (data: T) => ApiModels.Criterium;

export interface CriteriaToken {
  type: ApiModels.CriteriumType;
  factory: CriteriaFactory<unknown>;
}
