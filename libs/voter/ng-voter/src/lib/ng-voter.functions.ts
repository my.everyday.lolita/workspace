import { NgVoter, NgVoterPoll, NgVoterPollProps, NgVoterVoteCreator, NgVoterVoteCreatorFn } from './ng-voter.model';

export function createPoll<T extends string, P extends NgVoterPollProps>(type: T, props?: P): NgVoterPoll<P> {
  return {
    type,
    props,
  };
}

export function createVoter<T extends string, P extends NgVoterPollProps>(
  type: T,
  creator: NgVoterVoteCreatorFn<P>
): NgVoterVoteCreator<P> {
  return {
    type,
    creator,
  };
}

export function getCreatorsFromService<T extends NgVoterPollProps>(
  poll: NgVoterPoll<T>,
  service: NgVoter<T>
): NgVoterVoteCreatorFn<T>[] {
  return (
    Object.entries(service)
      // eslint-disable-next-line no-prototype-builtins
      .filter(([propKey, prop]) => service.hasOwnProperty(propKey) && prop && prop.type === poll.type)
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      .map(([_, prop]) => prop.creator)
  );
}
