import { Observable } from 'rxjs';

export type NgVoterPollProps = object;

export type NgVoterPoll<T extends NgVoterPollProps> = { type: string; props?: T };

export type NgVoter<T extends NgVoterPollProps> = {
  [s: string]: NgVoterVoteCreator<T>;
};

export type NgVoterVoteCreatorFn<T extends NgVoterPollProps> = (poll?: T) => Observable<boolean>;

export interface NgVoterVoteCreator<T extends NgVoterPollProps> {
  type: string;
  creator: NgVoterVoteCreatorFn<T>;
}
