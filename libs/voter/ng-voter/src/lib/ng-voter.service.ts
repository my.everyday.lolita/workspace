import { Inject, Injectable } from '@angular/core';
import { map, Observable, of, zip } from 'rxjs';
import { getCreatorsFromService } from './ng-voter.functions';
import { NgVoter, NgVoterPoll, NgVoterPollProps, NgVoterVoteCreatorFn } from './ng-voter.model';
import { NG_VOTER } from './ng-voter.token';

@Injectable({ providedIn: 'root' })
export class NgVoterService {
  constructor(@Inject(NG_VOTER) private readonly voters: NgVoter<NgVoterPollProps>[]) {}

  ask<T extends NgVoterPollProps>(poll: NgVoterPoll<T>): Observable<boolean> {
    const creators = this.voters
      .reduce((acc, service) => acc.concat(getCreatorsFromService(poll, service)), [] as NgVoterVoteCreatorFn<T>[])
      .map((creator) => creator(poll?.props));
    if (creators.length === 0) {
      return of(true);
    }
    return zip(...creators).pipe(map((results) => results.every((r) => !!r)));
  }
}
