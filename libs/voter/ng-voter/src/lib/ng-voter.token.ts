import { InjectionToken } from '@angular/core';
import { NgVoter, NgVoterPollProps } from './ng-voter.model';

export const NG_VOTER = new InjectionToken<NgVoter<NgVoterPollProps>>('NG_VOTER:VOTER');
