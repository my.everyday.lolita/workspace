gp env MEL_KEYCLOAK_DOMAIN=$(gp url 8080) && 
gp env MEL_API_DOMAIN=$(gp url 3000) && 
gp env MEL_ADMIN_DOMAIN=$(gp url 4300) && 
gp env MEL_DOMAIN=$(gp url 4200) && 
eval $(gp env -e) && 
envsubst < apps/mel/src/assets/env.template.js > apps/mel/src/assets/env.js &&
envsubst < gitpod-dev/docker-compose.template.yml > gitpod-dev/docker-compose.yml &&

docker-compose -f gitpod-dev/docker-compose.yml rm -sf && 
docker-compose -f gitpod-dev/docker-compose.yml up -d --remove-orphans
