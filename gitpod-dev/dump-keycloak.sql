--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: admin_event_entity; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.admin_event_entity (
    id character varying(36) NOT NULL,
    admin_event_time bigint,
    realm_id character varying(255),
    operation_type character varying(255),
    auth_realm_id character varying(255),
    auth_client_id character varying(255),
    auth_user_id character varying(255),
    ip_address character varying(255),
    resource_path character varying(2550),
    representation text,
    error character varying(255),
    resource_type character varying(64)
);


ALTER TABLE public.admin_event_entity OWNER TO keycloak;

--
-- Name: associated_policy; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.associated_policy (
    policy_id character varying(36) NOT NULL,
    associated_policy_id character varying(36) NOT NULL
);


ALTER TABLE public.associated_policy OWNER TO keycloak;

--
-- Name: authentication_execution; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.authentication_execution (
    id character varying(36) NOT NULL,
    alias character varying(255),
    authenticator character varying(36),
    realm_id character varying(36),
    flow_id character varying(36),
    requirement integer,
    priority integer,
    authenticator_flow boolean DEFAULT false NOT NULL,
    auth_flow_id character varying(36),
    auth_config character varying(36)
);


ALTER TABLE public.authentication_execution OWNER TO keycloak;

--
-- Name: authentication_flow; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.authentication_flow (
    id character varying(36) NOT NULL,
    alias character varying(255),
    description character varying(255),
    realm_id character varying(36),
    provider_id character varying(36) DEFAULT 'basic-flow'::character varying NOT NULL,
    top_level boolean DEFAULT false NOT NULL,
    built_in boolean DEFAULT false NOT NULL
);


ALTER TABLE public.authentication_flow OWNER TO keycloak;

--
-- Name: authenticator_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.authenticator_config (
    id character varying(36) NOT NULL,
    alias character varying(255),
    realm_id character varying(36)
);


ALTER TABLE public.authenticator_config OWNER TO keycloak;

--
-- Name: authenticator_config_entry; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.authenticator_config_entry (
    authenticator_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.authenticator_config_entry OWNER TO keycloak;

--
-- Name: broker_link; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.broker_link (
    identity_provider character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL,
    broker_user_id character varying(255),
    broker_username character varying(255),
    token text,
    user_id character varying(255) NOT NULL
);


ALTER TABLE public.broker_link OWNER TO keycloak;

--
-- Name: client; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client (
    id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    full_scope_allowed boolean DEFAULT false NOT NULL,
    client_id character varying(255),
    not_before integer,
    public_client boolean DEFAULT false NOT NULL,
    secret character varying(255),
    base_url character varying(255),
    bearer_only boolean DEFAULT false NOT NULL,
    management_url character varying(255),
    surrogate_auth_required boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    protocol character varying(255),
    node_rereg_timeout integer DEFAULT 0,
    frontchannel_logout boolean DEFAULT false NOT NULL,
    consent_required boolean DEFAULT false NOT NULL,
    name character varying(255),
    service_accounts_enabled boolean DEFAULT false NOT NULL,
    client_authenticator_type character varying(255),
    root_url character varying(255),
    description character varying(255),
    registration_token character varying(255),
    standard_flow_enabled boolean DEFAULT true NOT NULL,
    implicit_flow_enabled boolean DEFAULT false NOT NULL,
    direct_access_grants_enabled boolean DEFAULT false NOT NULL,
    always_display_in_console boolean DEFAULT false NOT NULL
);


ALTER TABLE public.client OWNER TO keycloak;

--
-- Name: client_attributes; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_attributes (
    client_id character varying(36) NOT NULL,
    value character varying(4000),
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_attributes OWNER TO keycloak;

--
-- Name: client_auth_flow_bindings; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_auth_flow_bindings (
    client_id character varying(36) NOT NULL,
    flow_id character varying(36),
    binding_name character varying(255) NOT NULL
);


ALTER TABLE public.client_auth_flow_bindings OWNER TO keycloak;

--
-- Name: client_default_roles; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_default_roles (
    client_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.client_default_roles OWNER TO keycloak;

--
-- Name: client_initial_access; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_initial_access (
    id character varying(36) NOT NULL,
    realm_id character varying(36) NOT NULL,
    "timestamp" integer,
    expiration integer,
    count integer,
    remaining_count integer
);


ALTER TABLE public.client_initial_access OWNER TO keycloak;

--
-- Name: client_node_registrations; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_node_registrations (
    client_id character varying(36) NOT NULL,
    value integer,
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_node_registrations OWNER TO keycloak;

--
-- Name: client_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_scope (
    id character varying(36) NOT NULL,
    name character varying(255),
    realm_id character varying(36),
    description character varying(255),
    protocol character varying(255)
);


ALTER TABLE public.client_scope OWNER TO keycloak;

--
-- Name: client_scope_attributes; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_scope_attributes (
    scope_id character varying(36) NOT NULL,
    value character varying(2048),
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_scope_attributes OWNER TO keycloak;

--
-- Name: client_scope_client; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_scope_client (
    client_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE public.client_scope_client OWNER TO keycloak;

--
-- Name: client_scope_role_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_scope_role_mapping (
    scope_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.client_scope_role_mapping OWNER TO keycloak;

--
-- Name: client_session; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session (
    id character varying(36) NOT NULL,
    client_id character varying(36),
    redirect_uri character varying(255),
    state character varying(255),
    "timestamp" integer,
    session_id character varying(36),
    auth_method character varying(255),
    realm_id character varying(255),
    auth_user_id character varying(36),
    current_action character varying(36)
);


ALTER TABLE public.client_session OWNER TO keycloak;

--
-- Name: client_session_auth_status; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session_auth_status (
    authenticator character varying(36) NOT NULL,
    status integer,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_auth_status OWNER TO keycloak;

--
-- Name: client_session_note; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session_note (
    name character varying(255) NOT NULL,
    value character varying(255),
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_note OWNER TO keycloak;

--
-- Name: client_session_prot_mapper; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session_prot_mapper (
    protocol_mapper_id character varying(36) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_prot_mapper OWNER TO keycloak;

--
-- Name: client_session_role; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session_role (
    role_id character varying(255) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_role OWNER TO keycloak;

--
-- Name: client_user_session_note; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_user_session_note (
    name character varying(255) NOT NULL,
    value character varying(2048),
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_user_session_note OWNER TO keycloak;

--
-- Name: component; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.component (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_id character varying(36),
    provider_id character varying(36),
    provider_type character varying(255),
    realm_id character varying(36),
    sub_type character varying(255)
);


ALTER TABLE public.component OWNER TO keycloak;

--
-- Name: component_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.component_config (
    id character varying(36) NOT NULL,
    component_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(4000)
);


ALTER TABLE public.component_config OWNER TO keycloak;

--
-- Name: composite_role; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.composite_role (
    composite character varying(36) NOT NULL,
    child_role character varying(36) NOT NULL
);


ALTER TABLE public.composite_role OWNER TO keycloak;

--
-- Name: credential; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.credential (
    id character varying(36) NOT NULL,
    salt bytea,
    type character varying(255),
    user_id character varying(36),
    created_date bigint,
    user_label character varying(255),
    secret_data text,
    credential_data text,
    priority integer
);


ALTER TABLE public.credential OWNER TO keycloak;

--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO keycloak;

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO keycloak;

--
-- Name: default_client_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.default_client_scope (
    realm_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE public.default_client_scope OWNER TO keycloak;

--
-- Name: event_entity; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.event_entity (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    details_json character varying(2550),
    error character varying(255),
    ip_address character varying(255),
    realm_id character varying(255),
    session_id character varying(255),
    event_time bigint,
    type character varying(255),
    user_id character varying(255)
);


ALTER TABLE public.event_entity OWNER TO keycloak;

--
-- Name: fed_user_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_attribute (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    value character varying(2024)
);


ALTER TABLE public.fed_user_attribute OWNER TO keycloak;

--
-- Name: fed_user_consent; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE public.fed_user_consent OWNER TO keycloak;

--
-- Name: fed_user_consent_cl_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_consent_cl_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.fed_user_consent_cl_scope OWNER TO keycloak;

--
-- Name: fed_user_credential; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_credential (
    id character varying(36) NOT NULL,
    salt bytea,
    type character varying(255),
    created_date bigint,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    user_label character varying(255),
    secret_data text,
    credential_data text,
    priority integer
);


ALTER TABLE public.fed_user_credential OWNER TO keycloak;

--
-- Name: fed_user_group_membership; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_group_membership OWNER TO keycloak;

--
-- Name: fed_user_required_action; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_required_action (
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_required_action OWNER TO keycloak;

--
-- Name: fed_user_role_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_role_mapping (
    role_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_role_mapping OWNER TO keycloak;

--
-- Name: federated_identity; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.federated_identity (
    identity_provider character varying(255) NOT NULL,
    realm_id character varying(36),
    federated_user_id character varying(255),
    federated_username character varying(255),
    token text,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.federated_identity OWNER TO keycloak;

--
-- Name: federated_user; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.federated_user (
    id character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.federated_user OWNER TO keycloak;

--
-- Name: group_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.group_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.group_attribute OWNER TO keycloak;

--
-- Name: group_role_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.group_role_mapping (
    role_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.group_role_mapping OWNER TO keycloak;

--
-- Name: identity_provider; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.identity_provider (
    internal_id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    provider_alias character varying(255),
    provider_id character varying(255),
    store_token boolean DEFAULT false NOT NULL,
    authenticate_by_default boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    add_token_role boolean DEFAULT true NOT NULL,
    trust_email boolean DEFAULT false NOT NULL,
    first_broker_login_flow_id character varying(36),
    post_broker_login_flow_id character varying(36),
    provider_display_name character varying(255),
    link_only boolean DEFAULT false NOT NULL
);


ALTER TABLE public.identity_provider OWNER TO keycloak;

--
-- Name: identity_provider_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.identity_provider_config (
    identity_provider_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.identity_provider_config OWNER TO keycloak;

--
-- Name: identity_provider_mapper; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.identity_provider_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    idp_alias character varying(255) NOT NULL,
    idp_mapper_name character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.identity_provider_mapper OWNER TO keycloak;

--
-- Name: idp_mapper_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.idp_mapper_config (
    idp_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.idp_mapper_config OWNER TO keycloak;

--
-- Name: keycloak_group; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.keycloak_group (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_group character varying(36) NOT NULL,
    realm_id character varying(36)
);


ALTER TABLE public.keycloak_group OWNER TO keycloak;

--
-- Name: keycloak_role; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.keycloak_role (
    id character varying(36) NOT NULL,
    client_realm_constraint character varying(255),
    client_role boolean DEFAULT false NOT NULL,
    description character varying(255),
    name character varying(255),
    realm_id character varying(255),
    client character varying(36),
    realm character varying(36)
);


ALTER TABLE public.keycloak_role OWNER TO keycloak;

--
-- Name: migration_model; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.migration_model (
    id character varying(36) NOT NULL,
    version character varying(36),
    update_time bigint DEFAULT 0 NOT NULL
);


ALTER TABLE public.migration_model OWNER TO keycloak;

--
-- Name: offline_client_session; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.offline_client_session (
    user_session_id character varying(36) NOT NULL,
    client_id character varying(255) NOT NULL,
    offline_flag character varying(4) NOT NULL,
    "timestamp" integer,
    data text,
    client_storage_provider character varying(36) DEFAULT 'local'::character varying NOT NULL,
    external_client_id character varying(255) DEFAULT 'local'::character varying NOT NULL
);


ALTER TABLE public.offline_client_session OWNER TO keycloak;

--
-- Name: offline_user_session; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.offline_user_session (
    user_session_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    created_on integer NOT NULL,
    offline_flag character varying(4) NOT NULL,
    data text,
    last_session_refresh integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.offline_user_session OWNER TO keycloak;

--
-- Name: policy_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.policy_config (
    policy_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value text
);


ALTER TABLE public.policy_config OWNER TO keycloak;

--
-- Name: protocol_mapper; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.protocol_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    protocol character varying(255) NOT NULL,
    protocol_mapper_name character varying(255) NOT NULL,
    client_id character varying(36),
    client_scope_id character varying(36)
);


ALTER TABLE public.protocol_mapper OWNER TO keycloak;

--
-- Name: protocol_mapper_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.protocol_mapper_config (
    protocol_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.protocol_mapper_config OWNER TO keycloak;

--
-- Name: realm; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm (
    id character varying(36) NOT NULL,
    access_code_lifespan integer,
    user_action_lifespan integer,
    access_token_lifespan integer,
    account_theme character varying(255),
    admin_theme character varying(255),
    email_theme character varying(255),
    enabled boolean DEFAULT false NOT NULL,
    events_enabled boolean DEFAULT false NOT NULL,
    events_expiration bigint,
    login_theme character varying(255),
    name character varying(255),
    not_before integer,
    password_policy character varying(2550),
    registration_allowed boolean DEFAULT false NOT NULL,
    remember_me boolean DEFAULT false NOT NULL,
    reset_password_allowed boolean DEFAULT false NOT NULL,
    social boolean DEFAULT false NOT NULL,
    ssl_required character varying(255),
    sso_idle_timeout integer,
    sso_max_lifespan integer,
    update_profile_on_soc_login boolean DEFAULT false NOT NULL,
    verify_email boolean DEFAULT false NOT NULL,
    master_admin_client character varying(36),
    login_lifespan integer,
    internationalization_enabled boolean DEFAULT false NOT NULL,
    default_locale character varying(255),
    reg_email_as_username boolean DEFAULT false NOT NULL,
    admin_events_enabled boolean DEFAULT false NOT NULL,
    admin_events_details_enabled boolean DEFAULT false NOT NULL,
    edit_username_allowed boolean DEFAULT false NOT NULL,
    otp_policy_counter integer DEFAULT 0,
    otp_policy_window integer DEFAULT 1,
    otp_policy_period integer DEFAULT 30,
    otp_policy_digits integer DEFAULT 6,
    otp_policy_alg character varying(36) DEFAULT 'HmacSHA1'::character varying,
    otp_policy_type character varying(36) DEFAULT 'totp'::character varying,
    browser_flow character varying(36),
    registration_flow character varying(36),
    direct_grant_flow character varying(36),
    reset_credentials_flow character varying(36),
    client_auth_flow character varying(36),
    offline_session_idle_timeout integer DEFAULT 0,
    revoke_refresh_token boolean DEFAULT false NOT NULL,
    access_token_life_implicit integer DEFAULT 0,
    login_with_email_allowed boolean DEFAULT true NOT NULL,
    duplicate_emails_allowed boolean DEFAULT false NOT NULL,
    docker_auth_flow character varying(36),
    refresh_token_max_reuse integer DEFAULT 0,
    allow_user_managed_access boolean DEFAULT false NOT NULL,
    sso_max_lifespan_remember_me integer DEFAULT 0 NOT NULL,
    sso_idle_timeout_remember_me integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.realm OWNER TO keycloak;

--
-- Name: realm_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_attribute (
    name character varying(255) NOT NULL,
    value character varying(255),
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_attribute OWNER TO keycloak;

--
-- Name: realm_default_groups; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_default_groups (
    realm_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_default_groups OWNER TO keycloak;

--
-- Name: realm_default_roles; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_default_roles (
    realm_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_default_roles OWNER TO keycloak;

--
-- Name: realm_enabled_event_types; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_enabled_event_types (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_enabled_event_types OWNER TO keycloak;

--
-- Name: realm_events_listeners; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_events_listeners (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_events_listeners OWNER TO keycloak;

--
-- Name: realm_localizations; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_localizations (
    realm_id character varying(255) NOT NULL,
    locale character varying(255) NOT NULL,
    texts text NOT NULL
);


ALTER TABLE public.realm_localizations OWNER TO keycloak;

--
-- Name: realm_required_credential; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_required_credential (
    type character varying(255) NOT NULL,
    form_label character varying(255),
    input boolean DEFAULT false NOT NULL,
    secret boolean DEFAULT false NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_required_credential OWNER TO keycloak;

--
-- Name: realm_smtp_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_smtp_config (
    realm_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.realm_smtp_config OWNER TO keycloak;

--
-- Name: realm_supported_locales; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_supported_locales (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_supported_locales OWNER TO keycloak;

--
-- Name: redirect_uris; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.redirect_uris (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.redirect_uris OWNER TO keycloak;

--
-- Name: required_action_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.required_action_config (
    required_action_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.required_action_config OWNER TO keycloak;

--
-- Name: required_action_provider; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.required_action_provider (
    id character varying(36) NOT NULL,
    alias character varying(255),
    name character varying(255),
    realm_id character varying(36),
    enabled boolean DEFAULT false NOT NULL,
    default_action boolean DEFAULT false NOT NULL,
    provider_id character varying(255),
    priority integer
);


ALTER TABLE public.required_action_provider OWNER TO keycloak;

--
-- Name: resource_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    resource_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_attribute OWNER TO keycloak;

--
-- Name: resource_policy; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_policy (
    resource_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_policy OWNER TO keycloak;

--
-- Name: resource_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_scope (
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_scope OWNER TO keycloak;

--
-- Name: resource_server; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server (
    id character varying(36) NOT NULL,
    allow_rs_remote_mgmt boolean DEFAULT false NOT NULL,
    policy_enforce_mode character varying(15) NOT NULL,
    decision_strategy smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.resource_server OWNER TO keycloak;

--
-- Name: resource_server_perm_ticket; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server_perm_ticket (
    id character varying(36) NOT NULL,
    owner character varying(255) NOT NULL,
    requester character varying(255) NOT NULL,
    created_timestamp bigint NOT NULL,
    granted_timestamp bigint,
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36),
    resource_server_id character varying(36) NOT NULL,
    policy_id character varying(36)
);


ALTER TABLE public.resource_server_perm_ticket OWNER TO keycloak;

--
-- Name: resource_server_policy; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server_policy (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    type character varying(255) NOT NULL,
    decision_strategy character varying(20),
    logic character varying(20),
    resource_server_id character varying(36) NOT NULL,
    owner character varying(255)
);


ALTER TABLE public.resource_server_policy OWNER TO keycloak;

--
-- Name: resource_server_resource; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server_resource (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255),
    icon_uri character varying(255),
    owner character varying(255) NOT NULL,
    resource_server_id character varying(36) NOT NULL,
    owner_managed_access boolean DEFAULT false NOT NULL,
    display_name character varying(255)
);


ALTER TABLE public.resource_server_resource OWNER TO keycloak;

--
-- Name: resource_server_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server_scope (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    icon_uri character varying(255),
    resource_server_id character varying(36) NOT NULL,
    display_name character varying(255)
);


ALTER TABLE public.resource_server_scope OWNER TO keycloak;

--
-- Name: resource_uris; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_uris (
    resource_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.resource_uris OWNER TO keycloak;

--
-- Name: role_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.role_attribute (
    id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255)
);


ALTER TABLE public.role_attribute OWNER TO keycloak;

--
-- Name: scope_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.scope_mapping (
    client_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.scope_mapping OWNER TO keycloak;

--
-- Name: scope_policy; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.scope_policy (
    scope_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE public.scope_policy OWNER TO keycloak;

--
-- Name: user_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_attribute (
    name character varying(255) NOT NULL,
    value character varying(255),
    user_id character varying(36) NOT NULL,
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL
);


ALTER TABLE public.user_attribute OWNER TO keycloak;

--
-- Name: user_consent; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    user_id character varying(36) NOT NULL,
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE public.user_consent OWNER TO keycloak;

--
-- Name: user_consent_client_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_consent_client_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.user_consent_client_scope OWNER TO keycloak;

--
-- Name: user_entity; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_entity (
    id character varying(36) NOT NULL,
    email character varying(255),
    email_constraint character varying(255),
    email_verified boolean DEFAULT false NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    federation_link character varying(255),
    first_name character varying(255),
    last_name character varying(255),
    realm_id character varying(255),
    username character varying(255),
    created_timestamp bigint,
    service_account_client_link character varying(255),
    not_before integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.user_entity OWNER TO keycloak;

--
-- Name: user_federation_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_federation_config (
    user_federation_provider_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_federation_config OWNER TO keycloak;

--
-- Name: user_federation_mapper; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_federation_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    federation_provider_id character varying(36) NOT NULL,
    federation_mapper_type character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.user_federation_mapper OWNER TO keycloak;

--
-- Name: user_federation_mapper_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_federation_mapper_config (
    user_federation_mapper_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_federation_mapper_config OWNER TO keycloak;

--
-- Name: user_federation_provider; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_federation_provider (
    id character varying(36) NOT NULL,
    changed_sync_period integer,
    display_name character varying(255),
    full_sync_period integer,
    last_sync integer,
    priority integer,
    provider_name character varying(255),
    realm_id character varying(36)
);


ALTER TABLE public.user_federation_provider OWNER TO keycloak;

--
-- Name: user_group_membership; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.user_group_membership OWNER TO keycloak;

--
-- Name: user_required_action; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_required_action (
    user_id character varying(36) NOT NULL,
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL
);


ALTER TABLE public.user_required_action OWNER TO keycloak;

--
-- Name: user_role_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_role_mapping (
    role_id character varying(255) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.user_role_mapping OWNER TO keycloak;

--
-- Name: user_session; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_session (
    id character varying(36) NOT NULL,
    auth_method character varying(255),
    ip_address character varying(255),
    last_session_refresh integer,
    login_username character varying(255),
    realm_id character varying(255),
    remember_me boolean DEFAULT false NOT NULL,
    started integer,
    user_id character varying(255),
    user_session_state integer,
    broker_session_id character varying(255),
    broker_user_id character varying(255)
);


ALTER TABLE public.user_session OWNER TO keycloak;

--
-- Name: user_session_note; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_session_note (
    user_session character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(2048)
);


ALTER TABLE public.user_session_note OWNER TO keycloak;

--
-- Name: username_login_failure; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.username_login_failure (
    realm_id character varying(36) NOT NULL,
    username character varying(255) NOT NULL,
    failed_login_not_before integer,
    last_failure bigint,
    last_ip_failure character varying(255),
    num_failures integer
);


ALTER TABLE public.username_login_failure OWNER TO keycloak;

--
-- Name: web_origins; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.web_origins (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.web_origins OWNER TO keycloak;

--
-- Data for Name: admin_event_entity; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.admin_event_entity (id, admin_event_time, realm_id, operation_type, auth_realm_id, auth_client_id, auth_user_id, ip_address, resource_path, representation, error, resource_type) FROM stdin;
\.


--
-- Data for Name: associated_policy; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.associated_policy (policy_id, associated_policy_id) FROM stdin;
b6d2a66e-09d7-456f-8c28-00e832964991	ba024e17-b552-473c-b613-dc79754ae21c
\.


--
-- Data for Name: authentication_execution; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.authentication_execution (id, alias, authenticator, realm_id, flow_id, requirement, priority, authenticator_flow, auth_flow_id, auth_config) FROM stdin;
e927f2b0-ae78-47f7-8066-4c3a54945846	\N	auth-cookie	master	44b0203e-2d28-4ad8-b936-58ae331c52e9	2	10	f	\N	\N
ede700f4-9386-4807-8a3c-6ebe1942ce1e	\N	auth-spnego	master	44b0203e-2d28-4ad8-b936-58ae331c52e9	3	20	f	\N	\N
bcda2338-9efe-4b5e-8bf7-938e965ad1a1	\N	identity-provider-redirector	master	44b0203e-2d28-4ad8-b936-58ae331c52e9	2	25	f	\N	\N
99250d09-d8d7-478b-916a-34fa1006fbd0	\N	\N	master	44b0203e-2d28-4ad8-b936-58ae331c52e9	2	30	t	9f21d0cc-5b2d-4e54-afb7-3a089b734bdd	\N
8ed155aa-4594-49c6-95f5-75c6802ddbb3	\N	auth-username-password-form	master	9f21d0cc-5b2d-4e54-afb7-3a089b734bdd	0	10	f	\N	\N
b673d0a0-0fc7-4a9f-806f-591936667825	\N	\N	master	9f21d0cc-5b2d-4e54-afb7-3a089b734bdd	1	20	t	58795b9b-589c-42ad-94e9-cd315a3a39db	\N
bf435c75-90a0-41a0-9856-c680c094a8d1	\N	conditional-user-configured	master	58795b9b-589c-42ad-94e9-cd315a3a39db	0	10	f	\N	\N
3fae60b7-f8e0-4275-a380-03ff6e649777	\N	auth-otp-form	master	58795b9b-589c-42ad-94e9-cd315a3a39db	0	20	f	\N	\N
8846147b-4aa2-4347-b3e6-1488d9818f4c	\N	direct-grant-validate-username	master	aeaba989-2a4f-4797-ab1b-ed4732f8c03b	0	10	f	\N	\N
864c2489-b267-4cac-8ad1-e9da4711bcc2	\N	direct-grant-validate-password	master	aeaba989-2a4f-4797-ab1b-ed4732f8c03b	0	20	f	\N	\N
bebc1d58-5737-468d-a4ad-cb47639c003b	\N	\N	master	aeaba989-2a4f-4797-ab1b-ed4732f8c03b	1	30	t	880cfdd6-41a5-4b2e-bbc5-1cc14fc3934a	\N
c2dd6ceb-191d-429f-82ad-d2e9435a9d7c	\N	conditional-user-configured	master	880cfdd6-41a5-4b2e-bbc5-1cc14fc3934a	0	10	f	\N	\N
b08f558b-1ff5-4792-824d-3a706ec28c6b	\N	direct-grant-validate-otp	master	880cfdd6-41a5-4b2e-bbc5-1cc14fc3934a	0	20	f	\N	\N
714a891f-b627-4be9-b2b7-f1b06114b759	\N	registration-page-form	master	b8d99c77-c010-4497-867a-4089ecefdec5	0	10	t	0d0df430-b34d-411f-b730-53d536784ecb	\N
5d1d0b43-ca89-4239-9c51-5058ffa7ade9	\N	registration-user-creation	master	0d0df430-b34d-411f-b730-53d536784ecb	0	20	f	\N	\N
8b9e8e01-2a21-49b3-a8ec-c5f118cc8fd6	\N	registration-profile-action	master	0d0df430-b34d-411f-b730-53d536784ecb	0	40	f	\N	\N
61266b33-7459-4cdb-88a0-5948a60b8caf	\N	registration-password-action	master	0d0df430-b34d-411f-b730-53d536784ecb	0	50	f	\N	\N
8805dde6-58b0-4b91-ad99-5045d57e7c7d	\N	registration-recaptcha-action	master	0d0df430-b34d-411f-b730-53d536784ecb	3	60	f	\N	\N
7d83e111-94dc-42e1-87a7-5910454c230b	\N	reset-credentials-choose-user	master	724537d7-cab6-43d6-8d2e-29128e278d9c	0	10	f	\N	\N
67c225ee-ce54-4d5f-8a02-4426e6202cc8	\N	reset-credential-email	master	724537d7-cab6-43d6-8d2e-29128e278d9c	0	20	f	\N	\N
735a7a51-2c59-41b7-a13f-e9cb45470134	\N	reset-password	master	724537d7-cab6-43d6-8d2e-29128e278d9c	0	30	f	\N	\N
47b3becd-d14e-43b3-9633-96ccb7b4c041	\N	\N	master	724537d7-cab6-43d6-8d2e-29128e278d9c	1	40	t	0302b9f3-e102-4e44-94ee-ea4c02ac2921	\N
918d821d-d33e-4b6f-a080-5ce7a69c1bf7	\N	conditional-user-configured	master	0302b9f3-e102-4e44-94ee-ea4c02ac2921	0	10	f	\N	\N
f687ac5c-f257-475b-ac55-87dfc1b604ad	\N	reset-otp	master	0302b9f3-e102-4e44-94ee-ea4c02ac2921	0	20	f	\N	\N
2d1827e2-e7dc-4c79-9bcb-428d9f617e94	\N	client-secret	master	caad2fba-0513-4f99-892d-eb52617c9feb	2	10	f	\N	\N
069c849c-bd06-4c01-81a6-b731a2a4dcf1	\N	client-jwt	master	caad2fba-0513-4f99-892d-eb52617c9feb	2	20	f	\N	\N
559d14a5-4166-490f-903d-0b689f374b5d	\N	client-secret-jwt	master	caad2fba-0513-4f99-892d-eb52617c9feb	2	30	f	\N	\N
371312dd-be9b-4c6d-9ea9-9b4751c77c52	\N	client-x509	master	caad2fba-0513-4f99-892d-eb52617c9feb	2	40	f	\N	\N
35c381da-7b70-4473-a64a-8af0a6af5853	\N	idp-review-profile	master	bfe7f4f1-42c6-405e-b794-5d1b0a038da1	0	10	f	\N	28ae8d99-3309-426e-abaa-69f58c09afa7
675c82b9-9601-4ac1-a44f-72292480c018	\N	\N	master	bfe7f4f1-42c6-405e-b794-5d1b0a038da1	0	20	t	281a8e45-98a0-4548-b8db-12425200815c	\N
d734347b-2252-48f7-a9c9-0f46d9344f95	\N	idp-create-user-if-unique	master	281a8e45-98a0-4548-b8db-12425200815c	2	10	f	\N	74d5ff30-a0b1-4752-bf50-a6ad3ed37cef
58521f60-25f0-485e-bdb9-e2cd5c9fce97	\N	\N	master	281a8e45-98a0-4548-b8db-12425200815c	2	20	t	d753d574-b1ad-490f-aacc-153635210ea1	\N
59484213-e68a-41a2-a5b2-036df14775f4	\N	idp-confirm-link	master	d753d574-b1ad-490f-aacc-153635210ea1	0	10	f	\N	\N
a33d7199-65c7-43da-99ba-4e263b7fa0ad	\N	\N	master	d753d574-b1ad-490f-aacc-153635210ea1	0	20	t	71e199ba-d001-436c-ad28-65570af2001e	\N
fe25917f-1b3d-404c-b4d9-8ef00070a0ff	\N	idp-email-verification	master	71e199ba-d001-436c-ad28-65570af2001e	2	10	f	\N	\N
5af9207b-810f-4cfe-8ea6-e2910744c69a	\N	\N	master	71e199ba-d001-436c-ad28-65570af2001e	2	20	t	c84b9b40-0d5c-445d-b5a3-a9c631df57b1	\N
22f1ab3d-98cc-41fb-b6ff-c0d5c5424ca1	\N	idp-username-password-form	master	c84b9b40-0d5c-445d-b5a3-a9c631df57b1	0	10	f	\N	\N
3f6df2a5-f5c7-4179-b04d-70a0324d2203	\N	\N	master	c84b9b40-0d5c-445d-b5a3-a9c631df57b1	1	20	t	fc572caf-707c-40e9-9c8d-25bc2d0bc9f3	\N
149e4c00-13c5-4e9f-b232-48a17fbc0803	\N	conditional-user-configured	master	fc572caf-707c-40e9-9c8d-25bc2d0bc9f3	0	10	f	\N	\N
cbc65abe-4c5a-4ca4-921f-75a9c3e5f0b2	\N	auth-otp-form	master	fc572caf-707c-40e9-9c8d-25bc2d0bc9f3	0	20	f	\N	\N
fc8c5b4a-0ccc-4bfb-a871-e05eb2f1653b	\N	http-basic-authenticator	master	c6c56271-f629-4d7c-9991-ec05bcba8b3c	0	10	f	\N	\N
87f24cec-5c60-4379-a2b7-e0fb7ceab6c7	\N	docker-http-basic-authenticator	master	761db40d-59c3-44e0-a5d0-358be36c7937	0	10	f	\N	\N
384ac9e6-3791-4de0-a5a0-5b596116c8ae	\N	no-cookie-redirect	master	de20a144-6d30-4b31-b217-d33995dd0c07	0	10	f	\N	\N
87cacf72-54f2-4aee-b662-5a704a5408f5	\N	\N	master	de20a144-6d30-4b31-b217-d33995dd0c07	0	20	t	17815672-c3c0-4820-84f0-133886e68a9c	\N
06c27fa3-4a91-4dcb-9665-86188ac7f082	\N	basic-auth	master	17815672-c3c0-4820-84f0-133886e68a9c	0	10	f	\N	\N
f342c19d-5af6-47c2-9174-f44ba712542b	\N	basic-auth-otp	master	17815672-c3c0-4820-84f0-133886e68a9c	3	20	f	\N	\N
ce999fac-5698-4c08-8528-c6399c432576	\N	auth-spnego	master	17815672-c3c0-4820-84f0-133886e68a9c	3	30	f	\N	\N
381315e3-2055-4a55-b735-bd357b0fba52	\N	idp-email-verification	my-everyday-lolita-realm	5e517997-0757-4690-8c70-805f15121ace	2	10	f	\N	\N
1ffad91e-de25-4442-a3e3-b78fba22f5b0	\N	\N	my-everyday-lolita-realm	5e517997-0757-4690-8c70-805f15121ace	2	20	t	7f8afff0-f44c-45c4-95da-33f361dfc82a	\N
1fe0d650-53ca-4191-acf3-d97aecee618f	\N	basic-auth	my-everyday-lolita-realm	e41774f9-7190-4aee-92fb-080f0145370e	0	10	f	\N	\N
f0ff06ce-3cd4-42a3-bc72-80f19adfa389	\N	basic-auth-otp	my-everyday-lolita-realm	e41774f9-7190-4aee-92fb-080f0145370e	3	20	f	\N	\N
82444335-9843-4087-ab88-4375b153abcf	\N	auth-spnego	my-everyday-lolita-realm	e41774f9-7190-4aee-92fb-080f0145370e	3	30	f	\N	\N
1e4ca2f9-bd9a-4bff-b55d-96e93358f08b	\N	conditional-user-configured	my-everyday-lolita-realm	92d0bde1-a588-42ff-8005-a695bbb8df8e	0	10	f	\N	\N
3096873d-b308-4e53-aea5-e2a8e2fda368	\N	auth-otp-form	my-everyday-lolita-realm	92d0bde1-a588-42ff-8005-a695bbb8df8e	0	20	f	\N	\N
6d255f09-b6b3-431f-b39b-03d985b11610	\N	conditional-user-configured	my-everyday-lolita-realm	af95383c-84bc-41ef-b6e6-cc3416b8c5b2	0	10	f	\N	\N
c258511c-b211-4078-8e32-516bfcf7b4bc	\N	direct-grant-validate-otp	my-everyday-lolita-realm	af95383c-84bc-41ef-b6e6-cc3416b8c5b2	0	20	f	\N	\N
ea5efae8-ad80-40a4-8a20-336b0303cc46	\N	conditional-user-configured	my-everyday-lolita-realm	6f45f0ab-9c52-4c09-966c-9dac1fe2b84c	0	10	f	\N	\N
1450b936-bbc0-4bf8-939b-460ffa54d9c5	\N	auth-otp-form	my-everyday-lolita-realm	6f45f0ab-9c52-4c09-966c-9dac1fe2b84c	0	20	f	\N	\N
09902201-b241-48c0-a8e7-1b64e433fb1b	\N	idp-confirm-link	my-everyday-lolita-realm	48fbfc7f-d753-4a4f-9e86-de8dc3f25fd7	0	10	f	\N	\N
88d52eb4-5fa0-40fb-a941-3aa5e138b391	\N	\N	my-everyday-lolita-realm	48fbfc7f-d753-4a4f-9e86-de8dc3f25fd7	0	20	t	5e517997-0757-4690-8c70-805f15121ace	\N
a848d83a-b369-44ea-8ad7-f10371f98958	\N	conditional-user-configured	my-everyday-lolita-realm	0b6b0aab-39f1-43d2-8408-35b3b3b29d60	0	10	f	\N	\N
ecda3c85-08e4-4fa8-bae7-4225b68bad6f	\N	reset-otp	my-everyday-lolita-realm	0b6b0aab-39f1-43d2-8408-35b3b3b29d60	0	20	f	\N	\N
2b7948d4-f0c0-4fed-9606-9f00e797464c	\N	idp-create-user-if-unique	my-everyday-lolita-realm	55c9ddd1-520d-4542-a110-fb60e06d9b6a	2	10	f	\N	f0d7929d-a03a-4f42-8666-86359b815ffc
2d3fc524-02d0-479c-b4e8-0d5e4901dca4	\N	\N	my-everyday-lolita-realm	55c9ddd1-520d-4542-a110-fb60e06d9b6a	2	20	t	48fbfc7f-d753-4a4f-9e86-de8dc3f25fd7	\N
e378b7d6-e11b-4f8f-880f-188b36756681	\N	idp-username-password-form	my-everyday-lolita-realm	7f8afff0-f44c-45c4-95da-33f361dfc82a	0	10	f	\N	\N
33eff2bc-f567-40ab-821b-d1b9dee43699	\N	\N	my-everyday-lolita-realm	7f8afff0-f44c-45c4-95da-33f361dfc82a	1	20	t	6f45f0ab-9c52-4c09-966c-9dac1fe2b84c	\N
17618363-ed0a-4acc-a87e-819f1f398698	\N	auth-cookie	my-everyday-lolita-realm	90e3d452-2eba-41c1-ac24-02509ff5d30f	2	10	f	\N	\N
ce33b0ad-4364-4c28-a373-da7d1342b0f7	\N	auth-spnego	my-everyday-lolita-realm	90e3d452-2eba-41c1-ac24-02509ff5d30f	3	20	f	\N	\N
2bc42d3e-3096-4b3a-bbea-4e134d2c5aa3	\N	identity-provider-redirector	my-everyday-lolita-realm	90e3d452-2eba-41c1-ac24-02509ff5d30f	2	25	f	\N	\N
7d939f67-4d71-4ef2-8b36-0a0e680bae42	\N	\N	my-everyday-lolita-realm	90e3d452-2eba-41c1-ac24-02509ff5d30f	2	30	t	7a18ad14-e1f2-4745-becb-a2fd08e41444	\N
3015d7aa-25f5-4ac6-ba85-22f3de84ec58	\N	client-secret	my-everyday-lolita-realm	ab5a9b6b-0f73-4846-93d7-f248f11c61bf	2	10	f	\N	\N
548503b1-a3d0-42ce-b0a0-5fe810b91f97	\N	client-jwt	my-everyday-lolita-realm	ab5a9b6b-0f73-4846-93d7-f248f11c61bf	2	20	f	\N	\N
2c05bfa5-3999-4b24-ab93-a9ecf6c1bdfc	\N	client-secret-jwt	my-everyday-lolita-realm	ab5a9b6b-0f73-4846-93d7-f248f11c61bf	2	30	f	\N	\N
e916d4bf-966c-47e6-a3bb-0167325140c0	\N	client-x509	my-everyday-lolita-realm	ab5a9b6b-0f73-4846-93d7-f248f11c61bf	2	40	f	\N	\N
7be33039-09a2-4e3f-b237-29079865bc32	\N	direct-grant-validate-username	my-everyday-lolita-realm	692b336c-5e29-45fc-bb76-fb36073eb320	0	10	f	\N	\N
f50e3a2e-b661-48af-b2c1-4644e4d020b6	\N	direct-grant-validate-password	my-everyday-lolita-realm	692b336c-5e29-45fc-bb76-fb36073eb320	0	20	f	\N	\N
26ee56fe-82a3-4314-ac96-a54fc5fa6478	\N	\N	my-everyday-lolita-realm	692b336c-5e29-45fc-bb76-fb36073eb320	1	30	t	af95383c-84bc-41ef-b6e6-cc3416b8c5b2	\N
2e60eb9a-26c5-486b-8962-552b387e89ac	\N	docker-http-basic-authenticator	my-everyday-lolita-realm	0545ff13-efe9-4bc0-8db3-3e3162da5d91	0	10	f	\N	\N
551e0add-a845-4e0e-9a9c-3408f01b77ea	\N	idp-review-profile	my-everyday-lolita-realm	9fd32a89-7a19-44ef-9c0d-df5e2c900bbb	0	10	f	\N	3b2a3084-e505-446e-ace6-a1ef97cdc9a2
0e9c1c17-49ee-46b2-b55d-5901a3b3810e	\N	\N	my-everyday-lolita-realm	9fd32a89-7a19-44ef-9c0d-df5e2c900bbb	0	20	t	55c9ddd1-520d-4542-a110-fb60e06d9b6a	\N
a5baca89-0433-4c2a-8c3e-cfeed995f7f6	\N	auth-username-password-form	my-everyday-lolita-realm	7a18ad14-e1f2-4745-becb-a2fd08e41444	0	10	f	\N	\N
2d6fc549-5390-42e8-9274-f19424e6fb48	\N	\N	my-everyday-lolita-realm	7a18ad14-e1f2-4745-becb-a2fd08e41444	1	20	t	92d0bde1-a588-42ff-8005-a695bbb8df8e	\N
092b3d47-5abd-47b4-b814-f0541bbe3b81	\N	no-cookie-redirect	my-everyday-lolita-realm	d65d1750-d75e-4103-87bf-ade6dacffd9c	0	10	f	\N	\N
de47f42a-95ea-42cf-842c-4cd966078976	\N	\N	my-everyday-lolita-realm	d65d1750-d75e-4103-87bf-ade6dacffd9c	0	20	t	e41774f9-7190-4aee-92fb-080f0145370e	\N
cbd1d655-002a-4b21-bd90-be77ac49ab24	\N	registration-page-form	my-everyday-lolita-realm	7c0e0550-cd7f-4531-b711-c6a6980ce31e	0	10	t	7e2aa6f6-4813-48c5-9bdb-bc1fd6cf2db5	\N
40b8b418-a08c-46b7-aaa9-d96c589cdf72	\N	registration-user-creation	my-everyday-lolita-realm	7e2aa6f6-4813-48c5-9bdb-bc1fd6cf2db5	0	20	f	\N	\N
a7fd1b7d-5c69-4db5-b975-acde982c0cbc	\N	registration-profile-action	my-everyday-lolita-realm	7e2aa6f6-4813-48c5-9bdb-bc1fd6cf2db5	0	40	f	\N	\N
1bf7f782-aea0-4bb6-abfc-b062fad527be	\N	registration-password-action	my-everyday-lolita-realm	7e2aa6f6-4813-48c5-9bdb-bc1fd6cf2db5	0	50	f	\N	\N
f8db6847-b5d7-421f-9238-86f81a044775	\N	registration-recaptcha-action	my-everyday-lolita-realm	7e2aa6f6-4813-48c5-9bdb-bc1fd6cf2db5	3	60	f	\N	\N
175e5c76-4609-46f9-9178-e63ed42c48f9	\N	reset-credentials-choose-user	my-everyday-lolita-realm	6ed585ee-39c4-429e-ade7-ba85e4111bd0	0	10	f	\N	\N
77921a0e-e829-4dec-bf3b-9166df6efc71	\N	reset-credential-email	my-everyday-lolita-realm	6ed585ee-39c4-429e-ade7-ba85e4111bd0	0	20	f	\N	\N
4609660b-de13-49c6-ace3-0d977d68e3f7	\N	reset-password	my-everyday-lolita-realm	6ed585ee-39c4-429e-ade7-ba85e4111bd0	0	30	f	\N	\N
77623bda-7650-4c30-b49a-72ec2b46c545	\N	\N	my-everyday-lolita-realm	6ed585ee-39c4-429e-ade7-ba85e4111bd0	1	40	t	0b6b0aab-39f1-43d2-8408-35b3b3b29d60	\N
571fe631-9515-4eb7-b40f-669bbbfcfca8	\N	http-basic-authenticator	my-everyday-lolita-realm	adbdd657-1e53-44f6-9127-3b4c6b614293	0	10	f	\N	\N
\.


--
-- Data for Name: authentication_flow; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.authentication_flow (id, alias, description, realm_id, provider_id, top_level, built_in) FROM stdin;
44b0203e-2d28-4ad8-b936-58ae331c52e9	browser	browser based authentication	master	basic-flow	t	t
9f21d0cc-5b2d-4e54-afb7-3a089b734bdd	forms	Username, password, otp and other auth forms.	master	basic-flow	f	t
58795b9b-589c-42ad-94e9-cd315a3a39db	Browser - Conditional OTP	Flow to determine if the OTP is required for the authentication	master	basic-flow	f	t
aeaba989-2a4f-4797-ab1b-ed4732f8c03b	direct grant	OpenID Connect Resource Owner Grant	master	basic-flow	t	t
880cfdd6-41a5-4b2e-bbc5-1cc14fc3934a	Direct Grant - Conditional OTP	Flow to determine if the OTP is required for the authentication	master	basic-flow	f	t
b8d99c77-c010-4497-867a-4089ecefdec5	registration	registration flow	master	basic-flow	t	t
0d0df430-b34d-411f-b730-53d536784ecb	registration form	registration form	master	form-flow	f	t
724537d7-cab6-43d6-8d2e-29128e278d9c	reset credentials	Reset credentials for a user if they forgot their password or something	master	basic-flow	t	t
0302b9f3-e102-4e44-94ee-ea4c02ac2921	Reset - Conditional OTP	Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.	master	basic-flow	f	t
caad2fba-0513-4f99-892d-eb52617c9feb	clients	Base authentication for clients	master	client-flow	t	t
bfe7f4f1-42c6-405e-b794-5d1b0a038da1	first broker login	Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account	master	basic-flow	t	t
281a8e45-98a0-4548-b8db-12425200815c	User creation or linking	Flow for the existing/non-existing user alternatives	master	basic-flow	f	t
d753d574-b1ad-490f-aacc-153635210ea1	Handle Existing Account	Handle what to do if there is existing account with same email/username like authenticated identity provider	master	basic-flow	f	t
71e199ba-d001-436c-ad28-65570af2001e	Account verification options	Method with which to verity the existing account	master	basic-flow	f	t
c84b9b40-0d5c-445d-b5a3-a9c631df57b1	Verify Existing Account by Re-authentication	Reauthentication of existing account	master	basic-flow	f	t
fc572caf-707c-40e9-9c8d-25bc2d0bc9f3	First broker login - Conditional OTP	Flow to determine if the OTP is required for the authentication	master	basic-flow	f	t
c6c56271-f629-4d7c-9991-ec05bcba8b3c	saml ecp	SAML ECP Profile Authentication Flow	master	basic-flow	t	t
761db40d-59c3-44e0-a5d0-358be36c7937	docker auth	Used by Docker clients to authenticate against the IDP	master	basic-flow	t	t
de20a144-6d30-4b31-b217-d33995dd0c07	http challenge	An authentication flow based on challenge-response HTTP Authentication Schemes	master	basic-flow	t	t
17815672-c3c0-4820-84f0-133886e68a9c	Authentication Options	Authentication options.	master	basic-flow	f	t
5e517997-0757-4690-8c70-805f15121ace	Account verification options	Method with which to verity the existing account	my-everyday-lolita-realm	basic-flow	f	t
e41774f9-7190-4aee-92fb-080f0145370e	Authentication Options	Authentication options.	my-everyday-lolita-realm	basic-flow	f	t
92d0bde1-a588-42ff-8005-a695bbb8df8e	Browser - Conditional OTP	Flow to determine if the OTP is required for the authentication	my-everyday-lolita-realm	basic-flow	f	t
af95383c-84bc-41ef-b6e6-cc3416b8c5b2	Direct Grant - Conditional OTP	Flow to determine if the OTP is required for the authentication	my-everyday-lolita-realm	basic-flow	f	t
6f45f0ab-9c52-4c09-966c-9dac1fe2b84c	First broker login - Conditional OTP	Flow to determine if the OTP is required for the authentication	my-everyday-lolita-realm	basic-flow	f	t
48fbfc7f-d753-4a4f-9e86-de8dc3f25fd7	Handle Existing Account	Handle what to do if there is existing account with same email/username like authenticated identity provider	my-everyday-lolita-realm	basic-flow	f	t
0b6b0aab-39f1-43d2-8408-35b3b3b29d60	Reset - Conditional OTP	Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.	my-everyday-lolita-realm	basic-flow	f	t
55c9ddd1-520d-4542-a110-fb60e06d9b6a	User creation or linking	Flow for the existing/non-existing user alternatives	my-everyday-lolita-realm	basic-flow	f	t
7f8afff0-f44c-45c4-95da-33f361dfc82a	Verify Existing Account by Re-authentication	Reauthentication of existing account	my-everyday-lolita-realm	basic-flow	f	t
90e3d452-2eba-41c1-ac24-02509ff5d30f	browser	browser based authentication	my-everyday-lolita-realm	basic-flow	t	t
ab5a9b6b-0f73-4846-93d7-f248f11c61bf	clients	Base authentication for clients	my-everyday-lolita-realm	client-flow	t	t
692b336c-5e29-45fc-bb76-fb36073eb320	direct grant	OpenID Connect Resource Owner Grant	my-everyday-lolita-realm	basic-flow	t	t
0545ff13-efe9-4bc0-8db3-3e3162da5d91	docker auth	Used by Docker clients to authenticate against the IDP	my-everyday-lolita-realm	basic-flow	t	t
9fd32a89-7a19-44ef-9c0d-df5e2c900bbb	first broker login	Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account	my-everyday-lolita-realm	basic-flow	t	t
7a18ad14-e1f2-4745-becb-a2fd08e41444	forms	Username, password, otp and other auth forms.	my-everyday-lolita-realm	basic-flow	f	t
d65d1750-d75e-4103-87bf-ade6dacffd9c	http challenge	An authentication flow based on challenge-response HTTP Authentication Schemes	my-everyday-lolita-realm	basic-flow	t	t
7c0e0550-cd7f-4531-b711-c6a6980ce31e	registration	registration flow	my-everyday-lolita-realm	basic-flow	t	t
7e2aa6f6-4813-48c5-9bdb-bc1fd6cf2db5	registration form	registration form	my-everyday-lolita-realm	form-flow	f	t
6ed585ee-39c4-429e-ade7-ba85e4111bd0	reset credentials	Reset credentials for a user if they forgot their password or something	my-everyday-lolita-realm	basic-flow	t	t
adbdd657-1e53-44f6-9127-3b4c6b614293	saml ecp	SAML ECP Profile Authentication Flow	my-everyday-lolita-realm	basic-flow	t	t
\.


--
-- Data for Name: authenticator_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.authenticator_config (id, alias, realm_id) FROM stdin;
28ae8d99-3309-426e-abaa-69f58c09afa7	review profile config	master
74d5ff30-a0b1-4752-bf50-a6ad3ed37cef	create unique user config	master
f0d7929d-a03a-4f42-8666-86359b815ffc	create unique user config	my-everyday-lolita-realm
3b2a3084-e505-446e-ace6-a1ef97cdc9a2	review profile config	my-everyday-lolita-realm
\.


--
-- Data for Name: authenticator_config_entry; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.authenticator_config_entry (authenticator_id, value, name) FROM stdin;
28ae8d99-3309-426e-abaa-69f58c09afa7	missing	update.profile.on.first.login
74d5ff30-a0b1-4752-bf50-a6ad3ed37cef	false	require.password.update.after.registration
f0d7929d-a03a-4f42-8666-86359b815ffc	false	require.password.update.after.registration
3b2a3084-e505-446e-ace6-a1ef97cdc9a2	missing	update.profile.on.first.login
\.


--
-- Data for Name: broker_link; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.broker_link (identity_provider, storage_provider_id, realm_id, broker_user_id, broker_username, token, user_id) FROM stdin;
\.


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client (id, enabled, full_scope_allowed, client_id, not_before, public_client, secret, base_url, bearer_only, management_url, surrogate_auth_required, realm_id, protocol, node_rereg_timeout, frontchannel_logout, consent_required, name, service_accounts_enabled, client_authenticator_type, root_url, description, registration_token, standard_flow_enabled, implicit_flow_enabled, direct_access_grants_enabled, always_display_in_console) FROM stdin;
ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	t	master-realm	0	f	67a75b75-f82c-471d-a763-9f827171e107	\N	t	\N	f	master	\N	0	f	f	master Realm	f	client-secret	\N	\N	\N	t	f	f	f
986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	t	f	account	0	f	292f6316-5604-4f0c-9dc1-d1da2ae0e270	/realms/master/account/	f	\N	f	master	openid-connect	0	f	f	${client_account}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
ec94f535-843f-4a0d-bcbd-278f29faa5e8	t	f	account-console	0	t	8afcbd24-cff7-4c99-b82d-c27e5dfd8a4b	/realms/master/account/	f	\N	f	master	openid-connect	0	f	f	${client_account-console}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
4e4ed2e2-feaf-48ac-93b6-ac90dca76334	t	f	broker	0	f	724a68c4-898a-48a0-b815-03c6ceb563b7	\N	f	\N	f	master	openid-connect	0	f	f	${client_broker}	f	client-secret	\N	\N	\N	t	f	f	f
d2c2af8c-d1e7-4789-8c86-5e752d9643d6	t	f	security-admin-console	0	t	23cb1c0d-2338-4722-b29a-a1611d9417c1	/admin/master/console/	f	\N	f	master	openid-connect	0	f	f	${client_security-admin-console}	f	client-secret	${authAdminUrl}	\N	\N	t	f	f	f
0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	t	my-everyday-lolita-realm-realm	0	f	f94cd9bd-fed5-404c-91eb-1e2049a540d1	\N	t	\N	f	master	\N	0	f	f	my-everyday-lolita-realm Realm	f	client-secret	\N	\N	\N	t	f	f	f
463f127e-1394-4aeb-8e97-588bc9275ad7	t	f	account	0	f	**********	/realms/my-everyday-lolita-realm/account/	f	\N	f	my-everyday-lolita-realm	openid-connect	0	f	f	${client_account}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
19554311-6c94-479d-bcfc-5cb5ab56d096	t	f	account-console	0	t	**********	/realms/my-everyday-lolita-realm/account/	f	\N	f	my-everyday-lolita-realm	openid-connect	0	f	f	${client_account-console}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
fb77d40d-3c09-45c0-acc2-017d62affe1c	t	f	broker	0	f	**********	\N	f	\N	f	my-everyday-lolita-realm	openid-connect	0	f	f	${client_broker}	f	client-secret	\N	\N	\N	t	f	f	f
0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	f	realm-management	0	f	**********	\N	t	\N	f	my-everyday-lolita-realm	openid-connect	0	f	f	${client_realm-management}	f	client-secret	\N	\N	\N	t	f	f	f
5b6378af-33db-444a-93b2-14bb194f954f	t	f	security-admin-console	0	t	**********	/admin/my-everyday-lolita-realm/console/	f	\N	f	my-everyday-lolita-realm	openid-connect	0	f	f	${client_security-admin-console}	f	client-secret	${authAdminUrl}	\N	\N	t	f	f	f
5b128c68-e165-43e5-be48-851fc7d4b5fc	t	f	admin-cli	0	f	ddce6eaa-ff2b-4707-aeea-efdc8fa096bf	http://localhost:8080/	f	\N	f	my-everyday-lolita-realm	openid-connect	0	f	f	${client_admin-cli}	t	client-secret	http://localhost:8080/	\N	\N	f	f	t	f
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	t	f	admin-cli	0	f	f961c52e-bee7-4e97-8a0c-f70d4f184580	\N	f	\N	f	master	openid-connect	0	f	f	${client_admin-cli}	t	client-secret	\N	\N	\N	f	f	t	f
d30771ac-77e3-4424-905e-50bc58920b78	t	t	web-lolita	0	t	**********	\N	f	http://localhost:8080/	f	my-everyday-lolita-realm	openid-connect	-1	f	f	\N	f	client-secret	http://localhost:8080/	\N	\N	t	f	t	f
\.


--
-- Data for Name: client_attributes; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_attributes (client_id, value, name) FROM stdin;
ec94f535-843f-4a0d-bcbd-278f29faa5e8	S256	pkce.code.challenge.method
d2c2af8c-d1e7-4789-8c86-5e752d9643d6	S256	pkce.code.challenge.method
19554311-6c94-479d-bcfc-5cb5ab56d096	S256	pkce.code.challenge.method
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	saml.assertion.signature
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	saml.force.post.binding
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	saml.multivalued.roles
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	saml.encrypt
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	backchannel.logout.revoke.offline.tokens
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	saml.server.signature
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	saml.server.signature.keyinfo.ext
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	exclude.session.state.from.auth.response
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	backchannel.logout.session.required
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	client_credentials.use_refresh_token
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	saml_force_name_id_format
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	saml.client.signature
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	tls.client.certificate.bound.access.tokens
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	saml.authnstatement
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	display.on.consent.screen
5b128c68-e165-43e5-be48-851fc7d4b5fc	false	saml.onetimeuse.condition
5b6378af-33db-444a-93b2-14bb194f954f	S256	pkce.code.challenge.method
d30771ac-77e3-4424-905e-50bc58920b78	false	saml.assertion.signature
d30771ac-77e3-4424-905e-50bc58920b78	false	saml.force.post.binding
d30771ac-77e3-4424-905e-50bc58920b78	false	saml.multivalued.roles
d30771ac-77e3-4424-905e-50bc58920b78	false	saml.encrypt
d30771ac-77e3-4424-905e-50bc58920b78	false	backchannel.logout.revoke.offline.tokens
d30771ac-77e3-4424-905e-50bc58920b78	false	saml.server.signature
d30771ac-77e3-4424-905e-50bc58920b78	false	saml.server.signature.keyinfo.ext
d30771ac-77e3-4424-905e-50bc58920b78	false	exclude.session.state.from.auth.response
d30771ac-77e3-4424-905e-50bc58920b78	true	backchannel.logout.session.required
d30771ac-77e3-4424-905e-50bc58920b78	false	client_credentials.use_refresh_token
d30771ac-77e3-4424-905e-50bc58920b78	false	saml_force_name_id_format
d30771ac-77e3-4424-905e-50bc58920b78	false	saml.client.signature
d30771ac-77e3-4424-905e-50bc58920b78	false	tls.client.certificate.bound.access.tokens
d30771ac-77e3-4424-905e-50bc58920b78	false	saml.authnstatement
d30771ac-77e3-4424-905e-50bc58920b78	false	display.on.consent.screen
d30771ac-77e3-4424-905e-50bc58920b78	false	saml.onetimeuse.condition
5b128c68-e165-43e5-be48-851fc7d4b5fc	\N	request.uris
d30771ac-77e3-4424-905e-50bc58920b78	\N	request.uris
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	\N	request.uris
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	saml.server.signature
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	saml.server.signature.keyinfo.ext
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	saml.assertion.signature
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	saml.client.signature
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	saml.encrypt
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	saml.authnstatement
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	saml.onetimeuse.condition
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	saml_force_name_id_format
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	saml.multivalued.roles
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	saml.force.post.binding
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	exclude.session.state.from.auth.response
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	tls.client.certificate.bound.access.tokens
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	client_credentials.use_refresh_token
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	display.on.consent.screen
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	backchannel.logout.session.required
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	false	backchannel.logout.revoke.offline.tokens
\.


--
-- Data for Name: client_auth_flow_bindings; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_auth_flow_bindings (client_id, flow_id, binding_name) FROM stdin;
d30771ac-77e3-4424-905e-50bc58920b78	90e3d452-2eba-41c1-ac24-02509ff5d30f	browser
d30771ac-77e3-4424-905e-50bc58920b78	692b336c-5e29-45fc-bb76-fb36073eb320	direct_grant
\.


--
-- Data for Name: client_default_roles; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_default_roles (client_id, role_id) FROM stdin;
986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	ff52cc7b-fffd-4e6f-a31d-d7e128910691
986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	a24019b4-1610-4d94-956d-0d43431bf65f
463f127e-1394-4aeb-8e97-588bc9275ad7	199da019-bb23-49d2-95db-eb0d1cdb3e3c
463f127e-1394-4aeb-8e97-588bc9275ad7	a1e1dfb6-85fe-40c5-90ca-5b0d16d43818
\.


--
-- Data for Name: client_initial_access; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_initial_access (id, realm_id, "timestamp", expiration, count, remaining_count) FROM stdin;
\.


--
-- Data for Name: client_node_registrations; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_node_registrations (client_id, value, name) FROM stdin;
\.


--
-- Data for Name: client_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_scope (id, name, realm_id, description, protocol) FROM stdin;
f99b0b25-a996-497d-a47d-99547ded0c2c	offline_access	master	OpenID Connect built-in scope: offline_access	openid-connect
2afe53d3-9e33-4330-a8b7-66272ae4f798	role_list	master	SAML role list	saml
0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a	profile	master	OpenID Connect built-in scope: profile	openid-connect
088c8ce8-5764-49c7-9055-348213800923	email	master	OpenID Connect built-in scope: email	openid-connect
97a54b1a-7718-4d41-af64-fa9df073ec0f	address	master	OpenID Connect built-in scope: address	openid-connect
d2779ef1-955c-4b13-921d-a8f1adb89092	phone	master	OpenID Connect built-in scope: phone	openid-connect
e8520c61-4b9b-48a3-8064-87b9c0a9ac67	roles	master	OpenID Connect scope for add user roles to the access token	openid-connect
111dc177-01d3-4c45-88ad-9eb94a6b193a	web-origins	master	OpenID Connect scope for add allowed web origins to the access token	openid-connect
85f399d0-8bc1-413e-8f7d-0cacaebd53c7	microprofile-jwt	master	Microprofile - JWT built-in scope	openid-connect
b4767745-2c5f-4721-ae07-dd1bf190b7bb	address	my-everyday-lolita-realm	OpenID Connect built-in scope: address	openid-connect
0ade4166-f086-4ad8-af3a-f9e6b47276e5	email	my-everyday-lolita-realm	OpenID Connect built-in scope: email	openid-connect
ade59ad3-b70e-47c8-9f4f-ace450d6297f	microprofile-jwt	my-everyday-lolita-realm	Microprofile - JWT built-in scope	openid-connect
15dc681b-1fe8-426f-81ae-e3632e7356c0	offline_access	my-everyday-lolita-realm	OpenID Connect built-in scope: offline_access	openid-connect
39e77af3-6506-4684-a2a5-2eda9c5459e5	phone	my-everyday-lolita-realm	OpenID Connect built-in scope: phone	openid-connect
06f61973-d7c6-4cf7-a201-72c69d949874	profile	my-everyday-lolita-realm	OpenID Connect built-in scope: profile	openid-connect
8e4b5136-5c51-478d-bb58-9fce0fae4e38	role_list	my-everyday-lolita-realm	SAML role list	saml
b0f7660e-3612-4037-9d72-ec3fd10e0a5c	roles	my-everyday-lolita-realm	OpenID Connect scope for add user roles to the access token	openid-connect
0361142e-dacf-4411-90a9-25a02b60523c	web-origins	my-everyday-lolita-realm	OpenID Connect scope for add allowed web origins to the access token	openid-connect
\.


--
-- Data for Name: client_scope_attributes; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_scope_attributes (scope_id, value, name) FROM stdin;
f99b0b25-a996-497d-a47d-99547ded0c2c	true	display.on.consent.screen
f99b0b25-a996-497d-a47d-99547ded0c2c	${offlineAccessScopeConsentText}	consent.screen.text
2afe53d3-9e33-4330-a8b7-66272ae4f798	true	display.on.consent.screen
2afe53d3-9e33-4330-a8b7-66272ae4f798	${samlRoleListScopeConsentText}	consent.screen.text
0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a	true	display.on.consent.screen
0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a	${profileScopeConsentText}	consent.screen.text
0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a	true	include.in.token.scope
088c8ce8-5764-49c7-9055-348213800923	true	display.on.consent.screen
088c8ce8-5764-49c7-9055-348213800923	${emailScopeConsentText}	consent.screen.text
088c8ce8-5764-49c7-9055-348213800923	true	include.in.token.scope
97a54b1a-7718-4d41-af64-fa9df073ec0f	true	display.on.consent.screen
97a54b1a-7718-4d41-af64-fa9df073ec0f	${addressScopeConsentText}	consent.screen.text
97a54b1a-7718-4d41-af64-fa9df073ec0f	true	include.in.token.scope
d2779ef1-955c-4b13-921d-a8f1adb89092	true	display.on.consent.screen
d2779ef1-955c-4b13-921d-a8f1adb89092	${phoneScopeConsentText}	consent.screen.text
d2779ef1-955c-4b13-921d-a8f1adb89092	true	include.in.token.scope
e8520c61-4b9b-48a3-8064-87b9c0a9ac67	true	display.on.consent.screen
e8520c61-4b9b-48a3-8064-87b9c0a9ac67	${rolesScopeConsentText}	consent.screen.text
e8520c61-4b9b-48a3-8064-87b9c0a9ac67	false	include.in.token.scope
111dc177-01d3-4c45-88ad-9eb94a6b193a	false	display.on.consent.screen
111dc177-01d3-4c45-88ad-9eb94a6b193a		consent.screen.text
111dc177-01d3-4c45-88ad-9eb94a6b193a	false	include.in.token.scope
85f399d0-8bc1-413e-8f7d-0cacaebd53c7	false	display.on.consent.screen
85f399d0-8bc1-413e-8f7d-0cacaebd53c7	true	include.in.token.scope
b4767745-2c5f-4721-ae07-dd1bf190b7bb	true	include.in.token.scope
b4767745-2c5f-4721-ae07-dd1bf190b7bb	true	display.on.consent.screen
b4767745-2c5f-4721-ae07-dd1bf190b7bb	${addressScopeConsentText}	consent.screen.text
0ade4166-f086-4ad8-af3a-f9e6b47276e5	true	include.in.token.scope
0ade4166-f086-4ad8-af3a-f9e6b47276e5	true	display.on.consent.screen
0ade4166-f086-4ad8-af3a-f9e6b47276e5	${emailScopeConsentText}	consent.screen.text
ade59ad3-b70e-47c8-9f4f-ace450d6297f	true	include.in.token.scope
ade59ad3-b70e-47c8-9f4f-ace450d6297f	false	display.on.consent.screen
15dc681b-1fe8-426f-81ae-e3632e7356c0	${offlineAccessScopeConsentText}	consent.screen.text
15dc681b-1fe8-426f-81ae-e3632e7356c0	true	display.on.consent.screen
39e77af3-6506-4684-a2a5-2eda9c5459e5	true	include.in.token.scope
39e77af3-6506-4684-a2a5-2eda9c5459e5	true	display.on.consent.screen
39e77af3-6506-4684-a2a5-2eda9c5459e5	${phoneScopeConsentText}	consent.screen.text
06f61973-d7c6-4cf7-a201-72c69d949874	true	include.in.token.scope
06f61973-d7c6-4cf7-a201-72c69d949874	true	display.on.consent.screen
06f61973-d7c6-4cf7-a201-72c69d949874	${profileScopeConsentText}	consent.screen.text
8e4b5136-5c51-478d-bb58-9fce0fae4e38	${samlRoleListScopeConsentText}	consent.screen.text
8e4b5136-5c51-478d-bb58-9fce0fae4e38	true	display.on.consent.screen
b0f7660e-3612-4037-9d72-ec3fd10e0a5c	false	include.in.token.scope
b0f7660e-3612-4037-9d72-ec3fd10e0a5c	true	display.on.consent.screen
b0f7660e-3612-4037-9d72-ec3fd10e0a5c	${rolesScopeConsentText}	consent.screen.text
0361142e-dacf-4411-90a9-25a02b60523c	false	include.in.token.scope
0361142e-dacf-4411-90a9-25a02b60523c	false	display.on.consent.screen
0361142e-dacf-4411-90a9-25a02b60523c		consent.screen.text
\.


--
-- Data for Name: client_scope_client; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_scope_client (client_id, scope_id, default_scope) FROM stdin;
986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	2afe53d3-9e33-4330-a8b7-66272ae4f798	t
ec94f535-843f-4a0d-bcbd-278f29faa5e8	2afe53d3-9e33-4330-a8b7-66272ae4f798	t
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	2afe53d3-9e33-4330-a8b7-66272ae4f798	t
4e4ed2e2-feaf-48ac-93b6-ac90dca76334	2afe53d3-9e33-4330-a8b7-66272ae4f798	t
ccea24b5-8d34-436f-8e4f-22b350ecdde4	2afe53d3-9e33-4330-a8b7-66272ae4f798	t
d2c2af8c-d1e7-4789-8c86-5e752d9643d6	2afe53d3-9e33-4330-a8b7-66272ae4f798	t
986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	111dc177-01d3-4c45-88ad-9eb94a6b193a	t
986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a	t
986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	088c8ce8-5764-49c7-9055-348213800923	t
986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	e8520c61-4b9b-48a3-8064-87b9c0a9ac67	t
986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	85f399d0-8bc1-413e-8f7d-0cacaebd53c7	f
986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	97a54b1a-7718-4d41-af64-fa9df073ec0f	f
986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	d2779ef1-955c-4b13-921d-a8f1adb89092	f
986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	f99b0b25-a996-497d-a47d-99547ded0c2c	f
ec94f535-843f-4a0d-bcbd-278f29faa5e8	111dc177-01d3-4c45-88ad-9eb94a6b193a	t
ec94f535-843f-4a0d-bcbd-278f29faa5e8	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a	t
ec94f535-843f-4a0d-bcbd-278f29faa5e8	088c8ce8-5764-49c7-9055-348213800923	t
ec94f535-843f-4a0d-bcbd-278f29faa5e8	e8520c61-4b9b-48a3-8064-87b9c0a9ac67	t
ec94f535-843f-4a0d-bcbd-278f29faa5e8	85f399d0-8bc1-413e-8f7d-0cacaebd53c7	f
ec94f535-843f-4a0d-bcbd-278f29faa5e8	97a54b1a-7718-4d41-af64-fa9df073ec0f	f
ec94f535-843f-4a0d-bcbd-278f29faa5e8	d2779ef1-955c-4b13-921d-a8f1adb89092	f
ec94f535-843f-4a0d-bcbd-278f29faa5e8	f99b0b25-a996-497d-a47d-99547ded0c2c	f
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	111dc177-01d3-4c45-88ad-9eb94a6b193a	t
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a	t
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	088c8ce8-5764-49c7-9055-348213800923	t
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	e8520c61-4b9b-48a3-8064-87b9c0a9ac67	t
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	85f399d0-8bc1-413e-8f7d-0cacaebd53c7	f
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	97a54b1a-7718-4d41-af64-fa9df073ec0f	f
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	d2779ef1-955c-4b13-921d-a8f1adb89092	f
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	f99b0b25-a996-497d-a47d-99547ded0c2c	f
4e4ed2e2-feaf-48ac-93b6-ac90dca76334	111dc177-01d3-4c45-88ad-9eb94a6b193a	t
4e4ed2e2-feaf-48ac-93b6-ac90dca76334	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a	t
4e4ed2e2-feaf-48ac-93b6-ac90dca76334	088c8ce8-5764-49c7-9055-348213800923	t
4e4ed2e2-feaf-48ac-93b6-ac90dca76334	e8520c61-4b9b-48a3-8064-87b9c0a9ac67	t
4e4ed2e2-feaf-48ac-93b6-ac90dca76334	85f399d0-8bc1-413e-8f7d-0cacaebd53c7	f
4e4ed2e2-feaf-48ac-93b6-ac90dca76334	97a54b1a-7718-4d41-af64-fa9df073ec0f	f
4e4ed2e2-feaf-48ac-93b6-ac90dca76334	d2779ef1-955c-4b13-921d-a8f1adb89092	f
4e4ed2e2-feaf-48ac-93b6-ac90dca76334	f99b0b25-a996-497d-a47d-99547ded0c2c	f
ccea24b5-8d34-436f-8e4f-22b350ecdde4	111dc177-01d3-4c45-88ad-9eb94a6b193a	t
ccea24b5-8d34-436f-8e4f-22b350ecdde4	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a	t
ccea24b5-8d34-436f-8e4f-22b350ecdde4	088c8ce8-5764-49c7-9055-348213800923	t
ccea24b5-8d34-436f-8e4f-22b350ecdde4	e8520c61-4b9b-48a3-8064-87b9c0a9ac67	t
ccea24b5-8d34-436f-8e4f-22b350ecdde4	85f399d0-8bc1-413e-8f7d-0cacaebd53c7	f
ccea24b5-8d34-436f-8e4f-22b350ecdde4	97a54b1a-7718-4d41-af64-fa9df073ec0f	f
ccea24b5-8d34-436f-8e4f-22b350ecdde4	d2779ef1-955c-4b13-921d-a8f1adb89092	f
ccea24b5-8d34-436f-8e4f-22b350ecdde4	f99b0b25-a996-497d-a47d-99547ded0c2c	f
d2c2af8c-d1e7-4789-8c86-5e752d9643d6	111dc177-01d3-4c45-88ad-9eb94a6b193a	t
d2c2af8c-d1e7-4789-8c86-5e752d9643d6	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a	t
d2c2af8c-d1e7-4789-8c86-5e752d9643d6	088c8ce8-5764-49c7-9055-348213800923	t
d2c2af8c-d1e7-4789-8c86-5e752d9643d6	e8520c61-4b9b-48a3-8064-87b9c0a9ac67	t
d2c2af8c-d1e7-4789-8c86-5e752d9643d6	85f399d0-8bc1-413e-8f7d-0cacaebd53c7	f
d2c2af8c-d1e7-4789-8c86-5e752d9643d6	97a54b1a-7718-4d41-af64-fa9df073ec0f	f
d2c2af8c-d1e7-4789-8c86-5e752d9643d6	d2779ef1-955c-4b13-921d-a8f1adb89092	f
d2c2af8c-d1e7-4789-8c86-5e752d9643d6	f99b0b25-a996-497d-a47d-99547ded0c2c	f
0886a50d-9edf-4645-a56f-be8bfbe9e82e	2afe53d3-9e33-4330-a8b7-66272ae4f798	t
0886a50d-9edf-4645-a56f-be8bfbe9e82e	111dc177-01d3-4c45-88ad-9eb94a6b193a	t
0886a50d-9edf-4645-a56f-be8bfbe9e82e	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a	t
0886a50d-9edf-4645-a56f-be8bfbe9e82e	088c8ce8-5764-49c7-9055-348213800923	t
0886a50d-9edf-4645-a56f-be8bfbe9e82e	e8520c61-4b9b-48a3-8064-87b9c0a9ac67	t
0886a50d-9edf-4645-a56f-be8bfbe9e82e	85f399d0-8bc1-413e-8f7d-0cacaebd53c7	f
0886a50d-9edf-4645-a56f-be8bfbe9e82e	97a54b1a-7718-4d41-af64-fa9df073ec0f	f
0886a50d-9edf-4645-a56f-be8bfbe9e82e	d2779ef1-955c-4b13-921d-a8f1adb89092	f
0886a50d-9edf-4645-a56f-be8bfbe9e82e	f99b0b25-a996-497d-a47d-99547ded0c2c	f
463f127e-1394-4aeb-8e97-588bc9275ad7	0361142e-dacf-4411-90a9-25a02b60523c	t
463f127e-1394-4aeb-8e97-588bc9275ad7	8e4b5136-5c51-478d-bb58-9fce0fae4e38	t
463f127e-1394-4aeb-8e97-588bc9275ad7	b0f7660e-3612-4037-9d72-ec3fd10e0a5c	t
463f127e-1394-4aeb-8e97-588bc9275ad7	06f61973-d7c6-4cf7-a201-72c69d949874	t
463f127e-1394-4aeb-8e97-588bc9275ad7	0ade4166-f086-4ad8-af3a-f9e6b47276e5	t
463f127e-1394-4aeb-8e97-588bc9275ad7	b4767745-2c5f-4721-ae07-dd1bf190b7bb	f
463f127e-1394-4aeb-8e97-588bc9275ad7	39e77af3-6506-4684-a2a5-2eda9c5459e5	f
463f127e-1394-4aeb-8e97-588bc9275ad7	15dc681b-1fe8-426f-81ae-e3632e7356c0	f
463f127e-1394-4aeb-8e97-588bc9275ad7	ade59ad3-b70e-47c8-9f4f-ace450d6297f	f
19554311-6c94-479d-bcfc-5cb5ab56d096	0361142e-dacf-4411-90a9-25a02b60523c	t
19554311-6c94-479d-bcfc-5cb5ab56d096	8e4b5136-5c51-478d-bb58-9fce0fae4e38	t
19554311-6c94-479d-bcfc-5cb5ab56d096	b0f7660e-3612-4037-9d72-ec3fd10e0a5c	t
19554311-6c94-479d-bcfc-5cb5ab56d096	06f61973-d7c6-4cf7-a201-72c69d949874	t
19554311-6c94-479d-bcfc-5cb5ab56d096	0ade4166-f086-4ad8-af3a-f9e6b47276e5	t
19554311-6c94-479d-bcfc-5cb5ab56d096	b4767745-2c5f-4721-ae07-dd1bf190b7bb	f
19554311-6c94-479d-bcfc-5cb5ab56d096	39e77af3-6506-4684-a2a5-2eda9c5459e5	f
19554311-6c94-479d-bcfc-5cb5ab56d096	15dc681b-1fe8-426f-81ae-e3632e7356c0	f
19554311-6c94-479d-bcfc-5cb5ab56d096	ade59ad3-b70e-47c8-9f4f-ace450d6297f	f
5b128c68-e165-43e5-be48-851fc7d4b5fc	0361142e-dacf-4411-90a9-25a02b60523c	t
5b128c68-e165-43e5-be48-851fc7d4b5fc	8e4b5136-5c51-478d-bb58-9fce0fae4e38	t
5b128c68-e165-43e5-be48-851fc7d4b5fc	b0f7660e-3612-4037-9d72-ec3fd10e0a5c	t
5b128c68-e165-43e5-be48-851fc7d4b5fc	06f61973-d7c6-4cf7-a201-72c69d949874	t
5b128c68-e165-43e5-be48-851fc7d4b5fc	0ade4166-f086-4ad8-af3a-f9e6b47276e5	t
5b128c68-e165-43e5-be48-851fc7d4b5fc	b4767745-2c5f-4721-ae07-dd1bf190b7bb	f
5b128c68-e165-43e5-be48-851fc7d4b5fc	39e77af3-6506-4684-a2a5-2eda9c5459e5	f
5b128c68-e165-43e5-be48-851fc7d4b5fc	15dc681b-1fe8-426f-81ae-e3632e7356c0	f
5b128c68-e165-43e5-be48-851fc7d4b5fc	ade59ad3-b70e-47c8-9f4f-ace450d6297f	f
fb77d40d-3c09-45c0-acc2-017d62affe1c	0361142e-dacf-4411-90a9-25a02b60523c	t
fb77d40d-3c09-45c0-acc2-017d62affe1c	8e4b5136-5c51-478d-bb58-9fce0fae4e38	t
fb77d40d-3c09-45c0-acc2-017d62affe1c	b0f7660e-3612-4037-9d72-ec3fd10e0a5c	t
fb77d40d-3c09-45c0-acc2-017d62affe1c	06f61973-d7c6-4cf7-a201-72c69d949874	t
fb77d40d-3c09-45c0-acc2-017d62affe1c	0ade4166-f086-4ad8-af3a-f9e6b47276e5	t
fb77d40d-3c09-45c0-acc2-017d62affe1c	b4767745-2c5f-4721-ae07-dd1bf190b7bb	f
fb77d40d-3c09-45c0-acc2-017d62affe1c	39e77af3-6506-4684-a2a5-2eda9c5459e5	f
fb77d40d-3c09-45c0-acc2-017d62affe1c	15dc681b-1fe8-426f-81ae-e3632e7356c0	f
fb77d40d-3c09-45c0-acc2-017d62affe1c	ade59ad3-b70e-47c8-9f4f-ace450d6297f	f
0d3aba2b-94e5-4e03-b4fd-94b04db07265	0361142e-dacf-4411-90a9-25a02b60523c	t
0d3aba2b-94e5-4e03-b4fd-94b04db07265	8e4b5136-5c51-478d-bb58-9fce0fae4e38	t
0d3aba2b-94e5-4e03-b4fd-94b04db07265	b0f7660e-3612-4037-9d72-ec3fd10e0a5c	t
0d3aba2b-94e5-4e03-b4fd-94b04db07265	06f61973-d7c6-4cf7-a201-72c69d949874	t
0d3aba2b-94e5-4e03-b4fd-94b04db07265	0ade4166-f086-4ad8-af3a-f9e6b47276e5	t
0d3aba2b-94e5-4e03-b4fd-94b04db07265	b4767745-2c5f-4721-ae07-dd1bf190b7bb	f
0d3aba2b-94e5-4e03-b4fd-94b04db07265	39e77af3-6506-4684-a2a5-2eda9c5459e5	f
0d3aba2b-94e5-4e03-b4fd-94b04db07265	15dc681b-1fe8-426f-81ae-e3632e7356c0	f
0d3aba2b-94e5-4e03-b4fd-94b04db07265	ade59ad3-b70e-47c8-9f4f-ace450d6297f	f
5b6378af-33db-444a-93b2-14bb194f954f	0361142e-dacf-4411-90a9-25a02b60523c	t
5b6378af-33db-444a-93b2-14bb194f954f	8e4b5136-5c51-478d-bb58-9fce0fae4e38	t
5b6378af-33db-444a-93b2-14bb194f954f	b0f7660e-3612-4037-9d72-ec3fd10e0a5c	t
5b6378af-33db-444a-93b2-14bb194f954f	06f61973-d7c6-4cf7-a201-72c69d949874	t
5b6378af-33db-444a-93b2-14bb194f954f	0ade4166-f086-4ad8-af3a-f9e6b47276e5	t
5b6378af-33db-444a-93b2-14bb194f954f	b4767745-2c5f-4721-ae07-dd1bf190b7bb	f
5b6378af-33db-444a-93b2-14bb194f954f	39e77af3-6506-4684-a2a5-2eda9c5459e5	f
5b6378af-33db-444a-93b2-14bb194f954f	15dc681b-1fe8-426f-81ae-e3632e7356c0	f
5b6378af-33db-444a-93b2-14bb194f954f	ade59ad3-b70e-47c8-9f4f-ace450d6297f	f
d30771ac-77e3-4424-905e-50bc58920b78	0361142e-dacf-4411-90a9-25a02b60523c	t
d30771ac-77e3-4424-905e-50bc58920b78	8e4b5136-5c51-478d-bb58-9fce0fae4e38	t
d30771ac-77e3-4424-905e-50bc58920b78	b0f7660e-3612-4037-9d72-ec3fd10e0a5c	t
d30771ac-77e3-4424-905e-50bc58920b78	06f61973-d7c6-4cf7-a201-72c69d949874	t
d30771ac-77e3-4424-905e-50bc58920b78	0ade4166-f086-4ad8-af3a-f9e6b47276e5	t
d30771ac-77e3-4424-905e-50bc58920b78	b4767745-2c5f-4721-ae07-dd1bf190b7bb	f
d30771ac-77e3-4424-905e-50bc58920b78	39e77af3-6506-4684-a2a5-2eda9c5459e5	f
d30771ac-77e3-4424-905e-50bc58920b78	15dc681b-1fe8-426f-81ae-e3632e7356c0	f
d30771ac-77e3-4424-905e-50bc58920b78	ade59ad3-b70e-47c8-9f4f-ace450d6297f	f
\.


--
-- Data for Name: client_scope_role_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_scope_role_mapping (scope_id, role_id) FROM stdin;
f99b0b25-a996-497d-a47d-99547ded0c2c	5b24a59a-7618-483a-bfa0-70da4201b0e7
15dc681b-1fe8-426f-81ae-e3632e7356c0	1147723f-9cd1-4519-9ef4-9493fc722196
\.


--
-- Data for Name: client_session; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session (id, client_id, redirect_uri, state, "timestamp", session_id, auth_method, realm_id, auth_user_id, current_action) FROM stdin;
\.


--
-- Data for Name: client_session_auth_status; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session_auth_status (authenticator, status, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_note; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session_note (name, value, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_prot_mapper; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session_prot_mapper (protocol_mapper_id, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_role; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session_role (role_id, client_session) FROM stdin;
\.


--
-- Data for Name: client_user_session_note; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_user_session_note (name, value, client_session) FROM stdin;
\.


--
-- Data for Name: component; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.component (id, name, parent_id, provider_id, provider_type, realm_id, sub_type) FROM stdin;
15b06e8c-461a-4805-9d6a-e6bdcefd2519	Trusted Hosts	master	trusted-hosts	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
21a4623a-a99e-4e5d-88e6-309f784293e1	Consent Required	master	consent-required	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
11ad6949-af1c-4c4d-8458-54547b2cfecb	Full Scope Disabled	master	scope	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
951911d0-e141-4ac8-b482-fc32517d7074	Max Clients Limit	master	max-clients	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
587f3b35-fb32-4c76-9b08-327975878e37	Allowed Protocol Mapper Types	master	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
e2c428d6-3461-4ede-82ef-7e9efe789510	Allowed Client Scopes	master	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
1721bcd0-6f2f-4d20-a592-8979956f76fb	Allowed Protocol Mapper Types	master	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	authenticated
b82205e8-0299-4902-81fd-b1a48a565405	Allowed Client Scopes	master	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	authenticated
e4182001-8ddd-4891-99fb-331d1dd20a12	fallback-HS256	master	hmac-generated	org.keycloak.keys.KeyProvider	master	\N
4245ec8f-10dc-4c85-8b59-45331e4d6f71	fallback-RS256	master	rsa-generated	org.keycloak.keys.KeyProvider	master	\N
9779cda5-7a0d-46a0-a83d-b6dd34cefe3e	Allowed Protocol Mapper Types	my-everyday-lolita-realm	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	my-everyday-lolita-realm	authenticated
6ea9dc0e-cc1d-4aa9-8a9b-064cc817c72f	Trusted Hosts	my-everyday-lolita-realm	trusted-hosts	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	my-everyday-lolita-realm	anonymous
df4bc170-abe3-46ac-b8e1-ac14212c34f9	Max Clients Limit	my-everyday-lolita-realm	max-clients	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	my-everyday-lolita-realm	anonymous
d594c7b4-4376-4426-9df9-1a60e3bf856a	Allowed Client Scopes	my-everyday-lolita-realm	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	my-everyday-lolita-realm	anonymous
8cf37b2f-f381-466d-a8f8-95598609ff11	Allowed Client Scopes	my-everyday-lolita-realm	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	my-everyday-lolita-realm	authenticated
eb2b6c21-f626-4f9e-bc0d-1de0870b9671	Consent Required	my-everyday-lolita-realm	consent-required	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	my-everyday-lolita-realm	anonymous
f09b6acd-3db8-466c-8c7a-a442e4454af1	Allowed Protocol Mapper Types	my-everyday-lolita-realm	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	my-everyday-lolita-realm	anonymous
53ff233d-2a83-41dc-8c60-1611853d816b	Full Scope Disabled	my-everyday-lolita-realm	scope	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	my-everyday-lolita-realm	anonymous
6867b6fd-b82d-41f9-9690-2d78d95ed8b1	hmac-generated	my-everyday-lolita-realm	hmac-generated	org.keycloak.keys.KeyProvider	my-everyday-lolita-realm	\N
73564b27-cc98-4dd8-8715-57891c27fcce	aes-generated	my-everyday-lolita-realm	aes-generated	org.keycloak.keys.KeyProvider	my-everyday-lolita-realm	\N
5bb54576-8086-4522-abc2-6813e197fbde	rsa-generated	my-everyday-lolita-realm	rsa-generated	org.keycloak.keys.KeyProvider	my-everyday-lolita-realm	\N
\.


--
-- Data for Name: component_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.component_config (id, component_id, name, value) FROM stdin;
248c2b53-6471-4047-b746-8c2ee9e38687	b82205e8-0299-4902-81fd-b1a48a565405	allow-default-scopes	true
ba716173-79b8-44f1-a288-82802e72383c	951911d0-e141-4ac8-b482-fc32517d7074	max-clients	200
ca73f386-3208-4c65-a19f-0ac9d11522d4	e2c428d6-3461-4ede-82ef-7e9efe789510	allow-default-scopes	true
5bc561d1-8f21-425f-b846-b54274dca103	1721bcd0-6f2f-4d20-a592-8979956f76fb	allowed-protocol-mapper-types	saml-user-attribute-mapper
562b25ff-37f0-4f30-aeb5-aaf16cd259e1	1721bcd0-6f2f-4d20-a592-8979956f76fb	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
37ca17d7-b057-41a8-9d9c-18aab75ea5b9	1721bcd0-6f2f-4d20-a592-8979956f76fb	allowed-protocol-mapper-types	oidc-address-mapper
841178ec-57c4-4b03-af86-608236fda1c7	1721bcd0-6f2f-4d20-a592-8979956f76fb	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
6a40b7b3-f01e-43f6-a0b6-8f54e25e7583	1721bcd0-6f2f-4d20-a592-8979956f76fb	allowed-protocol-mapper-types	saml-role-list-mapper
9fcbd49e-eb14-436c-8b8f-0a88956ee0ef	1721bcd0-6f2f-4d20-a592-8979956f76fb	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
764a34a3-a33b-4236-8981-f700d9d31e30	1721bcd0-6f2f-4d20-a592-8979956f76fb	allowed-protocol-mapper-types	oidc-full-name-mapper
a2f1529b-ae51-48e0-8021-6f7d5d56dd74	1721bcd0-6f2f-4d20-a592-8979956f76fb	allowed-protocol-mapper-types	saml-user-property-mapper
7961bf20-e871-448e-a41d-82ff44c14aca	15b06e8c-461a-4805-9d6a-e6bdcefd2519	client-uris-must-match	true
1b836e1e-3e48-468b-a036-decfa977072b	15b06e8c-461a-4805-9d6a-e6bdcefd2519	host-sending-registration-request-must-match	true
2d82e4e7-7519-49cf-9d61-af844e1ec42f	587f3b35-fb32-4c76-9b08-327975878e37	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
b9cb3341-937f-443d-84d0-0593189fe3c1	587f3b35-fb32-4c76-9b08-327975878e37	allowed-protocol-mapper-types	oidc-full-name-mapper
ce16517b-fcfa-4307-87c0-e2f0f5238e4e	587f3b35-fb32-4c76-9b08-327975878e37	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
656b9af9-a962-47fe-b300-de825fc27211	587f3b35-fb32-4c76-9b08-327975878e37	allowed-protocol-mapper-types	saml-user-property-mapper
3230a957-6131-4878-af77-3c9dc293c9b0	587f3b35-fb32-4c76-9b08-327975878e37	allowed-protocol-mapper-types	oidc-address-mapper
12bdcbd1-94e8-417c-8aa2-134c69dd70a0	587f3b35-fb32-4c76-9b08-327975878e37	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
3aa3a217-d443-4bfd-9961-16b1c15101ad	587f3b35-fb32-4c76-9b08-327975878e37	allowed-protocol-mapper-types	saml-user-attribute-mapper
904fd1c1-302d-4b31-b056-04169240ec85	587f3b35-fb32-4c76-9b08-327975878e37	allowed-protocol-mapper-types	saml-role-list-mapper
03e3dc3e-9f5d-4a88-924d-40f1973b31df	e4182001-8ddd-4891-99fb-331d1dd20a12	kid	dcbcaecb-237a-4040-80eb-2732ba290e3d
851b0a8b-b39b-4be9-abd0-fa7bde7052f5	e4182001-8ddd-4891-99fb-331d1dd20a12	priority	-100
236715b2-e2cf-47bb-92cc-f5df3fe96d30	e4182001-8ddd-4891-99fb-331d1dd20a12	secret	jJI66wR8qPEbqmqDxjlllvIhrmd79QPALCy6O21yoTN6_6e-US3P6QFTPuCDmU2ONvttAyt6KLf87uKc8sRjAQ
9110763c-f215-4e5f-9b2d-2725d635fafd	e4182001-8ddd-4891-99fb-331d1dd20a12	algorithm	HS256
b29527b6-9834-4aa9-a5dd-d888e58ab6f4	4245ec8f-10dc-4c85-8b59-45331e4d6f71	priority	-100
a92888d1-9aa3-40e1-9377-d4438624bdfc	4245ec8f-10dc-4c85-8b59-45331e4d6f71	privateKey	MIIEpQIBAAKCAQEAq8OV33RGFshpNsb88ZfbJvzeQyJFUo/8kGaf74Gab153IULocEkcl0ALBqXvPaCOys4PMgBg+to/WYw4LLUBtjxTXBicKmQGGMNugPZ0JHU14QlANUSWfMIpbBhoGxbCPHWdnEP9vKDPg9rF3BpynSTSQyBxZHcnTSGz7pUDRtE5tAqv2a0sfsLDXDRXgZIbALHJ9FK3L+3SPLnSk+1dKfKZbUfyLqa4A4MK1W6kO8MEXsH8YcI7ljahVohXqsoKfmfMNIKzVsV6XbywQHCN+KI7GRFu/9aB8lz9KE00GcQr7BHG0/7rzl07Vj+Te5pBVy8nthTDtQcBTq2xD+GSRwIDAQABAoIBAQCUibB7LE9MdoXedBjcSKmhvr3rKqDudExDCCy5b8gBFZ1KK1VdfSx3dE/UsP0Cu22g3q/vWrqDVOfB5evSZboUQXkJRNMQKa3IDoHqjrcDrEUq1dgr+KN3QmWV8ElfW8Afjsr9DyNH032BlljTLh/925DlshG9gvl+z22OHllWDD6b6cNB+EuHdiUybkaDp6V3EIvtP94tXh1dx483nm6HSHEs84u4uQdxnop6PcWCqgEls+KDABe6IFrGajmqFH+95A3rcizIovh5Ef/Sm00KoYS0Z66KAZEhMSfIgXJyd0mysSOcddZ1dZ0ziCMuLccLFzvzAgLR46xkSOdsAgsZAoGBAOspjuLxHAX8J1z9hPEDaVUoolANs8OMKrkS3uhLdTwGevbQYq3d8uFXVnCYdcNME7oJqq6V9mW1eXAlZ0ODqxNT6ZuyacO/I8u58a2WVASPSrtDkCdWPquv0bnRvKQARvecIcUpiv90bS9n5cPvS0ZvD3OyF2137AR4YTgsW5E1AoGBALr75PIrlmWb6HquHTn950DiJZY7eAW+IYSa3IFSNsbuYR2wmZPG294IRWQpbjsiSUHQjt85sjrbyuWqG4k4/CGejZ7Phu/cEvHt2xiKwi9lucYZvJ6qXXMnZnDSOHfoMSISoRbENzX6D1QHzXARET2w4BhIUEvq92R+NjIfIKELAoGAFw3u9S7AGZIb5dDw7JPyCqg+KWz+d6hRM+22rr1fE/Bqs7HASt1ch10Bm25G9UuLD40ND6qiClydODJiikrpRa7PUzVNiyQ9BScIm4UGXaASqoM4KbMZ18kX3YjDXqysSHAlG2g0OVEXtzMPYoMeNRyEPkpyY4gDOnj2xwQKQhUCgYEAqjmN6UXzKlewYpdGNjhHIlN/InDLSHcaLo2JNxYwlhQIR64xck40dcMT4SaympURGiCTIPd2oYzc/w5I+TjumHCuvyhw0MaKXMNMleAk/l9eq9FcXRn970qsQQOwssCokjthecdx9+rJr5dW2LJA/ueaa6DRsPuc81jPX/vkiZcCgYEAjmxMTpMopvGVN7SN+l1841QUqG5pnm7P67n2AgLxmazMJ2pFJCGp4kODTq+E6LKmJ1KbnqJwSkr4JoCuZSjc9aikKSIi9d/jXO9C98j6H/QadT8eqpGioOZrIStKnOU2dqREA0PBeQjTZBuAR59x2lN+D+NsdOspigSPj4wkuSQ=
cafe1de8-b19c-45a0-8f18-dfdd39951a2d	4245ec8f-10dc-4c85-8b59-45331e4d6f71	certificate	MIICmzCCAYMCBgF6pWh7sjANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjEwNzE0MTQyMzQ1WhcNMzEwNzE0MTQyNTI1WjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCrw5XfdEYWyGk2xvzxl9sm/N5DIkVSj/yQZp/vgZpvXnchQuhwSRyXQAsGpe89oI7Kzg8yAGD62j9ZjDgstQG2PFNcGJwqZAYYw26A9nQkdTXhCUA1RJZ8wilsGGgbFsI8dZ2cQ/28oM+D2sXcGnKdJNJDIHFkdydNIbPulQNG0Tm0Cq/ZrSx+wsNcNFeBkhsAscn0Urcv7dI8udKT7V0p8pltR/IuprgDgwrVbqQ7wwRewfxhwjuWNqFWiFeqygp+Z8w0grNWxXpdvLBAcI34ojsZEW7/1oHyXP0oTTQZxCvsEcbT/uvOXTtWP5N7mkFXLye2FMO1BwFOrbEP4ZJHAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAI/VhAxexhjZP6zsk986agVmnygTNa1FLY0vIZaksLJh5V9GDfb3EuANPXG0iaPeKapNvGtjRwL5zP5JBmEKjiyi0Ty6Uq83Z/8axUwUzm0CI03s6f5uD7LVcF5VDh0RXf/5Ftu3Ah6Z09ART2vUwlBY80UtPScs14gs75xUpWmD/cEchi8+4ohhaSIAHx3O6dOjm7u7wTERUSlpWV2hytuMzmCCobDjaixjazp+BaZrxa/KIwI9BJrtYa30Bs+70xzoZg93VYKaQo4nqX0WLsEuG+jDlfbwbgDHeiHNx9pUP7g9+GQolG2O0K9aITDsJvz6J7U+Quw6ZYITtDtj8yA=
b7d1a527-54ec-45c8-b0dc-139de8075f2f	4245ec8f-10dc-4c85-8b59-45331e4d6f71	algorithm	RS256
965bb310-1847-445f-82f7-68217a2cc9b0	6867b6fd-b82d-41f9-9690-2d78d95ed8b1	kid	8824c654-b809-46cf-a63c-5800443040f8
6f5da8fe-7814-43c9-8a12-e3a55e887bca	6867b6fd-b82d-41f9-9690-2d78d95ed8b1	algorithm	HS256
1c876aed-fd09-432f-83af-baefb4075e70	6867b6fd-b82d-41f9-9690-2d78d95ed8b1	priority	100
0c273165-533c-4247-9a27-5e586bb97b58	6867b6fd-b82d-41f9-9690-2d78d95ed8b1	secret	O6hf6qTJI2LThx7iRchr4dWyCnxIm0lMDui17dcW-_3X2WKnm7e_PO_fiwPKDbyZ7C8YICfVowC2qCFSvOZm2w
abc6df50-ed92-4ef3-8c0a-8363c361f835	9779cda5-7a0d-46a0-a83d-b6dd34cefe3e	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
f58b8530-14db-439f-ade7-dc7b53ca0c20	9779cda5-7a0d-46a0-a83d-b6dd34cefe3e	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
0ae685e6-a83f-47d3-9909-2a2325b5b76b	9779cda5-7a0d-46a0-a83d-b6dd34cefe3e	allowed-protocol-mapper-types	saml-role-list-mapper
d186009d-f1b5-42ff-974e-3b05da0c1cea	9779cda5-7a0d-46a0-a83d-b6dd34cefe3e	allowed-protocol-mapper-types	oidc-full-name-mapper
af45221f-f1b4-497c-85aa-2a8ace48dca1	9779cda5-7a0d-46a0-a83d-b6dd34cefe3e	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
951c1473-ea5b-4e7b-92cd-a84479597ee2	9779cda5-7a0d-46a0-a83d-b6dd34cefe3e	allowed-protocol-mapper-types	oidc-address-mapper
d76b2028-723d-47fb-a034-b51621b67b66	9779cda5-7a0d-46a0-a83d-b6dd34cefe3e	allowed-protocol-mapper-types	saml-user-property-mapper
b98e5304-1ef2-4995-b613-91e48e6c6f46	9779cda5-7a0d-46a0-a83d-b6dd34cefe3e	allowed-protocol-mapper-types	saml-user-attribute-mapper
1bee94e1-9901-4c41-9c61-383eac44b6da	73564b27-cc98-4dd8-8715-57891c27fcce	kid	cb8dce67-de01-4084-85c0-2740cdad2524
95771f43-8ce7-4a58-bae7-b51a5075a526	73564b27-cc98-4dd8-8715-57891c27fcce	priority	100
163daf0b-4da3-41a0-b9a4-9ae5e058fe4f	73564b27-cc98-4dd8-8715-57891c27fcce	secret	kDYFVXhR5DlVE-CHj1R-7g
b0d1c603-b36d-4311-8275-1557afdac3e6	5bb54576-8086-4522-abc2-6813e197fbde	certificate	MIICvzCCAacCBgF6pWlP1DANBgkqhkiG9w0BAQsFADAjMSEwHwYDVQQDDBhteS1ldmVyeWRheS1sb2xpdGEtcmVhbG0wHhcNMjEwNzE0MTQyNDQwWhcNMzEwNzE0MTQyNjIwWjAjMSEwHwYDVQQDDBhteS1ldmVyeWRheS1sb2xpdGEtcmVhbG0wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCWvyToV/AEwdcgQSZ8qAu7jvPVbhcK2cSZUr/Ot0837a3FzkGOdCdkugx0ZdcM0x9lCfIbQ3aITrYFtI2ttLJOTtUFLOI8q3b7zoQu3h45STnoI/V0G4tsYhWmbo4GaXrfF7ZtrVOEOMLo1xxAEgx7H2DlAiCUeJ1cLqrE00pAJydj+rwL8aD9/FFZ99jKaZAqzqVDoFslzLlNHRhm+wuMS7TvHddKSdlOBgEDDGvTluDQPS9k3d+90JPVcc+h3E0sKQYNaYLpxepQY24DUKfLDCDfL1sk/Ce9PB4oliJh2NVZwe4fP6U7AB3A+aIFMAgdPB4AkGAnwqUXEL5Jc9YfAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAHf5zVQxq7ITK4aWtu7s3QTdAob1/y0U7xOyuCllaBM6xT7KBzrk9adgRiQz1ox0B9V3iVgQgdCn9U8vl613IH3qyPVwkDX3ZPCMD5b75TJvgWxR5VURNQh7nG5byCpxOCSQp0lRx1qXiFNw+evzaAhkVG2UAhMXUaka+VaQyyQk56q0Q8ljTPhf6LaDIv87y/S2pSe0kawYaMhLIsRwZr7ZBtAoN4YyDNbpaz0XzQQP+2tugqNOj4KiWiwxHb4RrFuNltFYnNIhFFQIK1sdQqk77abTchgD2T0vWsONdUuvcrfYDHtZ4BVFdwP42itXmoXlqL1TlSGBjJwtpPgIB4M=
846ce962-8ca7-42f6-bcc5-73a6b1fc2c55	5bb54576-8086-4522-abc2-6813e197fbde	privateKey	MIIEpQIBAAKCAQEAlr8k6FfwBMHXIEEmfKgLu47z1W4XCtnEmVK/zrdPN+2txc5BjnQnZLoMdGXXDNMfZQnyG0N2iE62BbSNrbSyTk7VBSziPKt2+86ELt4eOUk56CP1dBuLbGIVpm6OBml63xe2ba1ThDjC6NccQBIMex9g5QIglHidXC6qxNNKQCcnY/q8C/Gg/fxRWffYymmQKs6lQ6BbJcy5TR0YZvsLjEu07x3XSknZTgYBAwxr05bg0D0vZN3fvdCT1XHPodxNLCkGDWmC6cXqUGNuA1Cnywwg3y9bJPwnvTweKJYiYdjVWcHuHz+lOwAdwPmiBTAIHTweAJBgJ8KlFxC+SXPWHwIDAQABAoIBADCU/48FsCvIUM1rUb0A528V3g1dwn1Tu2FyYrB7MUOkDMnf9A617nAu6Y6lt43BiK+maelRb6T0QQNLshFWV9SW4V4enfbRh7IVV1JHcVQi/k+LLzKxIdjXL4UIwirc2aWGXduw7WaCaIZjtQ4NAwq3DSIg8mEHEifAPnbaZWaQ+IN/PyTimi3ig6VpjygNYMGtQPm5kZ9bpjcVklPgmVFYFwb6mU9+FV8Tp2jpeUsLlhdvCjw+olf2GK9nxW2wq+qELBRTqTmrAj22Y8+zHygzYJjmKmDA6DHosuZzU9DaN8PJ7eiDnQz5zs/td/Sx41PfF+lGn1WEQ/n27hDaeokCgYEA2U7Y9zoVYOgaN/LKrIvpzQBHOTgrMWQmyTv51bNDIOdGUND06+wdFCdK4y/lR0z6SLiitx2NPspnbGpmT2AA2v+sYdyEa8LMota8BJkhqAmI8xSu4MDuo7iR62b3Uo7NzhJrw/0mbc6lFnfdFP/XjwNvae4zMqCsXl8yAv4x9rMCgYEAsZZaWjFi3IIvUDm+7cE86EEurc/E4Fe15MkuGoEqBJVeTbfRVQjUJUpOACikI+z1tytVjk95RjJGcdtl0FthztunPPpLZ/sVAyeQuktyS2i1zA8YbmFYsELCmXPjUH6x9WBbdwiEg39TetjSkWHgXo87O/CKejaZRSbiVMGJOOUCgYEAup7GIXsWRQTPD2FIwOnfPZiRkgg0v9P8ozEIm4e5jJZRgrIQxFMfneEopPbK9nKbNrypsIyGcngsXaT6fAw8b9bGWbyhVFIUn3u+OEyx5LdbjzYiAQ5vYtbtKbZhi9rrz59uQQA0cnTWvfbuPEyC+GTA174i5cWqgdlK/9sBWZUCgYEAiSUIGedR5eFUEKzH2m0FUIgmOGzgKjESc+E6DZX6kReZgIOY0gbDbxj3LAA24te42wBITmLp0WA6Luu0sTghTNMtUFYrrd2l2I3Ro01osvvmifSOdZAQIe8swb1Rq3Rskocbm82kKLlICvMvir7/u4b5w8x75O4NZ+4jlDjydFECgYEAoqugpbhFBziZZq7z/HvZYeRvv48Agg/eIcUA1GXCbjC2/P+UxNcyyz7vM/EA48soEcgqjnl1bEsNgR/4aglX//I6hmh/kGXrzUSOUQnLZJHl9VcSI24nAGJLKn3IaHVEH4a/n5NHqUxKIR5NYGdFULHCYUm8/CBmnlgYDfJz/RM=
39ca5dd6-7bd4-4e00-8b9f-f71b5db34408	5bb54576-8086-4522-abc2-6813e197fbde	priority	100
a46e7996-9145-43f1-92a6-8e23ec7e9d19	6ea9dc0e-cc1d-4aa9-8a9b-064cc817c72f	host-sending-registration-request-must-match	true
9cc28713-4bc8-453a-997b-53bcd4a58534	6ea9dc0e-cc1d-4aa9-8a9b-064cc817c72f	client-uris-must-match	true
0abe4da7-202d-44f6-accf-bf77cc0fd2d5	df4bc170-abe3-46ac-b8e1-ac14212c34f9	max-clients	200
b0e7f23e-3942-404a-80b7-3bd5bdd6815f	d594c7b4-4376-4426-9df9-1a60e3bf856a	allow-default-scopes	true
d2b9a6fb-97b6-4959-a306-aa5ddec07099	8cf37b2f-f381-466d-a8f8-95598609ff11	allow-default-scopes	true
ca57a0a3-5da9-4f10-bdea-fb0397a174d3	f09b6acd-3db8-466c-8c7a-a442e4454af1	allowed-protocol-mapper-types	saml-user-attribute-mapper
39235b68-936e-4f67-a195-4bc2f620d087	f09b6acd-3db8-466c-8c7a-a442e4454af1	allowed-protocol-mapper-types	oidc-full-name-mapper
52095ff2-3759-443e-ad3a-72e764f66113	f09b6acd-3db8-466c-8c7a-a442e4454af1	allowed-protocol-mapper-types	saml-role-list-mapper
cd2cb2f3-d9dc-48c3-86b2-3a3f439a58af	f09b6acd-3db8-466c-8c7a-a442e4454af1	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
680b1050-395a-4a2f-83a5-7f897015731e	f09b6acd-3db8-466c-8c7a-a442e4454af1	allowed-protocol-mapper-types	oidc-address-mapper
7cb09df4-bf8f-4d8a-85d4-be030cf3373d	f09b6acd-3db8-466c-8c7a-a442e4454af1	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
ae1b74ae-33a7-4226-adae-c494be298b12	f09b6acd-3db8-466c-8c7a-a442e4454af1	allowed-protocol-mapper-types	saml-user-property-mapper
f881cc9e-4093-4eb6-ac7b-0b3d4d314445	f09b6acd-3db8-466c-8c7a-a442e4454af1	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
\.


--
-- Data for Name: composite_role; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.composite_role (composite, child_role) FROM stdin;
f16ec247-e190-4571-83fd-7ebaaa8da265	3fae8297-8359-4e8a-81dd-8bc1a1cf5926
f16ec247-e190-4571-83fd-7ebaaa8da265	1103c56d-4c70-41d5-a03d-5ad953b60e3a
f16ec247-e190-4571-83fd-7ebaaa8da265	279a285b-173f-4ef0-9da2-c4fa35d10b26
f16ec247-e190-4571-83fd-7ebaaa8da265	52fdef77-d587-409b-803c-6ad347863c55
f16ec247-e190-4571-83fd-7ebaaa8da265	0d6e2e42-179d-4e79-b147-516cf4f1095c
f16ec247-e190-4571-83fd-7ebaaa8da265	db581e85-20d9-4ca1-aee4-1cc80b136571
f16ec247-e190-4571-83fd-7ebaaa8da265	22135b6f-d1a5-4742-9757-2e65ea0e9d21
f16ec247-e190-4571-83fd-7ebaaa8da265	806b93ee-a330-43f7-ae9c-95c5de0e5d91
f16ec247-e190-4571-83fd-7ebaaa8da265	69af75f4-b51a-4f87-8993-17709fb6ae01
f16ec247-e190-4571-83fd-7ebaaa8da265	23e17969-0cb5-47ad-b2f5-07bb27c866ba
f16ec247-e190-4571-83fd-7ebaaa8da265	c24f313c-9ec8-4d67-8a49-72a91ec07458
f16ec247-e190-4571-83fd-7ebaaa8da265	b04d1951-2ce4-44fa-b277-57b99771f871
f16ec247-e190-4571-83fd-7ebaaa8da265	7aae7bb3-ed89-41ff-921a-a7dd7f2321be
f16ec247-e190-4571-83fd-7ebaaa8da265	be759bc2-2f0b-47e6-81e5-5935a060baf1
f16ec247-e190-4571-83fd-7ebaaa8da265	216265bc-23d8-453f-84e6-4f7ef604ab7f
f16ec247-e190-4571-83fd-7ebaaa8da265	ea0f85e1-c518-4470-884f-fab898a9a856
f16ec247-e190-4571-83fd-7ebaaa8da265	01b2208f-b7ee-4726-8263-247d518af2c2
f16ec247-e190-4571-83fd-7ebaaa8da265	ebff1792-72e2-4456-af0f-6537d76cd694
0d6e2e42-179d-4e79-b147-516cf4f1095c	ea0f85e1-c518-4470-884f-fab898a9a856
52fdef77-d587-409b-803c-6ad347863c55	216265bc-23d8-453f-84e6-4f7ef604ab7f
52fdef77-d587-409b-803c-6ad347863c55	ebff1792-72e2-4456-af0f-6537d76cd694
a24019b4-1610-4d94-956d-0d43431bf65f	7333fabc-a5f8-44b7-a825-f9e6ec7eeafd
3501da45-6c0d-4bcb-98b2-72db31412fc2	e50f6ca3-2f69-45ee-b1ea-9a697d8adb08
f16ec247-e190-4571-83fd-7ebaaa8da265	9763f68d-c5ae-429a-9776-dac43aa527fb
f16ec247-e190-4571-83fd-7ebaaa8da265	bb5cedc5-61ea-45f0-a016-85d2b2e014c6
f16ec247-e190-4571-83fd-7ebaaa8da265	0d3ea6b6-0b67-4e71-a5dd-5d12fddeea4b
f16ec247-e190-4571-83fd-7ebaaa8da265	d17103a6-f45b-4ba0-8b4a-2896cbfbf24a
f16ec247-e190-4571-83fd-7ebaaa8da265	d3d31fae-a5b5-4e17-9624-c240d066cb98
f16ec247-e190-4571-83fd-7ebaaa8da265	bbec3dc1-5cd1-4024-8116-8f777d101ca9
f16ec247-e190-4571-83fd-7ebaaa8da265	a1b717d9-92d2-46d9-965b-5a0951165fb8
f16ec247-e190-4571-83fd-7ebaaa8da265	6325176a-6b0f-443a-810d-b3d1b645d116
f16ec247-e190-4571-83fd-7ebaaa8da265	21484593-ea88-40e0-967a-6a0db2519c49
f16ec247-e190-4571-83fd-7ebaaa8da265	541b50cb-3bc6-4ec0-870d-0960f486ff1d
f16ec247-e190-4571-83fd-7ebaaa8da265	8bad62c2-4b2d-43c1-8b0c-c42dc563931b
f16ec247-e190-4571-83fd-7ebaaa8da265	35dd8eb8-aeab-4ee8-8915-46cef7459059
f16ec247-e190-4571-83fd-7ebaaa8da265	a6189298-8713-43a6-9b67-6d48ae62bbd7
f16ec247-e190-4571-83fd-7ebaaa8da265	dd1384a9-062f-44e2-a39d-0eeffbbd6c46
f16ec247-e190-4571-83fd-7ebaaa8da265	05ec3d56-84b9-4d3b-9ff7-f0dd706d3b08
f16ec247-e190-4571-83fd-7ebaaa8da265	9ff17ef1-7321-4c44-9c13-d31fc33b0a33
f16ec247-e190-4571-83fd-7ebaaa8da265	5826461b-94b0-4ad9-8b9b-1b660ba1e0b8
f16ec247-e190-4571-83fd-7ebaaa8da265	f7d04592-2348-4cdc-a3a5-74ecb14a24bc
d3d31fae-a5b5-4e17-9624-c240d066cb98	9ff17ef1-7321-4c44-9c13-d31fc33b0a33
d17103a6-f45b-4ba0-8b4a-2896cbfbf24a	05ec3d56-84b9-4d3b-9ff7-f0dd706d3b08
d17103a6-f45b-4ba0-8b4a-2896cbfbf24a	f7d04592-2348-4cdc-a3a5-74ecb14a24bc
e4a368c6-2e0d-4294-8468-aaac12bef7e0	9699cb90-1bc6-4869-bcb3-503120414690
e4a368c6-2e0d-4294-8468-aaac12bef7e0	d72dcbb1-4e3d-4488-b581-1c6deb42b618
e4a368c6-2e0d-4294-8468-aaac12bef7e0	ffedf372-f9be-444e-9dc2-df703779b662
e4a368c6-2e0d-4294-8468-aaac12bef7e0	27ee1374-b3fe-4567-bed7-4a3e4be9e27b
e4a368c6-2e0d-4294-8468-aaac12bef7e0	3e1b0be7-4283-4886-a81c-b3a595a0fb04
e4a368c6-2e0d-4294-8468-aaac12bef7e0	82cf162c-64b8-4633-b6b9-95b63da11561
e4a368c6-2e0d-4294-8468-aaac12bef7e0	a9e89c0d-8df0-44b2-ad4d-10cee713cc24
e4a368c6-2e0d-4294-8468-aaac12bef7e0	27463e65-c884-4b7d-b2b7-d142514eeda8
e4a368c6-2e0d-4294-8468-aaac12bef7e0	34ccc0c5-0750-41a2-8961-9a0d97d44eee
e4a368c6-2e0d-4294-8468-aaac12bef7e0	b2f5f644-d161-4675-91c9-93359c944cf3
e4a368c6-2e0d-4294-8468-aaac12bef7e0	c754d187-e55e-4259-841e-e2bf8491ccce
e4a368c6-2e0d-4294-8468-aaac12bef7e0	55bd8471-135b-423b-b0b4-3731df6e34b9
e4a368c6-2e0d-4294-8468-aaac12bef7e0	9bddaff9-9a54-40e6-8130-c8df9f56f518
e4a368c6-2e0d-4294-8468-aaac12bef7e0	9eaf204e-8eaf-42dc-a42b-6b35c0f5e7f2
e4a368c6-2e0d-4294-8468-aaac12bef7e0	856e74df-bdbe-4493-9f11-4acbc586c5a9
e4a368c6-2e0d-4294-8468-aaac12bef7e0	0b338ed3-f094-41b0-a881-48d060b3b9b1
e4a368c6-2e0d-4294-8468-aaac12bef7e0	295775a0-32a9-41cd-8ccf-1995fd836138
e4a368c6-2e0d-4294-8468-aaac12bef7e0	16979ae5-317d-4f7a-82bd-8c8a2f74906d
295775a0-32a9-41cd-8ccf-1995fd836138	d72dcbb1-4e3d-4488-b581-1c6deb42b618
0b338ed3-f094-41b0-a881-48d060b3b9b1	34ccc0c5-0750-41a2-8961-9a0d97d44eee
0b338ed3-f094-41b0-a881-48d060b3b9b1	a9e89c0d-8df0-44b2-ad4d-10cee713cc24
199da019-bb23-49d2-95db-eb0d1cdb3e3c	3d2ccce0-4572-4c4f-b308-b4b84fe05271
fd608b12-4f4b-4201-822c-6d11593bdabe	372e14fb-946a-45b9-a077-655680f2fdda
f16ec247-e190-4571-83fd-7ebaaa8da265	9071777c-4a12-4b78-92f7-3cacd636fadf
5b12ec76-b86f-4420-82ec-475b9bd83381	816fa806-10e8-413b-95be-12173b8981eb
5b12ec76-b86f-4420-82ec-475b9bd83381	1147723f-9cd1-4519-9ef4-9493fc722196
5b12ec76-b86f-4420-82ec-475b9bd83381	419b7053-b11a-483c-8cbe-12f6cab38385
\.


--
-- Data for Name: credential; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.credential (id, salt, type, user_id, created_date, user_label, secret_data, credential_data, priority) FROM stdin;
6ec03942-7738-46a7-b39e-4002660c13ac	\N	password	7088b70f-fed1-4093-b2c5-9cd4678655c5	1626272693264	\N	{"value":"XRIWW9+lT6xGVxvlWgMHePGjNzDpUgYkd4R2Ki7zV8acV4PCOvBmC4szi4ydneyZsiOaOnnBFXST6X35WrveUQ==","salt":"a6ekNAQyBdBGqc0xDvBjhg==","additionalParameters":{}}	{"hashIterations":27500,"algorithm":"pbkdf2-sha256","additionalParameters":{}}	10
59a8ffed-441b-455c-aa1b-410bf41b8da8	\N	password	1a2be0fe-9f6a-4c2a-947d-bfa70092f57c	1642326400710	\N	{"value":"13QrKyGguUqwZp7pStAZVA7EVd4MS4XTdmuhOloS7puSyRQG5fQFBm0dNZ5zT676CYB9uQGL1Mt0iwyN/YUemg==","salt":"5/d5EKtoUHlSynG09kaHUg==","additionalParameters":{}}	{"hashIterations":27500,"algorithm":"pbkdf2-sha256","additionalParameters":{}}	10
\.


--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
1.0.0.Final-KEYCLOAK-5461	sthorger@redhat.com	META-INF/jpa-changelog-1.0.0.Final.xml	2021-07-14 14:24:47.024819	1	EXECUTED	7:4e70412f24a3f382c82183742ec79317	createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...		\N	3.5.4	\N	\N	6272686532
1.0.0.Final-KEYCLOAK-5461	sthorger@redhat.com	META-INF/db2-jpa-changelog-1.0.0.Final.xml	2021-07-14 14:24:47.041734	2	MARK_RAN	7:cb16724583e9675711801c6875114f28	createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...		\N	3.5.4	\N	\N	6272686532
1.1.0.Beta1	sthorger@redhat.com	META-INF/jpa-changelog-1.1.0.Beta1.xml	2021-07-14 14:24:47.094923	3	EXECUTED	7:0310eb8ba07cec616460794d42ade0fa	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=CLIENT_ATTRIBUTES; createTable tableName=CLIENT_SESSION_NOTE; createTable tableName=APP_NODE_REGISTRATIONS; addColumn table...		\N	3.5.4	\N	\N	6272686532
1.1.0.Final	sthorger@redhat.com	META-INF/jpa-changelog-1.1.0.Final.xml	2021-07-14 14:24:47.100003	4	EXECUTED	7:5d25857e708c3233ef4439df1f93f012	renameColumn newColumnName=EVENT_TIME, oldColumnName=TIME, tableName=EVENT_ENTITY		\N	3.5.4	\N	\N	6272686532
1.2.0.Beta1	psilva@redhat.com	META-INF/jpa-changelog-1.2.0.Beta1.xml	2021-07-14 14:24:47.274597	5	EXECUTED	7:c7a54a1041d58eb3817a4a883b4d4e84	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...		\N	3.5.4	\N	\N	6272686532
1.2.0.Beta1	psilva@redhat.com	META-INF/db2-jpa-changelog-1.2.0.Beta1.xml	2021-07-14 14:24:47.279234	6	MARK_RAN	7:2e01012df20974c1c2a605ef8afe25b7	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...		\N	3.5.4	\N	\N	6272686532
1.2.0.RC1	bburke@redhat.com	META-INF/jpa-changelog-1.2.0.CR1.xml	2021-07-14 14:24:47.437943	7	EXECUTED	7:0f08df48468428e0f30ee59a8ec01a41	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...		\N	3.5.4	\N	\N	6272686532
1.2.0.RC1	bburke@redhat.com	META-INF/db2-jpa-changelog-1.2.0.CR1.xml	2021-07-14 14:24:47.442161	8	MARK_RAN	7:a77ea2ad226b345e7d689d366f185c8c	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...		\N	3.5.4	\N	\N	6272686532
1.2.0.Final	keycloak	META-INF/jpa-changelog-1.2.0.Final.xml	2021-07-14 14:24:47.447102	9	EXECUTED	7:a3377a2059aefbf3b90ebb4c4cc8e2ab	update tableName=CLIENT; update tableName=CLIENT; update tableName=CLIENT		\N	3.5.4	\N	\N	6272686532
1.3.0	bburke@redhat.com	META-INF/jpa-changelog-1.3.0.xml	2021-07-14 14:24:47.637384	10	EXECUTED	7:04c1dbedc2aa3e9756d1a1668e003451	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=ADMI...		\N	3.5.4	\N	\N	6272686532
1.4.0	bburke@redhat.com	META-INF/jpa-changelog-1.4.0.xml	2021-07-14 14:24:47.779266	11	EXECUTED	7:36ef39ed560ad07062d956db861042ba	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	3.5.4	\N	\N	6272686532
1.4.0	bburke@redhat.com	META-INF/db2-jpa-changelog-1.4.0.xml	2021-07-14 14:24:47.782638	12	MARK_RAN	7:d909180b2530479a716d3f9c9eaea3d7	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	3.5.4	\N	\N	6272686532
1.5.0	bburke@redhat.com	META-INF/jpa-changelog-1.5.0.xml	2021-07-14 14:24:47.799762	13	EXECUTED	7:cf12b04b79bea5152f165eb41f3955f6	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	3.5.4	\N	\N	6272686532
1.6.1_from15	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2021-07-14 14:24:47.840216	14	EXECUTED	7:7e32c8f05c755e8675764e7d5f514509	addColumn tableName=REALM; addColumn tableName=KEYCLOAK_ROLE; addColumn tableName=CLIENT; createTable tableName=OFFLINE_USER_SESSION; createTable tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_US_SES_PK2, tableName=...		\N	3.5.4	\N	\N	6272686532
1.6.1_from16-pre	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2021-07-14 14:24:47.843618	15	MARK_RAN	7:980ba23cc0ec39cab731ce903dd01291	delete tableName=OFFLINE_CLIENT_SESSION; delete tableName=OFFLINE_USER_SESSION		\N	3.5.4	\N	\N	6272686532
1.6.1_from16	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2021-07-14 14:24:47.846507	16	MARK_RAN	7:2fa220758991285312eb84f3b4ff5336	dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_US_SES_PK, tableName=OFFLINE_USER_SESSION; dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_CL_SES_PK, tableName=OFFLINE_CLIENT_SESSION; addColumn tableName=OFFLINE_USER_SESSION; update tableName=OF...		\N	3.5.4	\N	\N	6272686532
1.6.1	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2021-07-14 14:24:47.849048	17	EXECUTED	7:d41d8cd98f00b204e9800998ecf8427e	empty		\N	3.5.4	\N	\N	6272686532
1.7.0	bburke@redhat.com	META-INF/jpa-changelog-1.7.0.xml	2021-07-14 14:24:47.910169	18	EXECUTED	7:91ace540896df890cc00a0490ee52bbc	createTable tableName=KEYCLOAK_GROUP; createTable tableName=GROUP_ROLE_MAPPING; createTable tableName=GROUP_ATTRIBUTE; createTable tableName=USER_GROUP_MEMBERSHIP; createTable tableName=REALM_DEFAULT_GROUPS; addColumn tableName=IDENTITY_PROVIDER; ...		\N	3.5.4	\N	\N	6272686532
1.8.0	mposolda@redhat.com	META-INF/jpa-changelog-1.8.0.xml	2021-07-14 14:24:47.967788	19	EXECUTED	7:c31d1646dfa2618a9335c00e07f89f24	addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...		\N	3.5.4	\N	\N	6272686532
1.8.0-2	keycloak	META-INF/jpa-changelog-1.8.0.xml	2021-07-14 14:24:47.974038	20	EXECUTED	7:df8bc21027a4f7cbbb01f6344e89ce07	dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL		\N	3.5.4	\N	\N	6272686532
authz-3.4.0.CR1-resource-server-pk-change-part1	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2021-07-14 14:24:48.893155	45	EXECUTED	7:6a48ce645a3525488a90fbf76adf3bb3	addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_RESOURCE; addColumn tableName=RESOURCE_SERVER_SCOPE		\N	3.5.4	\N	\N	6272686532
1.8.0	mposolda@redhat.com	META-INF/db2-jpa-changelog-1.8.0.xml	2021-07-14 14:24:47.977148	21	MARK_RAN	7:f987971fe6b37d963bc95fee2b27f8df	addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...		\N	3.5.4	\N	\N	6272686532
1.8.0-2	keycloak	META-INF/db2-jpa-changelog-1.8.0.xml	2021-07-14 14:24:47.980258	22	MARK_RAN	7:df8bc21027a4f7cbbb01f6344e89ce07	dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL		\N	3.5.4	\N	\N	6272686532
1.9.0	mposolda@redhat.com	META-INF/jpa-changelog-1.9.0.xml	2021-07-14 14:24:48.003286	23	EXECUTED	7:ed2dc7f799d19ac452cbcda56c929e47	update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=REALM; update tableName=REALM; customChange; dr...		\N	3.5.4	\N	\N	6272686532
1.9.1	keycloak	META-INF/jpa-changelog-1.9.1.xml	2021-07-14 14:24:48.009538	24	EXECUTED	7:80b5db88a5dda36ece5f235be8757615	modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=PUBLIC_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM		\N	3.5.4	\N	\N	6272686532
1.9.1	keycloak	META-INF/db2-jpa-changelog-1.9.1.xml	2021-07-14 14:24:48.012144	25	MARK_RAN	7:1437310ed1305a9b93f8848f301726ce	modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM		\N	3.5.4	\N	\N	6272686532
1.9.2	keycloak	META-INF/jpa-changelog-1.9.2.xml	2021-07-14 14:24:48.087021	26	EXECUTED	7:b82ffb34850fa0836be16deefc6a87c4	createIndex indexName=IDX_USER_EMAIL, tableName=USER_ENTITY; createIndex indexName=IDX_USER_ROLE_MAPPING, tableName=USER_ROLE_MAPPING; createIndex indexName=IDX_USER_GROUP_MAPPING, tableName=USER_GROUP_MEMBERSHIP; createIndex indexName=IDX_USER_CO...		\N	3.5.4	\N	\N	6272686532
authz-2.0.0	psilva@redhat.com	META-INF/jpa-changelog-authz-2.0.0.xml	2021-07-14 14:24:48.230603	27	EXECUTED	7:9cc98082921330d8d9266decdd4bd658	createTable tableName=RESOURCE_SERVER; addPrimaryKey constraintName=CONSTRAINT_FARS, tableName=RESOURCE_SERVER; addUniqueConstraint constraintName=UK_AU8TT6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER; createTable tableName=RESOURCE_SERVER_RESOU...		\N	3.5.4	\N	\N	6272686532
authz-2.5.1	psilva@redhat.com	META-INF/jpa-changelog-authz-2.5.1.xml	2021-07-14 14:24:48.234327	28	EXECUTED	7:03d64aeed9cb52b969bd30a7ac0db57e	update tableName=RESOURCE_SERVER_POLICY		\N	3.5.4	\N	\N	6272686532
2.1.0-KEYCLOAK-5461	bburke@redhat.com	META-INF/jpa-changelog-2.1.0.xml	2021-07-14 14:24:48.366585	29	EXECUTED	7:f1f9fd8710399d725b780f463c6b21cd	createTable tableName=BROKER_LINK; createTable tableName=FED_USER_ATTRIBUTE; createTable tableName=FED_USER_CONSENT; createTable tableName=FED_USER_CONSENT_ROLE; createTable tableName=FED_USER_CONSENT_PROT_MAPPER; createTable tableName=FED_USER_CR...		\N	3.5.4	\N	\N	6272686532
2.2.0	bburke@redhat.com	META-INF/jpa-changelog-2.2.0.xml	2021-07-14 14:24:48.391197	30	EXECUTED	7:53188c3eb1107546e6f765835705b6c1	addColumn tableName=ADMIN_EVENT_ENTITY; createTable tableName=CREDENTIAL_ATTRIBUTE; createTable tableName=FED_CREDENTIAL_ATTRIBUTE; modifyDataType columnName=VALUE, tableName=CREDENTIAL; addForeignKeyConstraint baseTableName=FED_CREDENTIAL_ATTRIBU...		\N	3.5.4	\N	\N	6272686532
2.3.0	bburke@redhat.com	META-INF/jpa-changelog-2.3.0.xml	2021-07-14 14:24:48.414776	31	EXECUTED	7:d6e6f3bc57a0c5586737d1351725d4d4	createTable tableName=FEDERATED_USER; addPrimaryKey constraintName=CONSTR_FEDERATED_USER, tableName=FEDERATED_USER; dropDefaultValue columnName=TOTP, tableName=USER_ENTITY; dropColumn columnName=TOTP, tableName=USER_ENTITY; addColumn tableName=IDE...		\N	3.5.4	\N	\N	6272686532
2.4.0	bburke@redhat.com	META-INF/jpa-changelog-2.4.0.xml	2021-07-14 14:24:48.419626	32	EXECUTED	7:454d604fbd755d9df3fd9c6329043aa5	customChange		\N	3.5.4	\N	\N	6272686532
2.5.0	bburke@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2021-07-14 14:24:48.425849	33	EXECUTED	7:57e98a3077e29caf562f7dbf80c72600	customChange; modifyDataType columnName=USER_ID, tableName=OFFLINE_USER_SESSION		\N	3.5.4	\N	\N	6272686532
2.5.0-unicode-oracle	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2021-07-14 14:24:48.428392	34	MARK_RAN	7:e4c7e8f2256210aee71ddc42f538b57a	modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...		\N	3.5.4	\N	\N	6272686532
2.5.0-unicode-other-dbs	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2021-07-14 14:24:48.465057	35	EXECUTED	7:09a43c97e49bc626460480aa1379b522	modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...		\N	3.5.4	\N	\N	6272686532
2.5.0-duplicate-email-support	slawomir@dabek.name	META-INF/jpa-changelog-2.5.0.xml	2021-07-14 14:24:48.47264	36	EXECUTED	7:26bfc7c74fefa9126f2ce702fb775553	addColumn tableName=REALM		\N	3.5.4	\N	\N	6272686532
2.5.0-unique-group-names	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2021-07-14 14:24:48.483831	37	EXECUTED	7:a161e2ae671a9020fff61e996a207377	addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	6272686532
2.5.1	bburke@redhat.com	META-INF/jpa-changelog-2.5.1.xml	2021-07-14 14:24:48.489994	38	EXECUTED	7:37fc1781855ac5388c494f1442b3f717	addColumn tableName=FED_USER_CONSENT		\N	3.5.4	\N	\N	6272686532
3.0.0	bburke@redhat.com	META-INF/jpa-changelog-3.0.0.xml	2021-07-14 14:24:48.494673	39	EXECUTED	7:13a27db0dae6049541136adad7261d27	addColumn tableName=IDENTITY_PROVIDER		\N	3.5.4	\N	\N	6272686532
3.2.0-fix	keycloak	META-INF/jpa-changelog-3.2.0.xml	2021-07-14 14:24:48.497228	40	MARK_RAN	7:550300617e3b59e8af3a6294df8248a3	addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS		\N	3.5.4	\N	\N	6272686532
3.2.0-fix-with-keycloak-5416	keycloak	META-INF/jpa-changelog-3.2.0.xml	2021-07-14 14:24:48.499942	41	MARK_RAN	7:e3a9482b8931481dc2772a5c07c44f17	dropIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS; addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS; createIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS		\N	3.5.4	\N	\N	6272686532
3.2.0-fix-offline-sessions	hmlnarik	META-INF/jpa-changelog-3.2.0.xml	2021-07-14 14:24:48.505315	42	EXECUTED	7:72b07d85a2677cb257edb02b408f332d	customChange		\N	3.5.4	\N	\N	6272686532
3.2.0-fixed	keycloak	META-INF/jpa-changelog-3.2.0.xml	2021-07-14 14:24:48.881119	43	EXECUTED	7:a72a7858967bd414835d19e04d880312	addColumn tableName=REALM; dropPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_PK2, tableName=OFFLINE_CLIENT_SESSION; dropColumn columnName=CLIENT_SESSION_ID, tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_P...		\N	3.5.4	\N	\N	6272686532
3.3.0	keycloak	META-INF/jpa-changelog-3.3.0.xml	2021-07-14 14:24:48.887415	44	EXECUTED	7:94edff7cf9ce179e7e85f0cd78a3cf2c	addColumn tableName=USER_ENTITY		\N	3.5.4	\N	\N	6272686532
authz-3.4.0.CR1-resource-server-pk-change-part2-KEYCLOAK-6095	hmlnarik@redhat.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2021-07-14 14:24:48.897275	46	EXECUTED	7:e64b5dcea7db06077c6e57d3b9e5ca14	customChange		\N	3.5.4	\N	\N	6272686532
authz-3.4.0.CR1-resource-server-pk-change-part3-fixed	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2021-07-14 14:24:48.899855	47	MARK_RAN	7:fd8cf02498f8b1e72496a20afc75178c	dropIndex indexName=IDX_RES_SERV_POL_RES_SERV, tableName=RESOURCE_SERVER_POLICY; dropIndex indexName=IDX_RES_SRV_RES_RES_SRV, tableName=RESOURCE_SERVER_RESOURCE; dropIndex indexName=IDX_RES_SRV_SCOPE_RES_SRV, tableName=RESOURCE_SERVER_SCOPE		\N	3.5.4	\N	\N	6272686532
authz-3.4.0.CR1-resource-server-pk-change-part3-fixed-nodropindex	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2021-07-14 14:24:48.958108	48	EXECUTED	7:542794f25aa2b1fbabb7e577d6646319	addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_POLICY; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_RESOURCE; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, ...		\N	3.5.4	\N	\N	6272686532
authn-3.4.0.CR1-refresh-token-max-reuse	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2021-07-14 14:24:48.964048	49	EXECUTED	7:edad604c882df12f74941dac3cc6d650	addColumn tableName=REALM		\N	3.5.4	\N	\N	6272686532
3.4.0	keycloak	META-INF/jpa-changelog-3.4.0.xml	2021-07-14 14:24:49.115546	50	EXECUTED	7:0f88b78b7b46480eb92690cbf5e44900	addPrimaryKey constraintName=CONSTRAINT_REALM_DEFAULT_ROLES, tableName=REALM_DEFAULT_ROLES; addPrimaryKey constraintName=CONSTRAINT_COMPOSITE_ROLE, tableName=COMPOSITE_ROLE; addPrimaryKey constraintName=CONSTR_REALM_DEFAULT_GROUPS, tableName=REALM...		\N	3.5.4	\N	\N	6272686532
3.4.0-KEYCLOAK-5230	hmlnarik@redhat.com	META-INF/jpa-changelog-3.4.0.xml	2021-07-14 14:24:49.183969	51	EXECUTED	7:d560e43982611d936457c327f872dd59	createIndex indexName=IDX_FU_ATTRIBUTE, tableName=FED_USER_ATTRIBUTE; createIndex indexName=IDX_FU_CONSENT, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CONSENT_RU, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CREDENTIAL, t...		\N	3.5.4	\N	\N	6272686532
3.4.1	psilva@redhat.com	META-INF/jpa-changelog-3.4.1.xml	2021-07-14 14:24:49.189229	52	EXECUTED	7:c155566c42b4d14ef07059ec3b3bbd8e	modifyDataType columnName=VALUE, tableName=CLIENT_ATTRIBUTES		\N	3.5.4	\N	\N	6272686532
3.4.2	keycloak	META-INF/jpa-changelog-3.4.2.xml	2021-07-14 14:24:49.192699	53	EXECUTED	7:b40376581f12d70f3c89ba8ddf5b7dea	update tableName=REALM		\N	3.5.4	\N	\N	6272686532
3.4.2-KEYCLOAK-5172	mkanis@redhat.com	META-INF/jpa-changelog-3.4.2.xml	2021-07-14 14:24:49.196114	54	EXECUTED	7:a1132cc395f7b95b3646146c2e38f168	update tableName=CLIENT		\N	3.5.4	\N	\N	6272686532
4.0.0-KEYCLOAK-6335	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2021-07-14 14:24:49.207708	55	EXECUTED	7:d8dc5d89c789105cfa7ca0e82cba60af	createTable tableName=CLIENT_AUTH_FLOW_BINDINGS; addPrimaryKey constraintName=C_CLI_FLOW_BIND, tableName=CLIENT_AUTH_FLOW_BINDINGS		\N	3.5.4	\N	\N	6272686532
4.0.0-CLEANUP-UNUSED-TABLE	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2021-07-14 14:24:49.213494	56	EXECUTED	7:7822e0165097182e8f653c35517656a3	dropTable tableName=CLIENT_IDENTITY_PROV_MAPPING		\N	3.5.4	\N	\N	6272686532
4.0.0-KEYCLOAK-6228	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2021-07-14 14:24:49.252631	57	EXECUTED	7:c6538c29b9c9a08f9e9ea2de5c2b6375	dropUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHOGM8UEWRT, tableName=USER_CONSENT; dropNotNullConstraint columnName=CLIENT_ID, tableName=USER_CONSENT; addColumn tableName=USER_CONSENT; addUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHO...		\N	3.5.4	\N	\N	6272686532
4.0.0-KEYCLOAK-5579-fixed	mposolda@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2021-07-14 14:24:49.427824	58	EXECUTED	7:6d4893e36de22369cf73bcb051ded875	dropForeignKeyConstraint baseTableName=CLIENT_TEMPLATE_ATTRIBUTES, constraintName=FK_CL_TEMPL_ATTR_TEMPL; renameTable newTableName=CLIENT_SCOPE_ATTRIBUTES, oldTableName=CLIENT_TEMPLATE_ATTRIBUTES; renameColumn newColumnName=SCOPE_ID, oldColumnName...		\N	3.5.4	\N	\N	6272686532
authz-4.0.0.CR1	psilva@redhat.com	META-INF/jpa-changelog-authz-4.0.0.CR1.xml	2021-07-14 14:24:49.470848	59	EXECUTED	7:57960fc0b0f0dd0563ea6f8b2e4a1707	createTable tableName=RESOURCE_SERVER_PERM_TICKET; addPrimaryKey constraintName=CONSTRAINT_FAPMT, tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRHO213XCX4WNKOG82SSPMT...		\N	3.5.4	\N	\N	6272686532
authz-4.0.0.Beta3	psilva@redhat.com	META-INF/jpa-changelog-authz-4.0.0.Beta3.xml	2021-07-14 14:24:49.477585	60	EXECUTED	7:2b4b8bff39944c7097977cc18dbceb3b	addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRPO2128CX4WNKOG82SSRFY, referencedTableName=RESOURCE_SERVER_POLICY		\N	3.5.4	\N	\N	6272686532
authz-4.2.0.Final	mhajas@redhat.com	META-INF/jpa-changelog-authz-4.2.0.Final.xml	2021-07-14 14:24:49.486669	61	EXECUTED	7:2aa42a964c59cd5b8ca9822340ba33a8	createTable tableName=RESOURCE_URIS; addForeignKeyConstraint baseTableName=RESOURCE_URIS, constraintName=FK_RESOURCE_SERVER_URIS, referencedTableName=RESOURCE_SERVER_RESOURCE; customChange; dropColumn columnName=URI, tableName=RESOURCE_SERVER_RESO...		\N	3.5.4	\N	\N	6272686532
authz-4.2.0.Final-KEYCLOAK-9944	hmlnarik@redhat.com	META-INF/jpa-changelog-authz-4.2.0.Final.xml	2021-07-14 14:24:49.497484	62	EXECUTED	7:9ac9e58545479929ba23f4a3087a0346	addPrimaryKey constraintName=CONSTRAINT_RESOUR_URIS_PK, tableName=RESOURCE_URIS		\N	3.5.4	\N	\N	6272686532
4.2.0-KEYCLOAK-6313	wadahiro@gmail.com	META-INF/jpa-changelog-4.2.0.xml	2021-07-14 14:24:49.502752	63	EXECUTED	7:14d407c35bc4fe1976867756bcea0c36	addColumn tableName=REQUIRED_ACTION_PROVIDER		\N	3.5.4	\N	\N	6272686532
4.3.0-KEYCLOAK-7984	wadahiro@gmail.com	META-INF/jpa-changelog-4.3.0.xml	2021-07-14 14:24:49.506313	64	EXECUTED	7:241a8030c748c8548e346adee548fa93	update tableName=REQUIRED_ACTION_PROVIDER		\N	3.5.4	\N	\N	6272686532
4.6.0-KEYCLOAK-7950	psilva@redhat.com	META-INF/jpa-changelog-4.6.0.xml	2021-07-14 14:24:49.509505	65	EXECUTED	7:7d3182f65a34fcc61e8d23def037dc3f	update tableName=RESOURCE_SERVER_RESOURCE		\N	3.5.4	\N	\N	6272686532
4.6.0-KEYCLOAK-8377	keycloak	META-INF/jpa-changelog-4.6.0.xml	2021-07-14 14:24:49.534426	66	EXECUTED	7:b30039e00a0b9715d430d1b0636728fa	createTable tableName=ROLE_ATTRIBUTE; addPrimaryKey constraintName=CONSTRAINT_ROLE_ATTRIBUTE_PK, tableName=ROLE_ATTRIBUTE; addForeignKeyConstraint baseTableName=ROLE_ATTRIBUTE, constraintName=FK_ROLE_ATTRIBUTE_ID, referencedTableName=KEYCLOAK_ROLE...		\N	3.5.4	\N	\N	6272686532
4.6.0-KEYCLOAK-8555	gideonray@gmail.com	META-INF/jpa-changelog-4.6.0.xml	2021-07-14 14:24:49.544463	67	EXECUTED	7:3797315ca61d531780f8e6f82f258159	createIndex indexName=IDX_COMPONENT_PROVIDER_TYPE, tableName=COMPONENT		\N	3.5.4	\N	\N	6272686532
4.7.0-KEYCLOAK-1267	sguilhen@redhat.com	META-INF/jpa-changelog-4.7.0.xml	2021-07-14 14:24:49.549802	68	EXECUTED	7:c7aa4c8d9573500c2d347c1941ff0301	addColumn tableName=REALM		\N	3.5.4	\N	\N	6272686532
4.7.0-KEYCLOAK-7275	keycloak	META-INF/jpa-changelog-4.7.0.xml	2021-07-14 14:24:49.56409	69	EXECUTED	7:b207faee394fc074a442ecd42185a5dd	renameColumn newColumnName=CREATED_ON, oldColumnName=LAST_SESSION_REFRESH, tableName=OFFLINE_USER_SESSION; addNotNullConstraint columnName=CREATED_ON, tableName=OFFLINE_USER_SESSION; addColumn tableName=OFFLINE_USER_SESSION; customChange; createIn...		\N	3.5.4	\N	\N	6272686532
4.8.0-KEYCLOAK-8835	sguilhen@redhat.com	META-INF/jpa-changelog-4.8.0.xml	2021-07-14 14:24:49.57648	70	EXECUTED	7:ab9a9762faaba4ddfa35514b212c4922	addNotNullConstraint columnName=SSO_MAX_LIFESPAN_REMEMBER_ME, tableName=REALM; addNotNullConstraint columnName=SSO_IDLE_TIMEOUT_REMEMBER_ME, tableName=REALM		\N	3.5.4	\N	\N	6272686532
authz-7.0.0-KEYCLOAK-10443	psilva@redhat.com	META-INF/jpa-changelog-authz-7.0.0.xml	2021-07-14 14:24:49.581052	71	EXECUTED	7:b9710f74515a6ccb51b72dc0d19df8c4	addColumn tableName=RESOURCE_SERVER		\N	3.5.4	\N	\N	6272686532
8.0.0-adding-credential-columns	keycloak	META-INF/jpa-changelog-8.0.0.xml	2021-07-14 14:24:49.588795	72	EXECUTED	7:ec9707ae4d4f0b7452fee20128083879	addColumn tableName=CREDENTIAL; addColumn tableName=FED_USER_CREDENTIAL		\N	3.5.4	\N	\N	6272686532
8.0.0-updating-credential-data-not-oracle	keycloak	META-INF/jpa-changelog-8.0.0.xml	2021-07-14 14:24:49.594495	73	EXECUTED	7:03b3f4b264c3c68ba082250a80b74216	update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL		\N	3.5.4	\N	\N	6272686532
8.0.0-updating-credential-data-oracle	keycloak	META-INF/jpa-changelog-8.0.0.xml	2021-07-14 14:24:49.596902	74	MARK_RAN	7:64c5728f5ca1f5aa4392217701c4fe23	update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL		\N	3.5.4	\N	\N	6272686532
8.0.0-credential-cleanup-fixed	keycloak	META-INF/jpa-changelog-8.0.0.xml	2021-07-14 14:24:49.611935	75	EXECUTED	7:b48da8c11a3d83ddd6b7d0c8c2219345	dropDefaultValue columnName=COUNTER, tableName=CREDENTIAL; dropDefaultValue columnName=DIGITS, tableName=CREDENTIAL; dropDefaultValue columnName=PERIOD, tableName=CREDENTIAL; dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; dropColumn ...		\N	3.5.4	\N	\N	6272686532
8.0.0-resource-tag-support	keycloak	META-INF/jpa-changelog-8.0.0.xml	2021-07-14 14:24:49.625675	76	EXECUTED	7:a73379915c23bfad3e8f5c6d5c0aa4bd	addColumn tableName=MIGRATION_MODEL; createIndex indexName=IDX_UPDATE_TIME, tableName=MIGRATION_MODEL		\N	3.5.4	\N	\N	6272686532
9.0.0-always-display-client	keycloak	META-INF/jpa-changelog-9.0.0.xml	2021-07-14 14:24:49.629862	77	EXECUTED	7:39e0073779aba192646291aa2332493d	addColumn tableName=CLIENT		\N	3.5.4	\N	\N	6272686532
9.0.0-drop-constraints-for-column-increase	keycloak	META-INF/jpa-changelog-9.0.0.xml	2021-07-14 14:24:49.632437	78	MARK_RAN	7:81f87368f00450799b4bf42ea0b3ec34	dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5PMT, tableName=RESOURCE_SERVER_PERM_TICKET; dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER_RESOURCE; dropPrimaryKey constraintName=CONSTRAINT_O...		\N	3.5.4	\N	\N	6272686532
9.0.0-increase-column-size-federated-fk	keycloak	META-INF/jpa-changelog-9.0.0.xml	2021-07-14 14:24:49.65888	79	EXECUTED	7:20b37422abb9fb6571c618148f013a15	modifyDataType columnName=CLIENT_ID, tableName=FED_USER_CONSENT; modifyDataType columnName=CLIENT_REALM_CONSTRAINT, tableName=KEYCLOAK_ROLE; modifyDataType columnName=OWNER, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=CLIENT_ID, ta...		\N	3.5.4	\N	\N	6272686532
9.0.0-recreate-constraints-after-column-increase	keycloak	META-INF/jpa-changelog-9.0.0.xml	2021-07-14 14:24:49.662298	80	MARK_RAN	7:1970bb6cfb5ee800736b95ad3fb3c78a	addNotNullConstraint columnName=CLIENT_ID, tableName=OFFLINE_CLIENT_SESSION; addNotNullConstraint columnName=OWNER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNullConstraint columnName=REQUESTER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNull...		\N	3.5.4	\N	\N	6272686532
9.0.1-add-index-to-client.client_id	keycloak	META-INF/jpa-changelog-9.0.1.xml	2021-07-14 14:24:49.673566	81	EXECUTED	7:45d9b25fc3b455d522d8dcc10a0f4c80	createIndex indexName=IDX_CLIENT_ID, tableName=CLIENT		\N	3.5.4	\N	\N	6272686532
9.0.1-KEYCLOAK-12579-drop-constraints	keycloak	META-INF/jpa-changelog-9.0.1.xml	2021-07-14 14:24:49.676587	82	MARK_RAN	7:890ae73712bc187a66c2813a724d037f	dropUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	6272686532
9.0.1-KEYCLOAK-12579-add-not-null-constraint	keycloak	META-INF/jpa-changelog-9.0.1.xml	2021-07-14 14:24:49.681438	83	EXECUTED	7:0a211980d27fafe3ff50d19a3a29b538	addNotNullConstraint columnName=PARENT_GROUP, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	6272686532
9.0.1-KEYCLOAK-12579-recreate-constraints	keycloak	META-INF/jpa-changelog-9.0.1.xml	2021-07-14 14:24:49.684246	84	MARK_RAN	7:a161e2ae671a9020fff61e996a207377	addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	6272686532
9.0.1-add-index-to-events	keycloak	META-INF/jpa-changelog-9.0.1.xml	2021-07-14 14:24:49.698998	85	EXECUTED	7:01c49302201bdf815b0a18d1f98a55dc	createIndex indexName=IDX_EVENT_TIME, tableName=EVENT_ENTITY		\N	3.5.4	\N	\N	6272686532
map-remove-ri	keycloak	META-INF/jpa-changelog-11.0.0.xml	2021-07-14 14:24:49.705778	86	EXECUTED	7:3dace6b144c11f53f1ad2c0361279b86	dropForeignKeyConstraint baseTableName=REALM, constraintName=FK_TRAF444KK6QRKMS7N56AIWQ5Y; dropForeignKeyConstraint baseTableName=KEYCLOAK_ROLE, constraintName=FK_KJHO5LE2C0RAL09FL8CM9WFW9		\N	3.5.4	\N	\N	6272686532
map-remove-ri	keycloak	META-INF/jpa-changelog-12.0.0.xml	2021-07-14 14:24:49.713008	87	EXECUTED	7:578d0b92077eaf2ab95ad0ec087aa903	dropForeignKeyConstraint baseTableName=REALM_DEFAULT_GROUPS, constraintName=FK_DEF_GROUPS_GROUP; dropForeignKeyConstraint baseTableName=REALM_DEFAULT_ROLES, constraintName=FK_H4WPD7W4HSOOLNI3H0SW7BTJE; dropForeignKeyConstraint baseTableName=CLIENT...		\N	3.5.4	\N	\N	6272686532
12.1.0-add-realm-localization-table	keycloak	META-INF/jpa-changelog-12.0.0.xml	2021-07-14 14:24:49.730804	88	EXECUTED	7:c95abe90d962c57a09ecaee57972835d	createTable tableName=REALM_LOCALIZATIONS; addPrimaryKey tableName=REALM_LOCALIZATIONS		\N	3.5.4	\N	\N	6272686532
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
1000	f	\N	\N
1001	f	\N	\N
\.


--
-- Data for Name: default_client_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.default_client_scope (realm_id, scope_id, default_scope) FROM stdin;
master	f99b0b25-a996-497d-a47d-99547ded0c2c	f
master	2afe53d3-9e33-4330-a8b7-66272ae4f798	t
master	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a	t
master	088c8ce8-5764-49c7-9055-348213800923	t
master	97a54b1a-7718-4d41-af64-fa9df073ec0f	f
master	d2779ef1-955c-4b13-921d-a8f1adb89092	f
master	e8520c61-4b9b-48a3-8064-87b9c0a9ac67	t
master	111dc177-01d3-4c45-88ad-9eb94a6b193a	t
master	85f399d0-8bc1-413e-8f7d-0cacaebd53c7	f
my-everyday-lolita-realm	8e4b5136-5c51-478d-bb58-9fce0fae4e38	t
my-everyday-lolita-realm	06f61973-d7c6-4cf7-a201-72c69d949874	t
my-everyday-lolita-realm	0ade4166-f086-4ad8-af3a-f9e6b47276e5	t
my-everyday-lolita-realm	b0f7660e-3612-4037-9d72-ec3fd10e0a5c	t
my-everyday-lolita-realm	0361142e-dacf-4411-90a9-25a02b60523c	t
my-everyday-lolita-realm	15dc681b-1fe8-426f-81ae-e3632e7356c0	f
my-everyday-lolita-realm	b4767745-2c5f-4721-ae07-dd1bf190b7bb	f
my-everyday-lolita-realm	39e77af3-6506-4684-a2a5-2eda9c5459e5	f
my-everyday-lolita-realm	ade59ad3-b70e-47c8-9f4f-ace450d6297f	f
\.


--
-- Data for Name: event_entity; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.event_entity (id, client_id, details_json, error, ip_address, realm_id, session_id, event_time, type, user_id) FROM stdin;
\.


--
-- Data for Name: fed_user_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_attribute (id, name, user_id, realm_id, storage_provider_id, value) FROM stdin;
\.


--
-- Data for Name: fed_user_consent; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_consent (id, client_id, user_id, realm_id, storage_provider_id, created_date, last_updated_date, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: fed_user_consent_cl_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_consent_cl_scope (user_consent_id, scope_id) FROM stdin;
\.


--
-- Data for Name: fed_user_credential; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_credential (id, salt, type, created_date, user_id, realm_id, storage_provider_id, user_label, secret_data, credential_data, priority) FROM stdin;
\.


--
-- Data for Name: fed_user_group_membership; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_group_membership (group_id, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: fed_user_required_action; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_required_action (required_action, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: fed_user_role_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_role_mapping (role_id, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: federated_identity; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.federated_identity (identity_provider, realm_id, federated_user_id, federated_username, token, user_id) FROM stdin;
\.


--
-- Data for Name: federated_user; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.federated_user (id, storage_provider_id, realm_id) FROM stdin;
\.


--
-- Data for Name: group_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.group_attribute (id, name, value, group_id) FROM stdin;
\.


--
-- Data for Name: group_role_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.group_role_mapping (role_id, group_id) FROM stdin;
\.


--
-- Data for Name: identity_provider; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.identity_provider (internal_id, enabled, provider_alias, provider_id, store_token, authenticate_by_default, realm_id, add_token_role, trust_email, first_broker_login_flow_id, post_broker_login_flow_id, provider_display_name, link_only) FROM stdin;
\.


--
-- Data for Name: identity_provider_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.identity_provider_config (identity_provider_id, value, name) FROM stdin;
\.


--
-- Data for Name: identity_provider_mapper; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.identity_provider_mapper (id, name, idp_alias, idp_mapper_name, realm_id) FROM stdin;
\.


--
-- Data for Name: idp_mapper_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.idp_mapper_config (idp_mapper_id, value, name) FROM stdin;
\.


--
-- Data for Name: keycloak_group; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.keycloak_group (id, name, parent_group, realm_id) FROM stdin;
\.


--
-- Data for Name: keycloak_role; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.keycloak_role (id, client_realm_constraint, client_role, description, name, realm_id, client, realm) FROM stdin;
f16ec247-e190-4571-83fd-7ebaaa8da265	master	f	${role_admin}	admin	master	\N	master
3fae8297-8359-4e8a-81dd-8bc1a1cf5926	master	f	${role_create-realm}	create-realm	master	\N	master
1103c56d-4c70-41d5-a03d-5ad953b60e3a	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_create-client}	create-client	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
279a285b-173f-4ef0-9da2-c4fa35d10b26	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_view-realm}	view-realm	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
52fdef77-d587-409b-803c-6ad347863c55	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_view-users}	view-users	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
0d6e2e42-179d-4e79-b147-516cf4f1095c	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_view-clients}	view-clients	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
db581e85-20d9-4ca1-aee4-1cc80b136571	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_view-events}	view-events	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
22135b6f-d1a5-4742-9757-2e65ea0e9d21	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_view-identity-providers}	view-identity-providers	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
806b93ee-a330-43f7-ae9c-95c5de0e5d91	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_view-authorization}	view-authorization	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
69af75f4-b51a-4f87-8993-17709fb6ae01	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_manage-realm}	manage-realm	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
23e17969-0cb5-47ad-b2f5-07bb27c866ba	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_manage-users}	manage-users	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
c24f313c-9ec8-4d67-8a49-72a91ec07458	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_manage-clients}	manage-clients	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
b04d1951-2ce4-44fa-b277-57b99771f871	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_manage-events}	manage-events	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
7aae7bb3-ed89-41ff-921a-a7dd7f2321be	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_manage-identity-providers}	manage-identity-providers	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
be759bc2-2f0b-47e6-81e5-5935a060baf1	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_manage-authorization}	manage-authorization	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
216265bc-23d8-453f-84e6-4f7ef604ab7f	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_query-users}	query-users	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
ea0f85e1-c518-4470-884f-fab898a9a856	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_query-clients}	query-clients	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
01b2208f-b7ee-4726-8263-247d518af2c2	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_query-realms}	query-realms	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
ebff1792-72e2-4456-af0f-6537d76cd694	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_query-groups}	query-groups	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
ff52cc7b-fffd-4e6f-a31d-d7e128910691	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	t	${role_view-profile}	view-profile	master	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	\N
a24019b4-1610-4d94-956d-0d43431bf65f	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	t	${role_manage-account}	manage-account	master	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	\N
7333fabc-a5f8-44b7-a825-f9e6ec7eeafd	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	t	${role_manage-account-links}	manage-account-links	master	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	\N
da6909b0-3f15-4405-b5ee-471b5ddb7fe0	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	t	${role_view-applications}	view-applications	master	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	\N
e50f6ca3-2f69-45ee-b1ea-9a697d8adb08	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	t	${role_view-consent}	view-consent	master	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	\N
3501da45-6c0d-4bcb-98b2-72db31412fc2	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	t	${role_manage-consent}	manage-consent	master	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	\N
c70cc198-9f38-4a9f-b5ab-7aad070b563a	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	t	${role_delete-account}	delete-account	master	986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	\N
f3b6bb75-b3d8-433e-9d1f-fc95fc17d966	4e4ed2e2-feaf-48ac-93b6-ac90dca76334	t	${role_read-token}	read-token	master	4e4ed2e2-feaf-48ac-93b6-ac90dca76334	\N
9763f68d-c5ae-429a-9776-dac43aa527fb	ccea24b5-8d34-436f-8e4f-22b350ecdde4	t	${role_impersonation}	impersonation	master	ccea24b5-8d34-436f-8e4f-22b350ecdde4	\N
5b24a59a-7618-483a-bfa0-70da4201b0e7	master	f	${role_offline-access}	offline_access	master	\N	master
ee2aff9a-269e-419c-88d0-30ee69fedba6	master	f	${role_uma_authorization}	uma_authorization	master	\N	master
bb5cedc5-61ea-45f0-a016-85d2b2e014c6	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_create-client}	create-client	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
0d3ea6b6-0b67-4e71-a5dd-5d12fddeea4b	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_view-realm}	view-realm	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
d17103a6-f45b-4ba0-8b4a-2896cbfbf24a	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_view-users}	view-users	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
d3d31fae-a5b5-4e17-9624-c240d066cb98	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_view-clients}	view-clients	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
bbec3dc1-5cd1-4024-8116-8f777d101ca9	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_view-events}	view-events	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
a1b717d9-92d2-46d9-965b-5a0951165fb8	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_view-identity-providers}	view-identity-providers	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
6325176a-6b0f-443a-810d-b3d1b645d116	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_view-authorization}	view-authorization	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
21484593-ea88-40e0-967a-6a0db2519c49	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_manage-realm}	manage-realm	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
541b50cb-3bc6-4ec0-870d-0960f486ff1d	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_manage-users}	manage-users	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
8bad62c2-4b2d-43c1-8b0c-c42dc563931b	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_manage-clients}	manage-clients	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
35dd8eb8-aeab-4ee8-8915-46cef7459059	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_manage-events}	manage-events	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
a6189298-8713-43a6-9b67-6d48ae62bbd7	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_manage-identity-providers}	manage-identity-providers	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
dd1384a9-062f-44e2-a39d-0eeffbbd6c46	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_manage-authorization}	manage-authorization	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
05ec3d56-84b9-4d3b-9ff7-f0dd706d3b08	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_query-users}	query-users	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
9ff17ef1-7321-4c44-9c13-d31fc33b0a33	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_query-clients}	query-clients	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
5826461b-94b0-4ad9-8b9b-1b660ba1e0b8	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_query-realms}	query-realms	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
f7d04592-2348-4cdc-a3a5-74ecb14a24bc	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_query-groups}	query-groups	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
1147723f-9cd1-4519-9ef4-9493fc722196	my-everyday-lolita-realm	f	${role_offline-access}	offline_access	my-everyday-lolita-realm	\N	my-everyday-lolita-realm
816fa806-10e8-413b-95be-12173b8981eb	my-everyday-lolita-realm	f	\N	admin	my-everyday-lolita-realm	\N	my-everyday-lolita-realm
419b7053-b11a-483c-8cbe-12f6cab38385	my-everyday-lolita-realm	f	${role_uma_authorization}	uma_authorization	my-everyday-lolita-realm	\N	my-everyday-lolita-realm
9699cb90-1bc6-4869-bcb3-503120414690	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_view-identity-providers}	view-identity-providers	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
d72dcbb1-4e3d-4488-b581-1c6deb42b618	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_query-clients}	query-clients	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
ffedf372-f9be-444e-9dc2-df703779b662	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_view-realm}	view-realm	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
27ee1374-b3fe-4567-bed7-4a3e4be9e27b	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_view-authorization}	view-authorization	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
3e1b0be7-4283-4886-a81c-b3a595a0fb04	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_manage-events}	manage-events	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
e4a368c6-2e0d-4294-8468-aaac12bef7e0	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_realm-admin}	realm-admin	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
82cf162c-64b8-4633-b6b9-95b63da11561	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_manage-clients}	manage-clients	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
a9e89c0d-8df0-44b2-ad4d-10cee713cc24	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_query-groups}	query-groups	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
27463e65-c884-4b7d-b2b7-d142514eeda8	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_manage-users}	manage-users	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
34ccc0c5-0750-41a2-8961-9a0d97d44eee	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_query-users}	query-users	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
b2f5f644-d161-4675-91c9-93359c944cf3	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_view-events}	view-events	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
c754d187-e55e-4259-841e-e2bf8491ccce	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_manage-authorization}	manage-authorization	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
55bd8471-135b-423b-b0b4-3731df6e34b9	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_impersonation}	impersonation	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
9bddaff9-9a54-40e6-8130-c8df9f56f518	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_manage-identity-providers}	manage-identity-providers	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
9eaf204e-8eaf-42dc-a42b-6b35c0f5e7f2	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_query-realms}	query-realms	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
856e74df-bdbe-4493-9f11-4acbc586c5a9	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_create-client}	create-client	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
295775a0-32a9-41cd-8ccf-1995fd836138	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_view-clients}	view-clients	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
0b338ed3-f094-41b0-a881-48d060b3b9b1	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_view-users}	view-users	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
16979ae5-317d-4f7a-82bd-8c8a2f74906d	0d3aba2b-94e5-4e03-b4fd-94b04db07265	t	${role_manage-realm}	manage-realm	my-everyday-lolita-realm	0d3aba2b-94e5-4e03-b4fd-94b04db07265	\N
4e7d5fe9-052a-446e-944d-5820b583f50d	fb77d40d-3c09-45c0-acc2-017d62affe1c	t	${role_read-token}	read-token	my-everyday-lolita-realm	fb77d40d-3c09-45c0-acc2-017d62affe1c	\N
372e14fb-946a-45b9-a077-655680f2fdda	463f127e-1394-4aeb-8e97-588bc9275ad7	t	${role_view-consent}	view-consent	my-everyday-lolita-realm	463f127e-1394-4aeb-8e97-588bc9275ad7	\N
23e6a079-caf1-462e-b1b3-0db1a854c77f	463f127e-1394-4aeb-8e97-588bc9275ad7	t	${role_view-applications}	view-applications	my-everyday-lolita-realm	463f127e-1394-4aeb-8e97-588bc9275ad7	\N
c26a0e77-5616-417d-a0d4-0bfeeca46cfa	463f127e-1394-4aeb-8e97-588bc9275ad7	t	${role_delete-account}	delete-account	my-everyday-lolita-realm	463f127e-1394-4aeb-8e97-588bc9275ad7	\N
199da019-bb23-49d2-95db-eb0d1cdb3e3c	463f127e-1394-4aeb-8e97-588bc9275ad7	t	${role_manage-account}	manage-account	my-everyday-lolita-realm	463f127e-1394-4aeb-8e97-588bc9275ad7	\N
3d2ccce0-4572-4c4f-b308-b4b84fe05271	463f127e-1394-4aeb-8e97-588bc9275ad7	t	${role_manage-account-links}	manage-account-links	my-everyday-lolita-realm	463f127e-1394-4aeb-8e97-588bc9275ad7	\N
a1e1dfb6-85fe-40c5-90ca-5b0d16d43818	463f127e-1394-4aeb-8e97-588bc9275ad7	t	${role_view-profile}	view-profile	my-everyday-lolita-realm	463f127e-1394-4aeb-8e97-588bc9275ad7	\N
fd608b12-4f4b-4201-822c-6d11593bdabe	463f127e-1394-4aeb-8e97-588bc9275ad7	t	${role_manage-consent}	manage-consent	my-everyday-lolita-realm	463f127e-1394-4aeb-8e97-588bc9275ad7	\N
89412be7-77cf-4692-a141-5b99ab8fc813	d30771ac-77e3-4424-905e-50bc58920b78	t	\N	create-item	my-everyday-lolita-realm	d30771ac-77e3-4424-905e-50bc58920b78	\N
9071777c-4a12-4b78-92f7-3cacd636fadf	0886a50d-9edf-4645-a56f-be8bfbe9e82e	t	${role_impersonation}	impersonation	master	0886a50d-9edf-4645-a56f-be8bfbe9e82e	\N
1e7198ce-ec4e-4810-b23f-e4c557cbd541	15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	t	\N	uma_protection	master	15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	\N
5b12ec76-b86f-4420-82ec-475b9bd83381	my-everyday-lolita-realm	f	\N	super-admin	my-everyday-lolita-realm	\N	my-everyday-lolita-realm
2d531a7f-d9de-4c7d-8733-3f949667f956	my-everyday-lolita-realm	f	\N	create-item	my-everyday-lolita-realm	\N	my-everyday-lolita-realm
\.


--
-- Data for Name: migration_model; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.migration_model (id, version, update_time) FROM stdin;
4pw1b	12.0.4	1626272692
\.


--
-- Data for Name: offline_client_session; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.offline_client_session (user_session_id, client_id, offline_flag, "timestamp", data, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: offline_user_session; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.offline_user_session (user_session_id, user_id, realm_id, created_on, offline_flag, data, last_session_refresh) FROM stdin;
\.


--
-- Data for Name: policy_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.policy_config (policy_id, name, value) FROM stdin;
ba024e17-b552-473c-b613-dc79754ae21c	code	// by default, grants any permission associated with this policy\n$evaluation.grant();\n
b6d2a66e-09d7-456f-8c28-00e832964991	defaultResourceType	urn:admin-cli:resources:default
\.


--
-- Data for Name: protocol_mapper; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.protocol_mapper (id, name, protocol, protocol_mapper_name, client_id, client_scope_id) FROM stdin;
b4c9c0ce-f6db-4416-a4fb-a6b2393f1161	audience resolve	openid-connect	oidc-audience-resolve-mapper	ec94f535-843f-4a0d-bcbd-278f29faa5e8	\N
3d2f2682-6ad6-40e0-acaa-7b771c1ad353	locale	openid-connect	oidc-usermodel-attribute-mapper	d2c2af8c-d1e7-4789-8c86-5e752d9643d6	\N
44bfc743-7ac3-45ae-9f9b-879e47d6c779	role list	saml	saml-role-list-mapper	\N	2afe53d3-9e33-4330-a8b7-66272ae4f798
c42ace7b-3dae-40bf-acd7-576e2b2f6912	full name	openid-connect	oidc-full-name-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
71003a37-9d0d-471a-8e9a-b4672a396321	family name	openid-connect	oidc-usermodel-property-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
80523c66-5ee5-4384-94ae-7a764dc649cb	given name	openid-connect	oidc-usermodel-property-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
55f751dd-577c-4c20-a64f-8257262f707d	middle name	openid-connect	oidc-usermodel-attribute-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
8d4bb6f3-2c81-4af9-a968-1c9e7fec493b	nickname	openid-connect	oidc-usermodel-attribute-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
e1ae0256-5f32-417c-b28a-28fa7dccce50	username	openid-connect	oidc-usermodel-property-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
24595cad-4ffb-45d4-9e6f-1dc59b91be8d	profile	openid-connect	oidc-usermodel-attribute-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
1c0e2007-9422-4b6f-b068-1df78bff8458	picture	openid-connect	oidc-usermodel-attribute-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
cd040db0-a070-44e1-838c-4162e7bde264	website	openid-connect	oidc-usermodel-attribute-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
523d36d0-9f44-48d1-b58b-f69c5b47c39b	gender	openid-connect	oidc-usermodel-attribute-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
d9c93592-6abc-4c2c-ac75-51b451434214	birthdate	openid-connect	oidc-usermodel-attribute-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
2fa9fdb0-58e2-4eb2-8bcc-b3a851942f1b	zoneinfo	openid-connect	oidc-usermodel-attribute-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
d6d3998e-2ddb-48d5-904b-6b269b7e8cc3	locale	openid-connect	oidc-usermodel-attribute-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
951cf349-805f-4757-9e12-c7e09be39f7a	updated at	openid-connect	oidc-usermodel-attribute-mapper	\N	0ef6c11f-ba01-4dd1-ae40-24dddcc68e7a
36141dad-91ee-49f3-b472-755047e301c7	email	openid-connect	oidc-usermodel-property-mapper	\N	088c8ce8-5764-49c7-9055-348213800923
984994d6-d065-4ffd-b730-e3a2fb7e46aa	email verified	openid-connect	oidc-usermodel-property-mapper	\N	088c8ce8-5764-49c7-9055-348213800923
b798f400-8ab8-4a6f-9f9c-56b943a7c08a	address	openid-connect	oidc-address-mapper	\N	97a54b1a-7718-4d41-af64-fa9df073ec0f
172c49d2-650a-4df6-8bd6-8dca4c90160d	phone number	openid-connect	oidc-usermodel-attribute-mapper	\N	d2779ef1-955c-4b13-921d-a8f1adb89092
113d65ed-c18e-4afb-8be9-f82dbae2ba7f	phone number verified	openid-connect	oidc-usermodel-attribute-mapper	\N	d2779ef1-955c-4b13-921d-a8f1adb89092
82088212-20e3-467a-ab4e-d3d382696b18	realm roles	openid-connect	oidc-usermodel-realm-role-mapper	\N	e8520c61-4b9b-48a3-8064-87b9c0a9ac67
be11e008-403b-4a45-8545-32e01b548c98	client roles	openid-connect	oidc-usermodel-client-role-mapper	\N	e8520c61-4b9b-48a3-8064-87b9c0a9ac67
00b9bf04-3b04-4143-bc61-40ac043041d4	audience resolve	openid-connect	oidc-audience-resolve-mapper	\N	e8520c61-4b9b-48a3-8064-87b9c0a9ac67
0b7ce49d-df21-4fcb-8956-493465f5e124	allowed web origins	openid-connect	oidc-allowed-origins-mapper	\N	111dc177-01d3-4c45-88ad-9eb94a6b193a
05737d33-7682-434e-b943-3f256f70fe59	upn	openid-connect	oidc-usermodel-property-mapper	\N	85f399d0-8bc1-413e-8f7d-0cacaebd53c7
4079b45c-56b8-45bd-967f-ad429cbf799e	groups	openid-connect	oidc-usermodel-realm-role-mapper	\N	85f399d0-8bc1-413e-8f7d-0cacaebd53c7
0a130771-b23e-4eff-9e58-c97cf4274463	address	openid-connect	oidc-address-mapper	\N	b4767745-2c5f-4721-ae07-dd1bf190b7bb
36d5f47a-8f42-4c20-b957-2d4fec1c15d2	email	openid-connect	oidc-usermodel-property-mapper	\N	0ade4166-f086-4ad8-af3a-f9e6b47276e5
ff65b209-9d67-45eb-aa26-9554876681f5	email verified	openid-connect	oidc-usermodel-property-mapper	\N	0ade4166-f086-4ad8-af3a-f9e6b47276e5
502f0449-1106-469c-9d7e-a9dd44a95171	groups	openid-connect	oidc-usermodel-realm-role-mapper	\N	ade59ad3-b70e-47c8-9f4f-ace450d6297f
fca62ace-ed02-471c-a7e2-45b64acffaf4	upn	openid-connect	oidc-usermodel-property-mapper	\N	ade59ad3-b70e-47c8-9f4f-ace450d6297f
b8b561da-70d2-4c96-886a-271140735dee	phone number verified	openid-connect	oidc-usermodel-attribute-mapper	\N	39e77af3-6506-4684-a2a5-2eda9c5459e5
aa791dd4-5da9-4379-bdfb-16acd62a26ff	phone number	openid-connect	oidc-usermodel-attribute-mapper	\N	39e77af3-6506-4684-a2a5-2eda9c5459e5
ca729c95-0c14-4f63-98cb-eef4f7a2ebdd	given name	openid-connect	oidc-usermodel-property-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
3c656a65-c7a2-476b-b642-75585c7dc099	username	openid-connect	oidc-usermodel-property-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
1f0dcaa6-67be-44a7-8844-346c84e8d318	locale	openid-connect	oidc-usermodel-attribute-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
76543b65-1f92-47ef-bae5-522725778617	profile	openid-connect	oidc-usermodel-attribute-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
50344589-41d8-4a86-9abb-f7395a18e872	full name	openid-connect	oidc-full-name-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
b8194ec3-eda9-45bb-a029-a1c5ddf4c5ed	family name	openid-connect	oidc-usermodel-property-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
0353d1fb-7cec-4f97-acdd-0f28fe366294	picture	openid-connect	oidc-usermodel-attribute-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
1112993f-6141-4fc1-93a7-9448c6ebe04e	gender	openid-connect	oidc-usermodel-attribute-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
85c4edae-a945-4af1-a3b1-9b1b311d78ad	website	openid-connect	oidc-usermodel-attribute-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
ca137868-6ea5-403d-96d2-5a943c44368c	birthdate	openid-connect	oidc-usermodel-attribute-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
0ed75dfb-1d79-4186-9389-a647f6f645c3	updated at	openid-connect	oidc-usermodel-attribute-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
bfd93ca1-4107-429f-a89c-173b048c5bd9	nickname	openid-connect	oidc-usermodel-attribute-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
af01b6e2-b827-4063-bf97-b01fbbc0e574	middle name	openid-connect	oidc-usermodel-attribute-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
8258103f-39aa-490c-b077-4bf1ab2fe9ed	zoneinfo	openid-connect	oidc-usermodel-attribute-mapper	\N	06f61973-d7c6-4cf7-a201-72c69d949874
309cd305-bc10-4027-ae06-1919ccc6db82	role list	saml	saml-role-list-mapper	\N	8e4b5136-5c51-478d-bb58-9fce0fae4e38
ecc8921f-f5f8-42ea-861f-cf9b96c2b9f5	audience resolve	openid-connect	oidc-audience-resolve-mapper	\N	b0f7660e-3612-4037-9d72-ec3fd10e0a5c
cdb28784-1bba-4fff-a0a7-129a69df8b10	client roles	openid-connect	oidc-usermodel-client-role-mapper	\N	b0f7660e-3612-4037-9d72-ec3fd10e0a5c
994c68c9-4290-4e17-8ba2-4f8626302bf7	realm roles	openid-connect	oidc-usermodel-realm-role-mapper	\N	b0f7660e-3612-4037-9d72-ec3fd10e0a5c
2c96564b-ec7f-41b9-a404-18271973554e	allowed web origins	openid-connect	oidc-allowed-origins-mapper	\N	0361142e-dacf-4411-90a9-25a02b60523c
75c2856a-b916-4a1c-9bf1-325cf0357e18	audience resolve	openid-connect	oidc-audience-resolve-mapper	19554311-6c94-479d-bcfc-5cb5ab56d096	\N
a226c17f-200e-4bbb-bdc9-6cfd9df3d9a4	Client ID	openid-connect	oidc-usersessionmodel-note-mapper	5b128c68-e165-43e5-be48-851fc7d4b5fc	\N
3951f64b-45c0-44e4-9835-5e617fa3bc79	Client Host	openid-connect	oidc-usersessionmodel-note-mapper	5b128c68-e165-43e5-be48-851fc7d4b5fc	\N
39acafdb-bf49-422d-94e6-f33687586c3c	Client IP Address	openid-connect	oidc-usersessionmodel-note-mapper	5b128c68-e165-43e5-be48-851fc7d4b5fc	\N
f29632c5-8df8-4349-9290-6fc340eef313	locale	openid-connect	oidc-usermodel-attribute-mapper	5b6378af-33db-444a-93b2-14bb194f954f	\N
a07d7b29-9bfc-4d04-9597-d017088909fa	Client ID	openid-connect	oidc-usersessionmodel-note-mapper	15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	\N
d7d84699-6870-4333-b790-8f17de7530ee	Client Host	openid-connect	oidc-usersessionmodel-note-mapper	15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	\N
c83cc5df-c0d3-40d1-b069-bfe2480c49aa	Client IP Address	openid-connect	oidc-usersessionmodel-note-mapper	15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	\N
\.


--
-- Data for Name: protocol_mapper_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.protocol_mapper_config (protocol_mapper_id, value, name) FROM stdin;
3d2f2682-6ad6-40e0-acaa-7b771c1ad353	true	userinfo.token.claim
3d2f2682-6ad6-40e0-acaa-7b771c1ad353	locale	user.attribute
3d2f2682-6ad6-40e0-acaa-7b771c1ad353	true	id.token.claim
3d2f2682-6ad6-40e0-acaa-7b771c1ad353	true	access.token.claim
3d2f2682-6ad6-40e0-acaa-7b771c1ad353	locale	claim.name
3d2f2682-6ad6-40e0-acaa-7b771c1ad353	String	jsonType.label
44bfc743-7ac3-45ae-9f9b-879e47d6c779	false	single
44bfc743-7ac3-45ae-9f9b-879e47d6c779	Basic	attribute.nameformat
44bfc743-7ac3-45ae-9f9b-879e47d6c779	Role	attribute.name
c42ace7b-3dae-40bf-acd7-576e2b2f6912	true	userinfo.token.claim
c42ace7b-3dae-40bf-acd7-576e2b2f6912	true	id.token.claim
c42ace7b-3dae-40bf-acd7-576e2b2f6912	true	access.token.claim
71003a37-9d0d-471a-8e9a-b4672a396321	true	userinfo.token.claim
71003a37-9d0d-471a-8e9a-b4672a396321	lastName	user.attribute
71003a37-9d0d-471a-8e9a-b4672a396321	true	id.token.claim
71003a37-9d0d-471a-8e9a-b4672a396321	true	access.token.claim
71003a37-9d0d-471a-8e9a-b4672a396321	family_name	claim.name
71003a37-9d0d-471a-8e9a-b4672a396321	String	jsonType.label
80523c66-5ee5-4384-94ae-7a764dc649cb	true	userinfo.token.claim
80523c66-5ee5-4384-94ae-7a764dc649cb	firstName	user.attribute
80523c66-5ee5-4384-94ae-7a764dc649cb	true	id.token.claim
80523c66-5ee5-4384-94ae-7a764dc649cb	true	access.token.claim
80523c66-5ee5-4384-94ae-7a764dc649cb	given_name	claim.name
80523c66-5ee5-4384-94ae-7a764dc649cb	String	jsonType.label
55f751dd-577c-4c20-a64f-8257262f707d	true	userinfo.token.claim
55f751dd-577c-4c20-a64f-8257262f707d	middleName	user.attribute
55f751dd-577c-4c20-a64f-8257262f707d	true	id.token.claim
55f751dd-577c-4c20-a64f-8257262f707d	true	access.token.claim
55f751dd-577c-4c20-a64f-8257262f707d	middle_name	claim.name
55f751dd-577c-4c20-a64f-8257262f707d	String	jsonType.label
8d4bb6f3-2c81-4af9-a968-1c9e7fec493b	true	userinfo.token.claim
8d4bb6f3-2c81-4af9-a968-1c9e7fec493b	nickname	user.attribute
8d4bb6f3-2c81-4af9-a968-1c9e7fec493b	true	id.token.claim
8d4bb6f3-2c81-4af9-a968-1c9e7fec493b	true	access.token.claim
8d4bb6f3-2c81-4af9-a968-1c9e7fec493b	nickname	claim.name
8d4bb6f3-2c81-4af9-a968-1c9e7fec493b	String	jsonType.label
e1ae0256-5f32-417c-b28a-28fa7dccce50	true	userinfo.token.claim
e1ae0256-5f32-417c-b28a-28fa7dccce50	username	user.attribute
e1ae0256-5f32-417c-b28a-28fa7dccce50	true	id.token.claim
e1ae0256-5f32-417c-b28a-28fa7dccce50	true	access.token.claim
e1ae0256-5f32-417c-b28a-28fa7dccce50	preferred_username	claim.name
e1ae0256-5f32-417c-b28a-28fa7dccce50	String	jsonType.label
24595cad-4ffb-45d4-9e6f-1dc59b91be8d	true	userinfo.token.claim
24595cad-4ffb-45d4-9e6f-1dc59b91be8d	profile	user.attribute
24595cad-4ffb-45d4-9e6f-1dc59b91be8d	true	id.token.claim
24595cad-4ffb-45d4-9e6f-1dc59b91be8d	true	access.token.claim
24595cad-4ffb-45d4-9e6f-1dc59b91be8d	profile	claim.name
24595cad-4ffb-45d4-9e6f-1dc59b91be8d	String	jsonType.label
1c0e2007-9422-4b6f-b068-1df78bff8458	true	userinfo.token.claim
1c0e2007-9422-4b6f-b068-1df78bff8458	picture	user.attribute
1c0e2007-9422-4b6f-b068-1df78bff8458	true	id.token.claim
1c0e2007-9422-4b6f-b068-1df78bff8458	true	access.token.claim
1c0e2007-9422-4b6f-b068-1df78bff8458	picture	claim.name
1c0e2007-9422-4b6f-b068-1df78bff8458	String	jsonType.label
cd040db0-a070-44e1-838c-4162e7bde264	true	userinfo.token.claim
cd040db0-a070-44e1-838c-4162e7bde264	website	user.attribute
cd040db0-a070-44e1-838c-4162e7bde264	true	id.token.claim
cd040db0-a070-44e1-838c-4162e7bde264	true	access.token.claim
cd040db0-a070-44e1-838c-4162e7bde264	website	claim.name
cd040db0-a070-44e1-838c-4162e7bde264	String	jsonType.label
523d36d0-9f44-48d1-b58b-f69c5b47c39b	true	userinfo.token.claim
523d36d0-9f44-48d1-b58b-f69c5b47c39b	gender	user.attribute
523d36d0-9f44-48d1-b58b-f69c5b47c39b	true	id.token.claim
523d36d0-9f44-48d1-b58b-f69c5b47c39b	true	access.token.claim
523d36d0-9f44-48d1-b58b-f69c5b47c39b	gender	claim.name
523d36d0-9f44-48d1-b58b-f69c5b47c39b	String	jsonType.label
d9c93592-6abc-4c2c-ac75-51b451434214	true	userinfo.token.claim
d9c93592-6abc-4c2c-ac75-51b451434214	birthdate	user.attribute
d9c93592-6abc-4c2c-ac75-51b451434214	true	id.token.claim
d9c93592-6abc-4c2c-ac75-51b451434214	true	access.token.claim
d9c93592-6abc-4c2c-ac75-51b451434214	birthdate	claim.name
d9c93592-6abc-4c2c-ac75-51b451434214	String	jsonType.label
2fa9fdb0-58e2-4eb2-8bcc-b3a851942f1b	true	userinfo.token.claim
2fa9fdb0-58e2-4eb2-8bcc-b3a851942f1b	zoneinfo	user.attribute
2fa9fdb0-58e2-4eb2-8bcc-b3a851942f1b	true	id.token.claim
2fa9fdb0-58e2-4eb2-8bcc-b3a851942f1b	true	access.token.claim
2fa9fdb0-58e2-4eb2-8bcc-b3a851942f1b	zoneinfo	claim.name
2fa9fdb0-58e2-4eb2-8bcc-b3a851942f1b	String	jsonType.label
d6d3998e-2ddb-48d5-904b-6b269b7e8cc3	true	userinfo.token.claim
d6d3998e-2ddb-48d5-904b-6b269b7e8cc3	locale	user.attribute
d6d3998e-2ddb-48d5-904b-6b269b7e8cc3	true	id.token.claim
d6d3998e-2ddb-48d5-904b-6b269b7e8cc3	true	access.token.claim
d6d3998e-2ddb-48d5-904b-6b269b7e8cc3	locale	claim.name
d6d3998e-2ddb-48d5-904b-6b269b7e8cc3	String	jsonType.label
951cf349-805f-4757-9e12-c7e09be39f7a	true	userinfo.token.claim
951cf349-805f-4757-9e12-c7e09be39f7a	updatedAt	user.attribute
951cf349-805f-4757-9e12-c7e09be39f7a	true	id.token.claim
951cf349-805f-4757-9e12-c7e09be39f7a	true	access.token.claim
951cf349-805f-4757-9e12-c7e09be39f7a	updated_at	claim.name
951cf349-805f-4757-9e12-c7e09be39f7a	String	jsonType.label
36141dad-91ee-49f3-b472-755047e301c7	true	userinfo.token.claim
36141dad-91ee-49f3-b472-755047e301c7	email	user.attribute
36141dad-91ee-49f3-b472-755047e301c7	true	id.token.claim
36141dad-91ee-49f3-b472-755047e301c7	true	access.token.claim
36141dad-91ee-49f3-b472-755047e301c7	email	claim.name
36141dad-91ee-49f3-b472-755047e301c7	String	jsonType.label
984994d6-d065-4ffd-b730-e3a2fb7e46aa	true	userinfo.token.claim
984994d6-d065-4ffd-b730-e3a2fb7e46aa	emailVerified	user.attribute
984994d6-d065-4ffd-b730-e3a2fb7e46aa	true	id.token.claim
984994d6-d065-4ffd-b730-e3a2fb7e46aa	true	access.token.claim
984994d6-d065-4ffd-b730-e3a2fb7e46aa	email_verified	claim.name
984994d6-d065-4ffd-b730-e3a2fb7e46aa	boolean	jsonType.label
b798f400-8ab8-4a6f-9f9c-56b943a7c08a	formatted	user.attribute.formatted
b798f400-8ab8-4a6f-9f9c-56b943a7c08a	country	user.attribute.country
b798f400-8ab8-4a6f-9f9c-56b943a7c08a	postal_code	user.attribute.postal_code
b798f400-8ab8-4a6f-9f9c-56b943a7c08a	true	userinfo.token.claim
b798f400-8ab8-4a6f-9f9c-56b943a7c08a	street	user.attribute.street
b798f400-8ab8-4a6f-9f9c-56b943a7c08a	true	id.token.claim
b798f400-8ab8-4a6f-9f9c-56b943a7c08a	region	user.attribute.region
b798f400-8ab8-4a6f-9f9c-56b943a7c08a	true	access.token.claim
b798f400-8ab8-4a6f-9f9c-56b943a7c08a	locality	user.attribute.locality
172c49d2-650a-4df6-8bd6-8dca4c90160d	true	userinfo.token.claim
172c49d2-650a-4df6-8bd6-8dca4c90160d	phoneNumber	user.attribute
172c49d2-650a-4df6-8bd6-8dca4c90160d	true	id.token.claim
172c49d2-650a-4df6-8bd6-8dca4c90160d	true	access.token.claim
172c49d2-650a-4df6-8bd6-8dca4c90160d	phone_number	claim.name
172c49d2-650a-4df6-8bd6-8dca4c90160d	String	jsonType.label
113d65ed-c18e-4afb-8be9-f82dbae2ba7f	true	userinfo.token.claim
113d65ed-c18e-4afb-8be9-f82dbae2ba7f	phoneNumberVerified	user.attribute
113d65ed-c18e-4afb-8be9-f82dbae2ba7f	true	id.token.claim
113d65ed-c18e-4afb-8be9-f82dbae2ba7f	true	access.token.claim
113d65ed-c18e-4afb-8be9-f82dbae2ba7f	phone_number_verified	claim.name
113d65ed-c18e-4afb-8be9-f82dbae2ba7f	boolean	jsonType.label
82088212-20e3-467a-ab4e-d3d382696b18	true	multivalued
82088212-20e3-467a-ab4e-d3d382696b18	foo	user.attribute
82088212-20e3-467a-ab4e-d3d382696b18	true	access.token.claim
82088212-20e3-467a-ab4e-d3d382696b18	realm_access.roles	claim.name
82088212-20e3-467a-ab4e-d3d382696b18	String	jsonType.label
be11e008-403b-4a45-8545-32e01b548c98	true	multivalued
be11e008-403b-4a45-8545-32e01b548c98	foo	user.attribute
be11e008-403b-4a45-8545-32e01b548c98	true	access.token.claim
be11e008-403b-4a45-8545-32e01b548c98	resource_access.${client_id}.roles	claim.name
be11e008-403b-4a45-8545-32e01b548c98	String	jsonType.label
05737d33-7682-434e-b943-3f256f70fe59	true	userinfo.token.claim
05737d33-7682-434e-b943-3f256f70fe59	username	user.attribute
05737d33-7682-434e-b943-3f256f70fe59	true	id.token.claim
05737d33-7682-434e-b943-3f256f70fe59	true	access.token.claim
05737d33-7682-434e-b943-3f256f70fe59	upn	claim.name
05737d33-7682-434e-b943-3f256f70fe59	String	jsonType.label
4079b45c-56b8-45bd-967f-ad429cbf799e	true	multivalued
4079b45c-56b8-45bd-967f-ad429cbf799e	foo	user.attribute
4079b45c-56b8-45bd-967f-ad429cbf799e	true	id.token.claim
4079b45c-56b8-45bd-967f-ad429cbf799e	true	access.token.claim
4079b45c-56b8-45bd-967f-ad429cbf799e	groups	claim.name
4079b45c-56b8-45bd-967f-ad429cbf799e	String	jsonType.label
0a130771-b23e-4eff-9e58-c97cf4274463	formatted	user.attribute.formatted
0a130771-b23e-4eff-9e58-c97cf4274463	country	user.attribute.country
0a130771-b23e-4eff-9e58-c97cf4274463	postal_code	user.attribute.postal_code
0a130771-b23e-4eff-9e58-c97cf4274463	true	userinfo.token.claim
0a130771-b23e-4eff-9e58-c97cf4274463	street	user.attribute.street
0a130771-b23e-4eff-9e58-c97cf4274463	true	id.token.claim
0a130771-b23e-4eff-9e58-c97cf4274463	region	user.attribute.region
0a130771-b23e-4eff-9e58-c97cf4274463	true	access.token.claim
0a130771-b23e-4eff-9e58-c97cf4274463	locality	user.attribute.locality
36d5f47a-8f42-4c20-b957-2d4fec1c15d2	true	userinfo.token.claim
36d5f47a-8f42-4c20-b957-2d4fec1c15d2	email	user.attribute
36d5f47a-8f42-4c20-b957-2d4fec1c15d2	true	id.token.claim
36d5f47a-8f42-4c20-b957-2d4fec1c15d2	true	access.token.claim
36d5f47a-8f42-4c20-b957-2d4fec1c15d2	email	claim.name
36d5f47a-8f42-4c20-b957-2d4fec1c15d2	String	jsonType.label
ff65b209-9d67-45eb-aa26-9554876681f5	true	userinfo.token.claim
ff65b209-9d67-45eb-aa26-9554876681f5	emailVerified	user.attribute
ff65b209-9d67-45eb-aa26-9554876681f5	true	id.token.claim
ff65b209-9d67-45eb-aa26-9554876681f5	true	access.token.claim
ff65b209-9d67-45eb-aa26-9554876681f5	email_verified	claim.name
ff65b209-9d67-45eb-aa26-9554876681f5	boolean	jsonType.label
502f0449-1106-469c-9d7e-a9dd44a95171	true	multivalued
502f0449-1106-469c-9d7e-a9dd44a95171	true	userinfo.token.claim
502f0449-1106-469c-9d7e-a9dd44a95171	foo	user.attribute
502f0449-1106-469c-9d7e-a9dd44a95171	true	id.token.claim
502f0449-1106-469c-9d7e-a9dd44a95171	true	access.token.claim
502f0449-1106-469c-9d7e-a9dd44a95171	groups	claim.name
502f0449-1106-469c-9d7e-a9dd44a95171	String	jsonType.label
fca62ace-ed02-471c-a7e2-45b64acffaf4	true	userinfo.token.claim
fca62ace-ed02-471c-a7e2-45b64acffaf4	username	user.attribute
fca62ace-ed02-471c-a7e2-45b64acffaf4	true	id.token.claim
fca62ace-ed02-471c-a7e2-45b64acffaf4	true	access.token.claim
fca62ace-ed02-471c-a7e2-45b64acffaf4	upn	claim.name
fca62ace-ed02-471c-a7e2-45b64acffaf4	String	jsonType.label
b8b561da-70d2-4c96-886a-271140735dee	true	userinfo.token.claim
b8b561da-70d2-4c96-886a-271140735dee	phoneNumberVerified	user.attribute
b8b561da-70d2-4c96-886a-271140735dee	true	id.token.claim
b8b561da-70d2-4c96-886a-271140735dee	true	access.token.claim
b8b561da-70d2-4c96-886a-271140735dee	phone_number_verified	claim.name
b8b561da-70d2-4c96-886a-271140735dee	boolean	jsonType.label
aa791dd4-5da9-4379-bdfb-16acd62a26ff	true	userinfo.token.claim
aa791dd4-5da9-4379-bdfb-16acd62a26ff	phoneNumber	user.attribute
aa791dd4-5da9-4379-bdfb-16acd62a26ff	true	id.token.claim
aa791dd4-5da9-4379-bdfb-16acd62a26ff	true	access.token.claim
aa791dd4-5da9-4379-bdfb-16acd62a26ff	phone_number	claim.name
aa791dd4-5da9-4379-bdfb-16acd62a26ff	String	jsonType.label
ca729c95-0c14-4f63-98cb-eef4f7a2ebdd	true	userinfo.token.claim
ca729c95-0c14-4f63-98cb-eef4f7a2ebdd	firstName	user.attribute
ca729c95-0c14-4f63-98cb-eef4f7a2ebdd	true	id.token.claim
ca729c95-0c14-4f63-98cb-eef4f7a2ebdd	true	access.token.claim
ca729c95-0c14-4f63-98cb-eef4f7a2ebdd	given_name	claim.name
ca729c95-0c14-4f63-98cb-eef4f7a2ebdd	String	jsonType.label
3c656a65-c7a2-476b-b642-75585c7dc099	true	userinfo.token.claim
3c656a65-c7a2-476b-b642-75585c7dc099	username	user.attribute
3c656a65-c7a2-476b-b642-75585c7dc099	true	id.token.claim
3c656a65-c7a2-476b-b642-75585c7dc099	true	access.token.claim
3c656a65-c7a2-476b-b642-75585c7dc099	preferred_username	claim.name
3c656a65-c7a2-476b-b642-75585c7dc099	String	jsonType.label
1f0dcaa6-67be-44a7-8844-346c84e8d318	true	userinfo.token.claim
1f0dcaa6-67be-44a7-8844-346c84e8d318	locale	user.attribute
1f0dcaa6-67be-44a7-8844-346c84e8d318	true	id.token.claim
1f0dcaa6-67be-44a7-8844-346c84e8d318	true	access.token.claim
1f0dcaa6-67be-44a7-8844-346c84e8d318	locale	claim.name
1f0dcaa6-67be-44a7-8844-346c84e8d318	String	jsonType.label
76543b65-1f92-47ef-bae5-522725778617	true	userinfo.token.claim
76543b65-1f92-47ef-bae5-522725778617	profile	user.attribute
76543b65-1f92-47ef-bae5-522725778617	true	id.token.claim
76543b65-1f92-47ef-bae5-522725778617	true	access.token.claim
76543b65-1f92-47ef-bae5-522725778617	profile	claim.name
76543b65-1f92-47ef-bae5-522725778617	String	jsonType.label
50344589-41d8-4a86-9abb-f7395a18e872	true	id.token.claim
50344589-41d8-4a86-9abb-f7395a18e872	true	access.token.claim
50344589-41d8-4a86-9abb-f7395a18e872	true	userinfo.token.claim
b8194ec3-eda9-45bb-a029-a1c5ddf4c5ed	true	userinfo.token.claim
b8194ec3-eda9-45bb-a029-a1c5ddf4c5ed	lastName	user.attribute
b8194ec3-eda9-45bb-a029-a1c5ddf4c5ed	true	id.token.claim
b8194ec3-eda9-45bb-a029-a1c5ddf4c5ed	true	access.token.claim
b8194ec3-eda9-45bb-a029-a1c5ddf4c5ed	family_name	claim.name
b8194ec3-eda9-45bb-a029-a1c5ddf4c5ed	String	jsonType.label
0353d1fb-7cec-4f97-acdd-0f28fe366294	true	userinfo.token.claim
0353d1fb-7cec-4f97-acdd-0f28fe366294	picture	user.attribute
0353d1fb-7cec-4f97-acdd-0f28fe366294	true	id.token.claim
0353d1fb-7cec-4f97-acdd-0f28fe366294	true	access.token.claim
0353d1fb-7cec-4f97-acdd-0f28fe366294	picture	claim.name
0353d1fb-7cec-4f97-acdd-0f28fe366294	String	jsonType.label
1112993f-6141-4fc1-93a7-9448c6ebe04e	true	userinfo.token.claim
1112993f-6141-4fc1-93a7-9448c6ebe04e	gender	user.attribute
1112993f-6141-4fc1-93a7-9448c6ebe04e	true	id.token.claim
1112993f-6141-4fc1-93a7-9448c6ebe04e	true	access.token.claim
1112993f-6141-4fc1-93a7-9448c6ebe04e	gender	claim.name
1112993f-6141-4fc1-93a7-9448c6ebe04e	String	jsonType.label
85c4edae-a945-4af1-a3b1-9b1b311d78ad	true	userinfo.token.claim
85c4edae-a945-4af1-a3b1-9b1b311d78ad	website	user.attribute
85c4edae-a945-4af1-a3b1-9b1b311d78ad	true	id.token.claim
85c4edae-a945-4af1-a3b1-9b1b311d78ad	true	access.token.claim
85c4edae-a945-4af1-a3b1-9b1b311d78ad	website	claim.name
85c4edae-a945-4af1-a3b1-9b1b311d78ad	String	jsonType.label
ca137868-6ea5-403d-96d2-5a943c44368c	true	userinfo.token.claim
ca137868-6ea5-403d-96d2-5a943c44368c	birthdate	user.attribute
ca137868-6ea5-403d-96d2-5a943c44368c	true	id.token.claim
ca137868-6ea5-403d-96d2-5a943c44368c	true	access.token.claim
ca137868-6ea5-403d-96d2-5a943c44368c	birthdate	claim.name
ca137868-6ea5-403d-96d2-5a943c44368c	String	jsonType.label
0ed75dfb-1d79-4186-9389-a647f6f645c3	true	userinfo.token.claim
0ed75dfb-1d79-4186-9389-a647f6f645c3	updatedAt	user.attribute
0ed75dfb-1d79-4186-9389-a647f6f645c3	true	id.token.claim
0ed75dfb-1d79-4186-9389-a647f6f645c3	true	access.token.claim
0ed75dfb-1d79-4186-9389-a647f6f645c3	updated_at	claim.name
0ed75dfb-1d79-4186-9389-a647f6f645c3	String	jsonType.label
bfd93ca1-4107-429f-a89c-173b048c5bd9	true	userinfo.token.claim
bfd93ca1-4107-429f-a89c-173b048c5bd9	nickname	user.attribute
bfd93ca1-4107-429f-a89c-173b048c5bd9	true	id.token.claim
bfd93ca1-4107-429f-a89c-173b048c5bd9	true	access.token.claim
bfd93ca1-4107-429f-a89c-173b048c5bd9	nickname	claim.name
bfd93ca1-4107-429f-a89c-173b048c5bd9	String	jsonType.label
af01b6e2-b827-4063-bf97-b01fbbc0e574	true	userinfo.token.claim
af01b6e2-b827-4063-bf97-b01fbbc0e574	middleName	user.attribute
af01b6e2-b827-4063-bf97-b01fbbc0e574	true	id.token.claim
af01b6e2-b827-4063-bf97-b01fbbc0e574	true	access.token.claim
af01b6e2-b827-4063-bf97-b01fbbc0e574	middle_name	claim.name
af01b6e2-b827-4063-bf97-b01fbbc0e574	String	jsonType.label
8258103f-39aa-490c-b077-4bf1ab2fe9ed	true	userinfo.token.claim
8258103f-39aa-490c-b077-4bf1ab2fe9ed	zoneinfo	user.attribute
8258103f-39aa-490c-b077-4bf1ab2fe9ed	true	id.token.claim
8258103f-39aa-490c-b077-4bf1ab2fe9ed	true	access.token.claim
8258103f-39aa-490c-b077-4bf1ab2fe9ed	zoneinfo	claim.name
8258103f-39aa-490c-b077-4bf1ab2fe9ed	String	jsonType.label
309cd305-bc10-4027-ae06-1919ccc6db82	false	single
309cd305-bc10-4027-ae06-1919ccc6db82	Basic	attribute.nameformat
309cd305-bc10-4027-ae06-1919ccc6db82	Role	attribute.name
cdb28784-1bba-4fff-a0a7-129a69df8b10	foo	user.attribute
cdb28784-1bba-4fff-a0a7-129a69df8b10	true	access.token.claim
cdb28784-1bba-4fff-a0a7-129a69df8b10	resource_access.${client_id}.roles	claim.name
cdb28784-1bba-4fff-a0a7-129a69df8b10	String	jsonType.label
cdb28784-1bba-4fff-a0a7-129a69df8b10	true	multivalued
994c68c9-4290-4e17-8ba2-4f8626302bf7	foo	user.attribute
994c68c9-4290-4e17-8ba2-4f8626302bf7	true	access.token.claim
994c68c9-4290-4e17-8ba2-4f8626302bf7	realm_access.roles	claim.name
994c68c9-4290-4e17-8ba2-4f8626302bf7	String	jsonType.label
994c68c9-4290-4e17-8ba2-4f8626302bf7	true	multivalued
a226c17f-200e-4bbb-bdc9-6cfd9df3d9a4	clientId	user.session.note
a226c17f-200e-4bbb-bdc9-6cfd9df3d9a4	true	id.token.claim
a226c17f-200e-4bbb-bdc9-6cfd9df3d9a4	true	access.token.claim
a226c17f-200e-4bbb-bdc9-6cfd9df3d9a4	clientId	claim.name
a226c17f-200e-4bbb-bdc9-6cfd9df3d9a4	String	jsonType.label
a226c17f-200e-4bbb-bdc9-6cfd9df3d9a4	true	userinfo.token.claim
3951f64b-45c0-44e4-9835-5e617fa3bc79	clientHost	user.session.note
3951f64b-45c0-44e4-9835-5e617fa3bc79	true	id.token.claim
3951f64b-45c0-44e4-9835-5e617fa3bc79	true	access.token.claim
3951f64b-45c0-44e4-9835-5e617fa3bc79	clientHost	claim.name
3951f64b-45c0-44e4-9835-5e617fa3bc79	String	jsonType.label
39acafdb-bf49-422d-94e6-f33687586c3c	clientAddress	user.session.note
39acafdb-bf49-422d-94e6-f33687586c3c	true	id.token.claim
39acafdb-bf49-422d-94e6-f33687586c3c	true	access.token.claim
39acafdb-bf49-422d-94e6-f33687586c3c	clientAddress	claim.name
39acafdb-bf49-422d-94e6-f33687586c3c	String	jsonType.label
3951f64b-45c0-44e4-9835-5e617fa3bc79	true	userinfo.token.claim
39acafdb-bf49-422d-94e6-f33687586c3c	true	userinfo.token.claim
f29632c5-8df8-4349-9290-6fc340eef313	true	userinfo.token.claim
f29632c5-8df8-4349-9290-6fc340eef313	locale	user.attribute
f29632c5-8df8-4349-9290-6fc340eef313	true	id.token.claim
f29632c5-8df8-4349-9290-6fc340eef313	true	access.token.claim
f29632c5-8df8-4349-9290-6fc340eef313	locale	claim.name
f29632c5-8df8-4349-9290-6fc340eef313	String	jsonType.label
a07d7b29-9bfc-4d04-9597-d017088909fa	clientId	user.session.note
a07d7b29-9bfc-4d04-9597-d017088909fa	true	id.token.claim
a07d7b29-9bfc-4d04-9597-d017088909fa	true	access.token.claim
a07d7b29-9bfc-4d04-9597-d017088909fa	clientId	claim.name
a07d7b29-9bfc-4d04-9597-d017088909fa	String	jsonType.label
d7d84699-6870-4333-b790-8f17de7530ee	clientHost	user.session.note
d7d84699-6870-4333-b790-8f17de7530ee	true	id.token.claim
d7d84699-6870-4333-b790-8f17de7530ee	true	access.token.claim
d7d84699-6870-4333-b790-8f17de7530ee	clientHost	claim.name
d7d84699-6870-4333-b790-8f17de7530ee	String	jsonType.label
c83cc5df-c0d3-40d1-b069-bfe2480c49aa	clientAddress	user.session.note
c83cc5df-c0d3-40d1-b069-bfe2480c49aa	true	id.token.claim
c83cc5df-c0d3-40d1-b069-bfe2480c49aa	true	access.token.claim
c83cc5df-c0d3-40d1-b069-bfe2480c49aa	clientAddress	claim.name
c83cc5df-c0d3-40d1-b069-bfe2480c49aa	String	jsonType.label
\.


--
-- Data for Name: realm; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm (id, access_code_lifespan, user_action_lifespan, access_token_lifespan, account_theme, admin_theme, email_theme, enabled, events_enabled, events_expiration, login_theme, name, not_before, password_policy, registration_allowed, remember_me, reset_password_allowed, social, ssl_required, sso_idle_timeout, sso_max_lifespan, update_profile_on_soc_login, verify_email, master_admin_client, login_lifespan, internationalization_enabled, default_locale, reg_email_as_username, admin_events_enabled, admin_events_details_enabled, edit_username_allowed, otp_policy_counter, otp_policy_window, otp_policy_period, otp_policy_digits, otp_policy_alg, otp_policy_type, browser_flow, registration_flow, direct_grant_flow, reset_credentials_flow, client_auth_flow, offline_session_idle_timeout, revoke_refresh_token, access_token_life_implicit, login_with_email_allowed, duplicate_emails_allowed, docker_auth_flow, refresh_token_max_reuse, allow_user_managed_access, sso_max_lifespan_remember_me, sso_idle_timeout_remember_me) FROM stdin;
master	60	300	60	\N	\N	\N	t	f	0	\N	master	0	\N	f	f	f	f	EXTERNAL	1800	36000	f	f	ccea24b5-8d34-436f-8e4f-22b350ecdde4	1800	f	\N	f	f	f	f	0	1	30	6	HmacSHA1	totp	44b0203e-2d28-4ad8-b936-58ae331c52e9	b8d99c77-c010-4497-867a-4089ecefdec5	aeaba989-2a4f-4797-ab1b-ed4732f8c03b	724537d7-cab6-43d6-8d2e-29128e278d9c	caad2fba-0513-4f99-892d-eb52617c9feb	2592000	f	900	t	f	761db40d-59c3-44e0-a5d0-358be36c7937	0	f	0	0
my-everyday-lolita-realm	60	300	3600	\N	\N	\N	t	f	0	\N	my-everyday-lolita-realm	0	\N	f	f	t	f	EXTERNAL	432000	2592000	f	f	0886a50d-9edf-4645-a56f-be8bfbe9e82e	1800	f	\N	f	f	f	f	0	1	30	6	HmacSHA1	totp	90e3d452-2eba-41c1-ac24-02509ff5d30f	7c0e0550-cd7f-4531-b711-c6a6980ce31e	692b336c-5e29-45fc-bb76-fb36073eb320	6ed585ee-39c4-429e-ade7-ba85e4111bd0	ab5a9b6b-0f73-4846-93d7-f248f11c61bf	2592000	f	900	t	f	0545ff13-efe9-4bc0-8db3-3e3162da5d91	0	f	0	0
\.


--
-- Data for Name: realm_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_attribute (name, value, realm_id) FROM stdin;
_browser_header.contentSecurityPolicyReportOnly		master
_browser_header.xContentTypeOptions	nosniff	master
_browser_header.xRobotsTag	none	master
_browser_header.xFrameOptions	SAMEORIGIN	master
_browser_header.contentSecurityPolicy	frame-src 'self'; frame-ancestors 'self'; object-src 'none';	master
_browser_header.xXSSProtection	1; mode=block	master
_browser_header.strictTransportSecurity	max-age=31536000; includeSubDomains	master
bruteForceProtected	false	master
permanentLockout	false	master
maxFailureWaitSeconds	900	master
minimumQuickLoginWaitSeconds	60	master
waitIncrementSeconds	60	master
quickLoginCheckMilliSeconds	1000	master
maxDeltaTimeSeconds	43200	master
failureFactor	30	master
displayName	Keycloak	master
displayNameHtml	<div class="kc-logo-text"><span>Keycloak</span></div>	master
offlineSessionMaxLifespanEnabled	false	master
offlineSessionMaxLifespan	5184000	master
clientSessionIdleTimeout	0	my-everyday-lolita-realm
clientSessionMaxLifespan	0	my-everyday-lolita-realm
clientOfflineSessionIdleTimeout	0	my-everyday-lolita-realm
clientOfflineSessionMaxLifespan	0	my-everyday-lolita-realm
bruteForceProtected	false	my-everyday-lolita-realm
permanentLockout	false	my-everyday-lolita-realm
maxFailureWaitSeconds	900	my-everyday-lolita-realm
minimumQuickLoginWaitSeconds	60	my-everyday-lolita-realm
waitIncrementSeconds	60	my-everyday-lolita-realm
quickLoginCheckMilliSeconds	1000	my-everyday-lolita-realm
maxDeltaTimeSeconds	43200	my-everyday-lolita-realm
failureFactor	30	my-everyday-lolita-realm
actionTokenGeneratedByAdminLifespan	43200	my-everyday-lolita-realm
actionTokenGeneratedByUserLifespan	300	my-everyday-lolita-realm
offlineSessionMaxLifespanEnabled	false	my-everyday-lolita-realm
offlineSessionMaxLifespan	5184000	my-everyday-lolita-realm
webAuthnPolicyRpEntityName	keycloak	my-everyday-lolita-realm
webAuthnPolicySignatureAlgorithms	ES256	my-everyday-lolita-realm
webAuthnPolicyRpId		my-everyday-lolita-realm
webAuthnPolicyAttestationConveyancePreference	not specified	my-everyday-lolita-realm
webAuthnPolicyAuthenticatorAttachment	not specified	my-everyday-lolita-realm
webAuthnPolicyRequireResidentKey	not specified	my-everyday-lolita-realm
webAuthnPolicyUserVerificationRequirement	not specified	my-everyday-lolita-realm
webAuthnPolicyCreateTimeout	0	my-everyday-lolita-realm
webAuthnPolicyAvoidSameAuthenticatorRegister	false	my-everyday-lolita-realm
webAuthnPolicyRpEntityNamePasswordless	keycloak	my-everyday-lolita-realm
webAuthnPolicySignatureAlgorithmsPasswordless	ES256	my-everyday-lolita-realm
webAuthnPolicyRpIdPasswordless		my-everyday-lolita-realm
webAuthnPolicyAttestationConveyancePreferencePasswordless	not specified	my-everyday-lolita-realm
webAuthnPolicyAuthenticatorAttachmentPasswordless	not specified	my-everyday-lolita-realm
webAuthnPolicyRequireResidentKeyPasswordless	not specified	my-everyday-lolita-realm
webAuthnPolicyUserVerificationRequirementPasswordless	not specified	my-everyday-lolita-realm
webAuthnPolicyCreateTimeoutPasswordless	0	my-everyday-lolita-realm
webAuthnPolicyAvoidSameAuthenticatorRegisterPasswordless	false	my-everyday-lolita-realm
_browser_header.contentSecurityPolicyReportOnly		my-everyday-lolita-realm
_browser_header.xContentTypeOptions	nosniff	my-everyday-lolita-realm
_browser_header.xRobotsTag	none	my-everyday-lolita-realm
_browser_header.xFrameOptions	SAMEORIGIN	my-everyday-lolita-realm
_browser_header.contentSecurityPolicy	frame-src 'self'; frame-ancestors 'self'; object-src 'none';	my-everyday-lolita-realm
_browser_header.xXSSProtection	1; mode=block	my-everyday-lolita-realm
_browser_header.strictTransportSecurity	max-age=31536000; includeSubDomains	my-everyday-lolita-realm
\.


--
-- Data for Name: realm_default_groups; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_default_groups (realm_id, group_id) FROM stdin;
\.


--
-- Data for Name: realm_default_roles; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_default_roles (realm_id, role_id) FROM stdin;
master	5b24a59a-7618-483a-bfa0-70da4201b0e7
master	ee2aff9a-269e-419c-88d0-30ee69fedba6
my-everyday-lolita-realm	1147723f-9cd1-4519-9ef4-9493fc722196
my-everyday-lolita-realm	419b7053-b11a-483c-8cbe-12f6cab38385
my-everyday-lolita-realm	2d531a7f-d9de-4c7d-8733-3f949667f956
\.


--
-- Data for Name: realm_enabled_event_types; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_enabled_event_types (realm_id, value) FROM stdin;
\.


--
-- Data for Name: realm_events_listeners; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_events_listeners (realm_id, value) FROM stdin;
master	jboss-logging
my-everyday-lolita-realm	jboss-logging
\.


--
-- Data for Name: realm_localizations; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_localizations (realm_id, locale, texts) FROM stdin;
\.


--
-- Data for Name: realm_required_credential; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_required_credential (type, form_label, input, secret, realm_id) FROM stdin;
password	password	t	t	master
password	password	t	t	my-everyday-lolita-realm
\.


--
-- Data for Name: realm_smtp_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_smtp_config (realm_id, value, name) FROM stdin;
\.


--
-- Data for Name: realm_supported_locales; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_supported_locales (realm_id, value) FROM stdin;
\.


--
-- Data for Name: redirect_uris; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.redirect_uris (client_id, value) FROM stdin;
986d3ab0-fbe3-42d0-be1b-5bacc5eaf375	/realms/master/account/*
ec94f535-843f-4a0d-bcbd-278f29faa5e8	/realms/master/account/*
d2c2af8c-d1e7-4789-8c86-5e752d9643d6	/admin/master/console/*
463f127e-1394-4aeb-8e97-588bc9275ad7	/realms/my-everyday-lolita-realm/account/*
19554311-6c94-479d-bcfc-5cb5ab56d096	/realms/my-everyday-lolita-realm/account/*
5b6378af-33db-444a-93b2-14bb194f954f	/admin/my-everyday-lolita-realm/console/*
d30771ac-77e3-4424-905e-50bc58920b78	http://localhost:8080/*
d30771ac-77e3-4424-905e-50bc58920b78	https://8080-*.gitpod.io/*
\.


--
-- Data for Name: required_action_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.required_action_config (required_action_id, value, name) FROM stdin;
\.


--
-- Data for Name: required_action_provider; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.required_action_provider (id, alias, name, realm_id, enabled, default_action, provider_id, priority) FROM stdin;
34a4851d-acbc-4b84-a355-675d9c9e85a8	VERIFY_EMAIL	Verify Email	master	t	f	VERIFY_EMAIL	50
135a611d-1ed3-4417-be50-502c43102652	UPDATE_PROFILE	Update Profile	master	t	f	UPDATE_PROFILE	40
161d057f-ca1a-40ae-8a48-5bb377a3e6cc	CONFIGURE_TOTP	Configure OTP	master	t	f	CONFIGURE_TOTP	10
24e4cc41-d178-496b-9dc3-74b521a835d5	UPDATE_PASSWORD	Update Password	master	t	f	UPDATE_PASSWORD	30
78630c68-8729-4e9d-a59d-ff9da442d81a	terms_and_conditions	Terms and Conditions	master	f	f	terms_and_conditions	20
1fcd0007-8285-426c-9b5e-d13b34fbbf8a	update_user_locale	Update User Locale	master	t	f	update_user_locale	1000
9bb4cd3b-29c0-4c93-9e9c-bbeed9ab1329	delete_account	Delete Account	master	f	f	delete_account	60
09bb4fb7-695e-49e8-a005-3bf16a4635fb	CONFIGURE_TOTP	Configure OTP	my-everyday-lolita-realm	t	f	CONFIGURE_TOTP	10
cb17e3e2-65b6-45bd-b1db-01205cf4e2d5	terms_and_conditions	Terms and Conditions	my-everyday-lolita-realm	f	f	terms_and_conditions	20
22cc7f36-931d-4d6e-ad56-3effcaa16fa1	UPDATE_PASSWORD	Update Password	my-everyday-lolita-realm	t	f	UPDATE_PASSWORD	30
72d1648f-d041-4524-9aa1-07dcc9df3737	UPDATE_PROFILE	Update Profile	my-everyday-lolita-realm	t	f	UPDATE_PROFILE	40
4bd1c0f2-40c5-47df-82ff-b10b21a54baa	VERIFY_EMAIL	Verify Email	my-everyday-lolita-realm	t	f	VERIFY_EMAIL	50
4abaf70d-8a8c-4965-8cae-a6cae2038698	delete_account	Delete Account	my-everyday-lolita-realm	f	f	delete_account	60
0dc1f452-684c-41da-9f82-88aca2cf8d0b	update_user_locale	Update User Locale	my-everyday-lolita-realm	t	f	update_user_locale	1000
\.


--
-- Data for Name: resource_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_attribute (id, name, value, resource_id) FROM stdin;
\.


--
-- Data for Name: resource_policy; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_policy (resource_id, policy_id) FROM stdin;
\.


--
-- Data for Name: resource_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_scope (resource_id, scope_id) FROM stdin;
\.


--
-- Data for Name: resource_server; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server (id, allow_rs_remote_mgmt, policy_enforce_mode, decision_strategy) FROM stdin;
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	t	0	1
\.


--
-- Data for Name: resource_server_perm_ticket; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server_perm_ticket (id, owner, requester, created_timestamp, granted_timestamp, resource_id, scope_id, resource_server_id, policy_id) FROM stdin;
\.


--
-- Data for Name: resource_server_policy; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server_policy (id, name, description, type, decision_strategy, logic, resource_server_id, owner) FROM stdin;
ba024e17-b552-473c-b613-dc79754ae21c	Default Policy	A policy that grants access only for users within this realm	js	0	0	15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	\N
b6d2a66e-09d7-456f-8c28-00e832964991	Default Permission	A permission that applies to the default resource type	resource	1	0	15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	\N
\.


--
-- Data for Name: resource_server_resource; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server_resource (id, name, type, icon_uri, owner, resource_server_id, owner_managed_access, display_name) FROM stdin;
edd3bb73-6c87-4f1c-8da7-87035329aca7	Default Resource	urn:admin-cli:resources:default	\N	15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	f	\N
\.


--
-- Data for Name: resource_server_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server_scope (id, name, icon_uri, resource_server_id, display_name) FROM stdin;
\.


--
-- Data for Name: resource_uris; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_uris (resource_id, value) FROM stdin;
edd3bb73-6c87-4f1c-8da7-87035329aca7	/*
\.


--
-- Data for Name: role_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.role_attribute (id, role_id, name, value) FROM stdin;
\.


--
-- Data for Name: scope_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.scope_mapping (client_id, role_id) FROM stdin;
ec94f535-843f-4a0d-bcbd-278f29faa5e8	a24019b4-1610-4d94-956d-0d43431bf65f
19554311-6c94-479d-bcfc-5cb5ab56d096	199da019-bb23-49d2-95db-eb0d1cdb3e3c
\.


--
-- Data for Name: scope_policy; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.scope_policy (scope_id, policy_id) FROM stdin;
\.


--
-- Data for Name: user_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_attribute (name, value, user_id, id) FROM stdin;
\.


--
-- Data for Name: user_consent; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_consent (id, client_id, user_id, created_date, last_updated_date, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: user_consent_client_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_consent_client_scope (user_consent_id, scope_id) FROM stdin;
\.


--
-- Data for Name: user_entity; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_entity (id, email, email_constraint, email_verified, enabled, federation_link, first_name, last_name, realm_id, username, created_timestamp, service_account_client_link, not_before) FROM stdin;
7088b70f-fed1-4093-b2c5-9cd4678655c5	\N	685c2887-2581-482c-8114-69e087eade79	f	t	\N	\N	\N	master	admin	1626272693064	\N	0
fc96376b-4fbc-4625-a310-1101bb566ffb	\N	755c5141-4844-4236-9663-00250a6e816f	f	t	\N	\N	\N	my-everyday-lolita-realm	service-account-admin-cli	1615401615970	5b128c68-e165-43e5-be48-851fc7d4b5fc	0
899b0405-00a6-4869-be9f-0a58dd03dfb7	\N	9c4c328a-db58-4cca-af88-528ad55e4a15	f	t	\N	\N	\N	master	service-account-admin-cli	1626272925123	15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	0
1a2be0fe-9f6a-4c2a-947d-bfa70092f57c	lorem@dolor.sit	lorem@dolor.sit	f	t	\N	\N	\N	my-everyday-lolita-realm	lheido	1638710912222	\N	0
\.


--
-- Data for Name: user_federation_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_federation_config (user_federation_provider_id, value, name) FROM stdin;
\.


--
-- Data for Name: user_federation_mapper; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_federation_mapper (id, name, federation_provider_id, federation_mapper_type, realm_id) FROM stdin;
\.


--
-- Data for Name: user_federation_mapper_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_federation_mapper_config (user_federation_mapper_id, value, name) FROM stdin;
\.


--
-- Data for Name: user_federation_provider; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_federation_provider (id, changed_sync_period, display_name, full_sync_period, last_sync, priority, provider_name, realm_id) FROM stdin;
\.


--
-- Data for Name: user_group_membership; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_group_membership (group_id, user_id) FROM stdin;
\.


--
-- Data for Name: user_required_action; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_required_action (user_id, required_action) FROM stdin;
\.


--
-- Data for Name: user_role_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_role_mapping (role_id, user_id) FROM stdin;
ee2aff9a-269e-419c-88d0-30ee69fedba6	7088b70f-fed1-4093-b2c5-9cd4678655c5
5b24a59a-7618-483a-bfa0-70da4201b0e7	7088b70f-fed1-4093-b2c5-9cd4678655c5
a24019b4-1610-4d94-956d-0d43431bf65f	7088b70f-fed1-4093-b2c5-9cd4678655c5
ff52cc7b-fffd-4e6f-a31d-d7e128910691	7088b70f-fed1-4093-b2c5-9cd4678655c5
f16ec247-e190-4571-83fd-7ebaaa8da265	7088b70f-fed1-4093-b2c5-9cd4678655c5
1147723f-9cd1-4519-9ef4-9493fc722196	fc96376b-4fbc-4625-a310-1101bb566ffb
419b7053-b11a-483c-8cbe-12f6cab38385	fc96376b-4fbc-4625-a310-1101bb566ffb
199da019-bb23-49d2-95db-eb0d1cdb3e3c	fc96376b-4fbc-4625-a310-1101bb566ffb
a1e1dfb6-85fe-40c5-90ca-5b0d16d43818	fc96376b-4fbc-4625-a310-1101bb566ffb
ee2aff9a-269e-419c-88d0-30ee69fedba6	899b0405-00a6-4869-be9f-0a58dd03dfb7
5b24a59a-7618-483a-bfa0-70da4201b0e7	899b0405-00a6-4869-be9f-0a58dd03dfb7
a24019b4-1610-4d94-956d-0d43431bf65f	899b0405-00a6-4869-be9f-0a58dd03dfb7
ff52cc7b-fffd-4e6f-a31d-d7e128910691	899b0405-00a6-4869-be9f-0a58dd03dfb7
1e7198ce-ec4e-4810-b23f-e4c557cbd541	899b0405-00a6-4869-be9f-0a58dd03dfb7
f16ec247-e190-4571-83fd-7ebaaa8da265	899b0405-00a6-4869-be9f-0a58dd03dfb7
1147723f-9cd1-4519-9ef4-9493fc722196	1a2be0fe-9f6a-4c2a-947d-bfa70092f57c
419b7053-b11a-483c-8cbe-12f6cab38385	1a2be0fe-9f6a-4c2a-947d-bfa70092f57c
2d531a7f-d9de-4c7d-8733-3f949667f956	1a2be0fe-9f6a-4c2a-947d-bfa70092f57c
199da019-bb23-49d2-95db-eb0d1cdb3e3c	1a2be0fe-9f6a-4c2a-947d-bfa70092f57c
a1e1dfb6-85fe-40c5-90ca-5b0d16d43818	1a2be0fe-9f6a-4c2a-947d-bfa70092f57c
816fa806-10e8-413b-95be-12173b8981eb	1a2be0fe-9f6a-4c2a-947d-bfa70092f57c
5b12ec76-b86f-4420-82ec-475b9bd83381	1a2be0fe-9f6a-4c2a-947d-bfa70092f57c
\.


--
-- Data for Name: user_session; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_session (id, auth_method, ip_address, last_session_refresh, login_username, realm_id, remember_me, started, user_id, user_session_state, broker_session_id, broker_user_id) FROM stdin;
\.


--
-- Data for Name: user_session_note; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_session_note (user_session, name, value) FROM stdin;
\.


--
-- Data for Name: username_login_failure; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.username_login_failure (realm_id, username, failed_login_not_before, last_failure, last_ip_failure, num_failures) FROM stdin;
\.


--
-- Data for Name: web_origins; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.web_origins (client_id, value) FROM stdin;
d2c2af8c-d1e7-4789-8c86-5e752d9643d6	+
5b6378af-33db-444a-93b2-14bb194f954f	+
5b128c68-e165-43e5-be48-851fc7d4b5fc	http://api.mel.localhost
5b128c68-e165-43e5-be48-851fc7d4b5fc	http://localhost:3001
15b3e2ea-630b-4589-99dd-7bdb9b42a0f6	http://registration.mel.localhost
d30771ac-77e3-4424-905e-50bc58920b78	*
\.


--
-- Name: username_login_failure CONSTRAINT_17-2; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.username_login_failure
    ADD CONSTRAINT "CONSTRAINT_17-2" PRIMARY KEY (realm_id, username);


--
-- Name: keycloak_role UK_J3RWUVD56ONTGSUHOGM184WW2-2; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT "UK_J3RWUVD56ONTGSUHOGM184WW2-2" UNIQUE (name, client_realm_constraint);


--
-- Name: client_auth_flow_bindings c_cli_flow_bind; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_auth_flow_bindings
    ADD CONSTRAINT c_cli_flow_bind PRIMARY KEY (client_id, binding_name);


--
-- Name: client_scope_client c_cli_scope_bind; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_client
    ADD CONSTRAINT c_cli_scope_bind PRIMARY KEY (client_id, scope_id);


--
-- Name: client_initial_access cnstr_client_init_acc_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_initial_access
    ADD CONSTRAINT cnstr_client_init_acc_pk PRIMARY KEY (id);


--
-- Name: realm_default_groups con_group_id_def_groups; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT con_group_id_def_groups UNIQUE (group_id);


--
-- Name: broker_link constr_broker_link_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.broker_link
    ADD CONSTRAINT constr_broker_link_pk PRIMARY KEY (identity_provider, user_id);


--
-- Name: client_user_session_note constr_cl_usr_ses_note; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_user_session_note
    ADD CONSTRAINT constr_cl_usr_ses_note PRIMARY KEY (client_session, name);


--
-- Name: client_default_roles constr_client_default_roles; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_default_roles
    ADD CONSTRAINT constr_client_default_roles PRIMARY KEY (client_id, role_id);


--
-- Name: component_config constr_component_config_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.component_config
    ADD CONSTRAINT constr_component_config_pk PRIMARY KEY (id);


--
-- Name: component constr_component_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT constr_component_pk PRIMARY KEY (id);


--
-- Name: fed_user_required_action constr_fed_required_action; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_required_action
    ADD CONSTRAINT constr_fed_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: fed_user_attribute constr_fed_user_attr_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_attribute
    ADD CONSTRAINT constr_fed_user_attr_pk PRIMARY KEY (id);


--
-- Name: fed_user_consent constr_fed_user_consent_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_consent
    ADD CONSTRAINT constr_fed_user_consent_pk PRIMARY KEY (id);


--
-- Name: fed_user_credential constr_fed_user_cred_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_credential
    ADD CONSTRAINT constr_fed_user_cred_pk PRIMARY KEY (id);


--
-- Name: fed_user_group_membership constr_fed_user_group; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_group_membership
    ADD CONSTRAINT constr_fed_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: fed_user_role_mapping constr_fed_user_role; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_role_mapping
    ADD CONSTRAINT constr_fed_user_role PRIMARY KEY (role_id, user_id);


--
-- Name: federated_user constr_federated_user; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.federated_user
    ADD CONSTRAINT constr_federated_user PRIMARY KEY (id);


--
-- Name: realm_default_groups constr_realm_default_groups; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT constr_realm_default_groups PRIMARY KEY (realm_id, group_id);


--
-- Name: realm_enabled_event_types constr_realm_enabl_event_types; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_enabled_event_types
    ADD CONSTRAINT constr_realm_enabl_event_types PRIMARY KEY (realm_id, value);


--
-- Name: realm_events_listeners constr_realm_events_listeners; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_events_listeners
    ADD CONSTRAINT constr_realm_events_listeners PRIMARY KEY (realm_id, value);


--
-- Name: realm_supported_locales constr_realm_supported_locales; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_supported_locales
    ADD CONSTRAINT constr_realm_supported_locales PRIMARY KEY (realm_id, value);


--
-- Name: identity_provider constraint_2b; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT constraint_2b PRIMARY KEY (internal_id);


--
-- Name: client_attributes constraint_3c; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_attributes
    ADD CONSTRAINT constraint_3c PRIMARY KEY (client_id, name);


--
-- Name: event_entity constraint_4; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.event_entity
    ADD CONSTRAINT constraint_4 PRIMARY KEY (id);


--
-- Name: federated_identity constraint_40; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.federated_identity
    ADD CONSTRAINT constraint_40 PRIMARY KEY (identity_provider, user_id);


--
-- Name: realm constraint_4a; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT constraint_4a PRIMARY KEY (id);


--
-- Name: client_session_role constraint_5; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_role
    ADD CONSTRAINT constraint_5 PRIMARY KEY (client_session, role_id);


--
-- Name: user_session constraint_57; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_session
    ADD CONSTRAINT constraint_57 PRIMARY KEY (id);


--
-- Name: user_federation_provider constraint_5c; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_provider
    ADD CONSTRAINT constraint_5c PRIMARY KEY (id);


--
-- Name: client_session_note constraint_5e; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_note
    ADD CONSTRAINT constraint_5e PRIMARY KEY (client_session, name);


--
-- Name: client constraint_7; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT constraint_7 PRIMARY KEY (id);


--
-- Name: client_session constraint_8; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session
    ADD CONSTRAINT constraint_8 PRIMARY KEY (id);


--
-- Name: scope_mapping constraint_81; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT constraint_81 PRIMARY KEY (client_id, role_id);


--
-- Name: client_node_registrations constraint_84; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_node_registrations
    ADD CONSTRAINT constraint_84 PRIMARY KEY (client_id, name);


--
-- Name: realm_attribute constraint_9; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_attribute
    ADD CONSTRAINT constraint_9 PRIMARY KEY (name, realm_id);


--
-- Name: realm_required_credential constraint_92; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_required_credential
    ADD CONSTRAINT constraint_92 PRIMARY KEY (realm_id, type);


--
-- Name: keycloak_role constraint_a; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT constraint_a PRIMARY KEY (id);


--
-- Name: admin_event_entity constraint_admin_event_entity; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.admin_event_entity
    ADD CONSTRAINT constraint_admin_event_entity PRIMARY KEY (id);


--
-- Name: authenticator_config_entry constraint_auth_cfg_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authenticator_config_entry
    ADD CONSTRAINT constraint_auth_cfg_pk PRIMARY KEY (authenticator_id, name);


--
-- Name: authentication_execution constraint_auth_exec_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT constraint_auth_exec_pk PRIMARY KEY (id);


--
-- Name: authentication_flow constraint_auth_flow_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_flow
    ADD CONSTRAINT constraint_auth_flow_pk PRIMARY KEY (id);


--
-- Name: authenticator_config constraint_auth_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authenticator_config
    ADD CONSTRAINT constraint_auth_pk PRIMARY KEY (id);


--
-- Name: client_session_auth_status constraint_auth_status_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_auth_status
    ADD CONSTRAINT constraint_auth_status_pk PRIMARY KEY (client_session, authenticator);


--
-- Name: user_role_mapping constraint_c; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_role_mapping
    ADD CONSTRAINT constraint_c PRIMARY KEY (role_id, user_id);


--
-- Name: composite_role constraint_composite_role; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT constraint_composite_role PRIMARY KEY (composite, child_role);


--
-- Name: client_session_prot_mapper constraint_cs_pmp_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_prot_mapper
    ADD CONSTRAINT constraint_cs_pmp_pk PRIMARY KEY (client_session, protocol_mapper_id);


--
-- Name: identity_provider_config constraint_d; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider_config
    ADD CONSTRAINT constraint_d PRIMARY KEY (identity_provider_id, name);


--
-- Name: policy_config constraint_dpc; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.policy_config
    ADD CONSTRAINT constraint_dpc PRIMARY KEY (policy_id, name);


--
-- Name: realm_smtp_config constraint_e; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_smtp_config
    ADD CONSTRAINT constraint_e PRIMARY KEY (realm_id, name);


--
-- Name: credential constraint_f; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.credential
    ADD CONSTRAINT constraint_f PRIMARY KEY (id);


--
-- Name: user_federation_config constraint_f9; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_config
    ADD CONSTRAINT constraint_f9 PRIMARY KEY (user_federation_provider_id, name);


--
-- Name: resource_server_perm_ticket constraint_fapmt; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT constraint_fapmt PRIMARY KEY (id);


--
-- Name: resource_server_resource constraint_farsr; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT constraint_farsr PRIMARY KEY (id);


--
-- Name: resource_server_policy constraint_farsrp; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT constraint_farsrp PRIMARY KEY (id);


--
-- Name: associated_policy constraint_farsrpap; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT constraint_farsrpap PRIMARY KEY (policy_id, associated_policy_id);


--
-- Name: resource_policy constraint_farsrpp; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT constraint_farsrpp PRIMARY KEY (resource_id, policy_id);


--
-- Name: resource_server_scope constraint_farsrs; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT constraint_farsrs PRIMARY KEY (id);


--
-- Name: resource_scope constraint_farsrsp; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT constraint_farsrsp PRIMARY KEY (resource_id, scope_id);


--
-- Name: scope_policy constraint_farsrsps; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT constraint_farsrsps PRIMARY KEY (scope_id, policy_id);


--
-- Name: user_entity constraint_fb; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT constraint_fb PRIMARY KEY (id);


--
-- Name: user_federation_mapper_config constraint_fedmapper_cfg_pm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper_config
    ADD CONSTRAINT constraint_fedmapper_cfg_pm PRIMARY KEY (user_federation_mapper_id, name);


--
-- Name: user_federation_mapper constraint_fedmapperpm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT constraint_fedmapperpm PRIMARY KEY (id);


--
-- Name: fed_user_consent_cl_scope constraint_fgrntcsnt_clsc_pm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_consent_cl_scope
    ADD CONSTRAINT constraint_fgrntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent_client_scope constraint_grntcsnt_clsc_pm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent_client_scope
    ADD CONSTRAINT constraint_grntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent constraint_grntcsnt_pm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT constraint_grntcsnt_pm PRIMARY KEY (id);


--
-- Name: keycloak_group constraint_group; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT constraint_group PRIMARY KEY (id);


--
-- Name: group_attribute constraint_group_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.group_attribute
    ADD CONSTRAINT constraint_group_attribute_pk PRIMARY KEY (id);


--
-- Name: group_role_mapping constraint_group_role; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT constraint_group_role PRIMARY KEY (role_id, group_id);


--
-- Name: identity_provider_mapper constraint_idpm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider_mapper
    ADD CONSTRAINT constraint_idpm PRIMARY KEY (id);


--
-- Name: idp_mapper_config constraint_idpmconfig; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.idp_mapper_config
    ADD CONSTRAINT constraint_idpmconfig PRIMARY KEY (idp_mapper_id, name);


--
-- Name: migration_model constraint_migmod; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.migration_model
    ADD CONSTRAINT constraint_migmod PRIMARY KEY (id);


--
-- Name: offline_client_session constraint_offl_cl_ses_pk3; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.offline_client_session
    ADD CONSTRAINT constraint_offl_cl_ses_pk3 PRIMARY KEY (user_session_id, client_id, client_storage_provider, external_client_id, offline_flag);


--
-- Name: offline_user_session constraint_offl_us_ses_pk2; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.offline_user_session
    ADD CONSTRAINT constraint_offl_us_ses_pk2 PRIMARY KEY (user_session_id, offline_flag);


--
-- Name: protocol_mapper constraint_pcm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT constraint_pcm PRIMARY KEY (id);


--
-- Name: protocol_mapper_config constraint_pmconfig; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper_config
    ADD CONSTRAINT constraint_pmconfig PRIMARY KEY (protocol_mapper_id, name);


--
-- Name: realm_default_roles constraint_realm_default_roles; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_default_roles
    ADD CONSTRAINT constraint_realm_default_roles PRIMARY KEY (realm_id, role_id);


--
-- Name: redirect_uris constraint_redirect_uris; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.redirect_uris
    ADD CONSTRAINT constraint_redirect_uris PRIMARY KEY (client_id, value);


--
-- Name: required_action_config constraint_req_act_cfg_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.required_action_config
    ADD CONSTRAINT constraint_req_act_cfg_pk PRIMARY KEY (required_action_id, name);


--
-- Name: required_action_provider constraint_req_act_prv_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.required_action_provider
    ADD CONSTRAINT constraint_req_act_prv_pk PRIMARY KEY (id);


--
-- Name: user_required_action constraint_required_action; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_required_action
    ADD CONSTRAINT constraint_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: resource_uris constraint_resour_uris_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_uris
    ADD CONSTRAINT constraint_resour_uris_pk PRIMARY KEY (resource_id, value);


--
-- Name: role_attribute constraint_role_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.role_attribute
    ADD CONSTRAINT constraint_role_attribute_pk PRIMARY KEY (id);


--
-- Name: user_attribute constraint_user_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_attribute
    ADD CONSTRAINT constraint_user_attribute_pk PRIMARY KEY (id);


--
-- Name: user_group_membership constraint_user_group; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_group_membership
    ADD CONSTRAINT constraint_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: user_session_note constraint_usn_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_session_note
    ADD CONSTRAINT constraint_usn_pk PRIMARY KEY (user_session, name);


--
-- Name: web_origins constraint_web_origins; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.web_origins
    ADD CONSTRAINT constraint_web_origins PRIMARY KEY (client_id, value);


--
-- Name: client_scope_attributes pk_cl_tmpl_attr; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_attributes
    ADD CONSTRAINT pk_cl_tmpl_attr PRIMARY KEY (scope_id, name);


--
-- Name: client_scope pk_cli_template; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT pk_cli_template PRIMARY KEY (id);


--
-- Name: databasechangeloglock pk_databasechangeloglock; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id);


--
-- Name: resource_server pk_resource_server; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server
    ADD CONSTRAINT pk_resource_server PRIMARY KEY (id);


--
-- Name: client_scope_role_mapping pk_template_scope; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT pk_template_scope PRIMARY KEY (scope_id, role_id);


--
-- Name: default_client_scope r_def_cli_scope_bind; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT r_def_cli_scope_bind PRIMARY KEY (realm_id, scope_id);


--
-- Name: realm_localizations realm_localizations_pkey; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_localizations
    ADD CONSTRAINT realm_localizations_pkey PRIMARY KEY (realm_id, locale);


--
-- Name: resource_attribute res_attr_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_attribute
    ADD CONSTRAINT res_attr_pk PRIMARY KEY (id);


--
-- Name: keycloak_group sibling_names; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT sibling_names UNIQUE (realm_id, parent_group, name);


--
-- Name: identity_provider uk_2daelwnibji49avxsrtuf6xj33; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT uk_2daelwnibji49avxsrtuf6xj33 UNIQUE (provider_alias, realm_id);


--
-- Name: client_default_roles uk_8aelwnibji49avxsrtuf6xjow; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_default_roles
    ADD CONSTRAINT uk_8aelwnibji49avxsrtuf6xjow UNIQUE (role_id);


--
-- Name: client uk_b71cjlbenv945rb6gcon438at; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT uk_b71cjlbenv945rb6gcon438at UNIQUE (realm_id, client_id);


--
-- Name: client_scope uk_cli_scope; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT uk_cli_scope UNIQUE (realm_id, name);


--
-- Name: user_entity uk_dykn684sl8up1crfei6eckhd7; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT uk_dykn684sl8up1crfei6eckhd7 UNIQUE (realm_id, email_constraint);


--
-- Name: resource_server_resource uk_frsr6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5ha6 UNIQUE (name, owner, resource_server_id);


--
-- Name: resource_server_perm_ticket uk_frsr6t700s9v50bu18ws5pmt; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5pmt UNIQUE (owner, requester, resource_server_id, resource_id, scope_id);


--
-- Name: resource_server_policy uk_frsrpt700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT uk_frsrpt700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: resource_server_scope uk_frsrst700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT uk_frsrst700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: realm_default_roles uk_h4wpd7w4hsoolni3h0sw7btje; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_default_roles
    ADD CONSTRAINT uk_h4wpd7w4hsoolni3h0sw7btje UNIQUE (role_id);


--
-- Name: user_consent uk_jkuwuvd56ontgsuhogm8uewrt; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT uk_jkuwuvd56ontgsuhogm8uewrt UNIQUE (client_id, client_storage_provider, external_client_id, user_id);


--
-- Name: realm uk_orvsdmla56612eaefiq6wl5oi; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT uk_orvsdmla56612eaefiq6wl5oi UNIQUE (name);


--
-- Name: user_entity uk_ru8tt6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT uk_ru8tt6t700s9v50bu18ws5ha6 UNIQUE (realm_id, username);


--
-- Name: idx_assoc_pol_assoc_pol_id; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_assoc_pol_assoc_pol_id ON public.associated_policy USING btree (associated_policy_id);


--
-- Name: idx_auth_config_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_auth_config_realm ON public.authenticator_config USING btree (realm_id);


--
-- Name: idx_auth_exec_flow; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_auth_exec_flow ON public.authentication_execution USING btree (flow_id);


--
-- Name: idx_auth_exec_realm_flow; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_auth_exec_realm_flow ON public.authentication_execution USING btree (realm_id, flow_id);


--
-- Name: idx_auth_flow_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_auth_flow_realm ON public.authentication_flow USING btree (realm_id);


--
-- Name: idx_cl_clscope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_cl_clscope ON public.client_scope_client USING btree (scope_id);


--
-- Name: idx_client_def_roles_client; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_client_def_roles_client ON public.client_default_roles USING btree (client_id);


--
-- Name: idx_client_id; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_client_id ON public.client USING btree (client_id);


--
-- Name: idx_client_init_acc_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_client_init_acc_realm ON public.client_initial_access USING btree (realm_id);


--
-- Name: idx_client_session_session; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_client_session_session ON public.client_session USING btree (session_id);


--
-- Name: idx_clscope_attrs; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_clscope_attrs ON public.client_scope_attributes USING btree (scope_id);


--
-- Name: idx_clscope_cl; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_clscope_cl ON public.client_scope_client USING btree (client_id);


--
-- Name: idx_clscope_protmap; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_clscope_protmap ON public.protocol_mapper USING btree (client_scope_id);


--
-- Name: idx_clscope_role; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_clscope_role ON public.client_scope_role_mapping USING btree (scope_id);


--
-- Name: idx_compo_config_compo; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_compo_config_compo ON public.component_config USING btree (component_id);


--
-- Name: idx_component_provider_type; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_component_provider_type ON public.component USING btree (provider_type);


--
-- Name: idx_component_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_component_realm ON public.component USING btree (realm_id);


--
-- Name: idx_composite; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_composite ON public.composite_role USING btree (composite);


--
-- Name: idx_composite_child; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_composite_child ON public.composite_role USING btree (child_role);


--
-- Name: idx_defcls_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_defcls_realm ON public.default_client_scope USING btree (realm_id);


--
-- Name: idx_defcls_scope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_defcls_scope ON public.default_client_scope USING btree (scope_id);


--
-- Name: idx_event_time; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_event_time ON public.event_entity USING btree (realm_id, event_time);


--
-- Name: idx_fedidentity_feduser; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fedidentity_feduser ON public.federated_identity USING btree (federated_user_id);


--
-- Name: idx_fedidentity_user; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fedidentity_user ON public.federated_identity USING btree (user_id);


--
-- Name: idx_fu_attribute; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_attribute ON public.fed_user_attribute USING btree (user_id, realm_id, name);


--
-- Name: idx_fu_cnsnt_ext; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_cnsnt_ext ON public.fed_user_consent USING btree (user_id, client_storage_provider, external_client_id);


--
-- Name: idx_fu_consent; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_consent ON public.fed_user_consent USING btree (user_id, client_id);


--
-- Name: idx_fu_consent_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_consent_ru ON public.fed_user_consent USING btree (realm_id, user_id);


--
-- Name: idx_fu_credential; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_credential ON public.fed_user_credential USING btree (user_id, type);


--
-- Name: idx_fu_credential_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_credential_ru ON public.fed_user_credential USING btree (realm_id, user_id);


--
-- Name: idx_fu_group_membership; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_group_membership ON public.fed_user_group_membership USING btree (user_id, group_id);


--
-- Name: idx_fu_group_membership_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_group_membership_ru ON public.fed_user_group_membership USING btree (realm_id, user_id);


--
-- Name: idx_fu_required_action; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_required_action ON public.fed_user_required_action USING btree (user_id, required_action);


--
-- Name: idx_fu_required_action_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_required_action_ru ON public.fed_user_required_action USING btree (realm_id, user_id);


--
-- Name: idx_fu_role_mapping; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_role_mapping ON public.fed_user_role_mapping USING btree (user_id, role_id);


--
-- Name: idx_fu_role_mapping_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_role_mapping_ru ON public.fed_user_role_mapping USING btree (realm_id, user_id);


--
-- Name: idx_group_attr_group; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_group_attr_group ON public.group_attribute USING btree (group_id);


--
-- Name: idx_group_role_mapp_group; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_group_role_mapp_group ON public.group_role_mapping USING btree (group_id);


--
-- Name: idx_id_prov_mapp_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_id_prov_mapp_realm ON public.identity_provider_mapper USING btree (realm_id);


--
-- Name: idx_ident_prov_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_ident_prov_realm ON public.identity_provider USING btree (realm_id);


--
-- Name: idx_keycloak_role_client; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_keycloak_role_client ON public.keycloak_role USING btree (client);


--
-- Name: idx_keycloak_role_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_keycloak_role_realm ON public.keycloak_role USING btree (realm);


--
-- Name: idx_offline_uss_createdon; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_offline_uss_createdon ON public.offline_user_session USING btree (created_on);


--
-- Name: idx_protocol_mapper_client; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_protocol_mapper_client ON public.protocol_mapper USING btree (client_id);


--
-- Name: idx_realm_attr_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_attr_realm ON public.realm_attribute USING btree (realm_id);


--
-- Name: idx_realm_clscope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_clscope ON public.client_scope USING btree (realm_id);


--
-- Name: idx_realm_def_grp_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_def_grp_realm ON public.realm_default_groups USING btree (realm_id);


--
-- Name: idx_realm_def_roles_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_def_roles_realm ON public.realm_default_roles USING btree (realm_id);


--
-- Name: idx_realm_evt_list_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_evt_list_realm ON public.realm_events_listeners USING btree (realm_id);


--
-- Name: idx_realm_evt_types_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_evt_types_realm ON public.realm_enabled_event_types USING btree (realm_id);


--
-- Name: idx_realm_master_adm_cli; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_master_adm_cli ON public.realm USING btree (master_admin_client);


--
-- Name: idx_realm_supp_local_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_supp_local_realm ON public.realm_supported_locales USING btree (realm_id);


--
-- Name: idx_redir_uri_client; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_redir_uri_client ON public.redirect_uris USING btree (client_id);


--
-- Name: idx_req_act_prov_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_req_act_prov_realm ON public.required_action_provider USING btree (realm_id);


--
-- Name: idx_res_policy_policy; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_policy_policy ON public.resource_policy USING btree (policy_id);


--
-- Name: idx_res_scope_scope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_scope_scope ON public.resource_scope USING btree (scope_id);


--
-- Name: idx_res_serv_pol_res_serv; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_serv_pol_res_serv ON public.resource_server_policy USING btree (resource_server_id);


--
-- Name: idx_res_srv_res_res_srv; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_srv_res_res_srv ON public.resource_server_resource USING btree (resource_server_id);


--
-- Name: idx_res_srv_scope_res_srv; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_srv_scope_res_srv ON public.resource_server_scope USING btree (resource_server_id);


--
-- Name: idx_role_attribute; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_role_attribute ON public.role_attribute USING btree (role_id);


--
-- Name: idx_role_clscope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_role_clscope ON public.client_scope_role_mapping USING btree (role_id);


--
-- Name: idx_scope_mapping_role; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_scope_mapping_role ON public.scope_mapping USING btree (role_id);


--
-- Name: idx_scope_policy_policy; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_scope_policy_policy ON public.scope_policy USING btree (policy_id);


--
-- Name: idx_update_time; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_update_time ON public.migration_model USING btree (update_time);


--
-- Name: idx_us_sess_id_on_cl_sess; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_us_sess_id_on_cl_sess ON public.offline_client_session USING btree (user_session_id);


--
-- Name: idx_usconsent_clscope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_usconsent_clscope ON public.user_consent_client_scope USING btree (user_consent_id);


--
-- Name: idx_user_attribute; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_attribute ON public.user_attribute USING btree (user_id);


--
-- Name: idx_user_consent; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_consent ON public.user_consent USING btree (user_id);


--
-- Name: idx_user_credential; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_credential ON public.credential USING btree (user_id);


--
-- Name: idx_user_email; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_email ON public.user_entity USING btree (email);


--
-- Name: idx_user_group_mapping; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_group_mapping ON public.user_group_membership USING btree (user_id);


--
-- Name: idx_user_reqactions; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_reqactions ON public.user_required_action USING btree (user_id);


--
-- Name: idx_user_role_mapping; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_role_mapping ON public.user_role_mapping USING btree (user_id);


--
-- Name: idx_usr_fed_map_fed_prv; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_usr_fed_map_fed_prv ON public.user_federation_mapper USING btree (federation_provider_id);


--
-- Name: idx_usr_fed_map_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_usr_fed_map_realm ON public.user_federation_mapper USING btree (realm_id);


--
-- Name: idx_usr_fed_prv_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_usr_fed_prv_realm ON public.user_federation_provider USING btree (realm_id);


--
-- Name: idx_web_orig_client; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_web_orig_client ON public.web_origins USING btree (client_id);


--
-- Name: client_session_auth_status auth_status_constraint; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_auth_status
    ADD CONSTRAINT auth_status_constraint FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: identity_provider fk2b4ebc52ae5c3b34; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT fk2b4ebc52ae5c3b34 FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_attributes fk3c47c64beacca966; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_attributes
    ADD CONSTRAINT fk3c47c64beacca966 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: federated_identity fk404288b92ef007a6; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.federated_identity
    ADD CONSTRAINT fk404288b92ef007a6 FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: client_node_registrations fk4129723ba992f594; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_node_registrations
    ADD CONSTRAINT fk4129723ba992f594 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client_session_note fk5edfb00ff51c2736; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_note
    ADD CONSTRAINT fk5edfb00ff51c2736 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: user_session_note fk5edfb00ff51d3472; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_session_note
    ADD CONSTRAINT fk5edfb00ff51d3472 FOREIGN KEY (user_session) REFERENCES public.user_session(id);


--
-- Name: client_session_role fk_11b7sgqw18i532811v7o2dv76; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_role
    ADD CONSTRAINT fk_11b7sgqw18i532811v7o2dv76 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: redirect_uris fk_1burs8pb4ouj97h5wuppahv9f; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.redirect_uris
    ADD CONSTRAINT fk_1burs8pb4ouj97h5wuppahv9f FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: user_federation_provider fk_1fj32f6ptolw2qy60cd8n01e8; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_provider
    ADD CONSTRAINT fk_1fj32f6ptolw2qy60cd8n01e8 FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_session_prot_mapper fk_33a8sgqw18i532811v7o2dk89; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_prot_mapper
    ADD CONSTRAINT fk_33a8sgqw18i532811v7o2dk89 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: realm_required_credential fk_5hg65lybevavkqfki3kponh9v; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_required_credential
    ADD CONSTRAINT fk_5hg65lybevavkqfki3kponh9v FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: resource_attribute fk_5hrm2vlf9ql5fu022kqepovbr; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu022kqepovbr FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: user_attribute fk_5hrm2vlf9ql5fu043kqepovbr; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu043kqepovbr FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: user_required_action fk_6qj3w1jw9cvafhe19bwsiuvmd; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_required_action
    ADD CONSTRAINT fk_6qj3w1jw9cvafhe19bwsiuvmd FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: keycloak_role fk_6vyqfe4cn4wlq8r6kt5vdsj5c; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT fk_6vyqfe4cn4wlq8r6kt5vdsj5c FOREIGN KEY (realm) REFERENCES public.realm(id);


--
-- Name: realm_smtp_config fk_70ej8xdxgxd0b9hh6180irr0o; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_smtp_config
    ADD CONSTRAINT fk_70ej8xdxgxd0b9hh6180irr0o FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_attribute fk_8shxd6l3e9atqukacxgpffptw; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_attribute
    ADD CONSTRAINT fk_8shxd6l3e9atqukacxgpffptw FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: composite_role fk_a63wvekftu8jo1pnj81e7mce2; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT fk_a63wvekftu8jo1pnj81e7mce2 FOREIGN KEY (composite) REFERENCES public.keycloak_role(id);


--
-- Name: authentication_execution fk_auth_exec_flow; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT fk_auth_exec_flow FOREIGN KEY (flow_id) REFERENCES public.authentication_flow(id);


--
-- Name: authentication_execution fk_auth_exec_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT fk_auth_exec_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: authentication_flow fk_auth_flow_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_flow
    ADD CONSTRAINT fk_auth_flow_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: authenticator_config fk_auth_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authenticator_config
    ADD CONSTRAINT fk_auth_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_session fk_b4ao2vcvat6ukau74wbwtfqo1; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session
    ADD CONSTRAINT fk_b4ao2vcvat6ukau74wbwtfqo1 FOREIGN KEY (session_id) REFERENCES public.user_session(id);


--
-- Name: user_role_mapping fk_c4fqv34p1mbylloxang7b1q3l; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_role_mapping
    ADD CONSTRAINT fk_c4fqv34p1mbylloxang7b1q3l FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: client_scope_client fk_c_cli_scope_client; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_client
    ADD CONSTRAINT fk_c_cli_scope_client FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client_scope_client fk_c_cli_scope_scope; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_client
    ADD CONSTRAINT fk_c_cli_scope_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_scope_attributes fk_cl_scope_attr_scope; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_attributes
    ADD CONSTRAINT fk_cl_scope_attr_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_scope_role_mapping fk_cl_scope_rm_scope; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT fk_cl_scope_rm_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_user_session_note fk_cl_usr_ses_note; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_user_session_note
    ADD CONSTRAINT fk_cl_usr_ses_note FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: protocol_mapper fk_cli_scope_mapper; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT fk_cli_scope_mapper FOREIGN KEY (client_scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_initial_access fk_client_init_acc_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_initial_access
    ADD CONSTRAINT fk_client_init_acc_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: component_config fk_component_config; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.component_config
    ADD CONSTRAINT fk_component_config FOREIGN KEY (component_id) REFERENCES public.component(id);


--
-- Name: component fk_component_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT fk_component_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_default_groups fk_def_groups_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT fk_def_groups_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_default_roles fk_evudb1ppw84oxfax2drs03icc; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_default_roles
    ADD CONSTRAINT fk_evudb1ppw84oxfax2drs03icc FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: user_federation_mapper_config fk_fedmapper_cfg; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper_config
    ADD CONSTRAINT fk_fedmapper_cfg FOREIGN KEY (user_federation_mapper_id) REFERENCES public.user_federation_mapper(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_fedprv; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_fedprv FOREIGN KEY (federation_provider_id) REFERENCES public.user_federation_provider(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: associated_policy fk_frsr5s213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT fk_frsr5s213xcx4wnkog82ssrfy FOREIGN KEY (associated_policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrasp13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT fk_frsrasp13xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog82sspmt; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82sspmt FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_server_resource fk_frsrho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog83sspmt; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog83sspmt FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog84sspmt; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog84sspmt FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: associated_policy fk_frsrpas14xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT fk_frsrpas14xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrpass3xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT fk_frsrpass3xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: resource_server_perm_ticket fk_frsrpo2128cx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrpo2128cx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_server_policy fk_frsrpo213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT fk_frsrpo213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_scope fk_frsrpos13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT fk_frsrpos13xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpos53xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT fk_frsrpos53xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpp213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT fk_frsrpp213xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_scope fk_frsrps213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT fk_frsrps213xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: resource_server_scope fk_frsrso213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT fk_frsrso213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: composite_role fk_gr7thllb9lu8q4vqa4524jjy8; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT fk_gr7thllb9lu8q4vqa4524jjy8 FOREIGN KEY (child_role) REFERENCES public.keycloak_role(id);


--
-- Name: user_consent_client_scope fk_grntcsnt_clsc_usc; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent_client_scope
    ADD CONSTRAINT fk_grntcsnt_clsc_usc FOREIGN KEY (user_consent_id) REFERENCES public.user_consent(id);


--
-- Name: user_consent fk_grntcsnt_user; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT fk_grntcsnt_user FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: group_attribute fk_group_attribute_group; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.group_attribute
    ADD CONSTRAINT fk_group_attribute_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: keycloak_group fk_group_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT fk_group_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: group_role_mapping fk_group_role_group; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT fk_group_role_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: realm_enabled_event_types fk_h846o4h0w8epx5nwedrf5y69j; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_enabled_event_types
    ADD CONSTRAINT fk_h846o4h0w8epx5nwedrf5y69j FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_events_listeners fk_h846o4h0w8epx5nxev9f5y69j; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_events_listeners
    ADD CONSTRAINT fk_h846o4h0w8epx5nxev9f5y69j FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: identity_provider_mapper fk_idpm_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider_mapper
    ADD CONSTRAINT fk_idpm_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: idp_mapper_config fk_idpmconfig; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.idp_mapper_config
    ADD CONSTRAINT fk_idpmconfig FOREIGN KEY (idp_mapper_id) REFERENCES public.identity_provider_mapper(id);


--
-- Name: web_origins fk_lojpho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.web_origins
    ADD CONSTRAINT fk_lojpho213xcx4wnkog82ssrfy FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client_default_roles fk_nuilts7klwqw2h8m2b5joytky; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_default_roles
    ADD CONSTRAINT fk_nuilts7klwqw2h8m2b5joytky FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: scope_mapping fk_ouse064plmlr732lxjcn1q5f1; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT fk_ouse064plmlr732lxjcn1q5f1 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client fk_p56ctinxxb9gsk57fo49f9tac; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT fk_p56ctinxxb9gsk57fo49f9tac FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: protocol_mapper fk_pcm_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT fk_pcm_realm FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: credential fk_pfyr0glasqyl0dei3kl69r6v0; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.credential
    ADD CONSTRAINT fk_pfyr0glasqyl0dei3kl69r6v0 FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: protocol_mapper_config fk_pmconfig; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper_config
    ADD CONSTRAINT fk_pmconfig FOREIGN KEY (protocol_mapper_id) REFERENCES public.protocol_mapper(id);


--
-- Name: default_client_scope fk_r_def_cli_scope_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT fk_r_def_cli_scope_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: default_client_scope fk_r_def_cli_scope_scope; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT fk_r_def_cli_scope_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_scope fk_realm_cli_scope; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT fk_realm_cli_scope FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: required_action_provider fk_req_act_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.required_action_provider
    ADD CONSTRAINT fk_req_act_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: resource_uris fk_resource_server_uris; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_uris
    ADD CONSTRAINT fk_resource_server_uris FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: role_attribute fk_role_attribute_id; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.role_attribute
    ADD CONSTRAINT fk_role_attribute_id FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: realm_supported_locales fk_supported_locales_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_supported_locales
    ADD CONSTRAINT fk_supported_locales_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: user_federation_config fk_t13hpu1j94r2ebpekr39x5eu5; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_config
    ADD CONSTRAINT fk_t13hpu1j94r2ebpekr39x5eu5 FOREIGN KEY (user_federation_provider_id) REFERENCES public.user_federation_provider(id);


--
-- Name: user_group_membership fk_user_group_user; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_group_membership
    ADD CONSTRAINT fk_user_group_user FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: policy_config fkdc34197cf864c4e43; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.policy_config
    ADD CONSTRAINT fkdc34197cf864c4e43 FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: identity_provider_config fkdc4897cf864c4e43; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider_config
    ADD CONSTRAINT fkdc4897cf864c4e43 FOREIGN KEY (identity_provider_id) REFERENCES public.identity_provider(internal_id);


--
-- PostgreSQL database dump complete
--

